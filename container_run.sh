#!bin/bash
CHROME_STRING=$(google-chrome --version)
CHROME_VERSION_STRING=$(echo "${CHROME_STRING}" | grep -oP "\d+\.\d+\.\d+\.\d+")
nebula_command="python3 -m nebula /usr/src/tests/ --headless -cv $CHROME_VERSION_STRING -tt Jenkins -s /usr/src/tests/test_selectors.yml -l /usr/src/tests/test_lang_file.yml -i http://usa-rnc-nebula:5000 -id Jenkins"
command="$nebula_command $1"
eval ${command}