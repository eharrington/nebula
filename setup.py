from setuptools import setup, find_packages
import os

def get_version():
    with open('./VERSION') as version_file:
        version = version_file.read().strip()
    return version

def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join('..', path, filename))
    return paths

nlu_engine_files = package_files('nebula/snips/web_engine/nebula_web_engine')
nlu_engine_files += package_files('nebula/snips/api_engine/nebula_api_engine')

setup(name='nebula',
    version=get_version(),
    author="Infrastructure Engineering",
    description='Extron NLP driven automated testing framework',
    packages=find_packages(),
    include_package_data=True,
    package_data={'nebula': nlu_engine_files},
    entry_points={
        'console_scripts':[
            'nebula = nebula.__main__:main',
            'download_webdrivers = nebula.download_webdrivers:main'
        ]
    },
    install_requires = [
        'beautifulsoup4',
        'boto3',
        'configparser',
        'lxml',
        'requests',
        'fastjsonschema',
        'Selenium==3.9.0',
        'pyserial',
        'pyyaml',
        'setuptools',
        'snips-nlu',
        'pytest',
        'dateparser',
        'colorama',
        'xlrd==1.2.0',
        'bcrypt==3.1.7',
        'asn1crypto==0.24.0',
        'PyNaCl==1.3.0',
        'cryptography==2.7',
        'paramiko==2.6.0',
        'dicttoxml==1.7.4',
        'urllib3==1.24.1',
        'opencv-python==3.4.3.18'
    ]
)
