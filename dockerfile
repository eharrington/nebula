FROM ubuntu:bionic

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN apt-get update && apt-get install -qqy --no-install-recommends \
    bash \
    software-properties-common \
    xvfb \ 
    curl \
    gpg-agent \
    firefox \
    python3 \
    python3-setuptools \
    python3-distutils \
    python3-pip \
    python-dev \
    python3-dev \
    build-essential \
    unzip \
    wget \
    libfontconfig \
    libfreetype6 \
    xfonts-cyrillic \
    xfonts-scalable \
    fonts-liberation \
    fonts-ipafont-gothic \
    fonts-wqy-zenhei \
    fonts-tlwg-loma-otf \
    ttf-ubuntu-font-family \
    && pip3 install wheel \
    && rm -rf /var/lib/apt/lists/* \
    && firefox -v

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install google-chrome-stable -qqy --no-install-recommends

WORKDIR /usr/src/app

COPY . .

RUN pip3 install . && pip3 install -r resources/dev.requirements.txt \
    && snips-nlu download en \
    && cd nebula && cd snips \
    && cd web_engine && rm -Rf nebula_web_engine \
    && snips-nlu generate-dataset en *.yaml > nebula_data.json \
    && snips-nlu train -r 42 nebula_data.json nebula_web_engine \
    && cd .. && cd api_engine && rm -Rf nebula_api_engine \
    && snips-nlu generate-dataset en *.yaml > nebula_data.json \
    && snips-nlu train -r 42 nebula_data.json nebula_api_engine

ENV WEBDRIVER_PATH=/usr/src/app/drivers
ENV DISPLAY=:99
ENV IMAGE_STORE_PATH=/usr/src/app/imageStore 

ENTRYPOINT ["bash","container_run.sh"]