def get_version():
    with open('./VERSION') as version_file:
        version = version_file.read().strip()
    return version

def update_version():
    current_version = get_version()
    maj, minor, inc = current_version.split('.')
    new_version = maj + '.' + minor + '.' + str(int(inc) + 1)

    with open('./VERSION', "w") as version_file:
        version_file.write(new_version)

update_version()