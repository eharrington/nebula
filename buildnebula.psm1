function Remove-NebulaDependencies {
    param (
        [parameter(Mandatory=$true)][string]$bpwd,
        [parameter(Mandatory=$false)][string]$python_home
    )
    try {
        if ( -not ($python_home) ) {
            $python_home = $(Get-Command -Name "python" | select Path -ExpandProperty Path | Split-Path -Parent)
        }
        if (( -Not ( Test-Path $bpwd\StndError.txt )) -or ( -Not ( Test-Path $bpwd\StndOut.txt ))) {
            Remove-Item -Path $bpwd\Stnd*.txt -Force
            New-Item -Path $bpwd\StndError.txt
            New-Item -Path $bpwd\StndOut.txt
        }
        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "-m pip uninstall -r $bpwd\resources\dev.requirements.txt -y" -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt -RedirectStandardOutput $bpwd\StndOut.txt
        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "-m pip uninstall wheel PyInstaller -y" -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt -RedirectStandardOutput $bpwd\StndOut.txt
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        Write-Host "Function Error: Remove-NebulaDependencies. The error message was $ErrorMessage"
        Get-Content -Path "$bpwd\StndError.txt"
        exit 1
    }
}

function Install-NebulaDependencies {
    param (
        [parameter(Mandatory=$true)][string]$bpwd,
        [parameter(Mandatory=$false)][string]$python_home
    )
    try {
        if ( -not ($python_home) ) {
            $python_home = $(Get-Command -Name "python" | select Path -ExpandProperty Path | Split-Path -Parent)
        }
        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "-m pip install wheel PyInstaller" -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt -RedirectStandardOutput $bpwd\StndOut.txt
        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "-m pip install --upgrade pip" -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt -RedirectStandardOutput $bpwd\StndOut.txt
        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "-m pip install -r $bpwd\resources\dev.requirements.txt" -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt -RedirectStandardOutput $bpwd\StndOut.txt
        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "-m snips_nlu download en" -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt -RedirectStandardOutput $bpwd\StndOut.txt
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        Write-Host "Function Error: Install-NebulaDependencies. The error message was $ErrorMessage"
        Get-Content -Path "$bpwd\StndError.txt"
        exit 1
    }
}

function Update-PythonFiles {
    param (
        [parameter(Mandatory=$true)][string]$bpwd,
        [parameter(Mandatory=$false)][string]$python_home
    )
    try {
        if ( -not ($python_home) ) {
            $python_home = $(Get-Command -Name "python" | select Path -ExpandProperty Path | Split-Path -Parent)
        }
        Get-ChildItem -Path $python_home -Filter "libsnips_nlu_parsers*win_amd64.pyd" -Recurse | Select FullName -ExpandProperty FullName
        $crf_slot_filler = $(Get-ChildItem -Path $python_home -Filter "crf_slot_filler.py" -Recurse -ErrorAction SilentlyContinue | select FullName -ExpandProperty FullName)
        $parsers_utils = $(Get-ChildItem -Path $python_home -Filter "snips_nlu_parsers" -Recurse -ErrorAction SilentlyContinue | Get-ChildItem -Recurse -Filter "utils.py" -ErrorAction SilentlyContinue | Select FullName -ExpandProperty FullName)
        $utils_utils = $(Get-ChildItem -Path $python_home -Filter "snips_nlu_utils" -Recurse -ErrorAction SilentlyContinue | Get-ChildItem -Recurse -Filter "utils.py" -ErrorAction SilentlyContinue | Select FullName -ExpandProperty FullName)
        Copy-Item -Path "$bpwd\resources\snips_file_updates\crf_slot_filler.py" -Destination $crf_slot_filler
        Copy-Item -Path "$bpwd\resources\snips_file_updates\parsers_utils.py" -Destination $parsers_utils
        Copy-Item -Path "$bpwd\resources\snips_file_updates\utils_utils.py" -Destination $utils_utils
        $parserpyd = $(Get-ChildItem -Path $python_home -Filter "libsnips_nlu_parsers*win_amd64.pyd" -Recurse | Select FullName -ExpandProperty FullName).replace("\","\\")
        Write-Host "Updating $bpwd\hook-snips_nlu_parsers.py with: $parserpyd"
        (Get-Content -Path "$bpwd\hook-snips_nlu_parsers.py").Replace("\\AppData\\Local\\Programs\\python\\python37\\lib\\site-packages\\snips_nlu_parsers\\dylib\\libsnips_nlu_parsers_rs.cp37-win_amd64.pyd","$parserpyd") | Set-Content -Path "$bpwd\hook-snips_nlu_parsers.py"
        $utilspyd = $(Get-ChildItem -Path $python_home -Filter "libsnips_nlu_utils*win_amd64.pyd" -Recurse | Select FullName -ExpandProperty FullName).replace("\","\\")
        Write-Host "Updating $bpwd\hook-snips_nlu_utils.py with: $utilspyd"
        (Get-Content -Path "$bpwd\hook-snips_nlu_utils.py").Replace("\\AppData\\Local\\Programs\\python\\python36\\lib\\site-packages\\snips_nlu_utils\\dylib\\libsnips_nlu_utils_rs.cp36-win_amd64.pyd","$utilspyd") | Set-Content -Path "$bpwd\hook-snips_nlu_utils.py"
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        Write-Host "Function Error: Update-PythonFiles. The error message was $ErrorMessage"
        exit 1
    }
}

function Initialize-NebualDataSet {
    param (
        [parameter(Mandatory=$true)][string]$bpwd,
        [parameter(Mandatory=$false)][string]$python_home
    )
    try {
        if ( -not ($python_home) ) {
            $python_home = $(Get-Command -Name "python" | select Path -ExpandProperty Path | Split-Path -Parent)
        }
        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "--version" -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt -RedirectStandardOutput $bpwd\StndOut.txt
        if( ( Test-Path $bpwd\nebula\snips\web_engine\nebula_data.json ) -or ( Test-Path -Path $bpwd\nebula\snips\web_engine\nebula_web_engine ) ) {
            Remove-Item -Path "$bpwd\nebula\snips\web_engine\nebula_data.json" -ErrorAction SilentlyContinue -Force
            Remove-Item -Path "$bpwd\nebula\snips\web_engine\nebula_web_engine" -ErrorAction SilentlyContinue -Recurse -Force
        }
        Remove-Variable -Name filelist -ErrorAction SilentlyContinue
        foreach ( $file in $(Get-ChildItem $bpwd\nebula\snips\web_engine)) {
            $filelist += "$bpwd\nebula\snips\web_engine\$file "
        }
        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "-m snips_nlu generate-dataset en $filelist" -RedirectStandardOutput $bpwd\nebula\snips\web_engine\nebula_data.json -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt
        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "-m snips_nlu train -r 42 $bpwd\nebula\snips\web_engine\nebula_data.json $bpwd\nebula\snips\web_engine\nebula_web_engine" -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt -RedirectStandardOutput $bpwd\StndOut.txt
        
        if( ( Test-Path $bpwd\nebula\snips\api_engine\nebula_data.json ) -or ( Test-Path -Path $bpwd\nebula\snips\api_engine\nebula_api_engine ) ) {
            Remove-Item -Path "$bpwd\nebula\snips\api_engine\nebula_data.json" -ErrorAction SilentlyContinue -Force
            Remove-Item -Path "$bpwd\nebula\snips\api_engine\nebula_api_engine" -ErrorAction SilentlyContinue -Recurse -Force
        }
        Remove-Variable -Name filelist -ErrorAction SilentlyContinue 
        foreach ( $file in $(Get-ChildItem $bpwd\nebula\snips\api_engine)) {
            $filelist += "$bpwd\nebula\snips\api_engine\$file "
        }
        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "-m snips_nlu generate-dataset en $filelist" -RedirectStandardOutput $bpwd\nebula\snips\api_engine\nebula_data.json -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt
        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "-m snips_nlu train -r 42 $bpwd\nebula\snips\api_engine\nebula_data.json $bpwd\nebula\snips\api_engine\nebula_api_engine" -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt -RedirectStandardOutput $bpwd\StndOut.txt
    
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        Write-Host "Function Error: Initialize-NebualDataSet. The error message was $ErrorMessage"
        Get-Content -Path "$bpwd\StndError.txt"
        exit 1
    }
}

function New-NebulaExe {
    param (
        [parameter(Mandatory=$true)][string]$bpwd,
        [parameter(Mandatory=$false)][string]$python_home
    )
    try {
        if ( -not ($python_home) ) {
            $python_home = $(Get-Command -Name "python" | select Path -ExpandProperty Path | Split-Path -Parent)
        }
        Remove-Item -LiteralPath "$bpwd\dist" -Force -Recurse -ErrorAction SilentlyContinue
        Remove-Item -LiteralPath "$bpwd\build" -Force -Recurse -ErrorAction SilentlyContinue
        Remove-Item -LiteralPath "$bpwd\__pycache__" -Force -Recurse -ErrorAction SilentlyContinue

        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "-m pip install wcwidth==0.1.9" -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt -RedirectStandardOutput $bpwd\StndOut.txt
        Start-Process -FilePath "$python_home\python.exe" -ArgumentList "-m PyInstaller -D $bpwd\nebula_2.0.spec" -Wait -NoNewWindow -RedirectStandardError $bpwd\StndError.txt -RedirectStandardOutput $bpwd\StndOut.txt

        $nebula_exe = "nebula_Windows_" + "$(Get-Content $bpwd\VERSION)" + ".exe"
        Copy-Item -Path "$bpwd\dist\$nebula_exe" -Destination "$bpwd"
        $env:NEBULA_PASSWORD="test123"
        #Start-Process -FilePath "$bpwd\$nebula_exe" -ArgumentList "$bpwd\tests\test_selectors -s $bpwd\tests\test_selectors\test_selectors.yml -l $bpwd\tests\test_selectors\test_lang_file.yml --headless -i http://usa-rnc-nebula:5000 -id WINDOWS_EXE" -Wait -RedirectStandardError $bpwd\StndError.txt -RedirectStandardOutput $bpwd\StndOut.txt

    }
    catch {
        $ErrorMessage = $_.Exception.Message
        Write-Host "Function Error: New-NebulaExe. The error message was $ErrorMessage"
        Get-Content -Path "$bpwd\StndError.txt"
        exit 1
    }
} 
