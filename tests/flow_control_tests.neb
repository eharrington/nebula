@Define test_page = tests/test.html
@DefineCollection unread_text = ['abc123451$', '-abc123451', '$stringstart', 'stringend$', 'somemore$%^chars']
@Define isTrue = True
@Define isFalse = False
@DefinePlaceholder valid1 = "Browser is Chrome. This should print."
@DefinePlaceholder valid2 = "1 + 1 = 2. This should print."
@DefinePlaceholder valid3 = "This variable equals True. This should print."
@DefinePlaceholder invalid = "This variable equals False. THIS SHOULD NOT PRINT."
@DefinePlaceholder exception = "This is garbage. THIS SHOULD NOT PRINT."
@DefineCollection ww1_countries = ['Prussia', 'Austria', 'England', 'Germany', 'USA']
@DefineCollection Table_Iterator = [0, 1, 2, 3]
@DefineRegex country_format = "[a-zA-Z]*" ####Used in test iterations
@DefineRegex name_format = "[A-Z]{1}[a-z]*" ####Used in test collections
@DefinePlaceholder city_check = You chose ${city}
@DefinePlaceholder color_check = You chose ${color}

@BeforeEach
    Nagivate to file ${test_page}

@AfterEach
    unfocus

@Define my_city = Raleigh
@Define my_color = Blue
@Define your_city = New York
@Define your_color = Yellow
@Test: Data Driven (data=[{color=${my_color}, city=${my_city}}, \
                          {color=Green, city=Raleigh}, \
                          {color=${your_color}, city=${your_city}}], \
                          devtime=30, \
                          tags=[Jenkins, AWS])
    From the City dropdown select ${city}
    verify if citySelectLabel equals ${city_check} #this value comes from a previous test
    Select ${color} from the Color dropdown
    verify if selectOptionLabel equals ${color_check}

@Test: Verify that test iterations work (devtime=30, tags=[Jenkins, AWS])
    Scroll Scandinavian Countries into view
    Get the country-list textboxes from the textbox list and store them in eu_scan_countries variable
    FOREACH: country in ${eu_scan_countries}
        Verify ${country} variable matches ${country_format}
    END:

@Test: Collections and For Each (devtime=30, tags=[Jenkins, AWS])
    Get the First_Name_list list and store in first_name_list variable
    FOREACH: first_name in ${first_name_list}
        Verify ${first_name} variable matches ${name_format}
        verify length of ${first_name} greater than 2
    END:


@Test: Foreach placeholders (devtime=30, tags=[Jenkins, AWS])
    clear "Textbox1"
    FOREACH: word in ${unread_text}
        Enter ${word} in Textbox1
        Verify if Textbox1 field has value ${word}
    END:

@Define test_var = False
@Test: Verify that Case blocks work (devtime=30, tags=[Jenkins, AWS])
    CASE: "${ww1_countries}[0]" is "Prussia"
        Set primitive ${test_var} to True
    END:
    verify that the value of ${test_var} variable is True

    ON_PASS: verify that the value of ${test_var} variable is True
        Set primitive ${test_var} to False
    END:
    verify that the value of ${test_var} variable is False

    CASE: str('${ww1_countries}[0]').startswith('NOT_Pr')  #this case will always be false
        verify that the value of ${test_var} variable is True        #so this assert, which should always fail, will never get called
    END:

    CASE: str('${ww1_countries}[0]').startswith('Pr')  #this case will always be true
        Set primitive ${test_var} to True
    END:
    verify that the value of ${test_var} variable is True

    CASE: 1 + 1 == 2
        Print ${valid2}
        CASE: ${Table_Iterator}[1] + ${Table_Iterator}[2] == ${Table_Iterator}[3]
            Set primitive ${test_var} to False
            CASE: ${isTrue}
                verify ${isFalse} variable is ${isFalse}
            END:
            CASE: ${isFalse}
                verify ${isFalse} variable is ${isTrue}
            END:
        END:
    END:
    verify that the value of ${test_var} variable is False

    # false should SKIP the test lines inside the case
    CASE: ${isFalse}
        verify ${isFalse} variable is ${isTrue}
    END:
    # invalid case statement should cause invalid test exception
    #CASE: garbage
    #Print ${exception}
    #END:
