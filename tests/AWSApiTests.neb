#This test expects the following variables to be defined in the environment
#RESPONDER_ADDR

@Include (snippets/rest_response_codes.neb:http_response_codes)
@Define initial_responder = 1670
@Define second_responder = 1672
@Define third_responder = 1673
@Define fourth_responder = 1674
@Define fifth_responder = 1675
@Define non_digit_var = "nodigitsallowed"
@DefineRegex digit_regex = "^\d+$"
@DefineRegex no_digit_regex = "^([^0-9]*)$"

#Define a couple useful json objects
@DefineJson first_response_json = {\
    "Get":{\
      "first_name":{"name":"ernest"},\
      "profession":{"job":"product owner"}\
      },\
    "Post":{\
      "first_name":{"name":"eric"},\
      "profession":{"job":"manager"}\
    },\
    "PUT":{\
      "first_name":{"name":"walter"},\
      "profession":{"job":"customer relations"}\
    },\
    "pATCH":{\
      "first_name":{"name":"nisarg"},\
      "profession":{"job":"engineer"}\
    },\
    "delete":{\
      "first_name":{"name":"nemo"},\
      "profession":{"job":"deep see diver"}\
    }\
  }

@IncludeJson nebula_json_variables/second_response.json as second_response_json

@Define xml_reponse = <root><GET><ENDPOINT path="users/1"><headers><content-type>text/plain</content-type><ETag>43</ETag><warning>Testing 123</warning></headers><delay>400</delay><status>300</status><message><id>1</id><first_name>Ernest</first_name><last_name>Christley</last_name><email>echristley@extron.com</email></message></ENDPOINT><ENDPOINT path="users/2"><delay>0</delay><status>200</status><message><id>1</id><first_name>foo</first_name><last_name>bar</last_name><email>foobar@extron.com</email></message></ENDPOINT></GET><PUT><ENDPOINT path="users/1"><delay>50</delay><status>200</status><message><id>1</id><first_name>foo</first_name><last_name>bar</last_name><email>foobar@extron.com</email></message></ENDPOINT><ENDPOINT path="users/2"><delay>200</delay><status>404</status><message>User not found.</message></ENDPOINT></PUT><POST><ENDPOINT path="users/1"><delay>300</delay><status>208</status><message>User already exists.</message></ENDPOINT><ENDPOINT path="users/3"><headers><content-type>application/xml</content-type><ETag>209</ETag><warning>Testing ABC</warning></headers><delay>500</delay><status>201</status><message><id>3</id><first_name>fizz</first_name><last_name>buzz</last_name><email>fizzbuzz@extron.com</email></message></ENDPOINT></POST><DELETE><ENDPOINT path="users/1"><delay>200</delay><status>401</status><message>No permission to delete user.</message></ENDPOINT><ENDPOINT path="users/3"><delay>800</delay><status>204</status><message/></ENDPOINT></DELETE></root>


@DefineJson update_user_json = { \
    "job": "potentate"           \
}

@DefineJson empty_json = {}

@DefineJson port_json = { \
    "port":1672           \
}

@DefineJson port_json_second = { \
    "port":1673           \
}

@DefineJson port_json_third = { \
    "port":1674           \
}

@DefineJson port_json_fourth = { \
    "port":1675           \
}

@DefineJson variable_json = {\
    "key1": "${initial_responder}",\
    "key2": "${second_responder}",\
    "key3": "${third_responder}"\
}

@DefineCollection variable_collection = [${initial_responder}, ${second_responder}, ${third_responder}]

#Read a json object. Schemas are usually large json structures, so it is cleaner to put
#them in a separate file
@IncludeJson nebula_json_variables/jsonUserSchema.json as user_schema


@Test: Check JSON And Collection Substitution (tags=[AWS], devtime=30)
  API: Verify ${variable_json}[key1] is 1670
  API: Verify ${variable_json}[key2] is 1672
  API: Verify ${variable_json}[key3] is 1673
  API: Verify ${variable_collection}[0] is 1670
  API: Verify ${variable_collection}[1] is 1672
  API: Verify ${variable_collection}[2] is 1673

@Test: Check Regex matching (tags=[AWS], devtime=30)
  API: Verify ${variable_json}[key1] matches ${digit_regex}
  POF:API: Verify ${variable_json}[key2] matches ${no_digit_regex}
  API: Verify ${non_digit_var} matches ${no_digit_regex}
  POF:API: Verify ${non_digit_var} matches ${digit_regex}

@Test: Standard GET method (tags=[AWS], devtime=30)
  #API: add application/json as the Content-Type header
  API: get responses from http://${RESPONDER_ADDR}:${initial_responder}/config
  API: verify ${responses} status is ${HTTP_OK}


@Test: POST an update (tags=[AWS], devtime=60)
  API: get responses from http://${RESPONDER_ADDR}:${initial_responder}/config
  API: verify ${responses} status is ${HTTP_OK}

  API: Post ${first_response_json} to http://${RESPONDER_ADDR}:${initial_responder}/update and store response in update_response
  API: verify ${update_response} status is ${HTTP_OK}

  API: get responses from http://${RESPONDER_ADDR}:${initial_responder}/config
  API: verify ${responses} status is ${HTTP_OK}
  API: verify ${responses}[pATCH][profession][job] is ${first_response_json}[pATCH][profession][job]

  #verify get endpoints work
  API: get response from  http://${RESPONDER_ADDR}:${initial_responder}/profession
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${first_response_json}[Get][profession][job]
  API: get response from  http://${RESPONDER_ADDR}:${initial_responder}/first_name
  API: verify ${response} status is ${HTTP_OK}
  API: pull ${response}[name] into my_name  #rest response data into a variable

  API: verify ernest is ${my_name}  #compare a literal to a variable
  API: verify ernest is ${response}[name]  #compare a literal to a rest response
  API: verify ernest is ${first_response_json}[Get][first_name][name]  #compare a literal to json

  API: verify ${my_name} is ernest  #compare a variable to a literal
  API: verify ${my_name} is ${response}[name]  #compare a variable to a rest response
  API: verify ${my_name} is ${first_response_json}[Get][first_name][name]  #compare a variable to json

  API: pull ${first_response_json}[Get][first_name][name] into my_name  #json data into a variable

  API: verify ${response}[name] is ernest  #compare a rest response to a literal
  API: verify ${response}[name] is ${my_name}  #compare a rest response to a variable
  API: verify ${response}[name] is ${first_response_json}[Get][first_name][name]  #compare a rest response to json

  API: verify ${first_response_json}[Get][first_name][name] is ernest  #compare a json to a literal
  API: verify ${first_response_json}[Get][first_name][name] is ${my_name}  #compare a json to a variable
  API: verify ${first_response_json}[Get][first_name][name] is ${response}[name]  #compare a json to a rest response


  #verify post endpoints work (using any bogus json)
  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${initial_responder}/first_name and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[name] is ${first_response_json}[Post][first_name][name]
  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${initial_responder}/profession and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${first_response_json}[Post][profession][job]

  #verify put endpoints work (using any bogus json)
  API: Put ${empty_json} to http://${RESPONDER_ADDR}:${initial_responder}/first_name and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[name] is ${first_response_json}[PUT][first_name][name]
  API: Put ${empty_json} to http://${RESPONDER_ADDR}:${initial_responder}/profession and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${first_response_json}[PUT][profession][job]

  #verify patch endpoints work (using any bogus json)
  API: Patch ${empty_json} to http://${RESPONDER_ADDR}:${initial_responder}/first_name and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[name] is ${first_response_json}[pATCH][first_name][name]
  API: Patch ${empty_json} to http://${RESPONDER_ADDR}:${initial_responder}/profession and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${first_response_json}[pATCH][profession][job]

  #verify delete endpoints work (using any bogus json)
  API: Delete http://${RESPONDER_ADDR}:${initial_responder}/first_name and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[name] is ${first_response_json}[delete][first_name][name]
  API: Delete http://${RESPONDER_ADDR}:${initial_responder}/profession and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${first_response_json}[delete][profession][job]


@Test: POST to config/start to start up new second_responder (tags=[AWS], devtime=60)
  # verify sending the port over as xml fails since we are expecting json
  API: add application/xml as the content-type header
  API: Post ${port_json} to http://${RESPONDER_ADDR}:${initial_responder}/config/start and store response in response
  API: verify ${response} status is ${HTTP_BAD_REQUEST}
  POF:API: verify ${response} status is ${HTTP_OK}
  # verify sending the port over as json passes
  API: add application/json as the content-type header
  API: Post ${port_json} to http://${RESPONDER_ADDR}:${initial_responder}/config/start and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[message] has Service successfully started on port ${second_responder}
  wait for 1 second

  #POST /update to create endpoints at new port
  API: Post ${first_response_json} to http://${RESPONDER_ADDR}:${second_responder}/update and store response in response
  API: verify ${response} status is ${HTTP_OK}

  API: get responses from http://${RESPONDER_ADDR}:${second_responder}/config
  API: verify ${responses} status is ${HTTP_OK}
  API: verify ${responses}[pATCH][profession][job] is ${first_response_json}[pATCH][profession][job]

  #verify get endpoints work
  API: get response from  http://${RESPONDER_ADDR}:${second_responder}/profession
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${first_response_json}[Get][profession][job]
  API: get response from  http://${RESPONDER_ADDR}:${second_responder}/first_name
  API: verify ${response} status is ${HTTP_OK}
  API: pull ${response}[name] into my_name  #rest response data into a variable

  API: verify ernest is ${my_name}  #compare a literal to a variable
  API: verify ernest is ${response}[name]  #compare a literal to a rest response
  API: verify ernest is ${first_response_json}[Get][first_name][name]  #compare a literal to json

  API: verify ${my_name} is ernest  #compare a variable to a literal
  API: verify ${my_name} is ${response}[name]  #compare a variable to a rest response
  API: verify ${my_name} is ${first_response_json}[Get][first_name][name]  #compare a variable to json

  API: pull ${first_response_json}[Get][first_name][name] into my_name  #json data into a variable

  API: verify ${response}[name] is ernest  #compare a rest response to a literal
  API: verify ${response}[name] is ${my_name}  #compare a rest response to a variable
  API: verify ${response}[name] is ${first_response_json}[Get][first_name][name]  #compare a rest response to json

  API: verify ${first_response_json}[Get][first_name][name] is ernest  #compare a json to a literal
  API: verify ${first_response_json}[Get][first_name][name] is ${my_name}  #compare a json to a variable
  API: verify ${first_response_json}[Get][first_name][name] is ${response}[name]  #compare a json to a rest response


  #verify post endpoints work (using any bogus json)
  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${second_responder}/first_name and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[name] is ${first_response_json}[Post][first_name][name]
  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${second_responder}/profession and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${first_response_json}[Post][profession][job]

  #verify put endpoints work (using any bogus json)
  API: Put ${empty_json} to http://${RESPONDER_ADDR}:${second_responder}/first_name and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[name] is ${first_response_json}[PUT][first_name][name]
  API: Put ${empty_json} to http://${RESPONDER_ADDR}:${second_responder}/profession and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${first_response_json}[PUT][profession][job]

  #verify patch endpoints work (using any bogus json)
  API: Patch ${empty_json} to http://${RESPONDER_ADDR}:${second_responder}/first_name and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[name] is ${first_response_json}[pATCH][first_name][name]
  API: Patch ${empty_json} to http://${RESPONDER_ADDR}:${second_responder}/profession and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${first_response_json}[pATCH][profession][job]

  #verify delete endpoints work (using any bogus json)
  API: Delete http://${RESPONDER_ADDR}:${second_responder}/first_name and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[name] is ${first_response_json}[delete][first_name][name]
  API: Delete http://${RESPONDER_ADDR}:${second_responder}/profession and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${first_response_json}[delete][profession][job]

  #POST /config/stop endpoint
  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${second_responder}/config/stop and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[message] has Exiting

  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${second_responder}/config/stop and store response in response
  POF:API: verify ${response} status is ${HTTP_OK}


@Test: Specifying status delay and headers (tags=[AWS], devtime=60)
  # This test disabled because of a change in the WAIT method. Should be enabled and this comment removed once IE-2452 is completed
  API: Post ${port_json_second} to http://${RESPONDER_ADDR}:${initial_responder}/config/start and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[message] has Service successfully started on port ${third_responder}
  wait for 1 second

  #POST /update to create endpoints at new port
  API: Post ${second_response_json} to http://${RESPONDER_ADDR}:${third_responder}/update and store response in response
  API: verify ${response} status is ${HTTP_OK}

  API: get responses from http://${RESPONDER_ADDR}:${third_responder}/config
  API: verify ${responses} status is ${HTTP_OK}
  API: verify ${responses}[delete][first_name][delay] is ${second_response_json}[delete][first_name][delay]

  #verify get endpoints work with specific delay and status
  API: get response from  http://${RESPONDER_ADDR}:${third_responder}/profession
  API: verify ${response} status is ${HTTP_MOVED}
  API: verify ${response}[delay] is ${second_response_json}[Get][profession][delay]
  API: verify ${response}[job] is ${second_response_json}[Get][profession][job]
  API: get response from  http://${RESPONDER_ADDR}:${third_responder}/first_name
  API: verify ${response} status is ${HTTP_MULTIPLE}
  API: pull ${response}[name] into my_name  #rest response data into a variable
  API: verify barret is ${my_name}  #compare a literal to a variable
  API: verify barret is ${response}[name]  #compare a literal to a rest response
  API: verify barret is ${second_response_json}[Get][first_name][name]  #compare a literal to json

  #verify post endpoints work with specific delay and status
  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${third_responder}/first_name and store response in response
  API: verify ${response} status is ${HTTP_NOT_FOUND}
  API: verify ${response}[name] is ${second_response_json}[Post][first_name][name]
  API: verify ${response}[delay] is ${second_response_json}[Post][first_name][delay]
  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${third_responder}/profession and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${second_response_json}[Post][profession][job]

  #verify put endpoints with specific delay, status, and headers
  API: Put ${empty_json} to http://${RESPONDER_ADDR}:${third_responder}/first_name and store response in response
  API: verify ${response} status is ${HTTP_UNAUTHORIZED}
  API: verify ${response}[name] is ${second_response_json}[PUT][first_name][name]
  API: verify ${response}[delay] is ${second_response_json}[PUT][first_name][delay]
  API: verify ${response}[headers] is ${second_response_json}[PUT][first_name][headers]
  API: Put ${empty_json} to http://${RESPONDER_ADDR}:${third_responder}/profession and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${second_response_json}[PUT][profession][job]

  #verify patch endpoints work with specific delay and status
  API: Patch ${empty_json} to http://${RESPONDER_ADDR}:${third_responder}/first_name and store response in response
  API: verify ${response} status is ${HTTP_ALREADY}
  API: verify ${response}[name] is ${second_response_json}[pATCH][first_name][name]
  API: verify ${response}[delay] is ${second_response_json}[pATCH][first_name][delay]
  API: Patch ${empty_json} to http://${RESPONDER_ADDR}:${third_responder}/profession and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${second_response_json}[pATCH][profession][job]

  #verify delete endpoints work without specifying any specific delay, status , or headers
  API: Delete http://${RESPONDER_ADDR}:${third_responder}/first_name and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[name] is ${second_response_json}[delete][first_name][name]
  API: Delete http://${RESPONDER_ADDR}:${third_responder}/profession and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${second_response_json}[delete][profession][job]

  #POST /config/stop endpoint
  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${third_responder}/config/stop and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[message] has Exiting

  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${third_responder}/config/stop and store response in response
  POF:API: verify ${response} status is ${HTTP_OK}


@Test: POST XML update (tags=[AWS], devtime=60)
  # This test disabled because of a change in the WAIT method. Should be enabled and this comment removed once IE-2452 is completed
  API: Post ${port_json_third} to http://${RESPONDER_ADDR}:${initial_responder}/config/start and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[message] has Service successfully started on port ${fourth_responder}
  wait for 1 second

  #POST /update to create endpoints at new port using xml
  API: add application/xml as the content-type header
  API: Post ${xml_reponse} to http://${RESPONDER_ADDR}:${fourth_responder}/update and store response in response
  API: verify ${response} status is ${HTTP_OK}

  API: get responses from http://${RESPONDER_ADDR}:${fourth_responder}/config
  API: verify ${responses} status is ${HTTP_OK}

  #verify get endpoints work (currently we can only test status since it's xml)
  API: get response from  http://${RESPONDER_ADDR}:${fourth_responder}/users/1
  API: verify ${response} status is ${HTTP_MULTIPLE}
  API: get response from  http://${RESPONDER_ADDR}:${fourth_responder}/users/2
  API: verify ${response} status is ${HTTP_OK}

  #verify put endpoints work (currently we can only test status since it's xml)
  API: Put ${empty_json} to  http://${RESPONDER_ADDR}:${fourth_responder}/users/1 and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: Put ${empty_json} to  http://${RESPONDER_ADDR}:${fourth_responder}/users/2 and store response in response
  API: verify ${response} status is ${HTTP_NOT_FOUND}

  #verify post endpoints work (currently we can only test status since it's xml)
  API: Post ${empty_json} to  http://${RESPONDER_ADDR}:${fourth_responder}/users/1 and store response in response
  API: verify ${response} status is ${HTTP_ALREADY}
  API: Post ${empty_json} to  http://${RESPONDER_ADDR}:${fourth_responder}/users/3 and store response in response
  API: verify ${response} status is ${HTTP_CREATED}

  #verify delete endpoints work (currently we can only test status since it's xml)
  API: Delete http://${RESPONDER_ADDR}:${fourth_responder}/users/1 and store response in response
  API: verify ${response} status is ${HTTP_UNAUTHORIZED}
  API: Delete http://${RESPONDER_ADDR}:${fourth_responder}/users/3 and store response in response
  API: verify ${response} status is ${HTTP_NO_CONTENT}

  #POST /config/stop endpoint
  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${fourth_responder}/config/stop and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[message] has Exiting

  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${fourth_responder}/config/stop and store response in response
  POF:API: verify ${response} status is ${HTTP_OK}


@Test: POST replace (tags=[AWS], devtime=60)
  # This test disabled because of a change in the WAIT method. Should be enabled and this comment removed once IE-2452 is completed
  API: add application/json as the content-type header # this is needed since the last test specified xml content type
  API: Post ${port_json_fourth} to http://${RESPONDER_ADDR}:${initial_responder}/config/start and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[message] has Service successfully started on port ${fifth_responder}
  wait for 1 second

  API: get responses from http://${RESPONDER_ADDR}:${fifth_responder}/config
  API: verify ${responses} status is ${HTTP_OK}

  #POST /update to create endpoints at new port
  API: Post ${first_response_json} to http://${RESPONDER_ADDR}:${fifth_responder}/update and store response in response
  API: verify ${response} status is ${HTTP_OK}

  API: get response from  http://${RESPONDER_ADDR}:${fifth_responder}/profession
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[job] is ${first_response_json}[Get][profession][job]
  API: get response from  http://${RESPONDER_ADDR}:${fifth_responder}/first_name
  API: verify ${response} status is ${HTTP_OK}

  #POST /update a different json file
  API: Post ${second_response_json} to http://${RESPONDER_ADDR}:${fifth_responder}/update and store response in response
  API: verify ${response} status is ${HTTP_OK}

  # verify the routes haven't been replaced
  API: get response from  http://${RESPONDER_ADDR}:${fifth_responder}/profession
  POF:API: verify ${response} status is ${HTTP_MOVED}
  API: get response from  http://${RESPONDER_ADDR}:${fifth_responder}/first_name
  POF:API: verify ${response} status is ${HTTP_MULTIPLE}

  #POST /replace a different json file
  API: Post ${second_response_json} to http://${RESPONDER_ADDR}:${fifth_responder}/replace and store response in response
  API: verify ${response} status is ${HTTP_OK}

  API: get response from  http://${RESPONDER_ADDR}:${fifth_responder}/profession
  API: verify ${response} status is ${HTTP_MOVED}
  API: get response from  http://${RESPONDER_ADDR}:${fifth_responder}/first_name
  API: verify ${response} status is ${HTTP_MULTIPLE}

  #POST /config/stop endpoint
  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${fifth_responder}/config/stop and store response in response
  API: verify ${response} status is ${HTTP_OK}
  API: verify ${response}[message] has Exiting

  API: Post ${empty_json} to http://${RESPONDER_ADDR}:${fifth_responder}/config/stop and store response in response
  POF:API: verify ${response} status is ${HTTP_OK}


@Define box-power-user = arn:aws:iam::401948580350:role/cd-role
@Define test_message = Hello, World
@Define queue_name = nebula_test

@Test: SQS Queues (tags=[AWS], devtime=40)
  make queue_name unique
  API: assume role box-power-user
  API: setup an SQS queue named ${queue_name}
  API: send message ${test_message} to the sqs queue ${queue_name}
  API: receive messages from sqs queue ${queue_name}
  #send five messages to the queue
  #retrieve two of the messages
  #retrieve the other three
  #retrieve another message, reporting an empty queue
  API: receive messages from sqs queue ${queue_name}
  API: delete sqs queue ${queue_name}
  POF: API: send message ${test_message} to the sqs queue ${queue_name}

@Define topic_name = nebula_test_topic
@Define topic_queue = nebula_sns_test
@Test: SNS Topics (tags=[AWS], devtime=30)
  make nebula_test_topic unique
  make topic_queue unique
  API: assume role box-power-user
  #create an SNS topic and a queue
  API: setup a sns topic with the name ${topic_name}
  API: setup an SQS queue named ${topic_queue}
  API: subscribe ${topic_queue} to the sns topic ${topic_name}
  API: publish ${test_message} to the sns topic ${topic_name}
  API: receive messages from sqs queue ${topic_queue}
  API: delete sns topic ${topic_name}
  API: delete sqs queue ${topic_queue}
  #Get arn of queue to send messages to
  #Get topic the queue should subscribe to
  #give sqs:SendMessage permission to the topic
  #subscribe queue to the topic
  #give user permission to publish to topic
  #give user permission to read queue
  #publish message to topic
  #read message from queue
  #remove the topic


@Define reboot_workflow = arn:aws:states:us-east-1:771302058135:stateMachine:dev-deviceReboot
@DefinePlaceholder workflow_input = { \
  "jobId": "A2421TV+2020-03-02T14:07:52.032Z",\
  "deviceId": "A2421TV",\
  "tenantId": "0123456",\
  "jobType": "deviceReboot",\
  "jobParams": [\
    {\
      "extronTraceId": "Nebula-Test-4c49-99a8-63cb6868657d"\
    }\
  ],\
  "jobState": "pending",\
  "createdAt": "2020-03-02T14:07:52.032Z",\
  "createdBy": "awsengineering@extron.com",\
  "modifiedAt": "2020-03-02T14:07:52.032Z"\
}

@Test: Start workflow (disabled, tags=[AWS], devtime=30)
  make workflow_name unique
  API: assume role box-power-user
  API: start the ${reboot_workflow} with ${workflow_input} as NebulaTestRun
  API: verify ${NebulaTestRun}[ResponseMetadata][HTTPStatusCode] is 200

@Test: V4SIG request signing (tags=[AWS], devtime=30)
 API: assume role box-power-user
 API: add identity as the Accept-Encoding header
 API: add application/x-www-form-urlencoded as the Content-Type header
 API: post nothing to https://sts.amazonaws.com/?GetCallerIdentity signed with v4sig and store response in send_request
 API: verify ${send_request} status is ${HTTP_OK}


@Define EX_NEB_COGNITO_USERNAME = awsengineering@extron.com
@Define EX_NEB_COGNITO_PASSWORD = Extron1111@
@Define EX_NEB_CLIENT_ID = soflcl2hune93o24bh4t5884n
@Define EX_NEB_USER_POOL_ID=us-east-1_DGvEGYe0V

@DefinePlaceholder  bearer = Bearer 
#
@Test: Retrieve Greenville Credentials (disabled, tags=[AWS, TOKENS])
 API: assume role tbd-dev-power-user
 API: Get the Greenville credentials as user_creds
 verify ${user_creds}[ResponseMetadata][HTTPStatusCode] variable is ${HTTP_OK}
 verify ${user_creds}[AuthenticationResult][AccessToken] variable has eyJ
 set primitive ${auth_header} to "Bearer id_token=${user_creds}[AuthenticationResult][IdToken]"
 add "&access_token=${user_creds}[AuthenticationResult][AccessToken]" to the end of primitive ${auth_header}
 print ${auth_header}
 #also valid indices:
 #[ChallengeParameters]
 #[AuthenticationResult][ExpiresIn]
 #[AuthenticationResult][TokenType]
 #[AuthenticationResult][RefreshToken]
 #[AuthenticationResult][IdToken]
 #[ResponseMetadata][RequestId]
 #[ResponseMetadata][HTTPStatusCode]
 #[ResponseMetadata][HTTPHeaders][date]
 #[ResponseMetadata][HTTPHeaders][content-type]
 #[ResponseMetadata][HTTPHeaders][content-length]
 #[ResponseMetadata][HTTPHeaders][connection]
 #[ResponseMetadata][HTTPHeaders][x-amzn-requestid]
 #[ResponseMetadata][HTTPHeaders][RetryAttempts]
 API: add ${auth_header} as the Authorization header
 API: add application/x-www-form-urlencoded as the Content-Type header
