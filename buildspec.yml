version: 0.2

env:
  # Adding parameter-store entries here as well for easy localbuilding using Codebuild's sandbox tool.
  parameter-store:
    NPM_REGISTRY: "/cicd/npm-registry"
    NPM_AUTH_API: "/cicd/npm-auth-api"
    RNC_BUILD_ARTIFACTORY_USER: "/cicd/rnc-build-artifactory-username"
    ARTIFACTORY_TOKEN: "/cicd/artifactory-token"
    ENG_TOOLING_CD_SECRET: "/cicd/cd-secrets/cd-user-secret-access-key"
    ENG_TOOLING_CD_KEY: "/cicd/cd-secrets/cd-user-access-key-id"
    RNC_BUILD_ARTIFACTORY_PASSWORD: "/cicd/rnc-build-artifactory-password"
    PYPI_REPO_SIMPLE: "/cicd/pypi-repo-simple"

#---------------------------------------------------------------------------------------------------
# Build Project Environment Variables:
#
#  parameter-store:
#    RNC_BUILD_ARTIFACTORY_USER     - The Artifactory username
#    RNC_BUILD_ARTIFACTORY_PASSWORD - The Artifactory password
#    RNC_BUILD_BITBUCKET_USERNAME   - The Git username
#    RNC_BUILD_BITBUCKET_PASSWORD   - The Git password
#    PYPI_REPO_SIMPLE               - The location of the pypi repo in Artifactory
#
# CodeBuild Environment Variables
# https://docs.aws.amazon.com/codebuild/latest/userguide/build-env-ref-env-vars.html
#---------------------------------------------------------------------------------------------------

phases:
  install:
    runtime-versions:
      docker: 18
    commands:
      # Check for ci-skip, if it is in the commit message, skip build.
      #- if [ `skip=$(git log -1 | grep -o "\[ci\ skip\]") && echo $? || echo $?` -eq 0 ]; then echo 'ci skip found in last commit, skipping build' && aws codebuild stop-build --id $CODEBUILD_BUILD_ID && exit 1; fi
      # Determine branch name and type
      - BPWD=$(pwd)
      - BRANCH_NAME=$(git branch --contains $CODEBUILD_SOURCE_VERSION --sort=-committerdate | awk '{print $1; exit}')
      - BRANCH_TYPE=$(echo $BRANCH_NAME | cut -d '/' -f1) #Only needed if checking that the branch is a feature, PR, etc.
      - git checkout $BRANCH_NAME
      - git branch
      - echo Running on branch $BRANCH_NAME
      # Install needed dependencies
      - echo Installing Dependencies
      - apt-get update && apt-get install -qqy --no-install-recommends xvfb dpkg fonts-liberation libappindicator3-1 libasound2 libatk-bridge2.0-0 libnspr4 libnss3 libxtst6 xdg-utils
      # Install Chrome
      - wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
      - dpkg -i google-chrome-stable_current_amd64.deb
      - apt-get install -f
      - CHROME_VERSION=$(google-chrome --version | sed 's/Google Chrome //')
      - echo $CHROME_VERSION
      - echo registry=${NPM_REGISTRY} > .npmrc
      - curl -u $RNC_BUILD_ARTIFACTORY_USER:$RNC_BUILD_ARTIFACTORY_PASSWORD ${NPM_AUTH_API} >> .npmrc
      - npm install nebula-responder
      - cd node_modules/nebula-responder
      - npm start &
      - cd ../..
      - export RESPONDER_ADDR=127.0.0.1

  pre_build:
    commands:
      # Install Project from Artifactory
      - pip install --upgrade pip
      - python -V
      - pip install -r resources/dev.requirements.txt
      - pip install nebula -i https://$RNC_BUILD_ARTIFACTORY_USER:$RNC_BUILD_ARTIFACTORY_PASSWORD@extron.jfrog.io/extron/api/pypi/PyPi-dev/simple
      - pip install snips-nlu
      - snips-nlu download en
  build:
    commands:
      - echo Training Nebula Natural Language Understanding
      - snips-nlu generate-dataset en nebula/snips/api_engine/*.yaml > nebula/snips/api_engine/nebula_data.json
      - snips-nlu train -r 42 nebula/snips/api_engine/nebula_data.json nebula/snips/api_engine/nebula_api_engine
      - snips-nlu generate-dataset en nebula/snips/web_engine/*.yaml > nebula/snips/web_engine/nebula_data.json
      - snips-nlu train -r 42 nebula/snips/web_engine/nebula_data.json nebula/snips/web_engine/nebula_web_engine
      - echo Running Unit Tests
      - python3 -m pytest ./nebula_tests/modules/common
      - echo Running Web Tests
      - export NEBULA_PASSWORD=Extron$
      - python3 -m nebula ./tests/AWSApiTests.neb -s ./tests/test_selectors/test_selectors.yml -tt AWS -l ./tests/test_selectors/test_lang_file.yml --headless -cv $CHROME_VERSION --notracking
  post_build:
    commands:
      # Create dir for the artifacts to copy over
      - mkdir -p artifacts/bin artifacts/dist artifacts/docs
      # Create temp file and copy it to artifacts and each of its subdirectories so that the build doesn't fail during non-deploy jobs.
      - touch "created_on_$(date +%s)" && find artifacts -type d -maxdepth 1 -exec cp created_on_* {} \;

reports:
  NebulaReports:
    files:
    - '**/*'
    base-directory: logs
    discard-paths: yes
# There are no logs if we don't run Sanity check
artifacts:
  files:
    - '**/*'
  base-directory: logs
  discard-paths: yes
  secondary-artifacts:
    dist:
        files:
          - '**/*'
        name: dist
        base-directory: artifacts/dist
        discard-paths: yes
    bin:
      files:
        - '**/*'
      name: bin
      base-directory: artifacts/bin
      discard-paths: yes
    test:
      files:
        - '**/*'
      name: test
      base-directory: logs
      discard-paths: yes
    docs:
      files:
        - '**/*'
      name: docs
      base-directory: artifacts/docs
      discard-paths: yes

