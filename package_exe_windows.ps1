# This script is to be run locally to build with windows exe. This is the simplest way to get files into OneDrive
# Make sure to run it as admin in Powershell.

Remove-Item -LiteralPath "dist" -Force -Recurse -ErrorAction SilentlyContinue
Remove-Item -LiteralPath "build" -Force -Recurse -ErrorAction SilentlyContinue

python package_exe_nebula.py

$version = GET-Content VERSION
$exeFile = "nebula_" + $version + "_Windows.exe"
$exePath = "..\" + $exeFile

Move-Item dist\nebula_2.0.exe -DestinationPath $exePath
#Compress-Archive -Path dist\nebula.exe -DestinationPath $zipPath

$env:NEBULA_PASSWORD="test123"
.\dist\nebula_2.0.exe  .\tests\test_selectors -s .\tests\test_selectors\test_selectors.yml -l .\tests\test_selectors\test_lang_file.yml --headless -i http://usa-rnc-nebula:5000 -id WINDOWS_EXE

#Move-Item .\dist\nebula_2.0.exe -Destination $exePath

#$ZipFilePath = "installer\" + $zipPath
#Copy-Item -Path $ZipFilePath -Destination "C:\Users\eparker\OneDrive - Extron Electronics\Nebula"