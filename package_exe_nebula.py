import os
import shutil


# remove current pyinsaller folders if they exist
if os.path.isdir('./build'):
	shutil.rmtree('./build', ignore_errors=True)
	shutil.rmtree('./dist', ignore_errors=True)
	shutil.rmtree('./__pycache__', ignore_errors=True)

# run pyinstaller
os.system('pyinstaller -D nebula_2.0.spec')

# place the user into the exe folder
os.chdir('./dist')



