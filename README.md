# Nebula

Nebula is a BDD framework using Natural Language Processing

## This is a Python project

* You will need to have Python 3.6.5 or above installed
* You will need to have pip installed
* You will need python and pip accessible through PATH environment variable
* Install Python using the customized option has the options to install pip and setup PATH evn.
* The project was created in VS Code.
* It utilizes Python 3.X and Selenium.
* It also utilizes Dialogflow as it's Natural Language Processing service.

## Project Goals

* Create a BDD framework which uses natural language to test web applications

## Documentation

Full documentation can be found on the wiki - <https://wiki.extron.com/display/IET/Nebula>

## Getting Started

1. Install Python 3.6 (3.6.5) or above.
2. Make sure PIP is installed (pip3 --version or pip --version)
3. Clone this repository
4. Download and install Visual Studio Code (<https://code.visualstudio.com/download)>
5. Open this cloned repository in VS Code
6. Install the python dependencies.
    Navigate to the base folder of this repo and run this command.

    ```bash
    pip3 install -r resources/dev.requirements.txt
    ```

    If you get an access denied error you may need to add "--user" to the pip3 command
7. Install the necessary browser drivers. We have a script that will download them into this project. Run this file at the project root level.
    
    ```bash
    python3 download_webdrivers.py
    ```

 NOTE: On some Linux versions, you might need to install libX11
  `sudo yum install libX11`

## How to debug and run tests Nebula in VSCode

VS Code can build and run Nebula in a Docker image. Perform these steps to start debugging.

1. Follow the steps in the "Getting Started" section
2. In the Debug window, choose the "Python: Test Selectors" configuration.
3. Click the Debug button

## Reference Information

### Run the Docker image from the command line

Run the following command to run the extron-nebula Docker image

```bash
docker run -it -v <path_to_tests_to_exec>:/usr/src/tests -v <path_to_output_files>:/usr/src/app/output_files extron-nebula "<optional parameters>"
```

You can also specify other nebula command line parameters after headless. Make sure it is in the quotes

NOTE: Updated instructions: <https://wiki.extron.com/display/SWENG/Nebula+Running+in+a+Docker+Container>

### Command Line Parameters

The following command line parameters can be used with nebula

* -b, --b - Browser to use when running tests (OPTIONAL)
  * OPTIONS: Firefox, Chrome
    * DEFAULT: (Chrome)
* -s, --selectors_file - The path to the selectors file (OPTIONAL)
* -i, --imageStore - The url to the imageStore(OPTIONAL)
  * Order of precedence: Command Line Parameter, Environmental Variable, Config file)
* -tr, -test_report, --test_report_location - The location where to create the test reports (OPTIONAL)
  * DEFAULT: output_files folder in current directory
* -l, --lang_file - A file path leading to a language file (OPTIONAL)
* --headless - Add this option to run the tests in headless mode (OPTIONAL)
* -id, --image_identifier - Add this option to save images in the specified named directory (OPTIONAL)
* --dev - using development version of DialogFlow (OPTIONAL)
* --rd - used when performing remote debugging of Docker container (OPTIONAL)

### Run in Intellij IDEA

Add a Python configuration to point to module: nebula

For Parameters, enter the location of the test file or folder, for example:
`./tests/test_selectors/testSelectors.txt -s ./tests/test_selectors/test_selectors.yml -l ./tests/test_selectors/test_lang_file.yml`

For Environment, add any additional parameters

Note: Make sure location of chromedriver and geckodriver is accessible through the PATH variable.

## Creating Executables


1.	Make sure you are on the master branch of Nebula and that you have pulled down the latest copy
2.  Run the package_exe_nebula.py script in it's current location within the pulled software package
    1. >$  python package_exe_nebula.py
3.  The executable is located in the dist folder within the software package



## Contributing Guidelines
If you are contributing to the project, here are some guidelines to follow.

### Unit Testing
We have implemented pytest as part of the project. Please follow these guidelines:
1. All methods, unless they interact with Selenium or Dialogflow, should have an accompanying unit test.
2. All pytest files go inside the nebula_tests directory in the matching directory of the file you are testing. 
3. All pytest files should be named like "test_<file_to_test>.py".
4. Pytest has been enabled in VS Code in the checked-in settings.json file.
5. Type "pytest" on the command line to run all tests. (On Linux you may have to use this instead: python3 -m pytest)

### Functional Testing
Functional tests should be created in the test_selectors/nebulaSanityCheck.txt file.

## Who do I talk to?
### Team Members

* Nisarg Vinchhi (nvinchhi@extron.com)

* Ernest Christley (echristley@extron.com)

