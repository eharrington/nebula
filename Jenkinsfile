#!groovy
@Library('jenkinshelper') _
pipeline {
    environment {
        GIT_REPO_URL = 'bitbucket.org/extron-electronics/nebula.git'
        SLACK_CHANNEL = '#team-te-rnc'
        AUTH_TOKEN = credentials('AWS_Auth_Token')
        AWS_REGION = credentials('AWS_REGION')
        AWS_USERNAME = credentials('AWS_USERNAME')
        AWS_PASSWORD = credentials('AWS_PASSWORD')
        AWS_USER_POOL_ID = credentials('AWS_USER_POOL_ID')
        AWS_CLIENT_ID = credentials('AWS_CLIENT_ID')
        PYPI_REPO_URL = 'https://extron.jfrog.io/extron/api/pypi/PyPi-dev'
        JENKINS_NODE = 'usa-rnc-nebula'
    }
    parameters {
        booleanParam(name: 'Dry_Run', defaultValue: 'false', description: 'Dry Run to update jenkins file and parameters, then abort.')
        string(name: 'artifactory', defaultValue: 'Extron-Artifactory', description: 'artifactory host')
        string(name: 'artifactory_path', defaultValue: 'Installers/Nebula', description: 'artifactory repository path')
        string(name: 'artifactory_host', defaultValue: 'https://extron.jfrog.io/artifactory', description: 'artifactory host URL')
    }
    agent {
        node {
            label "slave1"
        }
    }
    options {
        disableConcurrentBuilds()
        timeout time: 1,
                unit: 'HOURS'
        buildDiscarder(logRotator(daysToKeepStr: '15', numToKeepStr: '15'))
    }
    stages{
        stage('Dry Run') {
            steps {
                script {
                    if ("${params.Dry_Run}" == "true") {
                        currentBuild.result = 'ABORTED'
                        error('DRY RUN COMPLETED. JOB PARAMETERIZED.')
                    }
                }
            }
        }
        stage('SKIP CHECK'){
            when{
                expression{
                    env.BRANCH_NAME=='master'
                }
            }
            steps{
                script{
                    try {
                        // check for ci skip
                        gitCiSkip action: 'check'
                    }
                    catch(err) {
                        println(err)
                        currentBuild.result = 'FAILURE'
                        error("${env.JOB_NAME} failure.")
                    }
                }
            }
        }
        stage('Build Prep'){
            steps{
                script{
                    currentBuild.displayName = params.ProjectName
                    println env.BRANCH_NAME
			    }
            }
        }
        stage('Install Dependencies'){
            steps{
                script{
                    env.CHROME_VERSION = sh(script: 'google-chrome --version', returnStdout: true).trim()
                    env.CHROME_VERSION = env.CHROME_VERSION.replaceAll('Google Chrome ', '')
                    sh """
                        pwd
                        bpwd="${env.WORKSPACE}"
                        echo \$bpwd
                        sudo -H pip3 install -r resources/dev.requirements.txt
                        sudo -H snips-nlu download en

                        cd \$bpwd/nebula/snips/api_engine
                        snips-nlu generate-dataset en *.yaml > \$bpwd/nebula/snips/api_engine/nebula_data.json
                        snips-nlu train -r 42 nebula_data.json nebula_api_engine

                        cd \$bpwd/nebula/snips/web_engine
                        snips-nlu generate-dataset en *.yaml > \$bpwd/nebula/snips/web_engine/nebula_data.json
                        snips-nlu train -r 42 nebula_data.json nebula_web_engine

                        cd \$bpwd
                        python3 -c 'from nebula import download_webdrivers; download_webdrivers.get_chromedriver(\"${CHROME_VERSION}\")'
                    """
                }
            }
        }
        stage('Run Responder for API Tests'){
            steps{
                script{
                    try{
                        sh "docker rm -f nebula-responder && echo 'container nebula-responder removed' || echo 'container nebula-responder does not exist'"
                            }catch(err){
                                sh 'exit 1'
                            }
                            try{
                                sh "docker rmi -f nebula-responder"
                            }catch(err){
                                sh 'exit 1'
                            }
                    withEnv(['JENKINS_NODE_COOKIE=dontkill']){
                        withCredentials([usernamePassword(credentialsId: 'artifactory-credential', passwordVariable: 'ARTIFACTORY_PASSWORD', usernameVariable: 'ARTIFACTORY_USERNAME')]){
                            sh "docker run  -d -p 1670:1670 -p 1672:1672 --name nebula-responder extron-docker-test.jfrog.io/extron-nebula-responder"
                            def responder_addr = sh (script: """docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' nebula-responder""", returnStdout: true)
                            env.RESPONDER_ADDR = responder_addr.trim()
                        }
                    }
                }
            }
        }
        stage('Run Unit Tests'){
            steps{
                script{
                    sh 'python3 -m pytest'
                }
            }
        }
        stage('Run Tests in Dev for Chrome'){
            steps{
                script{
                    sh '''
                    export NEBULA_PASSWORD=test123
                    python3 -m nebula ./tests -tt Jenkins -s ./tests/test_selectors/test_selectors.yml -l ./tests/test_selectors/test_lang_file.yml --headless -i http://usa-rnc-nebula:5000 -id Jenkins --notracking
                    '''
                }
            }
            post{
                always{
                    junit 'logs/*.xml'
                }
            }
        }
        // stage('Run Tests in Dev for Firefox'){
        //     steps{
        //         script{
        //             sh '''
        //             export NEBULA_PASSWORD=test123
        //             python3 -m nebula ./tests/test_selectors/ -s ./tests/test_selectors/test_selectors.yml -l ./tests/test_selectors/test_lang_file.yml --headless -i http://usa-rnc-nebula:5000 --dev -id Jenkins -b Firefox --notracking
        //             '''
        //         }
        //     }
        //     post{
        //         always{
        //             junit 'output_files/*.xml'
        //         }
        //     }
        // }
        stage('Delete Old Docker Images'){
            when{
                expression{
                    anyOf {
                        env.BRANCH_NAME=='master'
                        env.BRANCH_NAME=='development'
                    }
                }
            }
            steps{
                script{
                    try {
                        sh "docker rmi -f extron-docker-test.jfrog.io/extron-nebula"
                    }
                    catch(err) {
                        sh 'exit 0'
                    }
                }
            }
        }
        stage('Build Docker Image'){
            when{
                expression{
                    anyOf {
                        env.BRANCH_NAME=='master'
                        env.BRANCH_NAME=='development'
                    }
                }
            }
            steps{
                script{
                    containerImage = docker.build("extron-docker-test.jfrog.io/extron-nebula", "-f dockerfile .")
                }
            }
        }
        stage('Test Docker Image for Chrome'){
            when{
                expression{
                    anyOf {
                        env.BRANCH_NAME=='master'
                        env.BRANCH_NAME=='development'
                    }
                }
            }
            steps{
                script{
                    sh "ls"
                    sh "docker run -v ${env.WORKSPACE}/output_files:/usr/src/app/output_files -v ${env.WORKSPACE}/tests/test_selectors:/usr/src/tests -e RESPONDER_ADDR=${env.RESPONDER_ADDR} -e NEBULA_PASSWORD=Extron\$ -e AUTH_TOKEN=${AUTH_TOKEN} -e AWS_REGION=${AWS_REGION} -e AWS_USERNAME=${AWS_USERNAME} -e AWS_PASSWORD=${AWS_PASSWORD} -e AWS_USER_POOL_ID=${AWS_USER_POOL_ID} -e AWS_CLIENT_ID=${AWS_CLIENT_ID} extron-docker-test.jfrog.io/extron-nebula"
                }
            }
            post{
                always{
                    junit "logs/*.xml"
                }
            }
        }
        stage('Update Version Numbers'){
            when{
                expression{
                    env.BRANCH_NAME=='master'
                }
            }
//            steps{
//                script{
//                    sh 'python3 update_version.py'
//                    env.CURRENT_VERSION = sh(script: 'cat VERSION', returnStdout: true)
//                }
//                withCredentials([usernamePassword(credentialsId: 'rnc-build-bitbucket-credentials', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
//                    sh '''
//                        git add VERSION
//                        git commit -m \"jenkins - Update version numbers [ci skip]\"
//                        git pull https://${GIT_USERNAME}:${GIT_PASSWORD}@${GIT_REPO_URL} HEAD:${BRANCH_NAME}
//                        git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${GIT_REPO_URL} HEAD:${BRANCH_NAME}
//                    '''
//				}
//            }
        }
        stage('Publish Docker Image'){
            when{
                expression{
                    env.BRANCH_NAME=='master'
                }
            }
//            steps{
//                script{
//                    docker.withRegistry('http://extron-docker-test.jfrog.io', 'artifactory-credential') {
//                        containerImage.push(env.CURRENT_VERSION)
//                        containerImage.push('latest')
//                    }
//                }
//            }
        }
        stage('Build and Publish PyPi'){
            when{
                expression{
                    env.BRANCH_NAME=='master'
                }
            }
//            steps{
//                script{
//                    sh 'sudo -H pip3 install --upgrade wheel twine'
//                    withCredentials([usernamePassword(credentialsId: 'artifactory-credential', passwordVariable: 'ARTIFACTORY_PASSWORD', usernameVariable: 'ARTIFACTORY_USERNAME')]){
//                        sh 'python3 setup.py sdist bdist_wheel'
//                        sh 'python3 -m twine upload -u ' + ARTIFACTORY_USERNAME + ' -p ' + ARTIFACTORY_PASSWORD + ' --repository-url ' + env.PYPI_REPO_URL + ' dist/*'
//                    }
//                }
//            }
        }
        stage('Build Linux Executable with Docker'){
            when{
                expression{
                    env.BRANCH_NAME=='master'
                }
            }
            agent {
                node {
                    label 'docker'
                }
            }
            steps{
                script{
                    sh 'printenv'
                    try {
                        println "Starting stage: Build Linux Executable with Docker"
                        withCredentials([
                            usernamePassword(credentialsId: '775cdcfb-d40d-4f6a-85ec-91f91ef3f526', usernameVariable: 'bitbucket_token', passwordVariable: 'bitbucket_secret'),
                            usernamePassword(credentialsId: 'artifactory-local', usernameVariable: 'artifactoryusername', passwordVariable: 'artifactorypassword')
                        ]) {
                            script {
                                println "CHANGE_BRANCH: ${env.CHANGE_BRANCH}"
                                println "GIT_BRANCH: ${env.GIT_BRANCH}"
                                if (env.CHANGE_BRANCH != null) {
                                    env.branch_git = env.CHANGE_BRANCH
                                    println "env.CHANGE_BRANCH not null -- branch: ${env.branch_git}"
                                } else if (env.GIT_BRANCH != null) {
                                    env.branch_git = env.GIT_BRANCH
                                    println "env.GIT_BRANCH not null -- branch: ${env.branch_git}"
                                } else {
                                    println "something broke"
                                    exit 1
                                }
//                                sh(returnStdout: true, script: """
//                                    echo "Building Linux Executable with Docker"
//                                    bpwd=\$(pwd)
//                                    docker build -t nebula_centos_docker --progress=plain --label nebula="centosbuild" --build-arg artifactoryusername="${env.artifactoryusername}" --build-arg artifactorypassword="${env.artifactorypassword}" --build-arg BRANCH_NAME="${env.branch_git}" --build-arg BITBUCKET_TOKEN="${env.bitbucket_token}" --build-arg BITBUCKET_SECRET="${env.bitbucket_secret}" --no-cache \$bpwd/resources/centos_docker
//                                    docker run --label nebula=centosbuild --env artifactoryusername="${env.artifactoryusername}" --env artifactorypassword="${env.artifactorypassword}" --env BRANCH_NAME="${env.branch_git}" --env BITBUCKET_TOKEN="${env.bitbucket_token}" --env BITBUCKET_SECRET="${env.bitbucket_secret}" nebula_centos_docker
//                                    dockerimageid=\$(docker images --filter "label=nebula=centosbuild" --quiet)
//                                    dockercontainerid=\$(docker ps -a --filter "label=nebula=centosbuild" --quiet)
//                                    echo "Removing docker image id: \$dockerimageid"
//                                    echo "Removing docker container id: \$dockercontainerid"
//                                    #docker rm \$dockercontainerid
//                                    #docker rmi \$dockerimageid
//                                """)
                            }
                        }
                    }
                    catch(err) {
                        println(err)
                        currentBuild.result = 'FAILURE'
                        error("${env.JOB_NAME} failure.")
                    }
                }
            }
        }
        stage('Build Windows Executable'){
            when{
                expression{
                    env.BRANCH_NAME=='master'
                }
            }
            agent {
                node {
                    label 'WindowsTest'
                }
            }
            steps{
                script{
                    try{
                        println "Starting stage: Build Windows Executable"
                        withCredentials([
                            usernamePassword(credentialsId: 'artifactory-credential', usernameVariable: 'artifactoryusername', passwordVariable: 'artifactorypassword')
                        ]) {
                            script {
                                powershell(returnStdout: true, script: """
                                    try {
                                        Write-Host "Importing powershell modules"
                                        Import-Module -Name "${env.WORKSPACE}\\buildnebula.psm1" -Force
                                        Write-Host "Removing dependencies"
                                        Remove-NebulaDependencies -bpwd "${env.WORKSPACE}"
                                        Write-Host "Installing dependencies"
                                        Install-NebulaDependencies -bpwd "${env.WORKSPACE}"
                                        Write-Host "Updating python files"
                                        Update-PythonFiles -bpwd "${env.WORKSPACE}"
                                        Write-Host "Initializing dataset"
                                        Initialize-NebualDataSet "${env.WORKSPACE}"
                                        Write-Host "Creating nebula exe"
                                        New-NebulaExe "${env.WORKSPACE}"
                                    }
                                    catch {
                                        \$ErrorMessage = \$_.Exception.Message
                                        Write-Host "Error: nebula - Build Windows Executable. The error message was \$ErrorMessage"
                                        exit 1
                                    }
                                """)
                            }
                        }
                        def nebulaexe = powershell(returnStdout: true, script: """
                            try {
                                 Get-ChildItem -Path "${env.WORKSPACE}" | Where-Object {\$_.Name -like "nebula_Windows_*.exe"} | Select-Object Name -ExpandProperty Name
                            }
                            catch {
                                \$ErrorMessage = \$_.Exception.Message
                                Write-Host "Error: nebula - Build Windows Executable. The error message was \$ErrorMessage"
                                exit 1
                            }
                        """)
                        env.nebulaexe = nebulaexe.trim()
                        println env.nebulaexe

                        println "Uploading windows exe to artifactory"
                        def server = Artifactory.server "Lab-Artifactory"
                        def uploadSpec = """{
                            "files":[
                                {
                                    "pattern": "${env.WORKSPACE}\\${env.nebulaexe}",
                                    "target": "generic-local/Installers/Nebula/"
                                }
                            ]
                        }"""
//                        server.upload spec: uploadSpec
                    }
                    catch(err) {
                        println(err)
                        currentBuild.result = 'FAILURE'
                        error("${env.JOB_NAME} failure.")
                    }
                }
            }
        }
    }//stages
    post{
        always{
            script{
                 currentBuild.description = 'Build: '+ env.BUILD_ID
                 println "Printing build description"
                 println currentBuild.description
            }
            gitCiSkip action: 'postProcess'
            sendSlackNotificationGit(buildStatus: currentBuild.result, buildName: currentBuild.displayName, buildDescription: currentBuild.description, slackChannel: env.SLACK_CHANNEL)
            deleteDir()
        }//always
    }//post
}
