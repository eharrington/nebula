cd nebula
cd snips
cd web_engine
python --version
del /S/Q nebula_data.json
rmdir /S/Q nebula_web_engine
snips-nlu generate-dataset en acceptAlertBox.yaml addValue.yaml assignValueToVariable.yaml browserInstances.yaml changeResolution.yaml clearCookies.yaml clearField.yaml clickButton.yaml clickCheckbox.yaml clickElementInSelectedRowFromTable.yaml clickListItem.yaml clickRadioButton.yaml closeCurrentWindow.yaml crawlAndScreenshot.yaml createUnique.yaml dismissAlertBox.yaml doubleClick.yaml dragAndDrop.yaml elementDoesntExist.yaml elementExists.yaml entities.yaml execute.yaml filter.yaml focus.yaml getCollection.yaml getLength.yaml getTableValues.yaml getValue.yaml hitKey.yaml inputText.yaml inputTextInAlert.yaml inputTextInList.yaml isEnabledOrDisabled.yaml islands.yaml listEntryDoesNotExist.yaml listItemExists.yaml moveMouse.yaml navigate.yaml printValue.yaml radioButtonIsSelectedOrNot.yaml rightClickAndSelect.yaml screenshot.yaml scrollIntoView.yaml selectFileinDialogbox.yaml selectFromDropdown.yaml setValue.yaml sliders.yaml timers.yaml urlencode.yaml validateCheckbox.yaml validateInput.yaml validateInputInList.yaml validateSortOrder.yaml verifyCurrentUrl.yaml verifyLengthCount.yaml waitForSeconds.yaml > nebula_data.json
snips-nlu train -r 42 nebula_data.json nebula_web_engine

cd..
cd api_engine
del /S/Q nebula_data.json
rmdir /S/Q nebula_api_engine
snips-nlu generate-dataset en addheader.yaml assumerole.yaml deleterequest.yaml entities.yaml getresponse.yaml invokelambda.yaml jsonschemacompare.yaml logintoapi.yaml patchrequest.yaml postrequest.yaml pullvalue.yaml putrequest.yaml removeheader.yaml retrieve_credentials.yaml sqsQueue.yaml switchsession.yaml verifyresponse.yaml workflows.yaml > nebula_data.json
snips-nlu train -r 42 nebula_data.json nebula_api_engine

cd../../..