# -*- mode: python -*-
import os
import sys
import platform
from pathlib import Path

# Get Current Nebula Version
f = open('./VERSION', 'r')
if f.mode =='r':
  version = f.read()


block_cipher = None

options = [ ('v', None, 'OPTION') ]

a = Analysis(['nebula/__main__.py'],
             pathex=[],
             binaries=[],
             #datas=[("/Users/wpowell/Bitbucket/nebula.git/VERSION", Path("."))],
             datas=[(os.getcwd() + "/VERSION", Path("."))],
             hiddenimports=[],
             hookspath=['.'],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='nebula_' + platform.system() + '_' + version,
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
