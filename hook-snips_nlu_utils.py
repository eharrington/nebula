from PyInstaller.compat import is_win, is_darwin
from PyInstaller.utils.hooks import collect_data_files
from PyInstaller.building.datastruct import Tree

import os

hiddenimports = ['snips_nlu_utils']
datas = [('nebula/snips', 'nebula/snips')]
if is_win:
    binaries=[('\\AppData\\Local\\Programs\\python\\python36\\lib\\site-packages\\snips_nlu_utils\\dylib\\libsnips_nlu_utils_rs.cp36-win_amd64.pyd', 'dylib')]

elif is_darwin:
    binaries=[('/usr/local/lib/python3.7/site-packages/snips_nlu_utils/dylib/libsnips_nlu_utils_rs.cpython-37m-darwin.so', 'dylib')]

else:
    binaries=[('/usr/local/lib64/python3.6/site-packages/snips_nlu_utils/dylib/libsnips_nlu_utils_rs.cpython-36m-x86_64-linux-gnu.so', 'snips_nlu_utils/dylib')]