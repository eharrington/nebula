from PyInstaller.compat import is_win, is_darwin
from PyInstaller.utils.hooks import collect_data_files
from PyInstaller.building.datastruct import Tree

import os

hiddenimports = ['sklearn', 'sklearn.utils._cython_blas', 'sklearn.neighbors.typedefs', 'sklearn.neighbors.quad_tree', 'sklearn.tree._utils', 'multiprocessing.get_context', 'sklearn.utils', 'pycrfsuite._dumpparser', 'pycrfsuite._logparser']
datas = [('nebula/snips', 'nebula/snips')]
if is_win:
    binaries=[('\\AppData\\Local\\Programs\\python\\python37\\lib\\site-packages\\snips_nlu_parsers\\dylib\\libsnips_nlu_parsers_rs.cp37-win_amd64.pyd', 'dylib')]

elif is_darwin:
    binaries=[('/usr/local/lib/python3.7/site-packages/snips_nlu_parsers/dylib/libsnips_nlu_parsers_rs.cpython-37m-darwin.so', 'dylib')]

else:
    binaries=[('/usr/local/lib64/python3.6/site-packages/snips_nlu_parsers/dylib/libsnips_nlu_parsers_rs.cpython-36m-x86_64-linux-gnu.so', 'snips_nlu_utils/dylib')]
