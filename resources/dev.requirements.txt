#setuptools
setuptools==40.0.0

#scipy
scipy==1.4.1

#Snips nlu
snips-nlu==0.20.0

# html parser
beautifulsoup4

# Cross-platform colored terminal text
colorama

# AWS SDK library
boto3

# config file parser
configparser

# JSON schema validator
fastjsonschema

# xml builder
lxml

# SIS commands
pyserial

# pytest - Unit Testing library
pytest
pytest_mock
mock

# handle yaml files
pyyaml

# request library
requests

# selenium web driver
Selenium

#microsofts webdriver
msedge-selenium-tools

# These are not always guaranteed to be installed
# pathlib
pathlib

# datetime
datetime
dateparser

#Excel -- csa
xlrd==1.2.0

#ssh related -- csa
bcrypt==3.1.7

#ssh related -- csa
asn1crypto==0.24.0

#ssh related -- csa
PyNaCl==1.3.0

#ssh related -- csa
cryptography==2.7

#ssh paramiko -- csa
paramiko==2.6.0

#dict to xml -- csa
dicttoxml==1.7.4

#http or https  -- csa
urllib3==1.24.1

# image manipulation
opencv-python==3.4.3.18