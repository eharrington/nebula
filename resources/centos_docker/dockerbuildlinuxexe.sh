#!/bin/bash

while getopts b:t:s: option
do
        case "${option}"
        in
                b) branchname=${OPTARG};;
                t) bitbucketkey=${OPTARG};;
                s) bitbucketsecret=${OPTARG};;
        esac
done

echo "Branch Name: $BRANCH_NAME"

if [ -z $branchname ]
then
    echo "branchname is empty. Trying environment variable."
    branchname=$BRANCH_NAME
    echo $BRANCH_NAME
fi

if [[ -z $bitbucketkey || -z $bitbucketsecret ]]
then
    printf "one of the bitbucket variables are empty.\nbitbucketkey: $bitbucketkey\nbitbucketsecret:$bitbucketsecret\n"
    exit 1
fi

function install_dependencies {
    echo "Installing dependencies"
    pythonver=$(python3 -V |  awk -F '.' '{ print$1"."$2 }' | sed s'/P/p/g' | sed s'/ //g')
    access_token=$(curl https://bitbucket.org/site/oauth2/access_token -d grant_type=client_credentials -u $bitbucketkey:$bitbucketsecret | jq '.access_token' | sed 's/"//g')
    if [ -d ./nebula ]
    then 
        rm -rf ./nebula
    fi
    git clone -b $BRANCH_NAME https://x-token-auth:$access_token@bitbucket.org/extron-electronics/nebula.git
    cd nebula
    bpwd=$(pwd)

    python3 -m pip install -r $bpwd/resources/dev.requirements.txt
    python3 -m snips_nlu download en
    python3 -m pip list
}

function modify_python_files {
    echo "Modifying python files"
    crf_slot_filler=$(find / -name crf_slot_filler.py | grep $pythonver)
    cp $bpwd/resources/snips_file_updates/crf_slot_filler.py $crf_slot_filler
    parsersutil=$(find / -name "utils.py" | grep "$pythonver/site-packages/snips_nlu_parsers")
    cp $bpwd/resources/snips_file_updates/parsers_utils.py $parsersutil
    utilsutil=$(find / -name "utils.py" | grep "$pythonver/site-packages/snips_nlu_utils")
    cp $bpwd/resources/snips_file_updates/utils_utils.py $utilsutil

    linuxlibsnipsparsersdir=$(find / -name "snips_nlu_parsers" | grep "$pythonver")
    linuxlibsnipsparsersname=$(ls $linuxlibsnipsparsersdir/dylib)
    chmod +x $linuxlibsnipsparsersdir/dylib/$linuxlibsnipsparsersname
    sed -i '/else:/,+1 d' $bpwd/hook-snips_nlu_parsers.py
    printf "else:\n    binaries=[('$linuxlibsnipsparsersdir/dylib/$linuxlibsnipsparsersname', 'dylib')]\n" >> $bpwd/hook-snips_nlu_parsers.py

    linuxlibsnipsutilsdir=$(find / -name "snips_nlu_utils" | grep "$pythonver")
    linuxlibsnipsutilsname=$(ls $linuxlibsnipsutilsdir/dylib)
    chmod +x $linuxlibsnipsutilsdir/dylib/$linuxlibsnipsutilsname
    sed -i '/else:/,+1 d' $bpwd/hook-snips_nlu_utils.py
    printf "else:\n    binaries=[('$linuxlibsnipsutilsdir/dylib/$linuxlibsnipsutilsname', 'dylib')]\n" >> $bpwd/hook-snips_nlu_utils.py
}

function build_nebula_executable {
    echo "Building Executable"
    $bpwd/nebula/snips/train_engines -b $bpwd
    python3 -m PyInstaller nebula_2.0.spec
}

function find_nebula_exe {
    #look for bulit executable
    unset nebulaexe
    nebulaexe=$(basename $bpwd/dist/nebula_Linux_*)
    if [ -z $nebulaexe ]
    then
        echo "Something went wrong and Nebula exe was not found."
        exit 1
    else
        echo "$nebulaexe"
    fi
}

function upload_artifactory {
    #Uploading nebula to artifactory
    username=$artifactoryusername
    password=$artifactorypassword
    url=http://usa-rnc-dockerhost:8081/artifactory/generic-local/Installers/Nebula/Test
    f="$bpwd/dist/$nebulaexe"
    echo "Uploading $f to artifactory"
    checksum=$(shasum -a 1 $f | awk '{ print $1 }')
    file=$(printf $f | awk -F "/" '{ print $NF}')
    curl --header "X-Checksum-Sha1:${checksum}" -u $username:$password -T $f "$url/$file"   
}

function test_google_url {
    #Independent Variables
    chromeapiurl="https://chromedriver.storage.googleapis.com"

    #Dependent Variables
    chromeversion=$(google-chrome --version | sed s'/Google Chrome //g' | awk -F "." '{ print $1"."$2"."$3 }')
    if $(curl $chromeapiurl > /dev/null 2>&1); then
            echo "connection successful"
            chromedriverversion=$(curl $chromeapiurl/LATEST_RELEASE_$chromeversion)
    else
            echo "connection failed"
        exit 1
	fi
}

function get_chromedriver {
        test_google_url

        #Download appropriate chrome driver based on installed chrome version.
        wget -O $bpwd/chromedriver_linux64_$chromedriverversion.zip $chromeapiurl/$chromedriverversion/chromedriver_linux64.zip

        #Extract chromedriver and place it within a PATH location.
        mkdir -p /opt/google/chrome/driver
        unzip -o -d /opt/google/chrome/driver $bpwd/chromedriver_linux64_$chromedriverversion.zip
        ln -f -s /opt/google/chrome/driver/chromedriver /usr/bin/chromedriver
}

function test_nebula_executable {
    #Testing the executable
    #$bpwd/dist/$nebulaexe
    $bpwd/dist/$nebulaexe $bpwd/tests/webTests -s $bpwd/tests/test_selectors/test_selectors.yml -l $bpwd/tests/test_selectors/test_lang_file.yml --headless -i http://usa-rnc-nebula:5000 --notracking -cv $(google-chrome-stable --version | sed s'/Google Chrome //g')
    #$bpwd/dist/$nebulaexe $bpwd/tests -tt Jenkins $bpwd/tests/test_selectors/test_selectors.yml -l $bpwd/tests/test_selectors/test_lang_file.yml --headless -i http://usa-rnc-nebula:5000 -id Jenkins --notracking -cv $(google-chrome-stable --version | sed s'/Google Chrome //g')
}

function keep_container_running {
    tail -f /dev/null
}

#Install the needed dependencies
install_dependencies
#Get ChromeDriver and install so that selenium tests are successful
get_chromedriver
#Modify Python files for nebula
modify_python_files
#build nebula exe
build_nebula_executable
#look for bulit executable
find_nebula_exe
#test executable
test_nebula_executable
#upload exe to artifactory
upload_artifactory
#keep the container running
#keep_container_running