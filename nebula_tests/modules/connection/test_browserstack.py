import pytest
import os

from pprint import pprint
from nebula.modules.connection import browserstack
from nebula.modules.report import nebula_test_exception as nte
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.remote.webdriver import WebDriver

browserstack_temp = None
access_key = 'kj4hcUVBUkDbwJA2B6po'

desired_cap = {
  'browserName': 'iPhone',
  'device': 'iPhone 8 Plus',
  'realMobile': 'true',
  'os_version': '11'
}

invalid_desired_cap = {
  'browserName': 'michaelsoft',
  'device': 'iPhone 8 Plus',
  'realMobile': 'true',
  'os_version': '2'
}

os.environ['BROWSERSTACK_USER'] = 'barretvasilchik3'
os.environ['BROWSERSTACK_KEY'] = access_key

def test_driver_none():
  with pytest.raises(TypeError):
    browserstack.get_driver()

def test_driver_no_caps():
    browserstack.get_driver({})

def test_driver_invalid():
  with pytest.raises(WebDriverException):
    browserstack.get_driver(invalid_desired_cap)

def test_get_driver():
  global browserstack_temp
  browserstack_temp = browserstack.get_driver(desired_cap)
  assert browserstack_temp is not None

def test_scenario():
  global browserstack_temp
  browserstack_temp = browserstack.get_driver(desired_cap)
  browserstack_temp.get("http://www.google.com")
  if not "Google" in browserstack_temp.title:
    raise Exception("Unable to load google page!")
  elem = browserstack_temp.find_element_by_name("q")
  elem.send_keys("BrowserStack")
  elem.submit()
  browserstack_temp.quit()

def test_driver_no_creds():
  with pytest.raises(nte.NebulaTestException):
    del os.environ['BROWSERSTACK_USER']
    del os.environ['BROWSERSTACK_KEY']
    browserstack.get_driver({})