import pytest

from nebula.modules.common.compare.compare_factory import CompareFactory
from nebula.modules.common.operand_enum import Operand
from nebula.modules.common.operator_enum import Operator
from nebula.modules.report import invalid_test_exception as ite
from nebula.modules.report import nebula_test_exception as nte


class TestCompareFactory(object):
    compare_factory = CompareFactory()

    # Numbers / ints
    def test_compare_ints_equals(self):
        int_compare = self.compare_factory.compare(5, 5, "=")
        assert int_compare is True

    def test_compare_ints_unequals(self):
        int_float_compare = self.compare_factory.compare(7, 6, "!=")
        assert int_float_compare is True

    def test_compare_ints_greater_than(self):
        int_compare = self.compare_factory.compare(10, 5, "greater than")
        assert int_compare is True

    def test_compare_ints_less_than(self):
        int_compare = self.compare_factory.compare(5, 10, "<")
        assert int_compare is True

    # Floats
    def test_compare_int_float_equals(self):
        int_float_compare = self.compare_factory.compare(5, 5.0, "==")
        assert int_float_compare is True

    def test_compare_int_float_greater_than(self):
        int_compare = self.compare_factory.compare(10, 5.4823525636, "greater than")
        assert int_compare is True

    def test_compare_int_float_less_than(self):
        int_compare = self.compare_factory.compare(5, 10.5669169211659, "<")
        assert int_compare is True

    def test_compare_float_int_unequals(self):
        int_float_compare = self.compare_factory.compare(7.5, 6, "!=")
        assert int_float_compare is True

    def test_compare_float_int_greater_than(self):
        with pytest.raises(nte.NebulaTestException):
            int_float_compare = self.compare_factory.compare(5.0, 5, ">")

    def test_compare_float_int_less_than(self):
        with pytest.raises(nte.NebulaTestException):
            int_float_compare = self.compare_factory.compare(8.98765, 4, "<")

    # String
    def test_compare_int_string_equals(self):
        int_string_compare = self.compare_factory.compare(9, "9", "equal to")
        assert int_string_compare is True

    def test_compare_string_int_equals(self):
        int_string_compare = self.compare_factory.compare("9", 9, "equal to")
        assert int_string_compare is True

    def test_compare_string_string_greater_than(self):
        string_string_compare = self.compare_factory.compare("9.75", "8.25", "greater than")
        assert string_string_compare is True

    def test_compare_string_float_greater_than(self):
        string_string_compare = self.compare_factory.compare("9.75", 8.25, "greater than")
        assert string_string_compare is True

    def test_compare_string_string_less_than(self):
        with pytest.raises(nte.NebulaTestException):
            string_string_compare = self.compare_factory.compare("9", "8.25", "less than")

    def test_compare_string_float_less_than(self):
        with pytest.raises(nte.NebulaTestException):
            string_string_compare = self.compare_factory.compare("9", 8.25, "less than")

    def test_compare_float_string_not_equals(self):
        string_string_compare = self.compare_factory.compare(9.5, "3.75", "not equal to")
        assert string_string_compare is True

    def test_compare_string_float_not_equals(self):
        string_string_compare = self.compare_factory.compare("9.5", 3.75, "not equal to")
        assert string_string_compare is True

    def test_compare_string_int_not_equals(self):
        string_string_compare = self.compare_factory.compare("9.5", 3, "not equal to")
        assert string_string_compare is True

    def test_compare_string_string_equals(self):
        string_string_compare = self.compare_factory.compare("101.25", "101.25", "==")
        assert string_string_compare is True

    def test_compare_string_regex(self):
        string_regex_compare = self.compare_factory.compare("1:52:38 pm", "(1*[0-9]):([0-5][0-9]):([0-5][0-9]) ([AaPp]*[Mm]*)", "matches")
        assert string_regex_compare is True

    # List
    def test_compare_list_length_equals(self):
        list_length_equals = self.compare_factory.compare([10, 20, 30, 40, 50], 5, "length ==")
        assert list_length_equals is True

    def test_compare_list_length_greater_than(self):
        list_length_greater_than = self.compare_factory.compare([10, 20, 30, 40, 50], 3, "length >")
        assert list_length_greater_than is True

    def test_compare_list_length_less_than(self):
        list_length_less_than = self.compare_factory.compare([10, 20, 30, 40, 50], "10", "length <")
        assert list_length_less_than is True

    def test_compare_list_length_not_equals(self):
        list_length_not_equals = self.compare_factory.compare([10, 20, 30, 40, 50], "12", "!=")
        assert list_length_not_equals is True

    def test_compare_list_contains(self):
        list_contains = self.compare_factory.compare([10, 20, 30, 40, 50], 10, "has")
        assert list_contains is True

    def test_compare_list_contains_string(self):
        list_contains = self.compare_factory.compare([10, 20, 30, 40, 50], "10", "has")
        assert list_contains is True

    def test_compare_list_contains_float(self):
        list_contains = self.compare_factory.compare(["10.5","11.75","5.75","8.9765","2.45"], 10.5, "has")
        assert list_contains is True

    def test_compare_list_contains_str(self):
        list_contains = self.compare_factory.compare(["10.5","11.75","5.75","8.9765","2.45"], "11.750", "has")
        assert list_contains is True

    def test_compare_list_contains_int(self):
        list_contains = self.compare_factory.compare(["10","11","5","8","2"], 10, "has")
        assert list_contains is True

    def test_compare_list_contains_str_int(self):
        list_contains = self.compare_factory.compare(["10","11","5","8","2"], "10", "has")
        assert list_contains is True

    def test_compare_list_does_not_contain(self):
        with pytest.raises(nte.NebulaTestException):
            list_does_not_contain = self.compare_factory.compare(["10","11","5","8","2"], 10, "does not have")

    def test_compare_list_does_not_contain_str(self):
        list_does_not_contain = self.compare_factory.compare([10, 20, 30, 40, 50], "10", "does not have")
        assert list_does_not_contain is False

    def test_compare_list_does_not_contain_float(self):
        list_does_not_contain = self.compare_factory.compare(["10.5","11.75","5.75","8.9765","2.45"], 11.25, "does not have")
        assert list_does_not_contain is True

    def test_compare_list_does_not_contain_int(self):
        list_does_not_contain = self.compare_factory.compare(["10","11.75","5","8.9765","2"], 11, "does not have")
        assert list_does_not_contain is True

    def test_compare_list_does_not_contain_str(self):
        with pytest.raises(nte.NebulaTestException):
            list_does_not_contain = self.compare_factory.compare([10, 20, 30, 40, 50], "10", "does not have")

    # Lists
    def test_compare_lists_length_equals(self):
        list_length_equals = self.compare_factory.compare([10, 20, 30, 40, 50], [10, 20, 30, 40, 50], "size")
        assert list_length_equals is True

    def test_compare_lists_length_equals_negative(self):
        with pytest.raises(nte.NebulaTestException):
            lists_not_equals = self.compare_factory.compare([10, 20, 30, 40, 50], [20, 30, 40, 50], "size")

    def test_compare_lists_equals(self):
        list_length_equals = self.compare_factory.compare([10, 20, 30, 40, 50],[10, 20, 30, 40, 50], "=")
        assert list_length_equals is True

    def test_compare_lists_equals(self):
        list_length_equals = self.compare_factory.compare([10, 20, 30, 40, 50],[10, 40, 30, 20, 50], "=")
        assert list_length_equals is True

    def test_compare_lists_equals_negative(self):
        with pytest.raises(nte.NebulaTestException):
            list_length_not_equals = self.compare_factory.compare([10, 20, 30, 40, 50],[10, 30, 40, 50], "=")

    def test_compare_lists_contains(self):
        list_contains = self.compare_factory.compare([10, 20, 30, 40, 50], [10, 20, 30, 40, 50], "contains")
        assert list_contains is True

    def test_compare_lists_contains_different_index(self):
        list_contains = self.compare_factory.compare([10, 20, 40, 30, 50], [50, 20, 30, 40, 10], "contains")
        assert list_contains is True

    def test_compare_lists_contains_different_index_expected(self):
        list_contains = self.compare_factory.compare([10, 20, 40, 30, 50], [50, 40, 30, 10, 20], "contains")
        assert list_contains is True

    def test_compare_lists_contains_negative(self):
        with pytest.raises(nte.NebulaTestException):
            list_contains = self.compare_factory.compare([11, 20, 30, 40, 50], [10, 20, 30, 40, 50], "contains")

    def test_compare_lists_does_not_contains(self):
        list_not_contains = self.compare_factory.compare([11, 20, 30, 40, 50], [10, 20, 30, 40, 50], "does not contain")
        assert list_not_contains is True

    def test_compare_lists_does_not_contains_missing(self):
        list_not_contains = self.compare_factory.compare([10, 20, 30, 40, 50], [10, 20, 33, 40, 50], "does not contain")
        assert list_not_contains is True

    def test_compare_lists_does_not_contains_diff_index(self):
        with pytest.raises(nte.NebulaTestException):
            list_not_contains = self.compare_factory.compare([10, 20], [10, 20, 30, 40, 50], "does not contain")

    # List Sort
    def test_compare_list_sort_asc_dates(self):
        list_sort_dates = self.compare_factory.compare(['2014/24/02', '31-12-2015', '2016/04/23', '2017-08-23', '03/23/2018'], "ASC", "sort")
        assert list_sort_dates is True
    
    def test_compare_list_not_sort_asc_dates(self):
        with pytest.raises(nte.NebulaTestException):
            list_sort_dates = self.compare_factory.compare(['2014/24/02', '2017-08-23', '31-12-2015', '2016/04/23', '03/23/2018'], "ASC", "sort")
    
    def test_compare_list_not_sort_desc_dates(self):
        with pytest.raises(nte.NebulaTestException):
            list_sort_dates = self.compare_factory.compare(['2014/24/02', '2017-08-23', '31-12-2015', '2016/04/23', '03/23/2018'], "DESC", "sort")
    
    def test_compare_list_sort_desc_dates(self):
        list_sort_dates = self.compare_factory.compare(['03/23/2018', '2017-08-23', '2016/04/23', '31-12-2015', '2014/24/02'], "DESC", "sort")
        assert list_sort_dates is True
    
    def test_compare_list_not_sort_asc_IP_Addresses(self):
        with pytest.raises(nte.NebulaTestException):
            list_sort_ip = self.compare_factory.compare(['192.168.102.105', '192.168.204.111', '192.168.99.11', '164.225.184.65', '87.23.12.45'], "ASC", "sort")
    
    def test_compare_list_sort_asc_IP_Addresses(self):
        list_sort_ip = self.compare_factory.compare(['87.23.12.45', '164.225.184.65', '192.168.99.11', '192.168.102.105', '192.168.204.45'], "ASC", "sort")
        assert list_sort_ip is True
    
    def test_compare_list_sort_asc_IP_Addresses_2(self):
        list_sort_ip = self.compare_factory.compare(['87.23.12.45', '164.225.184.65', '192.168.99.105', '192.168.124.15', '224.186.204.45'], "ASC", "sort")
        assert list_sort_ip is True
    
    def test_compare_list_not_sort_desc_IP_Addresses(self):
        with pytest.raises(nte.NebulaTestException):
            list_sort_ip = self.compare_factory.compare(['87.23.12.45', '192.168.99.11', '164.225.184.65', '192.168.204.45', '192.168.102.105'], "DESC", "sort")
    
    def test_compare_list_sort_desc_IP_Addresses(self):
        list_sort_ip = self.compare_factory.compare(['207.23.12.45', '192.168.204.45', '192.168.102.105', '183.166.145.247', '164.225.184.65'], "DESC", "sort")
        assert list_sort_ip is True
    
    def test_compare_list_not_sort_IP_Addresses(self):
        with pytest.raises(nte.NebulaTestException):
            list_sort_alphanumeric = self.compare_factory.compare(['Random String', 'New String', 'String list', 'Alphanumeric'], "ASC", "sort")
    
    def test_compare_list_not_sort_numbers(self):
        with pytest.raises(nte.NebulaTestException):
            list_sort_numbers = self.compare_factory.compare(["10","11.75","5","8.9765","2"], "ASC", "sort")
    
    def test_compare_list_sort_numbers(self):
        list_sort_numbers = self.compare_factory.compare(["4","12.4525","48","25965","30000"], "ASC", "sort")
        assert list_sort_numbers is True

    def test_compare_list_default(self):
        list_sort_default = self.compare_factory.compare(["Apple","Ball","Cat","Dog","Elephant"], "ASC", "sort")
    
    def test_compare_incompatible_types(self):
        with pytest.raises(ite.InvalidTestException):
            int_dict_compare = self.compare_factory.compare(45, {"Key": "Value"}, "=")
    
    # API
    def test_compare_api_string_not_equals_int_positive(self):
        api_not_equals = self.compare_factory.compare("422", 200, "status is not")
        assert api_not_equals is True

    def test_compare_api_string_not_equals_int_negative(self):
        with pytest.raises(nte.NebulaTestException):
            api_not_equals = self.compare_factory.compare("200", 200, "status is not")

    def test_compare_api_int_not_equals_string_positive(self):
        api_not_equals = self.compare_factory.compare(422, "200", "status is not")
        assert api_not_equals is True

    def test_compare_api_int_not_equals_string_negative(self):
        with pytest.raises(nte.NebulaTestException):
            api_not_equals = self.compare_factory.compare(200, "200", "status is not")

    def test_compare_api_string_equals_int_negative(self):
        with pytest.raises(nte.NebulaTestException):
            api_equals = self.compare_factory.compare("422", 200, "status is")

    def test_compare_api_int_equals_string_negative(self):
        with pytest.raises(nte.NebulaTestException):
            api_equals = self.compare_factory.compare(422, "200", "status is")

    def test_compare_api_string_equals_int_positive(self):
        api_equals = self.compare_factory.compare("200", 200, "status is")
        assert api_equals is True

    def test_compare_api_int_equals_string_positive(self):
        api_equals = self.compare_factory.compare(200, "200", "status is")
        assert api_equals is True
