#!/bin/bash

echo "Installing dependencies"
bpwd=$(pwd)
yum install -y curl git epel-release perl
yum install -y python3 python36-pip python2-pip
python3 -m pip install wheel pyinstaller
python3 -m pip install --upgrade pip
pythonver=$(python3 -V |  awk -F '.' '{ print$1"."$2 }' | sed s'/P/p/g' | sed s'/ //g')

python3 -m pip install -r $bpwd/resources/dev.requirements.txt
python3 -m snips_nlu download en
python -m snips_nlu download en

echo "Modifying python files"
#sudo vim /usr/local/lib/python3.6/site-packages/snips_nlu/slot_filler/crf_slot_filler.py
crf_slot_filler=$(find / -name crf_slot_filler.py | grep $pythonver)
perl -i -0p -e 's/    def __del__\(self\):\n        if self.crf_model is None or self.crf_model.modelfile.name is None:\n            return\n        try:\n            Path\(self.crf_model.modelfile.name\).unlink\(\)\n        except OSError as e:\n            logger.warning\("Unable to remove CRF model file at path '\''%s'\'': %s",\n                           self.crf_model.modelfile.name, repr\(e\)\)/    def __del__\(self\):\n            if self.crf_model is None or self.crf_model.modelfile.name is None:\n                return\n            try:\n                Path\(self.crf_model.modelfile.name\).unlink\(\)\n            except TypeError:\n                pass\n            except OSError as e:\n                logger.warning\("Unable to remove CRF model file at path '\''%s'\'': %s",\n                               self.crf_model.modelfile.name, repr\(e\)\)/' $crf_slot_filler
#sudo vim /usr/local/lib64/python3.6/site-packages/snips_nlu_parsers/utils.py
parsersutil=$(find / -name "utils.py" | grep "$pythonver/site-packages/snips_nlu_parsers")
perl -i -0p -e 's/import shutil\nfrom _ctypes import POINTER, Structure, byref\nfrom contextlib import contextmanager\nfrom ctypes import c_char_p, c_int32, cdll, string_at\nfrom pathlib import Path\nfrom tempfile import mkdtemp\n\nPACKAGE_PATH = Path\(__file__\).absolute\(\).parent\n\ndylib_dir = PACKAGE_PATH \/ "dylib"\ndylib_path = list\(dylib_dir.glob\("libsnips_nlu_parsers_rs\*"\)\)\[0\]\nlib = cdll.LoadLibrary\(str\(dylib_path\)\)/import shutil\nimport sys\nfrom _ctypes import POINTER, Structure, byref\nfrom contextlib import contextmanager\nfrom ctypes import c_char_p, c_int32, cdll, string_at\nfrom pathlib import Path\nfrom tempfile import mkdtemp\n\ntry:\n    PACKAGE_PATH = Path\(sys.MEIPASS\)\nexcept AttributeError:\n    PACKAGE_PATH = Path\(__file__\).absolute\(\).parent\n\ndylib_dir = PACKAGE_PATH \/ "dylib"\ndylib_path = list\(dylib_dir.glob\("libsnips_nlu_parsers_rs\*"\)\)\[0\]\nlib = cdll.LoadLibrary\(str\(dylib_path\)\)/' $parsersutil
#sudo vim /usr/local/lib64/python3.6/site-packages/snips_nlu_utils/utils.py
utilsutil=$(find / -name "utils.py" | grep "$pythonver/site-packages/snips_nlu_utils")
perl -i -0p -e 's/from _ctypes import POINTER, Structure, byref\nfrom contextlib import contextmanager\nfrom ctypes import c_char_p, c_int, c_int32, cdll, string_at\nfrom pathlib import Path\nfrom tempfile import mkdtemp\n \nPACKAGE_PATH = Path\(__file__\).absolute\(\).parent\n\ndylib_dir = PACKAGE_PATH \/ "dylib"\ndylib_path = list\(dylib_dir.glob\("libsnips_nlu_utils_rs\*"\)\)\[0\]\nlib = cdll.LoadLibrary\(str\(dylib_path\)\)/import shutil\nimport sys\nfrom _ctypes import POINTER, Structure, byref\nfrom contextlib import contextmanager\nfrom ctypes import c_char_p, c_int, c_int32, cdll, string_at\nfrom pathlib import Path\nfrom tempfile import mkdtemp\n \ntry:\n    PACKAGE_PATH = Path\(sys._MEIPASS\)\nexcept AttributeError:\n    PACKAGE_PATH = Path\(__file__\).absolute\(\).parent\n\ndylib_dir = PACKAGE_PATH \/ "dylib"\ndylib_path = list\(dylib_dir.glob\("libsnips_nlu_utils_rs\*"\)\)\[0\]\nlib = cdll.LoadLibrary\(str\(dylib_path\)\)/' $utilsutil

#sudo vim $bpwd/hook-snips_nlu_parsers.py
linuxlibsnipsparsersdir=$(find / -name "snips_nlu_parsers" | grep "$pythonver")
linuxlibsnipsparsersname=$(ls $linuxlibsnipsparsersdir/dylib)
chmod +x $linuxlibsnipsparsersdir/dylib/$linuxlibsnipsparsersname
sed -i '/else:/,+1 d' $bpwd/hook-snips_nlu_parsers.py
printf "else:\n    binaries=[('$linuxlibsnipsparsersdir/dylib/$linuxlibsnipsparsersname', 'snips_nlu_parsers/dylib')]\n" >> $bpwd/hook-snips_nlu_parsers.py

#sudo vim $bpwd/hook-snips_nlu_utils.py
linuxlibsnipsutilsdir=$(find / -name "snips_nlu_utils" | grep "$pythonver")
linuxlibsnipsutilsname=$(ls $linuxlibsnipsutilsdir/dylib)
chmod +x $linuxlibsnipsutilsdir/dylib/$linuxlibsnipsutilsname
sed -i '/else:/,+1 d' $bpwd/hook-snips_nlu_utils.py
printf "else:\n    binaries=[('$linuxlibsnipsutilsdir/dylib/$linuxlibsnipsutilsname', 'snips_nlu_utils/dylib')]\n" >> $bpwd/hook-snips_nlu_utils.py

echo "Building Executable"
$bpwd/nebula/snips/train_engines -b $bpwd
python3 -m PyInstaller nebula_2.0.spec

#access_token=$(curl https://bitbucket.org/site/oauth2/access_token -d grant_type=client_credentials -u $key:$secret | jq '.access_token' | sed 's/"//g')
#git clone https://x-token-auth:$access_token@bitbucket.org/extron-electronics/nebula.git