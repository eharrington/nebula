import datetime
import json
import logging
import os

class PerformancePageHandler:
    def __init__(self, webdriver):
        self.start = datetime.datetime.now()

        self.time_records = []

        self.time_record_file = "timerecord.{0}.json".format(str(self.start.microsecond))

    def handle_page(self, urlFragment):

        end = datetime.datetime.now()
        diff = end - self.start

        # convert to milliseconds
        timeDiff = str(int(diff.microseconds / 1000))

        self._add_time_record(urlFragment, timeDiff)

        self.start = datetime.datetime.now()

    def _add_time_record(self, urlFragment, timeDiff):
        record = {
            "url": urlFragment,
            "timeToNavigate": timeDiff
        }
        self.time_records.append(record)

    def __del__(self):
        if not os.path.exists('./reports'):
            os.mkdir('./reports')

        try:
            with open('./reports/' + self.time_record_file, 'w') as file:
                json.dump(self.time_records, file)   
        except Exception as e:
            logging.info(f"Unable to create time record file\n{e}")
