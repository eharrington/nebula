import datetime
from lxml import etree
import requests
from nebula.modules.common import config_helper


class TestFileReport:
    def __init__(self, suite_name, browser_name):
        self._current_suite = etree.Element("testsuite", name=suite_name)
        self._current_suite.set("browser", browser_name)
        self._start_time = datetime.datetime.now()
        self._test_time = datetime.datetime.now()
        self._num_tests = 0
        self._num_failures = 0
        self._num_skips = 0
        self._total_time = 0
        self._nebula_version = ""

    """Pass, fail, or skip, this does some bookkeeping when moving from one test to the next
    """
    def _add_base_testcase(self, test_report, status):
        self._num_tests += 1
        test_time = datetime.datetime.now() - self._test_time
        test_time = test_time.total_seconds() # float
        self._test_time = datetime.datetime.now()
        if config_helper.get_tracking():
            # we post to the server and if it fails we just ignore it
            try:
                test_tracking_obj = {}
                test_tracking_obj['datetime'] = str(datetime.datetime.now())
                test_tracking_obj['hostname'] = test_report.hostname
                test_tracking_obj['testId'] = self._current_suite.attrib['name'] + '_' + test_report.name
                test_tracking_obj['status'] = status
                test_tracking_obj['devtime'] = test_report.devtime
                test_tracking_obj['version'] = test_report.version
                test_tracking_obj['runtime'] = test_time
                requests.post(f"{config_helper.get_intent_tracker()}/tests", json = test_tracking_obj)
            except:
                pass

        return etree.SubElement(self._current_suite, "testcase", name=test_report.name, devtime=str(test_report.devtime), time=str(test_time))

    def add_test_success(self, test_report):
        return self._add_base_testcase(test_report, "pass")

    def add_test_failure(self, test_report):
        self._num_failures += 1
        testnode = self._add_base_testcase(test_report, "fail")
        if test_report.intent_tracker.get('testLine') is not None:
            etree.SubElement(testnode, "test-line", message=str(test_report.intent_tracker['testLine']))
        etree.SubElement(testnode, "failure", message=str(test_report.message))
        etree.SubElement(testnode, "system-out", text = str(test_report.output))

    def add_test_skip(self, test_report):
        self._num_skips += 1
        testnode = self._add_base_testcase(test_report, "skip")
        etree.SubElement(testnode, "skip", message=str(test_report.message))

    def complete_testsuite(self, test_info):
        self._current_suite.set("tests", str(self._num_tests))
        self._current_suite.set("failures", str(self._num_failures))
        self._current_suite.set("skips", str(self._num_skips))
        total_time = datetime.datetime.now() - self._start_time
        total_time = total_time.total_seconds() # float
        self._current_suite.set("time", str(total_time))
        self._current_suite.set("start_timestamp", str(self._start_time))
        self._current_suite.set("end_timestamp", str(datetime.datetime.now()))
        self._nebula_version = test_info.version

        if config_helper.get_tracking():
            # we post to the server and if it fails we just ignore it
            try:
                report = self._current_suite
                suite_tracking_obj = {}
                suite_tracking_obj['datetime'] = str(datetime.datetime.now())
                suite_tracking_obj['version'] = test_info.version
                suite_tracking_obj['hostname'] = test_info.hostname
                suite_tracking_obj['testfile'] = report.attrib['name']
                suite_tracking_obj['time'] = report.attrib['time']
                suite_tracking_obj['tests'] = report.attrib['tests']
                suite_tracking_obj['failures'] = report.attrib['failures']
                suite_tracking_obj['skips'] = report.attrib['skips']
                suite_tracking_obj['status'] = "Failed" if test_info.test_has_failed else "Passed"
                requests.post(f"{config_helper.get_intent_tracker()}/suites", json = suite_tracking_obj)
            except:
                pass

    def get_report(self):
        return self._current_suite