class TestInfo:
    # none of these are going to change on a run so we have a class variable to set globally
    version = ""      #Nebula version being used
    hostname = ""


    def __init__(self):
        #suite global variables
        self.testfile = ""     #testfile name, aka testsuite
        self.test_has_failed = False  #used to track if testing should stop because one failed
        self.sessionId = ""    #tracks the browserstack session id

        #test specific variables
        self.name = ''    #name of a test
        self.message = '' #error message that caused test to fail
        self.output = ''  #any console logs or other error messages
        self.devtime = 0  #how much time developers indicate test took to write

        #intent specific variables
        self.intent_tracker = {} #used to track individual intents
