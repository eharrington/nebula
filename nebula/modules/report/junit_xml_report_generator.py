import datetime
import logging
import os

from lxml import etree
from nebula.modules.common.base_colors import bcolors


class JUnitXmlReportGenerator:
    def __init__(self):
        self._root = None
        self._report_location = ''
        self._num_tests = 0
        self._num_failures = 0
        self._num_skips = 0
        self._suite_total_time = 0
        self._clock_time = 0

    def start_new_report(self, report_location):
        self._report_location = report_location
        self._root = None
        self._root = etree.Element("testsuites")
        self._clock_time = datetime.datetime.now()

    def generate_report(self, test_file_name, testsuites):
        file_location = f"{os.getcwd()}{self._report_location.replace('.','')}/{test_file_name}"
        logging.info(f"Test report available at {file_location}")
        tree = etree.ElementTree(self._root)
        for test in testsuites:
            if test != None and test.get_report().get("tests") != None:
                report = test.get_report()
                self._root.append(report)

                if self._num_failures == 0 and int(report.get("failures")) > 0:
                    logging.info(f"{bcolors.BOLD}******************************************{bcolors.ENDC}")
                    logging.info(f"{bcolors.FAIL}Failed Test Suites\n{bcolors.ENDC}")

                self._num_tests += int(report.get("tests"))
                self._num_failures += int(report.get("failures"))
                self._num_skips += int(report.get("skips"))
                self._suite_total_time += float(report.get("time"))

                if(int(report.get("failures")) > 0):
                    test_name = str(report.get("name"))
                    logging.info(f"{bcolors.FAIL}X {test_name}{bcolors.ENDC}")

                self._root.set("nebula_version", str(test._nebula_version))

        self._root.set("total_tests", str(self._num_tests))
        self._root.set("total_failures", str(self._num_failures))
        self._root.set("total_skips", str(self._num_skips))
        self._root.set("total_time", str(self._suite_total_time))
        clock_time = datetime.datetime.now() - self._clock_time
        clock_time = clock_time.total_seconds()
        self._root.set("clock_time", str(clock_time))

        if not os.path.exists(self._report_location):
            os.makedirs(self._report_location)

        filePath = os.path.join(self._report_location, test_file_name)

        tree.write(filePath, pretty_print=True, xml_declaration=True, encoding="utf-8")

        # print end test summary
        logging.info(f"{bcolors.BOLD}******************************************{bcolors.ENDC}")
        logging.info(f"{bcolors.CYAN}{len(testsuites)} suites{bcolors.ENDC}, {bcolors.CYAN}{self._num_tests} tests{bcolors.ENDC},{bcolors.FAIL} {self._num_failures} failures{bcolors.ENDC},{bcolors.WARNING} {self._num_skips} skipped{bcolors.ENDC}")
        logging.info(f"{bcolors.BOLD}Finished in {clock_time} seconds{bcolors.ENDC}")
        logging.info(f"{bcolors.BOLD}******************************************{bcolors.ENDC}")

        return self._num_failures