import json
import requests
import logging
import cv2
import base64
import numpy as np
from nebula.modules.common import config_helper


def save_images(images, path, file_name, page_width, failure=False):
    url = config_helper.get_image_store() + '/images'

    # using a GUID at the moment for auth
    headers = {'Content-Type': 'application/json',
               'Accept': 'application/json'}
    data = {'path':path, 'file_name': file_name,
            'page_width': page_width, 'images': images}

    if not failure:
        try: 
            requests.post(url, data=json.dumps(data), headers=headers, auth=('07062f9b-159d-4b0f-b46a-c481d4fca2f3', ''))
        except requests.exceptions.ConnectionError:
            logging.info(f'Saving screenshot locally at {path}/{file_name}')
            save_locally(data, failure) 
    else:
        if 'output_files' not in file_name:
            data['file_name'] = './output_files/' + file_name + '.png'
        logging.info(f'Saving screenshot of failure at {file_name}')
        save_locally(data, failure)
                

    
    
def save_locally(data, failure):
    try:       
        images = data['images']
        path = data['path']
        file_name = data['file_name']
        page_width = data['page_width']

        save_image(images, path, file_name, page_width, failure)

    except json.decoder.JSONDecodeError as j:
        logging.warning('Unable to save screenshot locally: ' + str(j))
    except Exception as e:
        logging.warning('Unable to save screenshot locally: ' + str(e))

def save_image(images, path, file_name, page_width, failure):
    if failure: 
        imagePath = file_name
    else:
        imagePath = '.' + path + '/' + file_name + '.png'
            
    image = merge_images(images, path, page_width)

    try:
        cv2.imwrite(imagePath, image)
    except Exception as e:
        logging.error("Error trying to save image: " + str(e))

def merge_images(images, identifier, page_width):
    cropped_images = []
        
    for i in range(0, len(images)):
        image = images[i]['image']
        (y,h,x,w) = images[i]['crop_dims']
        
        img = decode_image(image)
                        
        # crop the image if it is not a full height
        try:
            img = img[int(y):int(h), int(x):int(w)]
        except Exception:
            logging.info('Unable to save screenshot locally.')
            pass
        
        cropped_images.append(img)           
        
    nparr = np.concatenate(cropped_images, axis=0)
        
    return nparr

def decode_image(img):
    decoded = base64.b64decode(img)
    np_array = np.frombuffer(decoded, dtype=np.uint8)
    return cv2.imdecode(np_array, cv2.IMREAD_COLOR)

def need_to_save(path, file_name):
    url = config_helper.get_image_store() + '/save'
    # using a GUID at the moment for auth
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    data = {'path':path, 'file_name': file_name}

    
    try: 
        r = requests.get(url, data=json.dumps(data), headers=headers, auth=('07062f9b-159d-4b0f-b46a-c481d4fca2f3', ''))
        if r.text is None or r.text == 'False':
            return False
        else:
            return True
    except requests.exceptions.ConnectionError:
            raise Exception("Unable to save image")
        
