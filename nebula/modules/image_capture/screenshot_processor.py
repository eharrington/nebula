import logging
import requests
import json
from nebula.modules.common import config_helper
from nebula.modules.common.base_colors import bcolors
from nebula.modules.image_capture import image_save


class ScreenshotProcessor:

    def __init__(self, driver):
        self._driver = driver

    def validToken(self, token):
        url = f'{config_helper.get_image_ui()}/validateToken'
        # using a GUID at the moment for auth
        headers = {'Content-Type': 'application/json',
                'Accept': 'application/json'}
        data = {'token':token}

        try:
            r = requests.post(url, data=json.dumps(data), headers=headers, timeout=5)
            if r.status_code is not 200:
                logging.warning(bcolors.WARNING + "Invalid token (-id) supplied. Images will be saved locally to output_files directory." + bcolors.ENDC)
                return False
            else:
                return True
        except (requests.Timeout, requests.ConnectionError):
                logging.warning(bcolors.WARNING + "The server was down or slow in responding. Images will be saved locally to output_files directory." + bcolors.ENDC)
                return False


    def process_page(self, path, file_name, failure=False):
        ''' Take screenshot of entire page and save
        @param: identifier - Unique identifier used to create folder to save image
        '''
        isValidToken = False
        user_token = config_helper.get_image_identifier()
        if user_token is not None and failure is False:
            path = user_token + '/' + path
            isValidToken = self.validToken(user_token)

        # check to see if we even need to screenshot this page
        # only check if we were given a valid token
        if isValidToken and failure is False:
            if not image_save.need_to_save(path, file_name):
                return

        images = []

         # execute the before scroll JavaScript
        page_load_js = config_helper.get_crawl_config_setting('page_load_javascript')
        if page_load_js is not None:
            try:
                self._driver.execute_script(page_load_js)
            except Exception as e:
                logging.warning(f"{bcolors.WARNING}Error running the page load Javascript.\n{e}{bcolors.ENDC}")

        # scroll to the top to start
        self._driver.execute_script("window.scrollTo(0, 0);")
        firstViewport = self._driver.execute_script("return document.documentElement.getBoundingClientRect();")

        scrollHeight = self._driver.execute_script("return document.documentElement.scrollHeight;")
        clientHeight = self._driver.execute_script("return document.documentElement.clientHeight;")

        turns = scrollHeight / clientHeight

        pageShot = self._screenshot_page()

        images.append({
            'image': pageShot,
            'crop_dims': (0,clientHeight,0,firstViewport['width'])
            })

        scrollPosition = clientHeight
        prevTop = 0

        if turns > 1:
            turns = int(turns)
            # execute before scroll JavaScript
            before_scroll_js = config_helper.get_crawl_config_setting('before_scroll_javascript')
            if before_scroll_js is not None:
                try:
                    self._driver.execute_script(before_scroll_js)
                except Exception as e:
                    logging.warning(f"{bcolors.WARNING}Error running the before scroll Javascript.\n{e}{bcolors.ENDC}")

            for i in range(0, turns):
                self._driver.execute_script("window.scrollTo(0, {0});".format(str(scrollPosition)))

                subShot = self._screenshot_page()

                boundRect = self._driver.execute_script("return document.documentElement.getBoundingClientRect();")

                # crop the image if it is not a full height
                top = boundRect['top'] * -1
                scrollPosition += clientHeight
                if top % clientHeight > 0:
                    cropTop = int((clientHeight * i) + boundRect['bottom'])
                    images.append({
                        'image': subShot,
                        'crop_dims': (cropTop,clientHeight,0,int(firstViewport['width']))
                        })
                else:
                    images.append({
                        'image': subShot,
                        'crop_dims': (0,clientHeight,0,int(firstViewport['width']))
                        })

                prevTop = top

        # execute page reset JavaScript
        page_reset_js = config_helper.get_crawl_config_setting('page_reset_javascript')
        if page_reset_js is not None:
            try:
                self._driver.execute_script(page_reset_js)
            except Exception as e:
                logging.warning(f"{bcolors.WARNING}Error running the page reset Javascript.\n{e}{bcolors.ENDC}")
        # save to nebula-UI if we were given a valid token (if it's a failure screenshot we want to save to output_files instead)
        if isValidToken and failure is False:
            image_save.save_images(images, path, file_name, int(firstViewport['width']))
        # otherwise just save the images locally
        else:
            path = '/output_files'
            image_save.save_images(images, path, file_name, int(firstViewport['width']), failure=True)

    def _screenshot_page(self):
        '''Screenshots the current viewport of the page
        @returns: base 64 image
        '''
        return self._driver.get_screenshot_as_base64()
