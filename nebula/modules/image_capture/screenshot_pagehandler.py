import configparser
import logging
import re
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import ui
from selenium.webdriver.support import expected_conditions as EC
from nebula.modules.image_capture import screenshot_processor

class ScreenshotPageHandler:
    def __init__(self, webdriver):
        self._driver = webdriver

        # create screenshot processor
        self._screenshot_processor = screenshot_processor.ScreenshotProcessor(webdriver)

        self._wait = ui.WebDriverWait(self._driver, 5)

    def handle_page(self, urlFragment, old_page):
        ''' Screenshot the page and all elements
        @param: url - The url fragment of the page
        '''
        # wait until the dom loads
        try:
            self._wait.until(EC.staleness_of(old_page))
        except:
            pass

        logging.info(f"Processing page: {urlFragment}")
        path=''
        # take snapshot of entire page and save it first.
        self._screenshot_processor.process_page(path, urlFragment)

    def _get_clean_url_for_folder_name(self, url):
        '''Remove special URL characters so that we can create a proper folder name
        @param: url - URL to clean
        '''
        url = url.replace('?', '/')
        return re.sub(r'([\=@&]|%20)+', '', url)

    def _handle_element_with_no_size(self, element):
        ''' Handle page elements which have no size. Some need to be screenshotted. An example is a anchor tag with an image inside of it.
        @param: element - Element on the page to handle
        '''
        if element is None:
            return
        # if element has no size try to get an inner element. Happens for <a> especially
        if element.size['width'] == 0 or element.size['height'] == 0:
            try:
                subElement = element.find_element_by_css_selector("*")
                return self._handle_element_with_no_size(subElement)
            except Exception:
                return None
        else:
            return element