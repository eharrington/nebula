
import logging
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.remote.webdriver import WebDriver
from nebula.modules.report import nebula_test_exception as nte
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def get_driver(desired_cap):
  '''Allows for the created BrowserStack WebDriver
  @param: desired_cap - Device characteristics to run your tests against
  @returns WebDriver object
  '''
  #Browserstack Credentials
  remote_user = os.environ.get('BROWSERSTACK_USER')
  if remote_user == None:
      raise nte.NebulaTestException("Please Define Environment Variable BROWSERSTACK_USER to use remote browsers")
  remote_key = os.environ.get('BROWSERSTACK_KEY')
  if remote_key == None:
      raise nte.NebulaTestException("Please Define Environment Variable BROWSERSTACK_KEY to use remote browsers")

  return webdriver.Remote(
                command_executor=f'http://{remote_user}:{remote_key}@hub.browserstack.com:80/wd/hub',
                desired_capabilities=desired_cap)