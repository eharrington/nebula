from selenium import webdriver
import os
from nebula.modules.report import nebula_test_exception as nte
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def get_driver(desired_cap):
  '''Allows for the created Saucelabs WebDriver
  @param: remote_url - url for the sauce labs data center
  @param: desired_cap - Device characteristics to run your tests against
  @returns WebDriver object
  '''

  # Saucelabs Credentials
  remote_url = os.environ.get('SAUCE_ENDPOINT')
  # set to the default remote_url if none is provided.
  if remote_url == None:
      remote_url = "https://ondemand.saucelabs.com/wd/hub"
  remote_user = os.environ.get('SAUCE_USERNAME')
  if remote_user == None:
      raise nte.NebulaTestException("Please Define Environment Variable SAUCE_USERNAME to use remote browsers")
  remote_key = os.environ.get('SAUCE_ACCESS_KEY')
  if remote_key == None:
      raise nte.NebulaTestException("Please Define Environment Variable SAUCE_ACCESS_KEY to use remote browsers")

  insert_text = "://"
  i = remote_url.find(insert_text)
  # we are going to insert the username and access_key to the remote url
  remote_url = remote_url[:i + len(insert_text)] + remote_user + ":" + remote_key + "@" + remote_url[i + len(insert_text):]

  return webdriver.Remote(command_executor=remote_url, desired_capabilities=desired_cap)