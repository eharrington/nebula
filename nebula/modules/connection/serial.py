
import io
import logging
import serial
from serial import Serial, rs485
from serial.tools import list_ports
from nebula.modules.common.base_colors import bcolors
class SerialConn:

    def __init__(self, baudrate=9600, rs485_mode=False, port=None, url=None, timeout=1, do_not_open=False):
        '''Allows for the flexibility to create a Serial connection in different manners. Constructor needs at least either a port or url.
        @param: baudrate - Integer representing the speed of the connection
        @param: rs485_mode - Boolean to allow this mode
        @param: port - String representing the port of the connection
        @param: url - String representing the URL of the connection
        @param: timeout - Integer representing the amount of seconds before timeout
        @param: do_not_open - Boolean to allow open or not
        @returns object
        '''
        if port is not None:
            # Native port
            self.port = Serial(port, baudrate, timeout=timeout) # open serial port
            if rs485_mode:
                self.port.rs485_mode = rs485.RS485Settings()
        elif url is not None:
            self.port = serial.serial_for_url(url, timeout=timeout, do_not_open=do_not_open) # open serial port
        else:
            logging.warning(bcolors.WARNING + "No serial connection was made. Please provide a port or a url." + bcolors.ENDC)
            logging.warning(f"{bcolors.WARNING}Here are a list of available ports: {self.list_ports()}{bcolors.ENDC}")
            return None
        logging.verbose(f"Port {self.port.name} connection is open.")

    def send(self, msg):
        '''Allows for the writing data on the open serial connection. Thread safe writing.
        @param: msg - data in bytes to write
        '''
        self.port.write(msg.encode())

    def readLine(self):
        '''Read and return one line from the stream. Returns Bytes read from the port.
        @returns bytes
        '''
        return self.port.readline()

    def readLines(self, lines=1):
        '''Read and returns a specified number of lines from the stream. Returns Bytes read from the port.
        @param: msg - data in bytes to write
        @returns bytes
        '''
        return self.port.readlines(lines)

    def read(self, num_of_bytes=1):
        '''Reads from the open stream the specified number of bytes. Returns Bytes read from the port.
        @param:	size – Number of bytes to read.
        @returns bytes
        '''
        return self.port.read(size=num_of_bytes)

    def close(self):
        '''Closes the connection of the port.
        '''
        self.port.close()

    def open(self):
        self.port.open()

    def name(self):
        return self.port.name

    def instance(self):
        return self.port

    @staticmethod
    def list_ports():
        '''Reads list of available ports.
        @returns object
        '''
        connected = []
        comlist = list_ports.comports()
        for element in comlist:
            connected.append(element.device)
        return connected