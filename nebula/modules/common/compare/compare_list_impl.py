import re

from nebula.modules.common.operand_enum import Operand
from nebula.modules.common.operator_enum import Operator
from nebula.modules.report import nebula_test_exception as nte


def compare_lists_length(list_var1, list_var2):
    if len(list_var1) == len(list_var2): return True
    else: raise nte.NebulaTestException("The length of lists provided was not equal")

# Exact order, '==' works
def compare_lists_equals(list_var1, list_var2):
    if sorted(list_var1) == sorted(list_var2): return True
    else: raise nte.NebulaTestException("The lists provided are not equal")

def compare_lists_not_equals(list_var1, list_var2):
    if sorted(list_var1) != sorted(list_var2): return True
    else: raise nte.NebulaTestException("The lists provided are equal when they should not be")

# list1 is a subset of list
def compare_lists_contains(list_var1, list_var2):
    if all(x in list_var1 for x in list_var2): return True
    else: raise nte.NebulaTestException("List is not a subset of other list")

def compare_list_str_filter(list_var, regex_str):
    return_list = []
    for element in list_var:
        regex_match = re.search(regex_str, element)
        if regex_match:
            return_list.append(regex_match.group(1))
        else:
            raise nte.NebulaTestException(f"The filter {regex_str} did not result in any match for element {element} in the list")
    
    return return_list

# list1 is not subset of list_var2
def compare_lists_does_not_contain(list_var1, list_var2):
    if not (set(list_var1) <= set(list_var2)): return True
    else: raise nte.NebulaTestException("List is a subset of other list when it should not be")

points = [
    (Operand.LIST, Operand.LIST, Operator.EQUALS, lambda x, y: compare_lists_equals(x, y)),
    (Operand.LIST, Operand.LIST, Operator.LENGTH_EQUALS, lambda x, y: compare_lists_length(x, y)),
    (Operand.LIST, Operand.LIST, Operator.CONTAINS, lambda x, y: compare_lists_contains(x, y)),
    (Operand.LIST, Operand.LIST, Operator.DOES_NOT_CONTAIN, lambda x, y: compare_lists_does_not_contain(x, y)),
    (Operand.LIST, Operand.LIST, Operator.NOT_EQUALS, lambda x, y: compare_lists_not_equals(x, y)),
    (Operand.LIST, Operand.STR, Operator.FILTER, lambda x, y: compare_list_str_filter(x, y))
]

def get_points():
    return points
