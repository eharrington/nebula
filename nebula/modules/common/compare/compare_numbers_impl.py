from nebula.modules.common.operand_enum import Operand
from nebula.modules.common.operator_enum import Operator
from nebula.modules.report import nebula_test_exception as nte


def compare_numbers_equals(actual, expected):
    if actual == expected: return True
    else:
        raise nte.NebulaTestException(f"The values were not equal when they should have been. Expected: {expected}, Actual: {actual}")

def compare_numbers_greater_than(actual, expected):
    if actual > expected: return True
    else:
        raise nte.NebulaTestException(f"The actual value was not greater than expected. Expected: {expected}, Actual: {actual}")

def compare_numbers_less_than(actual, expected):
    if actual < expected: return True
    else:
        raise nte.NebulaTestException(f"The actual value was not less than expected. Expected: {expected}, Actual: {actual}")

def compare_numbers_not_equals(actual, expected):
    if actual != expected: return True
    else:
        raise nte.NebulaTestException(f"The values were equal when they should not have been. Expected: {expected}, Actual: {actual}")

points = [
    (Operand.INT, Operand.INT, Operator.EQUALS, lambda x, y: compare_numbers_equals(x, y)),
    (Operand.INT, Operand.INT, Operator.GREATER_THAN, lambda x, y: compare_numbers_greater_than(x, y)),
    (Operand.INT, Operand.INT, Operator.LESS_THAN, lambda x, y: compare_numbers_less_than(x, y)),
    (Operand.INT, Operand.INT, Operator.NOT_EQUALS, lambda x, y: compare_numbers_not_equals(x, y)),
    (Operand.INT, Operand.FLOAT, Operator.EQUALS, lambda x, y: compare_numbers_equals(x, y)),
    (Operand.INT, Operand.FLOAT, Operator.GREATER_THAN, lambda x, y: compare_numbers_greater_than(x, y)),
    (Operand.INT, Operand.FLOAT, Operator.LESS_THAN, lambda x, y: compare_numbers_less_than(x, y)),
    (Operand.INT, Operand.FLOAT, Operator.NOT_EQUALS, lambda x, y: compare_numbers_not_equals(x, y)),
    (Operand.FLOAT, Operand.FLOAT, Operator.EQUALS, lambda x, y: compare_numbers_equals(x, y)),
    (Operand.FLOAT, Operand.FLOAT, Operator.GREATER_THAN, lambda x, y: compare_numbers_greater_than(x, y)),
    (Operand.FLOAT, Operand.FLOAT, Operator.LESS_THAN, lambda x, y: compare_numbers_less_than(x, y)),
    (Operand.FLOAT, Operand.FLOAT, Operator.NOT_EQUALS, lambda x, y: compare_numbers_not_equals(x, y)),
    (Operand.FLOAT, Operand.INT, Operator.EQUALS, lambda x, y: compare_numbers_equals(x, y)),
    (Operand.FLOAT, Operand.INT, Operator.GREATER_THAN, lambda x, y: compare_numbers_greater_than(x, y)),
    (Operand.FLOAT, Operand.INT, Operator.LESS_THAN, lambda x, y: compare_numbers_less_than(x, y)),
    (Operand.FLOAT, Operand.INT, Operator.NOT_EQUALS, lambda x, y: compare_numbers_not_equals(x, y)),
]

def get_points():
    return points
