import ipaddress
import logging

import dateparser
import fastjsonschema

from nebula.modules.common.operand_enum import Operand
from nebula.modules.common.operator_enum import Operator
from nebula.modules.report import invalid_test_exception as ite
from nebula.modules.report import nebula_test_exception as nte


def compare_list_primitive_length_equals(list_var, length_var):
    actual = len(list_var)
    if actual == float(length_var): return True
    else:
        raise nte.NebulaTestException(f"The length of list was not equal to expected value. Expected: {length_var}, Actual: {actual}")

def compare_list_primitive_length_greater_than(list_var, length_var):
    actual = len(list_var)
    if actual > float(length_var): return True
    else:
        raise nte.NebulaTestException(f"The length of list was not greater than expected value. Expected: {length_var}, Actual: {actual}")

def compare_list_primitive_length_less_than(list_var, length_var):
    actual = len(list_var)
    if actual < float(length_var): return True
    else:
        raise nte.NebulaTestException(f"The length of list was not less than expected value. Expected: {length_var}, Actual: {actual}")

def compare_list_primitive_length_not_equals(list_var, length_var):
    actual = len(list_var)
    if actual != float(length_var): return True
    else:
        raise nte.NebulaTestException(f"The length of list was equal to expected value when it should not have been. Expected: {length_var}, Actual: {actual}")

def compare_dict_str_does_not_contain(actual_dict, expected_key):
    if expected_key not in actual_dict: return True
    else: raise nte.NebulaTestException(f"The value: {expected_key} was in {actual_dict}.")

def compare_dict_str_contains(actual_dict, expected_key):
    if expected_key in actual_dict: return True
    else: raise nte.NebulaTestException(f"The value: {expected_key} was not in {actual_dict}.")

def compare_dict_dict_equals(actual_dict, expected_dict):
    if actual_dict == expected_dict: return True
    else: raise nte.NebulaTestException(f"The value: {actual_dict} was not equal to {expected_dict}.")

def compare_dict_dict_json_schema(actual_dict, expected_dict):
    try:
        fastjsonschema.validate(expected_dict, actual_dict)
    except fastjsonschema.JsonSchemaDefinitionException as jsde:
        raise ite.InvalidTestException(f"JSON Schema is invalid\n{jsde}")
    except fastjsonschema.JsonSchemaException as jse:
        raise nte.NebulaTestException(f"JSON Schema comparison failed.\nCompare:  {actual_dict}\n\nAgainst: {expected_dict}\n\n{jse}")
    except Exception as e:
        logging.debug(f"Problem occured while using the JSON Schema:\n\n{expected_dict}\n\nto validate\n\n{actual_dict}\n\n{e}")
        raise e

def compare_dict_primitive_length_equals(actual_dict, expected_length):
    actual_len = len(actual_dict)
    if actual_len == float(expected_length): return True
    else:
        raise nte.NebulaTestException(f"The length of collection was not equal to expected value. Expected: {expected_length}, Actual: {actual_len}")

def compare_dict_primitive_length_less_than(actual_dict, expected_length):
    actual_len = len(actual_dict)
    if actual_len < float(expected_length): return True
    else:
        raise nte.NebulaTestException(f"The length of collection was not less than expected value. Expected: {expected_length}, Actual: {actual_len}")

def compare_dict_primitive_length_greater_than(actual_dict, expected_length):
    actual_len = len(actual_dict)
    if actual_len > float(expected_length): return True
    else:
        raise nte.NebulaTestException(f"The length of collection was not greater than expected value. Expected: {expected_length}, Actual: {actual_len}")

def compare_list_contains(list_var, expected):
    if expected in list_var: return True
    else:
        try:
            float_expected = float(expected)
            for i in range(0, len(list_var)):
                float_list_var = float(list_var[i])
                if float_expected == float_list_var:
                    return True
            raise nte.NebulaTestException(f"The value: {expected} was not in the list.")
        except:
            raise nte.NebulaTestException(f"The value: {expected} was not in the list.")

def compare_list_does_not_contain(list_var, expected):
    str_expected = str(expected)
    for i in range(0, len(list_var)):
        str_list_var = str(list_var[i])
        if str_expected == str_list_var:
            raise nte.NebulaTestException(f"The value: {expected} was in the list.")

    return True

def compare_list_verify_sort(list_var, sort_order):
    compare_list = check_IP_Address(list_var)
    if compare_list is None:
        compare_list = check_numbers(list_var)
    if compare_list is None:
        compare_list = check_dates(list_var)
    if compare_list is None:
        logging.debug("Verifying lexicographical sort order for the list")
        compare_list = list_var
    
    if sort_order == 'ASC':
        if sorted(compare_list) == compare_list: return True
        else: raise nte.NebulaTestException(f"The collection is not in {sort_order} order")
    if sort_order == 'DESC':
        if sorted(compare_list, reverse=True) == compare_list: return True
        else: raise nte.NebulaTestException(f"The collection is not in {sort_order} order")
    
def check_IP_Address(list_var):
    try:
        ip_list = [ipaddress.ip_address(element) for element in list_var]
    except ValueError:
        logging.debug("Could not find a list of valid IP Addresses")
        return None
    return ip_list

def check_dates(list_var):
    dates_list = [dateparser.parse(element, settings={'STRICT_PARSING': True}) for element in list_var]
    if None in dates_list:
        logging.debug("Could not find a list of valid dates")
        return None
    return dates_list

def check_numbers(list_var):
    try:
        numbers_list = [float(element) for element in list_var]
    except ValueError:
        logging.debug("Could not find a list of valid numbers")
        return None
    return numbers_list


points = [
    (Operand.LIST, Operand.INT, Operator.LENGTH_EQUALS, lambda x, y: compare_list_primitive_length_equals(x, y)),
    (Operand.LIST, Operand.INT, Operator.LENGTH_GREATER_THAN, lambda x, y: compare_list_primitive_length_greater_than(x, y)),
    (Operand.LIST, Operand.INT, Operator.LENGTH_LESS_THAN, lambda x, y: compare_list_primitive_length_less_than(x, y)),
    (Operand.LIST, Operand.INT, Operator.NOT_EQUALS, lambda x, y: compare_list_primitive_length_not_equals(x, y)),
    (Operand.LIST, Operand.FLOAT, Operator.LENGTH_EQUALS, lambda x, y: compare_list_primitive_length_equals(x, y)),
    (Operand.LIST, Operand.FLOAT, Operator.LENGTH_GREATER_THAN, lambda x, y: compare_list_primitive_length_greater_than(x, y)),
    (Operand.LIST, Operand.FLOAT, Operator.LENGTH_LESS_THAN, lambda x, y: compare_list_primitive_length_less_than(x, y)),
    (Operand.LIST, Operand.FLOAT, Operator.NOT_EQUALS, lambda x, y: compare_list_primitive_length_not_equals(x, y)),
    (Operand.LIST, Operand.STR, Operator.LENGTH_EQUALS, lambda x, y: compare_list_primitive_length_equals(x, y)),
    (Operand.LIST, Operand.STR, Operator.LENGTH_GREATER_THAN, lambda x, y: compare_list_primitive_length_greater_than(x, y)),
    (Operand.LIST, Operand.STR, Operator.LENGTH_LESS_THAN, lambda x, y: compare_list_primitive_length_less_than(x, y)),
    (Operand.LIST, Operand.STR, Operator.NOT_EQUALS, lambda x, y: compare_list_primitive_length_not_equals(x, y)),
    (Operand.LIST, Operand.STR, Operator.CONTAINS, lambda x, y: compare_list_contains(x, y)),
    (Operand.LIST, Operand.INT, Operator.CONTAINS, lambda x, y: compare_list_contains(x, y)),
    (Operand.LIST, Operand.FLOAT, Operator.CONTAINS, lambda x, y: compare_list_contains(x, y)),
    (Operand.LIST, Operand.STR, Operator.DOES_NOT_CONTAIN, lambda x, y: compare_list_does_not_contain(x, y)),
    (Operand.LIST, Operand.INT, Operator.DOES_NOT_CONTAIN, lambda x, y: compare_list_does_not_contain(x, y)),
    (Operand.LIST, Operand.FLOAT, Operator.DOES_NOT_CONTAIN, lambda x, y: compare_list_does_not_contain(x, y)),
    (Operand.LIST, Operand.STR, Operator.SORT, lambda x, y: compare_list_verify_sort(x, y)),
    (Operand.DICT, Operand.STR, Operator.DOES_NOT_CONTAIN, lambda x, y: compare_dict_str_does_not_contain(x, y)),
    (Operand.DICT, Operand.STR, Operator.CONTAINS, lambda x, y: compare_dict_str_contains(x, y)),
    (Operand.DICT, Operand.DICT, Operator.JSON_SCHEMA_COMPARE, lambda x, y: compare_dict_dict_json_schema(x, y)),
    (Operand.DICT, Operand.DICT, Operator.EQUALS, lambda x, y: compare_dict_dict_equals(x, y)),
    (Operand.DICT, Operand.STR, Operator.LENGTH_EQUALS, lambda x, y: compare_dict_primitive_length_equals(x, y)),
    (Operand.DICT, Operand.INT, Operator.LENGTH_EQUALS, lambda x, y: compare_dict_primitive_length_equals(x, y)),
    (Operand.DICT, Operand.FLOAT, Operator.LENGTH_EQUALS, lambda x, y: compare_dict_primitive_length_equals(x, y)),
    (Operand.DICT, Operand.STR, Operator.LENGTH_LESS_THAN, lambda x, y: compare_dict_primitive_length_less_than(x, y)),
    (Operand.DICT, Operand.INT, Operator.LENGTH_LESS_THAN, lambda x, y: compare_dict_primitive_length_less_than(x, y)),
    (Operand.DICT, Operand.FLOAT, Operator.LENGTH_LESS_THAN, lambda x, y: compare_dict_primitive_length_less_than(x, y)),
    (Operand.DICT, Operand.STR, Operator.LENGTH_GREATER_THAN, lambda x, y: compare_dict_primitive_length_greater_than(x, y)),
    (Operand.DICT, Operand.INT, Operator.LENGTH_GREATER_THAN, lambda x, y: compare_dict_primitive_length_greater_than(x, y)),
    (Operand.DICT, Operand.FLOAT, Operator.LENGTH_GREATER_THAN, lambda x, y: compare_dict_primitive_length_greater_than(x, y))
]


def get_points():
    return points
