import re

from nebula.modules.common.operand_enum import Operand
from nebula.modules.common.operator_enum import Operator
from nebula.modules.report import nebula_test_exception as nte


def compare_strings_equals(actual, expected):
    if actual == expected: return True
    else: raise nte.NebulaTestException(f"The values were not equal when they should have been. Expected: {expected}, Actual: {actual}")

def compare_strings_not_equals(actual, expected):
    if actual != expected: return True
    else: raise nte.NebulaTestException(f"The values were equal when they should not have been. Expected: {expected}, Actual: {actual}")

def compare_strings_contains(smaller, larger):
    if smaller in larger: return True
    else: raise nte.NebulaTestException(f"The larger string {larger} does not contain {smaller}")

def compare_string_greater_than(actual, expected):
    if float(actual) > float(expected): return True
    else: raise nte.NebulaTestException(f"The actual value was not greater than expected. Expected: {expected}, Actual: {actual}")

def compare_string_less_than(actual, expected):
    if float(actual) < float(expected): return True
    else: raise nte.NebulaTestException(f"The actual value was not less than expected. Expected: {expected}, Actual: {actual}")

def compare_number_string_equals(actual, expected):
    if actual == float(expected): return True
    else: raise nte.NebulaTestException(f"The values were not equal when they should have been. Expected: {expected}, Actual: {actual}")

def compare_string_number_equals(actual, expected):
    if float(actual) == expected: return True
    else: raise nte.NebulaTestException(f"The values were not equal when they should have been. Expected: {expected}, Actual: {actual}")

def compare_number_string_less_than(actual, expected):
    if actual < float(expected): return True
    else: raise nte.NebulaTestExceptionf(f"The actual value was not less than expected. Expected: {expected}, Actual: {actual}")

def compare_number_string_greater_than(actual, expected):
    if actual > float(expected): return True
    else: raise nte.NebulaTestException(f"The actual value was not greater than expected. Expected: {expected}, Actual: {actual}")

def compare_string_number_less_than(actual, expected):
    if float(actual) < expected: return True
    else: raise nte.NebulaTestException(f"The actual value was not less than expected. Expected: {expected}, Actual: {actual}")

def compare_string_number_greater_than(actual, expected):
    if float(actual) > expected: return True
    else: raise nte.NebulaTestException(f"The actual value was not greater than expected. Expected: {expected}, Actual: {actual}")

def compare_number_string_not_equals(actual, expected):
    if actual != float(expected): return True
    else: raise nte.NebulaTestException(f"The values were equal when they should not have been. Expected: {expected}, Actual: {actual}")

def compare_string_int_not_equals(actual, expected):
    if float(actual) != expected: return True
    else: raise nte.NebulaTestException(f"The values were equal when they should not have been. Expected: {expected}, Actual: {actual}")

def compare_string_int_equals(actual, expected):
    if float(actual) == expected: return True
    else: raise nte.NebulaTestException(f"The values were not equal when they should have been. Expected: {expected}, Actual: {actual}")

def compare_string_number_not_equals(actual, expected):
    if float(actual) != expected: return True
    else: raise nte.NebulaTestException(f"The values were equal when they should not have been. Expected: {expected}, Actual: {actual}")

def compare_bool_string_equals(actual, expected):
    if str(actual) == expected: return True
    else: raise nte.NebulaTestException(f"The values were not equal when they should have been. Expected: {expected}, Actual: {actual}")

def compare_string_bool_equals(actual, expected):
    if actual == str(expected): return True
    else: raise nte.NebulaTestException(f"The values were not equal when they should have been. Expected: {expected}, Actual: {actual}")

def compare_str_str_length_greater_than(actual, expected_length):
    if len(actual) > float(expected_length): return True
    else: raise nte.NebulaTestException(f"The length of {actual} was not greater than {expected_length}")

def compare_str_num_length_greater_than(actual, expected_length):
    if len(actual) > expected_length: return True
    else: raise nte.NebulaTestException(f"The length of {actual} was not greater than {expected_length}")

def compare_str_str_length_less_than(actual, expected_length):
    if len(actual) < float(expected_length): return True
    else: raise nte.NebulaTestException(f"The length of {actual} was not less than {expected_length}")

def compare_str_num_length_less_than(actual, expected_length):
    if len(actual) < expected_length: return True
    else: raise nte.NebulaTestException(f"The length of {actual} was not less than {expected_length}")

def compare_str_str_length_equals(actual, expected_length):
    if len(actual) == float(expected_length): return True
    else: raise nte.NebulaTestException(f"The length of {actual} was not equal to {expected_length}")

def compare_str_num_length_equals(actual, expected_length):
    if len(actual) == expected_length: return True
    else: raise nte.NebulaTestException(f"The length of {actual} was not equal to {expected_length}")

def compare_str_str_contains(actual, expected):
    if expected in actual: return True
    else: raise nte.NebulaTestException(f"The expected string was not present in actual. Expected: {expected}, Actual: {actual}")

def compare_str_str_does_not_contain(actual, expected):
    if expected not in actual: return True
    else:
        raise nte.NebulaTestException(f"The expected string was present in actual when it should not have been. Expected: {expected}, Actual: {actual}")

def compare_string_regex_filter(src_str, regex_str):
    regex_match = re.search(regex_str, src_str)
    if regex_match:
        return regex_match.group(1)
    else:
        raise nte.NebulaTestException(f"The filter {regex_str} did not result in any matches in {src_str}")

def compare_string_regex_impl(actual, expected):
    should_match_regex = re.search(expected, actual)
    if should_match_regex:
        return True
    else:
        raise nte.NebulaTestException(f"The value for Expected: {expected}, does not match the regex for Actual: {actual}")

points = [
    (Operand.STR, Operand.STR, Operator.EQUALS, lambda x, y: compare_strings_equals(x, y)),
    (Operand.STR, Operand.STR, Operator.NOT_EQUALS, lambda x, y: compare_strings_not_equals(x, y)),
    (Operand.STR, Operand.STR, Operator.GREATER_THAN, lambda x, y: compare_string_greater_than(x, y)),
    (Operand.STR, Operand.STR, Operator.LESS_THAN, lambda x, y: compare_string_less_than(x, y)),
    (Operand.INT, Operand.STR, Operator.EQUALS, lambda x, y: compare_number_string_equals(x, y)),
    (Operand.INT, Operand.STR, Operator.LESS_THAN, lambda x, y: compare_number_string_less_than(x, y)),
    (Operand.INT, Operand.STR, Operator.GREATER_THAN, lambda x, y: compare_number_string_greater_than(x, y)),
    (Operand.INT, Operand.STR, Operator.NOT_EQUALS, lambda x, y: compare_number_string_not_equals(x, y)),
    (Operand.STR, Operand.INT, Operator.EQUALS, lambda x, y: compare_string_number_equals(x, y)),
    (Operand.STR, Operand.INT, Operator.LESS_THAN, lambda x, y: compare_string_number_less_than(x, y)),
    (Operand.STR, Operand.INT, Operator.GREATER_THAN, lambda x, y: compare_string_number_greater_than(x, y)),
    (Operand.STR, Operand.INT, Operator.NOT_EQUALS, lambda x, y: compare_string_number_not_equals(x, y)),
    (Operand.FLOAT, Operand.STR, Operator.EQUALS, lambda x, y: compare_number_string_equals(x, y)),
    (Operand.FLOAT, Operand.STR, Operator.LESS_THAN, lambda x, y: compare_number_string_less_than(x, y)),
    (Operand.FLOAT, Operand.STR, Operator.GREATER_THAN, lambda x, y: compare_number_string_greater_than(x, y)),
    (Operand.FLOAT, Operand.STR, Operator.NOT_EQUALS, lambda x, y: compare_number_string_not_equals(x, y)),
    (Operand.STR, Operand.FLOAT, Operator.NOT_EQUALS, lambda x, y: compare_string_number_not_equals(x, y)),
    (Operand.STR, Operand.FLOAT, Operator.EQUALS, lambda x, y: compare_number_string_equals(x, y)),
    (Operand.STR, Operand.FLOAT, Operator.LESS_THAN, lambda x, y: compare_string_number_less_than(x, y)),
    (Operand.STR, Operand.FLOAT, Operator.GREATER_THAN, lambda x, y: compare_string_number_greater_than(x, y)),
    (Operand.STR, Operand.STR, Operator.MATCHES, lambda x, y: compare_string_regex_impl(x, y)),
    (Operand.STR, Operand.STR, Operator.FILTER, lambda x, y: compare_string_regex_filter(x, y)),
    (Operand.BOOL, Operand.STR, Operator.EQUALS, lambda x, y: compare_bool_string_equals(x, y)),
    (Operand.STR, Operand.BOOL, Operator.EQUALS, lambda x, y: compare_string_bool_equals(x, y)),
    (Operand.STR, Operand.STR, Operator.LENGTH_GREATER_THAN, lambda x, y: compare_str_str_length_greater_than(x, y)),
    (Operand.STR, Operand.STR, Operator.LENGTH_LESS_THAN, lambda x, y: compare_str_str_length_less_than(x, y)),
    (Operand.STR, Operand.STR, Operator.LENGTH_EQUALS, lambda x, y: compare_str_str_length_equals(x, y)),
    (Operand.STR, Operand.INT, Operator.LENGTH_GREATER_THAN, lambda x, y: compare_str_num_length_greater_than(x, y)),
    (Operand.STR, Operand.INT, Operator.LENGTH_LESS_THAN, lambda x, y: compare_str_num_length_less_than(x, y)),
    (Operand.STR, Operand.INT, Operator.LENGTH_EQUALS, lambda x, y: compare_str_num_length_equals(x, y)),
    (Operand.STR, Operand.FLOAT, Operator.LENGTH_GREATER_THAN, lambda x, y: compare_str_num_length_greater_than(x, y)),
    (Operand.STR, Operand.FLOAT, Operator.LENGTH_LESS_THAN, lambda x, y: compare_str_num_length_less_than(x, y)),
    (Operand.STR, Operand.FLOAT, Operator.LENGTH_EQUALS, lambda x, y: compare_str_num_length_equals(x, y)),
    (Operand.STR, Operand.STR, Operator.CONTAINS, lambda x, y: compare_str_str_contains(x, y)),
    (Operand.STR, Operand.STR, Operator.DOES_NOT_CONTAIN, lambda x, y: compare_str_str_does_not_contain(x, y))
]

def get_points():
    return points
