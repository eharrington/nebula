from nebula.modules.common import operand_enum, operator_enum
from nebula.modules.common.compare import (compare_collections_impl,
                                           compare_list_impl,
                                           compare_numbers_impl,
                                           compare_strings_impl)
from nebula.modules.common.operand_enum import Operand
from nebula.modules.common.operator_enum import Operator
from nebula.modules.report import invalid_test_exception as ite


'''
Based on the Factory Pattern, CompareFactory takes input of operands
and operators, routes them to the correct compare lambda function
and either returns true if the comparison was successful, or raises
and exception if it was not
Easily extensible, adding more compare methods is easier and modular
'''
class CompareFactory:
    def __init__(self):
        self._compare_matrix = {}
        self._compare_impls = []
        self._compare_impls.append(compare_numbers_impl.get_points())
        self._compare_impls.append(compare_strings_impl.get_points())
        self._compare_impls.append(compare_collections_impl.get_points())
        self._compare_impls.append(compare_list_impl.get_points())

        for impl in self._compare_impls:
            for point in impl:
                self.set_compare_point(point)

    def set_compare_point(self, point):
        '''
        @param point - A 4-element tuple consisting of 2 operands, 
        an operator, and the lambda function. Used to build the compare
        matrix 
        '''
        length = point[0].value
        breadth = point[1].value
        height = point[2].value
        self._compare_matrix[length, breadth, height] = point[3]

    def compare(self, operandA, operandB, operator):
        '''
        @param operandA - The first operand in the compare operation
        operandB - The second operand in the compare operation
        operator - The operator. Can be ==, !=, sort, length, etc
        Determines the 3 dimensional points of the compare matrix, which
        are then used to determine the compare method and call it.
        '''
        typeA = type(operandA)
        typeB = type(operandB)
        typeO = operator

        typeA = operand_enum.get_operand_type(typeA)
        typeB = operand_enum.get_operand_type(typeB)
        typeO = operator_enum.get_operator(typeO)

        try:
            compare_method = self._compare_matrix[typeA.value, typeB.value, typeO.value]
            return compare_method(operandA, operandB)
        except KeyError:
            raise ite.InvalidTestException(f"{operandA} and {operandB} are of incompatible types and cannot be compared")
