from enum import Enum


'''
An Enum of different Operand types
'''
class Operand(Enum):
    INT = 1
    FLOAT = 2
    STR = 3
    LIST = 4
    DICT = 5
    BOOL = 6


operand_dict = {
    int:Operand.INT,
    float:Operand.FLOAT,
    str:Operand.STR,
    list:Operand.LIST,
    dict:Operand.DICT,
    bool:Operand.BOOL
}

def get_operand_type(enum_key):
    '''
    @param enum_key Accept python type as parameter,
    look up the dictionary and return respective operand enum
    '''
    return operand_dict[enum_key]
