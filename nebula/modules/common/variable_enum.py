from enum import Enum


class Variable(Enum):
    PRIMITIVE = 1
    COLLECTION = 2
    ENCRYPTED = 3
    JSON = 4
    REGEX = 5
    RESTRESPONSE = 6
    PLACEHOLDER = 7
    SHELLRESPONSE = 8
