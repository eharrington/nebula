import configparser
import os

import yaml
from pkg_resources import resource_filename


'''Config helper stores configuration information.
'''

class ConfigHelper:
    class __ConfigHelper:
        def __init__(self):
            self.browser = "Chrome"
            self.test_file_path = None
            self.image_store_url = _get_value('IMAGE_STORE_URL', 'http://usa-rnc-nebula:5000')
            self.image_ui_url = _get_value('IMAGE_UI_URL', 'http://usa-rnc-nebula:3000')
            self.intent_tracker_url = _get_value('INTENT_TRACKER_URL', 'http://usa-rnc-nebula:3005')
            self.report_location = _get_value('REPORT_LOCATION', './output_files')
            self.logging_location = _get_value('LOGGING_LOCATION', './output_files')
            self.selectors_file = _get_value('SELECTOR_FILE')
            self.lang_file = _get_value('LANG_FILE')
            self.lang_file_contents = []
            self.image_identifier = _get_value('NEBULA_IMAGE_INDENTIFIER')
            self.headless = False
            self.failure = _get_value('FAILURE', False)
            self.global_config_path = _get_value('NEBULA_CONFIG')
            self.global_config_contents = None
            self.crawl_config_path = _get_value('NEBULA_CRAWL_CONFIG')
            self.crawl_config_contents = None
            self.variable_dict = {}
            self.specific_tests = []
            self.test_tags = []
            self.browser_resolution = (1600,900)
            self.chrome_version = None
            self.log_level = None
            self.tracking = False
            self.process_count = 1
            self.intent_confidence_cutoff = 0.0
        def __str__(self):
            return repr(self)
    instance = None
    def __init__(self):
        if not ConfigHelper.instance:
            ConfigHelper.instance = ConfigHelper.__ConfigHelper()
        else:
            ConfigHelper.instance
    def __getattr__(self, name):
        return getattr(self.instance, name)
    def __setattr__(self, name, value):
        setattr(self.instance, name, value)

def get_instance():
    return ConfigHelper()

def _get_value(setting, default_value=None):
    '''Gets the setting value by checking if an environmental variable exists first. If not returns the default value.
    @param: setting - String representing the variable to look for
    @param: default_value - The default value to assign to the setting
    @returns primitive
    '''
    if setting in os.environ:
        return os.environ[setting]
    else:
        return default_value

def get_boolean_value(setting):
    value = _get_value(setting)

    if value is None:
        return False
    else:
        return value.lower() == 'true'

def set_test_file_path(test_file_path):
    '''Sets the test_file_path
    @param test_file_path
    '''
    get_instance().__setattr__("test_file_path", test_file_path)

def get_test_file_path():
    '''Retrieves test_file_path
    @returns test_file_path
    '''
    return get_instance().__getattr__("test_file_path")

def set_log_level(log_level):
    '''Set the log level
    @param log_level
    '''
    get_instance().__setattr__("log_level", log_level)

def get_log_level():
    '''Retrieves the log level
    @returns log_level
    '''
    return get_instance().__getattr__("log_level")

def set_browser(browser):
    '''Sets the browser
    @param browser
    '''
    get_instance().__setattr__("browser", browser)

def get_browser():
    '''Retrieves browser
    @returns browser
    '''
    return get_instance().__getattr__("browser")

def set_chrome_version(version):
    ''' Set the chrome version
    @param version
    '''
    get_instance().__setattr__("chrome_version", version)

def get_chrome_version():
    ''' Retrieves chrome version
    @returns version
    '''
    return get_instance().__getattr__("chrome_version")

def set_exit_on_failure(failure):
    '''Sets the failure
    @param failure
    '''
    get_instance().__setattr__("failure", failure)

def get_exit_on_failure():
    '''Retrieves failure
    @returns failure
    '''
    return get_instance().__getattr__("failure")

def set_image_store(path):
    '''Sets image store url
    @param path
    '''
    get_instance().__setattr__("image_store_url", path)

def get_image_store():
    '''Retrieves image store url
    @returns image_store_url
    '''
    return get_instance().__getattr__("image_store_url")

def set_image_ui(path):
    '''Sets image ui url
    @param path
    '''
    get_instance().__setattr__("image_ui_url", path)

def get_image_ui():
    '''Retrieves image ui url
    @returns image_ui_url
    '''
    return get_instance().__getattr__("image_ui_url")

def set_intent_tracker(path):
    '''Sets intent tracker url
    @param path
    '''
    get_instance().__setattr__("intent_tracker_url", path)

def get_intent_tracker():
    '''Retrieves intent tracker url
    @returns intent_tracker_url
    '''
    return get_instance().__getattr__("intent_tracker_url")

def get_selectors_file():
    '''Retrieves the selectors file
    @returns selectors_file
    '''
    return get_instance().__getattr__("selectors_file")

def set_selectors_file(selectors_file):
    '''Sets the selectors file string
    @param selectors_files
    '''
    get_instance().__setattr__("selectors_file", selectors_file)

def get_lang_file_name():
    '''Retrieves the name of the language file
    @returns lang_file
    '''
    return get_instance().__getattr__("lang_file")

def set_lang_file(lang_file_name):
    get_instance().__setattr__("lang_file", lang_file_name)
    if lang_file_name is not None:
        with open(lang_file_name, "r", encoding='utf-8') as yamlfile:
            _lang_file = yaml.safe_load(yamlfile)
        get_instance().__setattr__("lang_file_contents", _lang_file)

def get_translation(original_lang):
    try:
        lang_file_contents = get_instance().__getattr__("lang_file_contents")
        if lang_file_contents is None:
            return
        else:
            return lang_file_contents[original_lang]
    except:
        return None

def get_browser_resolution():
    '''Retrieves the browser resolution
    @returns browser_resolution
    '''
    return get_instance().__getattr__("browser_resolution")

def set_browser_resolution(browser_resolution):
    '''Sets the browser resolution
    @param browser_resolution
    '''
    get_instance().__setattr__("browser_resolution", browser_resolution)

def set_report_location(path):
    '''Sets report_location path
    @param path
    '''
    get_instance().__setattr__("report_location", path)

def get_report_location():
    '''Retrieves report_location path
    @returns report_location
    '''
    return get_instance().__getattr__("report_location")

def set_logging_location(path):
    '''Sets logging_location path
    @param path
    '''
    get_instance().__setattr__("logging_location", path)

def get_logging_location():
    '''Retrieves logging_location path
    @returns logging_location
    '''
    return get_instance().__getattr__("logging_location")

def set_headless(headless):
    '''Sets headless boolean value
    @param headless
    '''
    get_instance().__setattr__("headless", headless)

def get_headless():
    '''Retrieves headless boolean value
    @returns headless
    '''
    return get_instance().__getattr__("headless")

def set_variable_dict(var_dict):
    '''Wipes out and create a new set of test variables
    @param var_dict
    '''
    get_instance().__setattr__("variable_dict", var_dict)

def get_variable_dict():
    '''Retrieves the set of available test variables
    @returns variable_dict
    '''
    return get_instance().__getattr__("variable_dict")

def set_specific_tests(list_of_tests):
    '''Wipes out and create a new array of tests to run
    @param list_of_tests - a comma separated list of tests to run
    '''
    get_instance().__setattr__("specific_tests", list_of_tests)

def get_specific_tests():
    '''Retrieves the set of tests to run
    @returns specific_tests
    '''
    return get_instance().__getattr__("specific_tests")

def set_test_tags(list_of_tags):
    '''Wipes out and create a new array of test tags to check against
    @param list_of_tags - a comma separated list of tags
    '''
    get_instance().__setattr__("test_tags", list_of_tags)

def get_test_tags():
    '''Retrieves the set of test tags
    @returns test_tags
    '''
    return get_instance().__getattr__("test_tags")

def set_image_identifier(identifier):
    '''Sets the image identifier
    @param identifier
    '''
    get_instance().__setattr__("image_identifier", identifier)

def get_image_identifier():
    '''Retrieves the image identifier value
    @returns image_identifier
    '''
    return get_instance().__getattr__("image_identifier")

def set_confidence_cutoff(confidence):
    '''Sets the level for required intent confidence
    @param confidence
    '''
    get_instance().__setattr__("intent_confidence_cutoff", confidence)

def get_confidence_cutoff():
    '''Retrieves the level for required intent confidence
    @returns confidence 
    '''
    return get_instance().__getattr__("intent_confidence_cutoff")

def set_crawl_config_file(file):
    '''Sets the crawl config file to use
    @param file
    '''
    get_instance().__setattr__("crawl_config_path", file)

def _get_crawl_config_contents():
    contents = get_instance().__getattr__("crawl_config_contents")
    if contents is None:
        crawl_config_path = get_instance().__getattr__("crawl_config_path")
        if crawl_config_path is None:
            return None
        else:
            with open(crawl_config_path, "r") as js:
                contents = yaml.safe_load(js)
            get_instance().__setattr__("crawl_config_contents", contents)

    return  get_instance().__getattr__("crawl_config_contents")

def get_crawl_config_setting(setting_name):
    '''Retrieves the crawl config setting associated with the setting name
    @param setting_name
    @returns value in yaml file
    '''
    contents = _get_crawl_config_contents()
    if contents is None:
        return None
    if setting_name in contents:
        return contents[setting_name]
    else:
        return None

def set_global_config_file(file):
    '''Sets the config file of command line parameters to use
    @param file
    '''
    get_instance().__setattr__("global_config_path", file)

def _get_global_config_contents():
    contents = get_instance().__getattr__("global_config_contents")
    if contents is None:
        global_config_path = get_instance().__getattr__("global_config_path")
        if global_config_path is None:
            return None
        else:
            with open(global_config_path, "r") as js:
                contents = yaml.safe_load(js)
            get_instance().__setattr__("global_config_contents", contents)

    return  get_instance().__getattr__("global_config_contents")


def set_tracking(tracking):
    '''Sets tracking boolean value.  True means it is being set
    @param tracking
    '''
    get_instance().__setattr__("tracking", tracking)

def get_tracking():
    '''Retrieves tracking boolean value
    @returns tracking
    '''
    return get_instance().__getattr__("tracking")


def set_process_count(count):
    '''Sets the number of concurrent test threads to run
    @param count
    '''
    get_instance().__setattr__("process_count", count)

def get_process_count():
    '''Retrieves the number of concurrent threads to run
    @returns count
    '''
    return get_instance().__getattr__("process_count")

def get_global_config_setting(setting_name):
    '''Retrieves the global config setting associated with the setting name
    @param setting_name
    @returns value in yaml file
    '''
    contents = _get_global_config_contents()
    if contents is None:
        return None
    if setting_name in contents:
        return contents[setting_name]
    else:
        return None

def process_config_file(config_file):
    '''Retrieves the global config file and reads in the settings
    @param config_file
    '''
    set_global_config_file(config_file)
    contents = _get_global_config_contents()
    if contents is None:
        return None
    for setting_name in contents:
        get_instance().__setattr__(setting_name, contents[setting_name])
