import logging
import threading

class ThreadLogFilter(logging.Filter):
    def __init__(self, threadname):
        self.threadname = threadname
        
    def filter(self, record):
        return self.threadname == threading.current_thread().name
