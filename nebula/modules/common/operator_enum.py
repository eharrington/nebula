from enum import Enum


'''
An Enum consisting of different operators
'''
class Operator(Enum):
    EQUALS = 1
    NOT_EQUALS = 2
    GREATER_THAN = 3
    LESS_THAN = 4
    LENGTH_EQUALS = 5
    LENGTH_NOT_EQUALS = 6
    LENGTH_GREATER_THAN = 7
    LENGTH_LESS_THAN = 8
    CONTAINS = 9
    DOES_NOT_CONTAIN = 10
    MATCHES = 11
    SORT = 12
    JSON_SCHEMA_COMPARE = 13
    FILTER = 14


enum_dict = {
    "=":Operator.EQUALS, "equals":Operator.EQUALS, "==":Operator.EQUALS, "is":Operator.EQUALS,
    "same as":Operator.EQUALS, "has value":Operator.EQUALS, "status is":Operator.EQUALS,
    "equal to":Operator.EQUALS, "length >":Operator.LENGTH_GREATER_THAN,
    "!=":Operator.NOT_EQUALS, "status is not":Operator.NOT_EQUALS, "is not":Operator.NOT_EQUALS,
    "does not match":Operator.NOT_EQUALS,"unequal to":Operator.NOT_EQUALS,
    "not equal to":Operator.NOT_EQUALS, "not the same":Operator.NOT_EQUALS,
    ">":Operator.GREATER_THAN, "greater than":Operator.GREATER_THAN,
    "more than":Operator.GREATER_THAN, "higher than":Operator.GREATER_THAN,
    "<":Operator.LESS_THAN, "less than":Operator.LESS_THAN,
    "fewer than":Operator.LESS_THAN, "lower than":Operator.LESS_THAN,
    "length":Operator.LENGTH_EQUALS, "size":Operator.LENGTH_EQUALS,
    "contains":Operator.CONTAINS, "has":Operator.CONTAINS,
    "does not have":Operator.DOES_NOT_CONTAIN, "does not contain":Operator.DOES_NOT_CONTAIN,
    "matches":Operator.MATCHES, "sort":Operator.SORT, "filter":Operator.FILTER,
    "length <":Operator.LENGTH_LESS_THAN, "length ==":Operator.LENGTH_EQUALS,
    "json schema compare":Operator.JSON_SCHEMA_COMPARE,
    "length !=":Operator.LENGTH_NOT_EQUALS
}

def get_operator(enum_key):
    '''
    @param enum_key Accept a string signifying the operator,
    look up the dictionary and return the respective operator enum
    '''
    return enum_dict[enum_key.lower()]
