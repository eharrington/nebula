class PageItem:
    '''Object to save page item properties'''
    def __init__(self, html, tag_name, text, href, attributes, click_type = None):
        # html of element
        self.html = html
        # tag type
        self.tag_name = tag_name
        # text found in the element
        self.text = text
        # href
        self.href = href
        # array of all attributes
        self.attributes = attributes
        # integer of click type
        self.click_type = click_type