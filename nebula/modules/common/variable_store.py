import copy
import json
import logging
import os
import re
from ast import literal_eval

from nebula.modules.common import config_helper
from nebula.modules.common.variable_enum import Variable
from nebula.modules.report import nebula_test_exception as nte


'''Variable store is a convenient place to store and retrieve
variables'''


class VariableStore:
    def __init__(self):
        self.variable_dict = {}
        #default values
        self.set_value('${WAIT_TIME}', '1')
        self.set_value('${WEBDRIVER_PATH}', os.getcwd() + '/drivers')
        self.set_value('${EX_NEB_ELECTRON_WEBDRIVER_PATH}', os.getcwd() + '/drivers')

        #read in env variables
        # store all environmental variables in variable_store
        for evn_name in os.environ.keys():
            value = os.environ.get(evn_name)
            evn_name = "${" + evn_name.strip() + "}"
            self.set_value(evn_name, value.strip(), Variable.ENCRYPTED)

    def add_variable_value(self, var_name, var_type, var_value, position):
        '''
        This method will add to an existing variable, the
        value and type provided
        @param var_name - The name of the variable with which to reference it
        @param var_type - The type of the variable
        @param var_value - The value of the variable provided
        @param position - Whether to add the value to the beginning or end
        '''
        try:
            if var_type in ['PRIMITIVE', 'REGEX', 'PLACEHOLDER']:
                original_value = self.get_value(var_name.strip())

                if position == 'end':
                    new_value = original_value + var_value.strip()
                else:
                    new_value = var_value.strip() + original_value

                self.set_value(var_name.strip(), new_value, Variable[var_type])
            elif var_type == 'ENCRYPTED':
                logging.warning("Cannot add to the value of an encrypted variable in the test")

            # TODO: add ability to add to JSON
            elif var_type == 'JSON':
                target, indices = self._parse_var_name(var_name)
                stored_value = self.get_value(target.strip())
                if stored_value is None:
                    raise nte.NebulaTestException(f"The collection {target} has not been defined. Cannot edit a non-existent Json")
                else:
                    stored_value = self._replace_collection_target_val(stored_value, indices, var_value, position)

                self.set_value(target.strip(), json.dumps(stored_value), Variable[var_type])

            elif var_type == 'COLLECTION':
                target, indices = self._parse_var_name(var_name)
                stored_value = self.get_value(target.strip())
                if stored_value is None:
                    raise nte.NebulaTestException(f"The collection {target} has not been defined. Cannot add to a non-existent collection")
                else:
                    stored_value = self._replace_collection_target_val(stored_value, indices, var_value, position)

                self.set_value(target.strip(), str(stored_value), Variable[var_type])
        except IndexError as ie:
            logging.warning("Error while parsing the collection/JSON. Is the collection/JSON indexed correctly?")
            raise ie

    def _parse_var_name(self, var_name):
        '''Breaks a variable down to access the members of a complex data structure
        '''
        indices =[]
        if re.search(r"\${\w+}\[", var_name):
            var_name, indices = var_name.split('[', 1)
            indices = re.split(r"\]\[", indices.strip(']'))

        # This regex is useful for when we want to get element css/attribute values into a variable
        elif re.search(r"\w+\[", var_name):
            var_name, indices = var_name.split('[', 1)
            indices = re.split(r"\]\[", indices.strip(']'))

        return var_name, indices

    def set_variable_value(self, var_name, var_type, var_value):
        '''
        This method will set either an existing variable, or a new one, to the
        value and type provided
        @param var_name - The name of the variable with which to reference it
        @param var_type - The type of the variable
        @param var_value - The new/updated value of the variable provided
        '''
        try:
            if var_type in ['PRIMITIVE', 'REGEX', 'PLACEHOLDER']:
                self.set_value(var_name.strip(), var_value, Variable[var_type])
            elif var_type == 'ENCRYPTED':
                logging.warning("Cannot set value of an encrypted variable in the test")
            elif var_type == 'JSON':
                target, indices = self._parse_var_name(var_name)
                stored_value = self.get_value(target.strip())
                if stored_value is None:
                    raise nte.NebulaTestException(f"The collection {target} has not been defined. Cannot edit a non-existent collection")
                else:
                    stored_value = self._replace_collection_target_val(stored_value, indices, var_value, 'none')

                self.set_value(target.strip(), json.dumps(stored_value), Variable[var_type])

            elif var_type == 'COLLECTION':
                target, indices = self._parse_var_name(var_name)
                stored_value = self.get_value(target.strip())
                if stored_value is None:
                    raise nte.NebulaTestException(f"The collection {target} has not been defined. Cannot edit a non-existent collection")
                else:
                    stored_value = self._replace_collection_target_val(stored_value, indices, var_value, 'none')

                self.set_value(target.strip(), str(stored_value), Variable[var_type])
        except IndexError as ie:
            logging.warning("Error while parsing the collection/JSON. Is the collection/JSON indexed correctly?")
            raise ie

    def _resolve_collection_target_val(self, collection, indices):
        '''Pull a variable from the store, with the expectation that it will be a
        collection object or a scalar, and then drill down to a specific value using the indices
        @param collection - an arbitrarily deep structue compose of arrays, dictionaries, and scalars
        @param indices - an array of indexes into the collection
        This method is safe to call with an empty set for indices, and will return doing nothing
        '''
        index = None
        try:
            for index in indices:
                if '=' in index:  #this is the flag to find a particular dictionary in an array
                    key, value = index.split('=',1)
                    for dictionary in collection:
                        try:
                            if dictionary[key] == value:
                                collection = dictionary
                                break
                        except KeyError:
                            continue
                elif isinstance(collection, list): #this is the case of having an index into an array.  The index is assumed to be an integer.
                    collection = collection[int(index)]
                else: #in this final case, the index is assumed to be a key into a dictionary.
                    collection = collection[index]
        except Exception:
            logging.verbose(f"Encountered possible problem at index '{index}' while resolving the value for {str(indices)} within {str(collection)}")
            return ''
        return collection

    def check_exist_collection_target_val(self, collection, indices):
        if (bool(self._resolve_collection_target_val(collection, indices))):
            return True
        else:
            return False

    def _replace_collection_target_val(self, collection, indices, value, position):
        '''
        This method is used to update the values of collections provided a set
        of indices is supplied. If the set is empty, it considers it a primitive
        type and replaces that value
        @param collection - The collection in which a value needs to be replaced
        @param indices - A list of indices to successively drill down into
        @param value - The updated value to be set in the collection
        @param position - Whether to add the value to the beginning or end
        '''
        #index = None
        if len(indices) > 0:
            for i in range(0, len(indices), 1):
                if isinstance(collection, dict):
                    for counter, key in enumerate(collection):
                        if indices[i] == key:
                            new_value = self._replace_collection_target_val(collection[key], indices[i+1:], value, position)
                            collection[key] = new_value

                if isinstance(collection, list):
                    for counter, _ in enumerate(collection):
                        if int(indices[i]) == counter:
                            new_value = self._replace_collection_target_val(collection[counter], indices[i+1:], value, position)
                            is_collection = True if isinstance(collection[counter], list) else False

                            if position == 'beginning':
                                if is_collection:
                                    return collection
                                else:
                                    collection.insert(0, new_value)
                            elif position == 'end':
                                if is_collection:
                                    collection[counter].insert(len(collection), new_value)
                                else:
                                    collection.insert(len(collection), new_value)

                            else:
                                collection[counter] = new_value
                            return collection

        else:
            if isinstance(collection, list):
                if position == 'beginning':
                    collection.insert(0, value)
                elif position == 'end':
                    collection.insert(len(collection), value)
                else:
                    collection = value
            elif isinstance(collection, dict):
                if position == 'beginning' or position == 'end':
                    collection.update(value)
            else:
                collection = value

        return collection

    def get_value(self, variable):
        '''Gets the variable value by checking if it exists first, then looks for its value
        @param: variable - String representing the variable to look for
        @returns primitive, or None if there is a problem finding the variable
        '''
        if variable in self.variable_dict:
            variable_value = self.get_tuple(variable)['value']

            cleartext = variable_value
            pattern = re.compile(r'(\${\w+})')
            for m in re.finditer(pattern, str(variable_value)):
                term = m.group(1)
                term_value = self.get_value(term)

                if term_value is not None:
                    cleartext = cleartext.replace(term, str(term_value))

            variable_value = cleartext

            # This should stay, since get_value should return an actual
            # collection/dictionary if the type of the variable is such
            variable_type = self.get_tuple(variable)['type']
            if variable_type == Variable.JSON and not isinstance(variable_value, dict):
                variable_value = json.loads(variable_value)

            if variable_type == Variable.COLLECTION:
                variable_value = literal_eval(variable_value)

            return variable_value

    def get_type(self, variable):
        '''Gets the variable type by checking if it exists first, then looks for its type.
        @param: variable - String representing the variable to look for
        @returns Enum
        '''
        if variable in self.variable_dict:
            return self.get_tuple(variable)['type']

    def get_tuple(self, variable):
        '''Gets the variable tuple by checking if it exists first
        @param: variable - String representing the variable to look for
        @returns {Enum, value}
        '''
        if variable in self.variable_dict:
            return self.variable_dict[variable]

    def set_value(self, variable, value, type=Variable.PRIMITIVE):
        '''Sets the variable value, replacing what was there if necessary
        @param: variable - String representing the variable to look for
        @param: value - String representing the value to set
        @param: type - Enum representing of the type to set
        '''
        self.variable_dict[variable] = {'value':value, 'type':type}
        if type == Variable.ENCRYPTED:
            logging.verbose(f"{variable} has been set")
        else:
            logging.verbose(f"{variable} has been set to {value}")

    def items(self):
        '''Return a list of variables stored.
        '''
        return list(self.variable_dict.keys())

    def var_replace(self, text):
        indices = []

        use_Css = True if "_css_" in str(text) else False
        use_Attr = True if "_attr_" in str(text) else False
        # strip out the _css_ tag
        if use_Css:
            text = text.replace("_css_", "")
        # strip out the _attr_ tag
        if use_Attr:
            text = text.replace("_attr_", "")

        cleartext = text

        # Causing issues in set and edit values test
        pattern = re.compile(r'(\${[^:/\[\$]*?})(\[\S*])*')
        for m in re.finditer(pattern, str(text)):
            term = m.group(1)
            term = term.strip(',').strip('"').strip("'")
            term = term.replace("']", "").replace('"]','')
            term = term.replace("'}", "").replace('"}','')
            indices_text =  "" if m.group(2) == None else str(m.group(2))
            term = term+indices_text
            target, indices = self._parse_var_name(term)

            for counter, _ in enumerate(indices):
                indices[counter] = self.var_replace(indices[counter])

            target_val = self.get_value(target)
            target_type = self.get_type(target)



            if target_val is not None:
                if target_type == Variable.JSON:
                    replace_value = json.dumps(self._resolve_collection_target_val(target_val, indices))
                    # remove double quotes that get added to a normal string that wasn't supposed to have them
                    if replace_value.startswith('"'):
                        replace_value = replace_value.strip('\"')
                    elif replace_value.startswith("'"):
                        replace_value = replace_value.strip("\'")

                elif target_type == Variable.RESTRESPONSE:
                    return target_val

                elif use_Css or use_Attr:
                    replace_value = target_val+indices_text

                else:
                    replace_value = str(self._resolve_collection_target_val(target_val, indices))

                if isinstance(cleartext, dict) or isinstance(cleartext, list):
                    cleartext = str(cleartext)
                    cleartext = cleartext.replace(term, replace_value)

                    if isinstance(literal_eval(cleartext), dict) or isinstance(literal_eval(cleartext), list):
                        cleartext = literal_eval(cleartext)

                else:
                    cleartext = cleartext.replace(term, replace_value)
                    if target_type == Variable.COLLECTION and len(indices) == 0:
                        cleartext = literal_eval(cleartext)

                    if target_type == Variable.JSON and len(indices) == 0:
                        cleartext = json.loads(cleartext)
                logging.debug(f'Variable {term} substituted with its value: {replace_value}')

        if cleartext == text:
            return cleartext

        if re.search(r'\${\w+}', str(cleartext)):
            cleartext = self.var_replace(cleartext)

        return cleartext
