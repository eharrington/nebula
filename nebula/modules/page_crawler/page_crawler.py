import os
import logging
import re
from bs4 import BeautifulSoup
from urllib.parse import urlparse, urljoin
from nebula.modules.page_crawler import nav_tree_node
from nebula.modules.page_crawler import page_crawler_base
from nebula.modules.common import page_item as pi
from nebula.modules.common import config_helper

class PageCrawler(page_crawler_base.PageCrawlerBase):
    def __init__(self, webdriver, page_handler):
        super().__init__(webdriver, page_handler)
        self.urls_to_ignore = config_helper.get_crawl_config_setting("urls_to_ignore")

    def get_url_fragment(self, url):
        ''' Returns a fragment of the provided URL. Removes domain name and prefix
        @param: url - The url to parse
        @returns: String
        '''
        try:
            parsedUrl = urlparse(url)
            domain = parsedUrl.netloc
            fragment = parsedUrl.fragment
            if fragment == '':
                fragment = parsedUrl.path

            if not parsedUrl.query == '':
                fragment = fragment + '?' + parsedUrl.query

            if not fragment.startswith("/"):
                fragment = "/" + fragment

            new_url = domain + fragment
            new_url = new_url.replace('?', '/')
            new_url = new_url.replace(' ', '_')
            return re.sub(r'([\=@&:%\'\"])+', '_', new_url)
        except Exception as e:
            logging.error(f"Unable to get fragment of url: {url}")
            logging.exception(e)

    def navigate_tree_and_perform_actions(self, parent_nav_tree_node):
        ''' Screenshot the page and its elements then navigate all it's links
        @param: parentNavTreeNode - The parent node of the new node we are about to create
        '''
        # check to make sure the page we went to is in the same domain
        if not self._is_same_url_domain(self._driver.current_url) and not self._is_in_ignore_urls(self._driver.current_url):
            return

        # store that we have captured this link
        fragment = self.get_url_fragment(self._driver.current_url)

        # check to see if we already did this page
        if fragment in self.list_of_links_captured:
            return

        self.list_of_links_captured.append(self._driver.current_url)

        self._pagehandler.handle_page(fragment, self._driver.find_element_by_tag_name('html'))
        # scroll back to top
        super().scroll_to_top()

        # get all the links on the page
        page_item_list = self._get_list_of_page_items_on_page()

        # we don't want to go any deeper that 6. This could make navigation really long
        # this sets the max height of the tree to 6 nodes
        # any page that takes more than 6 clicks we might not want to check
        # or the website is designed really badly
        # this does not affect width
        if parent_nav_tree_node.node_level >= 6:
            return

        # add a nav node for each link
        for pageItem in page_item_list:
            # create a new tree node
            newNode = nav_tree_node.NavTreeNode(pageItem, parent_nav_tree_node.node_level + 1)
            # set nav path so that it can inherit paths from the parent
            for h in parent_nav_tree_node.navigation_path:
                newNode.navigation_path.append(h)
            # add current href to path
            newNode.navigation_path.append(pageItem)
            parent_nav_tree_node.child_nodes.append(newNode)

        for node in parent_nav_tree_node.child_nodes:
            self.navigate_up_to_node_link(node)
            self.navigate_tree_and_perform_actions(node)

    def _get_list_of_page_items_on_page(self):
        ''' Finds all <a> elements on a page and returns the list as PageItems.
        @returns: array[PageItems]
        '''
        source = self._driver.page_source
        soup = BeautifulSoup(source, "html.parser")

        list_of_page_items = []
        links_on_page = soup.find_all('a')
        # loop through all the links we find
        for link in links_on_page:

            # get the href element
            href = link.get('href')

            if href is None:
                continue

            # check to see if the URL needs to be ignored
            if self._is_in_ignore_urls(href):
                continue

            #get if it has the target element
            target = link.get('target')

            # build full URL so we account for sites not at the root of the web server
            if not href.startswith('http'):
                current_url = self._driver.current_url
                href = urljoin(current_url, href)
            # check to see if the link has already been visited
            if not href in self.list_of_links_captured:
                # don't add the link if it is already stored
                newPageItem = pi.PageItem(link, link.name, link.text, href, link.attrs)
                if not newPageItem in list_of_page_items:

                    # only add if in same domain and does not open another tab
                    if href and href != "#" and link.hidden is False and self._is_same_url_domain(href) is True \
                                and target not in ['blank', '_blank'] \
                                and 'javascript' not in href \
                                and 'mailto' not in href:
                        # add the item to the list to navigate
                        list_of_page_items.append(newPageItem)

        return list_of_page_items

    def _is_same_url_domain(self, url):
        ''' Checks to see if the url is the same as the url used in login
        @param: url - The url you want checked
        @returns: boolean
        '''
        new_domain = urlparse(url).netloc
        if new_domain == '':
            return True
        else:
            base_domain = urlparse(self.base_url_domain).netloc
            return new_domain.lower() == base_domain.lower()

    def _is_in_ignore_urls(self, url):
        if self.urls_to_ignore is None:
            return False

        for ig_url in self.urls_to_ignore:
            if url == ig_url or ig_url in url:
                return True

        return False

