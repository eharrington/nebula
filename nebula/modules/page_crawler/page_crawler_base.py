from urllib.parse import urlparse
from nebula.modules.common import config_helper
from nebula.modules.page_crawler import nav_tree_node
from selenium import webdriver
import datetime
import json
import logging
import time
import os

class PageCrawlerBase:
    def __init__(self, webdriver, pagehandler):

        self._driver = webdriver
        self._pagehandler = pagehandler

        self._base_node = None

        # list of links we have captured so we only do each one once
        self.list_of_links_captured = []

        # save the base domain of the home page
        self.base_url_domain = ''

        self._start_page_url = ''

    def begin_crawl(self, site_url):
        ''' Begin crawling the web app
        '''
        try:
            start = time.time()
            logging.verbose("Start Time: " + str(start))

             # save base URL info
            parsedUrl = urlparse(site_url)
            self.base_url_domain = parsedUrl.scheme + "://" + parsedUrl.netloc

            self._driver.get(site_url)

            # create base nav node
            self._base_node = nav_tree_node.NavTreeNode(None, 1)
            # save the home page so we can go back
            self._start_page_url = self._driver.current_url

            # begin navigating the app and perform actions
            self.navigate_tree_and_perform_actions(self._base_node)

            end = time.time()

            self.generate_nav_tree()

            logging.verbose("Run time: " + str(datetime.timedelta(seconds=(end - start))))

        except Exception as e:
            logging.exception(e)
            self._driver.quit()

    def navigate_tree_and_perform_actions(self, parentNavTreeNode):
        '''To be overridden'''
        return None

    def _get_home_page(self):
        ''' Navigate to the home page
        '''
        logging.debug('Going to home page: ' + self._start_page_url)
        self._driver.get(self._start_page_url)

    def _navigate_tree_and_perform_actions(self, urlFragment, parentNavTreeNode):
        '''To be overridden'''
        return None

    def click_element(self, pageItem):
        '''Find an element by it's xpath then click on it
        @param: pageItem - PageItem object
        @returns: Array (locationX, locationY, sizeW, sizeH)
        '''
        try:
            xpath = f"//a[@href='{pageItem.href}']"

            click_elements = self._driver.find_elements_by_xpath(xpath)

            if len(click_elements) > 1:
                logging.verbose("More than one element was found for: " + xpath)
                logging.verbose("Going to click on the first visible element")

            for clickElement in click_elements:
                # don't do anything if it is not displayed
                if not clickElement.is_displayed():
                    continue

                location = clickElement.location
                size = clickElement.size

                self._driver.execute_script('arguments[0].scrollIntoView({block: "center"});', clickElement)

                logging.debug("Clicking element: " + clickElement.get_attribute('outerHTML'))
                clickElement.click()

                return (location['x'], location['y'], size['width'], size['height'])
        except Exception as e:
            logging.error(f"Unable to click element { clickElement.get_attribute('outerHTML') }\n{e}") #should we exit, or is this really a warning

    def navigate_up_to_node_link(self, nav_tree_node):
        ''' Navigate from the home page all the way up to the node
        @param: navTreeNode - NavTreeNode you want to navigate to
        '''
        try:
            href = nav_tree_node.page_item.href
            logging.debug(f"Navigating to: {href}")
            self._driver.get(href)
        except Exception as e:
            logging.error(f"Unable to navigate to {href}\n{e}") #should we exit, or is this really a warning

    def scroll_to_top(self):
        '''Scrolls to the top of the page
        '''
        self._driver.execute_script("window.scrollTo(0, 0);")

    def generate_nav_tree(self):
        '''Generates an json representation of what was navigated during the process
        '''
        if not os.path.exists(config_helper.get_report_location()):
            os.mkdir(config_helper.get_report_location())

        reportString = self.generate_nav_node_json(self._base_node)
        with open(config_helper.get_report_location() + '/navTree.json', 'w') as f:
            json.dump(reportString, f)

    def generate_nav_node_json(self, node):
        '''Generates the json of a navigation node for the report
        @param: node - A NavTreeNode instance
        '''

        page_item = node.page_item
        if page_item is None:
            html = 'parent'
        else:
            html = page_item.href

        nodeObj = {}

        nodeObj["nodeElement"] = html
        nodeObj["children"] = []

        for child in node.child_nodes:
            nodeObj["children"].append(self.generate_nav_node_json(child))

        return nodeObj