class NavTreeNode:

    '''Class used to create a navigation tree'''
    def __init__(self, page_item, node_level):
        # page item object for element
        self.page_item = page_item
        # node level. Root node starts at 0
        self.node_level = node_level
        # list of child NavTreeNodes
        self.child_nodes = []
        # Links or objects which designate how to navigate to this object
        self.navigation_path = []
        # sub items on this page
        self.sub_page_items = []
