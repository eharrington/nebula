import logging
from nebula.modules.image_capture import screenshot_processor as sp
from nebula.modules.image_capture import screenshot_pagehandler as sph
from nebula.modules.page_crawler import page_crawler as crawler
from nebula.modules.common import config_helper


class ImageActions:
    def __init__(self, webdriver):
        self._driver = webdriver
        self._screenshot_processor = sp.ScreenshotProcessor(self._driver)
        self._screenshot_pagehandler = sph.ScreenshotPageHandler(self._driver)
        self._crawler = crawler.PageCrawler(self._driver, self._screenshot_pagehandler)

    def screenshot_current_page(self, page_identifier='', failure=False):
        # don't take a screenshot if we don't have the image store define
        # doing this so tests won't fail without this
        image_store_path = config_helper.get_image_store()
        if image_store_path is None:
            logging.error("Unable to snapshot the page because the IMAGE_STORE_URL is not defined.")

        try:
            current_url = self._driver.current_url
            path = self._crawler.get_url_fragment(current_url)
            # page_identifier is the optional filename, aka screenshot the page as homepage where homepage is the page_identifier
            # and should save an image called homepage.png instead of page.png (default)
            if page_identifier == '':
                page_identifier = 'page'
        except AttributeError:
            logging.warning("Trying to take a screenshot when there is not page to take a screenshot of.")
            return


        return self._screenshot_processor.process_page(path, page_identifier, failure)

    def screenshot_entire_site(self, site_url):
        # don't take a screenshot if we don't have the image store define
        # doing this so tests won't fail without this
        image_store_path = config_helper.get_image_store()
        if image_store_path is None:
            logging.error("Unable to snapshot the page because the IMAGE_STORE_URL is not defined.")

        if site_url == '':
            site_url = self._driver.current_url
        return self._crawler.begin_crawl(site_url)