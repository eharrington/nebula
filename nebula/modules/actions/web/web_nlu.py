import json
import logging
import os
import re
import sys
from ast import literal_eval
from datetime import datetime
from pathlib import Path

from snips_nlu import SnipsNLUEngine, exceptions

from nebula.modules.actions.base import base_actions, base_nlu
from nebula.modules.actions.csa import csa_actions
from nebula.modules.actions.nlp import nlp_exception
from nebula.modules.actions.web import web_actions
from nebula.modules.common import config_helper, variable_store
from nebula.modules.common.base_colors import bcolors
from nebula.modules.common.variable_enum import Variable
from nebula.modules.report import invalid_test_exception as ite
from nebula.modules.report import nebula_test_exception as nte


class WebNlu(base_nlu.BaseNlu):

    def __init__(self, vs, browser):
        super().__init__(vs)
        self.web_action = None
        self.base_actions = None
        self.print_snips = True
        self.target_browser = browser

    def close_all(self):
        if self.web_action is not None:
            self.web_action.close_all()


    def do_action(self, test_line, nlu_engine, test_info):
        test_line = self.nlu_hack(test_line)
        response = nlu_engine.parse(test_line)
        if response['intent']['probability'] < 0.25:
            logging.warning(f"{bcolors.WARNING}Intent confidences of {response['intent']['probability']} is very low{bcolors.ENDC}")
        cutoff = config_helper.get_confidence_cutoff()
        if response['intent']['probability'] < cutoff:
            raise ite.InvalidTestException(f"Intent confidence of {response['intent']['probability']} falls below the {cutoff} cutoff you specified.")

        if self.base_actions is None:
            self.base_actions = base_actions.BaseActions(self.variable_store)

        try:
            intent_name = response['intent']['intentName']
        except KeyError:
            raise nlp_exception.NlpException(f"Response was not received from Snips")

        #undo the hack to stop snips from stripping trailing curly brace
        # TODO find a better solution for this
        for slot in response['slots']:
            if slot['value']['kind'] == "Custom":
                slot['value']['value'] = self.nlu_unhack(slot['value']['value'])
            slot['rawValue'] = self.nlu_unhack(slot['rawValue'])

        response['input'] = self.nlu_unhack(response['input'])
        logging.verbose(f"{bcolors.CYAN}With confidence of {response['intent']['probability']} Intent Returned: {intent_name} with parameters {response['slots']}{bcolors.ENDC}")

        test_info.intent_tracker['datetime'] = str(datetime.now())
        test_info.intent_tracker['intentName'] = intent_name
        test_info.intent_tracker['confidence'] = response['intent']['probability']
        test_info.intent_tracker['response'] = json.dumps(response['slots'])

        if (intent_name is None):
            #log this request with the Nebula_UI
            parameters = ""
            if len(response['slots']) > 0:
                parameters = ". Parameters: " + str(response['slots'])
            logging.error("Unable to map this text to an intent: " + response['input'] +  parameters + "\n")
            return

        elif (intent_name == 'addValue'):
            variable_name = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable', 'value', 'value'])
            variable_type = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=type', 'value', 'value'])
            variable_value = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=value', 'value', 'value']))
            position = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=position', 'value', 'value']))

            # if no position is supplied then we assume they mean the end of the variable
            if position is '':
                position = 'end'

            if variable_type == 'COLLECTION' and isinstance(variable_value, str) and variable_value.startswith("[") and isinstance(literal_eval(variable_value), list):
                variable_value = literal_eval(variable_value)

            self.variable_store.add_variable_value(variable_name, variable_type, variable_value, position)

            return

        elif (intent_name == 'createUnique'):
            variable_name = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable', 'value', 'value'])
            length = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=count', 'value', 'value'])
            uneeckifier = str(self.base_actions.get_unique_string())
            if 'UUID1' in response['input']:
                uneeckifier = str(self.base_actions.get_UUID1())

            if 'UUID4' in response['input']:
                uneeckifier = str(self.base_actions.get_UUID4())

            if length == '':
                length = len(uneeckifier)

            uneeckifier = uneeckifier[:int(length)]
            var_val = self.variable_store.var_replace(variable_name)

            if var_val == variable_name:
                self.variable_store.set_value("${"+variable_name+"}", uneeckifier)
            else:
                self.variable_store.set_value(variable_name, var_val+uneeckifier)

            return

        elif (intent_name == "execute"):
            if 'javascript_script' in response['input']:
                # Is the variable resolve necessary here?
                target = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
                script = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=command','rawValue']))
                if self.web_action is None:
                    self.web_action = web_actions.WebActions(self.variable_store, test_info, self.target_browser)
                self.web_action.execute_javascript(script, target)
            else:
                execCmdStr = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=command','rawValue']))
                if '$' in execCmdStr:
                    logging.verbose(f"{bcolors.WARNING}WARNING! Your command {response['input']} contains at least one '$'. The shell will interpret it as an environment variable.”.{bcolors.ENDC}")
                self.base_actions.execute(self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=command','rawValue'])))
            return



        elif (intent_name == 'filter'):
            self.base_actions.filter_using_regex(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=src_string', 'value', 'value']),
                                                self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=regex', 'value', 'value']),
                                                self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=dest_string', 'value', 'value']))

            return

        elif (intent_name == 'getLength'):
            self.base_actions.store_variable_length(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable','value','value']),
                                                    self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=length_variable','value','value']))
            return

        elif (intent_name == 'printValue'):
            variable = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable','rawValue'])
            self.base_actions.print_variable_value(variable)
            return

        elif (intent_name == 'setValue'):
            variable_name = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable', 'value', 'value'])
            variable_type = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=type', 'value', 'value'])
            variable_value = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=value', 'value', 'value']))

            if variable_type == 'COLLECTION' and isinstance(variable_value, str) and variable_value.startswith("[") and isinstance(literal_eval(variable_value), list):
                variable_value = literal_eval(variable_value)

            self.variable_store.set_variable_value(variable_name, variable_type, variable_value)
            return

        elif (intent_name == 'start_timer'):
            self.base_actions.start_timer()
            return

        elif (intent_name == 'stop_timer'):
            self.base_actions.stop_timer()
            return

        elif (intent_name == 'urlencode'):
            self.base_actions.urlencode(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=var_name','value','value']))
            return

        elif (intent_name == 'validateSortOrder'):
            target = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=collection', 'value', 'value']))
            operator = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=compare', 'value', 'value'])
            self.base_actions.variable_compare(target, "sort", operator)
            return

        elif (intent_name == 'verifyLengthCount'):
            operator = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=compare', 'value', 'value'])
            variable = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable','value','value']))
            exp_count = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=count','value','value']))

            # this is needed for when the count is a variable
            if exp_count == '':
                exp_count = float(self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=stringCount','value','value'])))

            if operator == '>':
                self.base_actions.variable_compare(variable, "length >", exp_count)
            elif operator == '<':
                self.base_actions.variable_compare(variable, "length <", exp_count)
            elif operator == '==' or operator == 'has':
                self.base_actions.variable_compare(variable, "length ==", exp_count)
            else:
                #Change this raise to something else
                raise nlp_exception.NlpException(f"The operator: {operator} is not valid for intent: {intent_name}\n")
            return

        elif (intent_name == 'waitForSeconds'):
            # Use raw value by default for accuracy
            wait_time = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=time','rawValue'])

            # If the raw value is a string like one, two, three, then we use the snips number value
            if not wait_time.isdigit():
                wait_time = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=time','value','value'])

            self.base_actions.wait_for(wait_time)
            return

        # All remaining intents require a web browser
        if self.web_action is None:
            self.web_action = web_actions.WebActions(self.variable_store, test_info, self.target_browser)

        if (intent_name == 'assignValueToVariable'):
            self.web_action.assign_element_value_to_variable(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']),
                                                           self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable','value','value']))

        elif (intent_name == 'acceptAlertBox'):
            self.web_action.accept_alert()

        elif (intent_name == 'backBrowser'):
            self.web_action.back_browser()

        elif (intent_name == 'changeResolution'):
            self.web_action.change_browser_resolution(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=resolution','value','value']))

        elif (intent_name == 'clearCookies'):
            self.web_action.clear_cookies()

        elif (intent_name == 'clearField'):
            item = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            item = self.web_action.find_textbox(item)
            self.web_action.enter_text(item, '')

        elif (intent_name == 'clickButton'):
            item = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            self.web_action.find_item_by_text(item, self.web_action.click_item)

        elif (intent_name == 'clickCheckbox'):
            item = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            self.web_action.find_checkbox(item, self.web_action.click_item)

        elif (intent_name == 'clickElementInSelectedRowFromTable'):
            rowContext = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=rowContext','value','value']))
            elementName = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=elementName','rawValue']))
            tableName = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=tableName','value','value']))
            if rowContext == '':
                logging.verbose(f"There was no context for row provided. Using: {elementName} instead")
                rowContext = elementName

            rowElements = self.web_action.find_rows_in_table(tableName, rowContext)
            if elementName != '':
                self.web_action.selected_row_from_table(rowElements, elementName, self.web_action.click_item)
            else:
                logging.verbose(f"There was no context to click on for {tableName} and {rowContext}")

        elif (intent_name == 'clickListItem'):
            listName = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=listName','value','value']))
            entry = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=entry','value','value']))
            element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            item = self.web_action.find_list_exact_entry(listName, entry, element)
            self.web_action.click_item(item)

        elif (intent_name == 'clickRadioButton'):
            item = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            item = self.web_action.find_element(item)
            self.web_action.click_item(item)

        elif (intent_name == 'closeBrowser'):
            self.web_action.close_browser(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=name','value','value']))

        elif (intent_name == 'closeCurrentWindow'):
            self.web_action.close_window()

        elif (intent_name == 'crawlAndScreenshot'):
            url = ''
            try:
                url = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=url','value','value'])
                logging.verbose(f"with parameter {url}")
            except:
                # do nothing as the command might not specify the url.
                # Then next method will take care of that
                pass
            self.web_action.screenshot_entire_site(url, response['input'])

        elif (intent_name == 'dismissAlertBox'):
            self.web_action.dimiss_alert()

        elif (intent_name == 'doubleClick'):
            item = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            self.web_action.find_item_by_text(item, self.web_action.double_click_on_element)

        elif (intent_name == 'dragAndDrop'):
            element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            destination = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=destination','rawValue']))
            self.web_action.drag_item_drop(element, destination)

        elif (intent_name == 'elementDoesntExist'):
            item = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            self.web_action.verify_item_does_not_exists(item)

        elif (intent_name == 'elementExists'):
            item = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            self.web_action.find_element(item)

        elif (intent_name == 'focus'):
            item = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            self.web_action.focus_on_element(item)

        elif (intent_name == 'getCollection'):
            collection_element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            collection_element = self.web_action.find_collection(collection_element)
            self.web_action.store_collection(collection_element, self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable','rawValue']))

        elif (intent_name == 'getTableValues'):
            table_element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            table_element = self.web_action.find_table(table_element)
            self.web_action.store_all_table_elements(table_element, self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable','value','value']))

        elif (intent_name == 'getValue'):
            element = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element', 'rawValue'])

            # adding tag to know this is a css property using a variable
            if element.startswith("${") and 'css' in response['input']:
                element = "_css_" + element

            # adding tag to know this is a attribute using a variable
            elif element.startswith("${") and 'attribute' in response['input']:
                element = "_attr_" + element

            element = self.variable_store.var_replace(element)
            variable = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable', 'value', 'value'])
            self.web_action.get_element_value_into_variable(response['input'], element, variable)

        elif (intent_name == 'hitKey'):
            key = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=key','value','value'])
            item = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            item = self.web_action.find_element(item)
            self.web_action.hit_key(key, item)

        elif (intent_name == 'inputText'):
            item = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            item = self.web_action.find_textbox(item)
            text = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=text','value','value']))
            self.web_action.enter_text(item, text)

        elif (intent_name == 'inputTextInList'):
            listname = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=list','value','value']))
            entry = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=entry','value','value']))
            element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            textbox = self.web_action.find_list_exact_entry(listname, entry, element)
            text_to_enter = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=text','rawValue']))
            self.web_action.enter_text(textbox, text_to_enter)

        elif (intent_name == 'inputTextInAlert'):
            alert = self.web_action.get_alert_obj()
            text_to_enter = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=text','value','value']))
            self.web_action.enter_text(alert, text_to_enter)

        elif (intent_name == 'isEnabledOrDisabled'):
            element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            if self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=state','value', 'value']) == 'enabled':
                self.web_action.verify_enabled(element)
            else:
                self.web_action.verify_disabled(element)

        elif (intent_name == 'islands'):
            action = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=action', 'value', 'value'])

            if action == 'delete':
                if 'every' in response['input'].lower() or 'all' in response['input'].lower():
                    self.web_action.delete_all_islands()
                else:
                    island_id = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=island_id','value','value']))
                    self.web_action.delete_island(island_id)

            elif action == 'update' or action == 'create':
                response_variable = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable', 'value', 'value'])
                island_data = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=island_data','value','value']))

                if action == 'update':
                    if 'every' in response['input'].lower() or 'all' in response['input'].lower():
                        self.web_action.update_all_islands(island_data, response_variable)
                    else:
                        island_id = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=island_id','value','value']))
                        self.web_action.update_island(island_data, island_id, response_variable)
                else:
                    self.web_action.create_island(island_data, response_variable)
            else:
                raise nlp_exception.NlpException(f"The action: {action} is not valid for intent: {intent_name}\n")

        elif (intent_name == 'listEntryDoesNotExist'):
            element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            entry = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=entry','value','value']))
            subelement = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=subElement','value','value']))
            self.web_action.verify_list_entry_does_not_exist(element, entry, subelement)

        elif (intent_name == 'listItemExists'):
            element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            entry = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=entry','value','value']))
            subelement = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=subElement','rawValue']))
            self.web_action.find_list_exact_entry(element, entry, subelement)

        elif (intent_name == 'moveMouse'):
            element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            select = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=select','rawValue']))
            self.web_action.move_mouse_to(element, select)

        elif (intent_name == 'moveSlider'):
            slider = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=slider','rawValue']))
            directory = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=dir','value','value'])
            self.web_action.move_slider(slider, directory,
                                        int(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=count','value','value'])))

        elif (intent_name == 'navigate'):
            navigation_url = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=url','value','value']))
            self.web_action.navigate_to_page(navigation_url,response['input'])

        elif (intent_name == 'openNewBrowser'):
            browser = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=browser','value','value']))
            if browser is '' or browser.lower() == 'default':
                browser = "Chrome"
            self.web_action.open_browser(browser,
                                         self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=name','value','value']),
                                         executable=self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=executable','value','value'])))

        elif (intent_name == 'radioButtonIsSelectedOrNot'):
            element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            if any(str in response['input'].lower() for str in ('click', 'enable')):
                self.web_action.verify_radio_enabled(element)
            elif any(str in response['input'].lower() for str in ('disable' or 'not')):
                self.web_action.verify_radio_disabled(element)

        elif (intent_name == 'refreshPage'):
            self.web_action.refresh_page()

        elif (intent_name == 'rightClickAndSelect'):
            element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            menuoption = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=menuOption','rawValue']))
            item = self.web_action.find_exact_item_by_text(element)
            self.web_action.right_click_select(item, menuoption)

        elif (intent_name == 'scrollIntoView'):
            element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element', 'rawValue']))
            self.web_action.scroll_list_to_bottom(response['input'], element)

        elif (intent_name == 'screenshot'):
            self.web_action.screenshot_current_page(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=imageName','value','value']).replace('\'', ''))

        elif (intent_name == 'selectFromDropdown'):
            dropdown = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            dropdown = self.web_action.find_dropdown(dropdown)
            select_option = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=select','value','value']))
            self.web_action.select_option_from_dropdown(dropdown, select_option)

        elif (intent_name == 'selectFileinDialogbox'):
            dialog_element = self.variable_store.var_replace(self.web_action.find_dialogfile(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue'])))
            select_file = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=select','value','value']))
            self.web_action.select_option_from_opendialog(dialog_element, select_file)

        elif (intent_name == 'switchToBrowser'):
            new_browser = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=name','value','value']))
            self.web_action.switch_to_browser(new_browser)

        elif (intent_name == 'switchToNewWindow'):
            self.web_action.switch_to_new_window()

        elif (intent_name == 'unfocus'):
            self.web_action.unfocus_on_element()

        elif (intent_name == 'validateInput'):
            target = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            operator = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=compare','value','value'])
            exp_value = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=expected','value','value']))

            if 'variable' in response['input']:  #TODO: This goes away when we switch variable processing
                if ('rgba(' in target or 'rgb(' in target):
                    exp_value = self.web_action.css_color_conversion_check(exp_value, target)
                    self.base_actions.variable_compare(target, operator, exp_value)
                self.base_actions.variable_compare(target, operator, exp_value)
            else:
                item = self.web_action.find_element(target)
                self.web_action.verify_element_value(item, exp_value)

        elif (intent_name == 'validateInputInList'):
            text_box = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=text_box','rawValue']))
            list_name = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=list_name','rawValue']))
            entry = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=entry','value','value']))
            list_item = self.web_action.find_item_in_list(text_box, list_name, entry)
            value = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=value','rawValue']))
            self.web_action.verify_element_value(list_item, value)

        elif (intent_name == 'validateCheckbox'):

            element = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=element','rawValue']))
            is_checked = False
            true_values = ['selected', 'checked', 'true', '1']
            checkboxValue = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=expected','value','value']))
            if checkboxValue.lower() in true_values: #or if variable_compare(true_values, "has", checkboxValue)
                is_checked = True
            self.web_action.verify_checkbox_value(element, is_checked)

        elif (intent_name == 'verifyCurrentUrl'):
            url = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=url','value','value']))
            operator = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=compare','value','value'])
            self.web_action.verify_current_url(operator, url)

        else:
            logging.critical(f"Cannot find an associated method for intent: {intent_name}\n")
            raise nlp_exception.NlpException(f"Cannot find an associated method for intent: {intent_name}\n")
