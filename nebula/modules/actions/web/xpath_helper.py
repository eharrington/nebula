'''
Builds XPATH for different types of selections

DO NOT USE CONTAINS!!!!

Contains often finds things that it should not. If needed, use translate to remove items from text or use
translate.
'''

def build_xpath_for_item_by_text(text):
    '''Build an xpath expression to search for items by text. Useful to find items to click.
    @param text - The text associated with the item
    '''
    xpaths = [
        ".//a[.='{0}']",
        ".//*[normalize-space(@id)='{0}']",
        ".//input[@type='button' and @value='{0}']",
        ".//input[@type='radio' and @value='{0}']",
        ".//*[@svgicon='{0}']",
        ".//input[@type='Submit' and @value='{0}']",
        ".//input[@value='{0}']",
        ".//*[normalize-space(text())='{0}']",
        ".//*[translate(text(), ':', '')='{0}']",
        ".//*[translate(text(), '?', '')='{0}']"
    ]

    xpath = " | ".join(xpaths).format(text)
    xpath = xpath.format(text)

    return xpath

def build_xpath_for_checkbox(checkbox_name):
    '''Build an xpath expression to search for a checkbox.
    @param checkbox_name - The name of the checkbox to search for
    '''
    text_xpath = ".//*[normalize-space(text())='{0}']".format(checkbox_name)
    xpaths = [
        "{0}//following-sibling::input[@type = 'checkbox']",
        "{0}//preceding-sibling::input[@type = 'checkbox']"
    ]
    xpath = " | ".join(xpaths).format(text_xpath)

    return xpath

def build_xpath_for_item_by_tag(element_tag):
    '''Build an xpath expression to search for items by tag. Useful to find items to click.
    @param element_tag - The tag associated with the item
    '''
    return ".//{0}".format(element_tag)

def build_xpath_to_find_textbox(text_box_name):
    '''Build an xpath expression to search for a textbox
    @param text_box_name - The name of the textbox to search for
    '''
    xpaths = [
        ".//*[normalize-space(@id)='{0}']",
        ".//*[@{1}='{0}']",  # find by placeholder
        ".//*[@{2}='{0}']",  # find by name
        ".//*[@{3}='{0}']",  # find by class
        ".//label[normalize-space(.)='{0}']/following::input[1]",
        ".//label[normalize-space(translate(., ':', ''))='{0}']/following::input[1]",
        ".//label[normalize-space(translate(., '?', ''))='{0}']/following::input[1]",
        ".//*[normalize-space(text())='{0}']//following-sibling::*/input[@type='text']",
        ".//*[normalize-space(text())='{0}']/following-sibling::*/textarea",
        ".//*[normalize-space(text())='{0}']/following-sibling::textarea"
    ]

    xpath = " | ".join(xpaths)
    xpath = xpath.format(text_box_name, "placeholder", "name", "class")

    return xpath

def build_xpath_to_find_elements_with_value(identifier):
    '''Builds an xpath expression to find generic things with value. Use this first then try searching for textboxes and checkboxes.
    @param identifier - The identifier to use to find the item
    '''
    xpaths = [
        ".//*[normalize-space(@id)='{0}']",
        ".//*[@{1}='{0}']",  # find by placeholder
        ".//*[@{2}='{0}']",  # find by name
        ".//*[@{3}='{0}']",  # find by class
        ".//label[normalize-space(.)='{0}']/following::input[1]",
        ".//*[normalize-space(text())='{0}']",
        ".//*[translate(text(), ':', '')='{0}']",
        ".//*[translate(text(), '?', '')='{0}']"
    ]

    xpath = " | ".join(xpaths)
    xpath = xpath.format(identifier, "placeholder", "name", "class")

    return xpath