import json
import logging
import os
import platform
import pprint
import re
import ssl
import threading
import time
from pathlib import Path

import requests
import yaml
from selenium import webdriver
from selenium.common.exceptions import (StaleElementReferenceException,
                                        TimeoutException, WebDriverException)
from selenium.webdriver import ActionChains
from selenium.webdriver.common import desired_capabilities
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support import ui
from selenium.webdriver.support.color import Color
from selenium.webdriver.support.ui import Select
from msedge.selenium_tools import Edge, EdgeOptions

from nebula import download_webdrivers as dw
from nebula.modules.actions.base import base_actions
from nebula.modules.actions.image import image_actions as ia
from nebula.modules.actions.web import (local_storage, session_storage,
                                        xpath_helper)
from nebula.modules.common import config_helper, variable_store
from nebula.modules.common.base_colors import bcolors
from nebula.modules.common.variable_enum import Variable
from nebula.modules.connection import browserstack
from nebula.modules.connection import saucelabs
from nebula.modules.report import invalid_test_exception as ite
from nebula.modules.report import nebula_test_exception as nte

ssl._create_default_https_context = ssl._create_unverified_context
os_base = platform.system()



class WebActions(base_actions.BaseActions):
    driver_install_sem = threading.Semaphore()

    def __init__(self, vs, test_info, browser):
        super().__init__(vs)
        self.test_info = test_info
        self.browser = browser
        self.driver = None
        self.drivers = {}
        self.islands_token = None
        self.islands_devices = []
        # Check for webdriver here, and download if not present
        self._check_for_webdriver()
        self.open_browser(self.browser, "default")

    def close_all(self):
        for name in self.drivers:
            self.drivers[name].quit()

##################### Browser Manipulation Methods #######################
## These methods manipulate the browser at a page granularity or larger
    def change_browser_resolution(self, new_browser_resolution):
        browser_resolution_list = [new_browser_resolution.split("x")[0], new_browser_resolution.split("x")[1]]
        self.driver.set_window_size(int(browser_resolution_list[0]),int(browser_resolution_list[1]))

    def clear_cookies(self):
        ''' Clears the browser cookies
        '''
        self.driver.delete_all_cookies()

    def clear_local_storage(self):
        # clear the local storage
        self._local_storage.clear()

    def clear_session_storage(self):
        # clear the session storage
        self._session_storage.clear()

    def get_local_storage(self, var_name):
        # save the local storage
        self.variable_store.set_value("${"+var_name+"}",
                                  json.dumps(self._local_storage.items()),
                                  Variable.COLLECTION)

    def get_session_storage(self, var_name):
        # save the session storage
        self.variable_store.set_value("${"+var_name+"}",
                                  json.dumps(self._session_storage.items()),
                                  Variable.COLLECTION)

    def set_local_storage(self, value, key):
        # check if its a variable
        temp_value = self.variable_store.get_value(value)
        if temp_value is not None:
            value = temp_value
        # store a value in the local storage
        self._local_storage.set(key, value)

    def set_session_storage(self, value, key):
       # check if its a variable
        temp_value = self.variable_store.get_value(value)
        if temp_value is not None:
            value = temp_value
        # store a value in the session storage
        self._session_storage.set(key, value)
    def switch_to_new_window(self):
        '''Switches to the newest window
        '''
        num_handles = len(self.driver.window_handles)
        if num_handles > 1:
            self.driver.switch_to.window(self.driver.window_handles[num_handles - 1])
        else:
            logging.warning(bcolors.WARNING + "No new window was open." + bcolors.ENDC)

    def close_window(self):
        '''Closes the current window and switches back to default window if there is one
        '''
        self.driver.close()
        num_handles = len(self.driver.window_handles)
        if num_handles != 0:
            self.driver.switch_to.window(self.driver.window_handles[num_handles - 1])
            self.wait_for_page_to_load()
        else:
            logging.warning(bcolors.WARNING + "No other windows open. Cannot switch to previous window" + bcolors.ENDC)

    def close_browser(self, window_name):
        ''' Closes the named browser. Closing the active window is a test exception unless it is the last one
        '''
        if self.drivers[window_name] is self.driver and len(self.drivers) > 1:
            raise ite.InvalidTestException("TEST EXCEPTION: Can not close the active browser unless it is the last one.  Switch to a different browser first.")
        self.drivers.pop(window_name).quit()

    def open_browser(self, browser, window_name, executable=None):
        '''Opens a new window and stores the session with the given name
            While a "browser" paramenter is accepted, nothing is done with it at this time.
        '''
        if window_name in self.drivers:
            raise ite.InvalidTestException(f"A test can only have one browser session named {window_name}.")
        self.driver = self._get_driver(browser, executable)
        self._local_storage = local_storage.LocalStorage(self.driver)
        self._session_storage = session_storage.SessionStorage(self.driver)
        self._image_actions = ia.ImageActions(self.driver)
        self.drivers[window_name] = self.driver
        # lowering this to 1 second
        self.wait = ui.WebDriverWait(self.driver, 1)
        self.root_element = self.driver

    def switch_to_browser(self, window_name):
        '''Switches to the specified named browser. It is a test error if the browser has not been created.
        '''
        try:
            self.root_element = self.driver = self.drivers[window_name] #note that assignment will not happen in case of an exception
        except (KeyError, NameError):
            raise Exception(f"Could not switch to {window_name}, because it does not exist.")

    def navigate_to_page(self, url, query):
        ''' Go to the page or file specified '''
        #query could be evn passed into the nebula so we need to parse the env
        url = self._parse_url(url, query)
        self.driver.get(url.replace("'","").replace('"','')) #necessary for URLs defined as variables that include hash symbols.
        self.wait_for_page_to_load()
        self.wait_for_angular()

    def refresh_page(self):
        ''' Refreshes the page
        '''
        self.driver.refresh()

    def wait_for_page_to_load(self):
        # wait until the dom loads

        # increasing wait to avoid false timeouts
        self.wait = ui.WebDriverWait(self.driver, 15)
        try:
            self.wait.until(
                ec.presence_of_element_located((By.CSS_SELECTOR, "div")))
            # allow for one stale ref and try again
        except StaleElementReferenceException:
            self.wait.until(
                ec.presence_of_element_located((By.CSS_SELECTOR, "div")))
            # reset wait back to default wait time
            self.wait = ui.WebDriverWait(self.driver, 1)
        except Exception as e:
            logging.exception(f"Warning: Issue loading page.\n{e}")
            # reset wait back to default wait time
            self.wait = ui.WebDriverWait(self.driver, 1)

    def wait_for_angular(self):
        should_wait_for_angular = self.variable_store.get_value('${WAIT_FOR_ANGULAR}')
        if str(should_wait_for_angular).lower() !='false':
            try:
                is_angular_app = self.driver.execute_script('return getAllAngularRootElements()[0].attributes["ng-version"];')
                if is_angular_app:
                    self.driver.set_script_timeout(10)
                    time_to_wait_for_angular = 0
                    while time_to_wait_for_angular <= 10:
                        is_angular_ready = self.driver.execute_script("return window.getAllAngularTestabilities().findIndex(x=>!x.isStable()) === -1")
                        logging.debug(f"{bcolors.WARNING}Waiting For Angular {time_to_wait_for_angular} -  {is_angular_ready}{bcolors.ENDC}")
                        if is_angular_ready: return True
                        time.sleep(0.1)
                        time_to_wait_for_angular += 0.1
                    return False
            except:
                # if it's not an angular app then we'll get an exception when we call the line below.
                # is_angular_app = self.driver.execute_script('return getAllAngularRootElements()[0].attributes["ng-version"];')
                pass
        else:
            logging.debug(f"{bcolors.WARNING}Bypassing Waiting For Angular{bcolors.ENDC}")

    def back_browser(self):
        ''' Back up a page on the currently active browser
        '''
        if self.driver is not None:
            self.driver.back()
            self.wait_for_page_to_load()
            self.wait_for_angular()

    def screenshot_current_page(self, page_identifier):
        temp_content = self.driver
        self.driver.switch_to_default_content()
        self._image_actions.screenshot_current_page(page_identifier)
        self.root_element = temp_content

    def screenshot_entire_site(self, url='', query=''):
        url = self._parse_url(url, query)
        return self._image_actions.screenshot_entire_site(url)

    def get_island_token(self):
        self.islands_token = self.variable_store.get_value('${EX_NEB_ISLANDS_TOKEN}')
        if self.islands_token == None:
            raise ite.InvalidTestException("You have to log in and retrieve a token, and set EX_NEB_ISLANDS_TOKEN before working with island.")

    def create_island(self, island_json, response_var,):
        # if we have no token then we need to get one
        if self.islands_token == None:
            self.get_island_token()
        endpoint = self.variable_store.get_value('${EX_NEB_ISLANDS_API}')
        if endpoint == None:
            raise ite.InvalidTestException("You have to define EX_NEB_ISLANDS_API before working with island.")
        headers = {"Authorization": f"Bearer {self.islands_token}"}
        response = requests.post(f"{endpoint}/devices", json=island_json, headers=headers)

        if response.status_code != 201:
            raise nte.NebulaTestException(f"Island not created. Status Code: {response.status_code}")
        if response_var != None:
            self.variable_store.set_value("${"+response_var+"}", response, Variable.RESTRESPONSE)

        # save the device ids so we can delete/update them later
        devices = json.loads(response.text)
        for device in devices['devices']:
            island_device_id = device['deviceId']
            logging.debug(f"Successfully created island with id: {island_device_id}")
            logging.debug(f"Island created with JSON: {island_json}")
            self.islands_devices.append(island_device_id)

            island_is_running = False
            wait_time = 120
            timestamp = time.time() + wait_time
            # polling the islands api to check if the island device is ready
            while time.time() < timestamp:
                try:
                    endpoint = self.variable_store.get_value('${EX_NEB_ISLANDS_API}')
                    if endpoint == None:
                        raise ite.InvalidTestException("You have to define EX_NEB_ISLANDS_API before working with island.")
                    headers = {"Authorization": f"Bearer {self.islands_token}"}
                    response = requests.get(f"{endpoint}/devices/{island_device_id}", json=island_json, headers=headers)
                    logging.verbose(f"Polling the island if ready: {island_device_id}")
                    # check if the island is up and running
                    if response.status_code == 200 and 'Running' in response.text:
                        island_is_running = True
                        break
                except:
                    pass
                # only poll every second
                time.sleep(1)

            if not island_is_running:
                raise nte.NebulaTestException(f"Island with id: {island_device_id} is not running.")

        # save the collection of island devices for access within a nebula test
        self.variable_store.set_value("${EX_NEB_ISLAND_DEVICES}", self.islands_devices, Variable.COLLECTION)

    def update_island(self, island_json, island_device_id, response_var):
        time.sleep(2)
        # we need to specify the device that we want to update
        island_json['device']['deviceId'] = island_device_id

        # if we have no token then we need to get one
        if self.islands_token == None:
            self.get_island_token()
        endpoint = self.variable_store.get_value('${EX_NEB_ISLANDS_API}')
        if endpoint == None:
            raise ite.InvalidTestException("You have to define EX_NEB_ISLANDS_API before working with island.")
        headers = {"Authorization": f"Bearer {self.islands_token}"}
        response = requests.put(f"{endpoint}/devices", json=island_json, headers=headers)

        if response.status_code != 200:
            raise nte.NebulaTestException(f"Island not updated. Status Code: {response.status_code}")
        if response_var != None:
            self.variable_store.set_value("${"+response_var+"}", response, Variable.RESTRESPONSE)

        logging.debug(f"Successfully updated island with id: {island_device_id}")
        logging.debug(f"Island updated with JSON: {island_json}")

    def update_all_islands(self, island_json, response_var):
        for island_device_id in self.islands_devices[:]:
            self.update_island(island_json, island_device_id, response_var)

    def delete_island(self, island_device_id):
        if self.islands_token == None:
            self.get_island_token()
        endpoint = self.variable_store.get_value('${EX_NEB_ISLANDS_API}')
        if endpoint == None:
            raise ite.InvalidTestException("You have to define EX_NEB_ISLANDS_API before working with island.")
        headers = {"Authorization": f"Bearer {self.islands_token}"}
        response = requests.delete(f"{endpoint}/devices/{island_device_id}", headers=headers)

        if response.status_code != 200:
            raise nte.NebulaTestException(f"Island not deleted. Status Code: {response.status_code}")

        logging.debug(f"Successfully deleted island with id: {island_device_id}")
        self.islands_devices.remove(island_device_id)
        self.variable_store.set_value("${EX_NEB_ISLAND_DEVICES}", self.islands_devices, Variable.COLLECTION)

    def delete_all_islands(self):
        for island_device_id in self.islands_devices[:]:
            self.delete_island(island_device_id)



##################### Page Manipulation Methods #######################
## Methods that manipulate whole web pages
    def move_mouse_to(self, element, select_item=''):
        ''' Move mouse to an area to hover and possible click an item
        @param element - The name of the element to move to
        @param select_item - The item to click on if provided
        '''
        action_chains = ActionChains(self.driver)
        item = self.find_element(element)

        if item is None:
            item = self.find_element(element)
        try:
            action_chains.move_to_element(item).perform()
        except:
            self.scroll_into_view(item)
            action_chains.move_to_element(item).perform()

        # To hover over a menu to reveal a submenu and click it
        # only click on something if select_item is not empty
        if select_item is not '':
            select = self.find_element(select_item)
            action_chains.click(select)
            action_chains.perform()

    def move_slider(self, slider, direction, count):
        ''' Move slider in direction specified
        @param slider - The name of the element to move
        @param direction - up, down, left or right
        @param count - how far to move the slider, each count being a press of an arrow button
        '''
        item = self.find_element(slider)
        arrow = None
        if direction == 'up':
            arrow = Keys.UP
        elif direction == 'down':
            arrow = Keys.DOWN
        elif direction == 'left':
            arrow = Keys.LEFT
        elif direction == 'right':
            arrow = Keys.RIGHT
        else:
            raise ite.InvalidTestException(f"{direction} is not a valid direction to move a slider.")
        for _ in range(count):
            item.send_keys(arrow)


    def unfocus_on_element(self):
        '''Returns the focus to the top of the page
        USES:
          - Bring the application back to the top of the DOM.
        '''
        self.driver.switch_to_default_content()
        self.root_element = self.driver

    def scroll_to_top(self):
        '''Scrolls to the top of the page
        '''
        self.driver.execute_script("window.scrollTo(0, 0);")

    def scroll_list_to_bottom(self, query, element):
        '''Scroll and element into view
        @param element - The web element to scroll into view
        '''

        if re.search(r'\blast\b|\bbottom\b', query.lower()):
            list_items = self.find_elements(element)
            current_rows = len(list_items)
            logging.debug(f"Current rows: {current_rows}")

            while True:
                self.driver.execute_script("return arguments[0].scrollIntoView();", list_items[-1])
                try:
                    ui.WebDriverWait(self.driver, 5).until(lambda x: len(self.find_elements(element)) > current_rows)
                    list_items = self.find_elements(element)
                    current_rows = len(list_items)
                    logging.debug(f"Updated rows: {current_rows}")
                except TimeoutException:
                    break
        else:
            item = self.find_element(element)
            self.scroll_into_view(item)

    def scroll_into_view(self, element):
        '''Scroll and element into view
        @param element - The web element to scroll into view
        '''
        return self.driver.execute_script("return arguments[0].scrollIntoView();", element)

    def _remove_hidden_elements(self, items):
        """Remove all items which are not displayed"""
        new_list = []
        logging.debug("Removing items which are hidden/not displayed")
        for item in items:
            if item.is_displayed():
                new_list.append(item)

        return new_list

    def _handle_frames(self, func):
        return_val = None
        try:
            iframes = self.root_element.find_elements_by_xpath(".//iframe")

            # put iframes in dict list so that we can add to it if we find dynamically created iframes
            iframe_list = []
            for f in iframes:
                iframe_list.append({'iframe': f, 'parent': None})

            i = 0
            item_found = False
            while i < len(iframe_list):
                try:
                    self._switch_to_default_content()
                    iframe_dict = iframe_list[i]
                    iframe = iframe_dict['iframe']
                    parent = iframe_dict['parent']

                    if parent is not None:
                        self.driver.switch_to_frame(parent)

                    self.driver.switch_to_frame(iframe)
                    return_val = func()
                    if len(return_val) > 0:
                        item_found = True
                        break
                except Exception as e:
                    logging.debug(e)
                    pass
                finally:
                    i += 1
                    newFrames = self.root_element.find_elements_by_xpath(".//iframe")
                    for f in newFrames:
                        iframe_list.append({'iframe': f, 'parent': iframe})

        finally:
            if item_found:
                return return_val
            else:
                return []

    def _has_iframes(self):
        return len(self.root_element.find_elements_by_xpath(".//iframe")) > 0


#################### Search Methods ######################################
#Using the most basic Selenium interface, find and return a list of web
#elements based on the given criteria
    def _execute_search_xpath(self, webelement, xpath):
        '''Execute xpath search
        PRIVATE METHOD
        @param xpath - XPATH to use in the search
        @returns List of Web Elements
        '''
        try:
            items = webelement.find_elements_by_xpath(xpath)
        except StaleElementReferenceException as stfe:
            logging.debug(stfe)
            items = webelement.find_elements_by_xpath(xpath)
        return self._remove_hidden_elements(items)

    def _execute_search_css(self, webelement, cssSelector):
        '''Execute css search
        PRIVATE METHOD
        @param cssSelector - CSS to use in the search
        @returns List of Web Elements
        '''
        try:
            items = webelement.find_elements_by_css_selector(cssSelector)
        except StaleElementReferenceException as stfe:
            logging.debug(stfe)
            items = webelement.find_elements_by_css_selector(cssSelector)
        return self._remove_hidden_elements(items)

    def _execute_search_tag(self, webelement, tag_name):
        '''Execute search for items with tag
        PRIVATE METHOD
        @param tag_name - tag to use in the search
        @returns List of Web Elements
        '''
        try:
            items = webelement.find_elements_by_tag_name(tag_name)
        except StaleElementReferenceException as stfe:
            logging.debug(stfe)
            items = webelement.find_elements_by_tag_name(tag_name)
        return self._remove_hidden_elements(items)

    def _execute_search_id(self, webelement, idSelector):
        '''Execute ID search
        PRIVATE METHOD
        @param idSelector - ID to use in the search
        @returns List of Web Elements
        '''
        try:
            items = webelement.find_elements_by_id(idSelector)
        except StaleElementReferenceException as stfe:
            logging.debug(stfe)
            items = webelement.find_elements_by_id(idSelector)
        if len(items) is 0:
            self._switch_to_default_content()
            if self._has_iframes():
                items = self._handle_frames(lambda: webelement.find_elements_by_id(idSelector))
        if items is not None:
            return self._remove_hidden_elements(items)

    def _handle_search(self, selector_key, selector_option='self_selector', sub_selector_text=None, webelement=None):
        selector = selector_key
        if webelement is None:
            webelement = self.root_element
        if sub_selector_text is not None:
            selector = selector.format(sub_selector_text)

        if selector.startswith('xpath'):
            selector = selector[len('xpath('):-1]
            selector_element = self._execute_search_xpath(webelement,  selector)
        elif selector.startswith('css'):
            selector = selector[len('css('):-1]
            selector_element = self._execute_search_css(webelement, selector)
        elif selector.startswith('id'):
            selector = selector[len('id('):-1]
            selector_element = self._execute_search_id(webelement, selector)
        elif selector.startswith('tag'):
            selector = selector[len('tag('):-1]
            selector_element = self._execute_search_tag(webelement, selector)

        return selector_element


########## Element Manipulation Methods ##############################################

    def store_collection(self, collection_name, variable):
        self.variable_store.set_value("${"+variable+"}", str(collection_name), Variable.COLLECTION)

    def double_click_on_element(self, item):
        ''' Double clicks on webelement
        @param item - webelement to perform the double click action
        '''
        try:
            actionChains = ActionChains(self.driver)
            actionChains.double_click(item).perform()
        except:
            raise nte.NebulaTestException(f"Unable to double click on item.")

    def verify_element_value(self, element_item, value):
        '''Verifies an elements value is the same as what is specified
        @param element_item - webelement to get its value
        @param value - The expected value
        USES:
          - Use this method when verifying the value of basic html elements
        '''
        if element_item:
            try:
                actualValue = self.get_item_value(element_item)
            except:
                raise nte.NebulaTestException(f"There was no WebElement visible to verify its value.")
        else:
            raise nte.NebulaTestException(f"There was NO element found.")

        if actualValue is not None:
            cleartext = self.variable_store.var_replace(value)
            # check if the value matches the text of the element
            self.variable_compare(actualValue,"==",cleartext)

        else:
            raise nte.NebulaTestException(f"Unable to get the actual value.")

    def assign_element_value_to_variable(self, element_name, variable):
        '''Finds the element and assigns the value to the global variable dictionary
        @param element_name - Text associated with the element to find
        @param variable - The variable to assign the value to
        USES:
          - Use this method when needing to save the value of basic html elements
          - This may not find values in complicated elements
          - Does use a selector file entry if present
          - Searches recursively in IFrames
        '''
        item = self.find_element(element_name)
        if item is None:
            if self._has_iframes():
                item = self._handle_frames(lambda: self.find_element(element_name))
            else:
                raise nte.NebulaTestException(f"Could not find: {element_name}.")
        actualValue = self.get_item_value(item)
        if actualValue is not None:
            self.variable_store.set_value("${"+variable+"}", actualValue)
        else:
            raise nte.NebulaTestException(f"{element_name} does not have a value.")

    def store_all_table_elements(self, table_element, variable):
        '''Copies everything from a table element to a two dimensional array,
        the inner array being composed of rows. The array is then stored in the
        global variable dictionary
        @param table_element - WebElement that refers to a table
        @param variable - The variable to assign the value to
        USES:
          - Use this method when needing to save contents of a table
        '''
        rows = table_element.find_elements_by_tag_name("tr") # get all of the rows in the table
        row_list = []
        for row in rows:
            # Get the columns (all the column 2)
            cols = row.find_elements_by_tag_name("td")
            col_list = []
            for col in cols:
                col_list.append(col.text)
            row_list.append(col_list)
        if len(row_list) is 0:
            raise nte.NebulaTestException(f"Unable to find rows in table.")
        self.variable_store.set_value("${"+variable+"}", str(row_list), Variable.COLLECTION)

    def find_rows_in_table(self, table_selector, row_value):
        '''Finds a row within a table passed on the parameters table_name and row_value. If table_name is null, it
        will look for row selectors only
        '''
        self._switch_to_default_content()
        xpaths = [
            ".//tr/td//*[normalize-space(text())='{0}']", ".//tr/td//*[@name='{0}']"
        ]
        xpath = " | ".join(xpaths).format(row_value)
        if table_selector is not None and len(table_selector) > 0:
            table_element = self.find_table(table_selector)
            row_elements = self._execute_search_xpath(table_element, xpath)
            # no table
        else:
            row_elements = self._execute_search_xpath(self.root_element, xpath)

        if len(row_elements) is 0:
            if self._has_iframes():
                return self._handle_frames(lambda: self._execute_search_xpath(self.root_element, xpath))[0]
            raise nte.NebulaTestException(f"Unable to find row {row_value}.")
        return row_elements

    def drag_item_drop(self, element, destination):
        ''' Drag and drop an item from one element to another
        @param element - The name of the element to click and drag
        @param destination - End point of the element being drug.
        '''
        action_chains = ActionChains(self.driver)
        source = self.find_element(element)
        targetElement = self.find_element(destination)

        action_chains.drag_and_drop(source, targetElement).perform()

    def focus_on_element(self, element):
        ''' Find and change root_element to that element
        @param element - string mechanism to find the element
        '''
        item = self.find_element(element)
        if item is not None:
            self.root_element = item
            return
        if self._has_iframes():
            item = self._handle_frames(lambda: self.find_element(element))
            if item is None:
                raise nte.NebulaTestException(f"Unable to find a item: '{element}'.")
            self.root_element = item
        else:
            raise nte.NebulaTestException(f"Unable to find a item: '{element}'.")

    def right_click_select(self, element, select_menu_option):
        ''' Right click on an item and select a sub-menu item
        @param element_name - The name of the element to find and right click.
        @param select_menu_option - The menu option element to click.
        '''
        action_chains = ActionChains(self.driver)
        action_chains.context_click(element).perform()
        select = self.find_element(select_menu_option)
        action_chains.click(select).perform()

    def list_click_option(self, list_option_element, sub_element=None):
        '''Click a value in a list.
        @param list_option_element - The option in the list to select
        @param sub_element - If supplied, goes down one level from the list
        '''
        if list_option_element is None:
            raise nte.NebulaTestException(f"Unable to identify list option.")
        if sub_element is None or sub_element is '':
            list_option_element[0].click()
        else:
            list_option_element[0].find_element_by_tag_name(sub_element).click()

    def click_item(self, item):
        '''Perform a click action
        @param item - Webelement object
        '''
        if item is None:
            raise nte.NebulaTestException(f"Unable to identify item to click.")
        item.click()
        # this is to check if we have an alert and if we don't then check for angular because check for angular dismisses the alert
        try:
            self.driver.switch_to.alert
        except:
            self.wait_for_angular()
            pass


    def get_alert_obj(self):
        '''Returns the alert object
        '''
        return self.driver.switch_to.alert

    def accept_alert(self):
        '''Perform a accept alert action on an alert box
        '''
        self.driver.switch_to.alert.accept()

    def dimiss_alert(self):
        '''Perform a dimiss alert action on an alert box
        '''
        self.driver.switch_to.alert.dismiss()

    def hit_key(self, key, item):
        '''Send a key_code to whatever has the focus
        @param item - The webelement to address
        @param key - The key to send to the webpage
        '''
        if item is None:
            raise nte.NebulaTestException(f"Unable to identify where to send {key}.")
        item.send_keys(self.key_lookup(key))

    def key_lookup(self, key_name):
        '''Returns the Selenium Keys object for the named key'''
        if key_name == "ADD":
            return Keys.ADD
        if key_name == "ALT":
            return Keys.ALT
        if key_name == "ARROW_DOWN":
            return Keys.ARROW_DOWN
        if key_name == "ARROW_LEFT":
            return Keys.ARROW_LEFT
        if key_name == "ARROW_RIGHT":
            return Keys.ARROW_RIGHT
        if key_name == "ARROW_UP":
            return Keys.ARROW_UP
        if key_name == "BACKSPACE":
            return Keys.BACKSPACE
        if key_name == "CANCEL":
            return Keys.CANCEL
        if key_name == "CLEAR":
            return Keys.CLEAR
        if key_name == "COMMAND":
            return Keys.COMMAND
        if key_name == "CONTROL":
            return Keys.CONTROL
        if key_name == "DECIMAL":
            return Keys.DECIMAL
        if key_name == "DELETE":
            return Keys.DELETE
        if key_name == "DIVIDE":
            return Keys.DIVIDE
        if key_name == "END":
            return Keys.END
        if key_name == "ENTER":
            return Keys.ENTER
        if key_name == "EQUALS":
            return Keys.EQUALS
        if key_name == "ESCAPE":
            return Keys.ESCAPE
        if key_name == "F1":
            return Keys.F1
        if key_name == "F10":
            return Keys.F10
        if key_name == "F11":
            return Keys.F11
        if key_name == "F12":
            return Keys.F12
        if key_name == "F2":
            return Keys.F2
        if key_name == "F3":
            return Keys.F3
        if key_name == "F4":
            return Keys.F4
        if key_name == "F5":
            return Keys.F5
        if key_name == "F6":
            return Keys.F6
        if key_name == "F7":
            return Keys.F7
        if key_name == "F8":
            return Keys.F8
        if key_name == "F9":
            return Keys.F9
        if key_name == "HELP":
            return Keys.HELP
        if key_name == "HOME":
            return Keys.HOME
        if key_name == "INSERT":
            return Keys.INSERT
        if key_name == "SHIFT":
            return Keys.SHIFT
        if key_name == "META":
            return Keys.META
        if key_name == "MULTIPLY":
            return Keys.MULTIPLY
        if key_name == "NULL":
            return Keys.NULL
        if key_name == "NUMPAD0":
            return Keys.NUMPAD0
        if key_name == "NUMPAD1":
            return Keys.NUMPAD1
        if key_name == "NUMPAD2":
            return Keys.NUMPAD2
        if key_name == "NUMPAD3":
            return Keys.NUMPAD3
        if key_name == "NUMPAD4":
            return Keys.NUMPAD4
        if key_name == "NUMPAD5":
            return Keys.NUMPAD5
        if key_name == "NUMPAD6":
            return Keys.NUMPAD6
        if key_name == "NUMPAD7":
            return Keys.NUMPAD7
        if key_name == "NUMPAD8":
            return Keys.NUMPAD8
        if key_name == "NUMPAD9":
            return Keys.NUMPAD9
        if key_name == "PAGE_DOWN":
            return Keys.PAGE_DOWN
        if key_name == "PAGE_UP":
            return Keys.PAGE_UP
        if key_name == "PAUSE":
            return Keys.PAUSE
        if key_name == "RETURN":
            return Keys.RETURN
        if key_name == "SEMICOLON":
            return Keys.SEMICOLON
        if key_name == "SEPARATOR":
            return Keys.SEPARATOR
        if key_name == "SHIFT":
            return Keys.SHIFT
        if key_name == "SPACE":
            return Keys.SPACE
        if key_name == "SUBTRACT":
            return Keys.SUBTRACT
        if key_name == "TAB":
            return Keys.TAB

    def enter_text(self, item, text_to_enter):
        ''' Enters text in webelement
        PUBLIC METHOD
        @param item - The webelement to enter to into
        @param text_to_enter - The text to enter.
        '''
        if item is None:
            raise nte.NebulaTestException("Unable to identify textbox to enter.")

        # alerts can't be scrolled into view or cleared
        if not isinstance(item, Alert):
            self.scroll_into_view(item)
            self.clear(item)
        cleartext = self.variable_store.var_replace(text_to_enter)
        item.send_keys(cleartext)

    def execute_javascript(self, script, item):
        ''' Executes javscript in the browser
        PUBLIC METHOD
        @param script - The javscript to execute.
        @param item - The webelement to execute the script on
        '''
        if not item == '':
            elem = self.find_element(item)
            self.driver.execute_script(script, elem)
        else:
            # executing a script without any arguments
            self.driver.execute_script(script)

    def clear(self, item):
        '''Performs a uniform clear
        @param item - Web Element
        '''
        if item is None:
            raise nte.NebulaTestException(f"Clear requires a textbox.")
        if os_base == 'Darwin':
            item.send_keys(Keys.COMMAND + "a")
            item.send_keys(Keys.DELETE)
        else:
            item.send_keys(Keys.CONTROL + "a")
            item.send_keys(Keys.DELETE)
        item.clear()

    def get_element_value_into_variable(self, query, element, var_name):
        '''
        This method will get the values related to an element and store them in
        a variable. The values can be the text value, an attribute, or a CSS
        property
        @param query - The input query by the user. Used to check for attribute/css/element
        @param element - The element selector used to find the web element
        @param var_name - The name of the variable in which to store the value
        '''
        try:
            if 'attribute' in query:
                item, attribute = self.variable_store._parse_var_name(element)
                web_element = self.find_element(item)
                var_value = self.get_attribute_value(web_element, attribute[0])

            if 'css' in query:
                item, css_property = self.variable_store._parse_var_name(element)
                web_element = self.find_element(item)
                var_value = self.get_css_property(web_element, css_property[0])

            if 'element' in query:
                item = self.find_element(element)
                var_value = self.get_item_value(item)

            if 'alert' in query:
                alert = self.get_alert_obj()
                var_value = alert.text

            self.variable_store.set_variable_value('${'+var_name+'}', 'PRIMITIVE', var_value)
        except IndexError as ie:
            logging.warning("Encountered an index error. Is the attribute/CSS property correctly specified in []?")
            raise ie

    def get_item_value(self, item):
        '''Returns the value attribute of the web element
        @param item - Web Element
        @returns value
        '''
        if item is not None:
            self.scroll_into_view(item)
            value = item.get_attribute('value')
        else:
            raise nte.NebulaTestException(f"Unable to get a value from: {item}")
        if value is None or value == '':
            return item.text
        else:
            return value

    def selected_row_from_table(self, row_elements, text, func=None):
        '''Finds a row in a table based on some type of row_context in that row, and click ons an element on that
        row based on the passed in text. The text could be the name, id, etc of the clickable element.
        Used for cases where we want to click certain links/buttons/icons on a specific row.
        PUBLIC METHOD
        '''
        # Go through the list of rows that possibly match and see if we can click on an element that has the text.
        for row in row_elements:
            parent = row.find_element_by_xpath('..')
            try:
                xpaths = [
                    "./*[normalize-space(text())='{0}']", "./*[@name='{0}']"
                ]
                xpath = " | ".join(xpaths).format(text)
                row_item = self._execute_search_xpath(parent, xpath)
                if len(row_item) > 0:
                    row_item = row_item[0]
                    if func is not None:
                        try:
                            func(row_item)
                            # possible stale element here try once again
                        except StaleElementReferenceException as stfe:
                            logging.debug(stfe)
                            row_item = self._execute_search_xpath(parent, xpath)
                            if len(row_item) > 0:
                                row_item = row_item[0]
                                func(row_item)
                            break
                    else:
                        return row_item
                    break
            except:
                continue

    def select_option_from_dropdown(self, dropdown_item, select_menu_option):
        '''Select a value in a dropdown.
        @param dropdown_item - The webelement of the dropdown
        @param select_menu_option - The option in the dropdown to select
        USES:
          - Best way for this to work is by using a selector file. See the README.md on how to use
          - This will work as is on HTML Select elements
          - Performs the translation of the text if available
        '''
        select_menu_option = self.get_translation(select_menu_option)
        if isinstance(dropdown_item, Select):
            # select by visible text
            dropdown_item.select_by_visible_text(select_menu_option)
        else:
            if dropdown_item is not None and len(dropdown_item) > 0:
                dropdown_item[0].click()
                self.wait_for(1)
                self.find_exact_item_by_text(select_menu_option).click()
            else:
                raise nte.NebulaTestException(f"Unable to find dropdown/menu option associated with: {select_menu_option}.")

    def select_option_from_opendialog(self, dialog_button, dialogfile):
        '''Select a value in a dropbox.
        @param dialog_button - The webelement of the dialog button
        @param dialogfile - The option in the opendialog to select
        USES:
          - Best way for this to work is by using a selector file. See the README.md on how to use
          - This will work as is on HTML Select elements
          - Performs the translation of the text if available
        '''

        try:
            dialogfile = self.get_translation(dialogfile)
            dialog_button.send_keys(f"{dialogfile}")
        except:
            raise nte.NebulaTestException(f"Unable to find file associated with: {dialogfile}.")

########## FIND METHODS  ##############################################
    def find_collection(self, collection_name):
        collection_list = []
        if len(collection_name) > 0:
            collection_elements = self.find_elements_with_attribute(collection_name)

        if len(collection_elements) is 0:
            collection_elements = self.find_elements(collection_name)

        if len(collection_elements) is 0:
            raise nte.NebulaTestException(f"Unable to find any elements in collection '{collection_name}'")

        for collection_element in collection_elements:
            collection_list.append(self.get_item_value(collection_element))

        return collection_list

    def find_table(self, table_name, row_text=None):
        '''Finds a table by name or that contacts a row_text
        @param table_name - Text associated with the table element to find
        USES:
          - Does use a selector file entry if present
          - Searches recursively in IFrames
        '''
        table_element = None

        if len(table_name) > 0:
            text_to_search = self.variable_store.var_replace(table_name)
            selector = self._handle_selectors(text_to_search)
            if selector is not None:
                table_element = self._handle_search(selector)
                if len(table_element) <= 0:
                    if self._has_iframes():
                        table_element = self._handle_frames(lambda: self._handle_search(selector))
                        if len(table_element) > 0:
                            table_element = table_element[0]
                    raise nte.NebulaTestException(f"Unable to find table using selector '{selector}'.")
                else:
                    table_element = table_element[0]
            else:
                table_element = self.find_element(table_name)
        else:
            self._switch_to_default_content()
            table_element = self.find_element(row_text)
            # need to make sure its table element
            while table_element.__getattribute__("tag_name") != 'table' and table_element.__getattribute__("tag_name") != 'tbody':
                table_element = table_element.find_elements_by_xpath('..')
                if isinstance(table_element, webdriver.remote.webelement.WebElement) is False:
                     table_element = table_element[0]
        if table_element is None:
            raise nte.NebulaTestException(f"The table named {table_name} was not found.")

        self.scroll_into_view(table_element)
        return table_element

    def find_exact_item_by_text(self, text):
#TODO: Almost the exact same code as below.  Combine the two or have this one call that one with item_num set to zero
        '''Find an single item by it's text. Does not look for inputs. Returns the web element
        PUBLIC METHOD
        @param text - The text of the item to find and click
        @returns WebElement
        USES:
          - Looks for anchor tags, inputs, SVG images, buttons, etc
          - Can use this to find an exact item and return it's web element to retrieve it's value
          - DO NOT call publicly. Only call from within this class
          - Translation needs to have already happened
        '''
        text_to_search = self.variable_store.var_replace(text)
        selector = self._handle_selectors(text_to_search)
        if selector is not None:
            results = self._handle_search(selector)
            if len(results) > 0:
                return results[0]
            else:
                if self._has_iframes():
                    item = self._handle_frames(lambda: self._handle_search(selector))
                    if len(item) > 0:
                        return item[0]
                    raise nte.NebulaTestException(f"Unable to find any item associated with '{text}' using selector '{selector}'.")
        else:
            text_to_search = self.get_translation(text_to_search)
            xpath = xpath_helper.build_xpath_for_item_by_text(text_to_search)
            item = self._execute_search_xpath(self.root_element, xpath)

            if len(item) > 0:
                return item[0]
            else:
                if self._has_iframes():
                    item =  self._handle_frames(lambda: self._execute_search_xpath(self.root_element, xpath))
                    if len(item) > 0:
                        return item[0]
                    raise nte.NebulaTestException(f"Unable to find any item associated with '{text}' using xpath '{xpath}'.")

    def find_checkbox(self, checkbox_name, func=None):
        '''Find a checkbox.
        PUBLIC METHOD
        @param checkbox_name - The name of the checkbox to find
        USES:
          - When you need to find a checkbox
          - Can be used in a recursive search but does not search IFrames
          - DO NOT call publicly. Only call from within this class
        '''
        items = self.find_checkboxes(checkbox_name)
        if len(items) > 0:
            if func is not None:
                try:
                    func(items[0])
                except StaleElementReferenceException as stfe:
                    logging.debug(stfe)
                    items = self.find_checkboxes(checkbox_name)
                    if len(items) > 0:
                        func(items[0])
                    else:
                        raise nte.NebulaTestException(f"Unable to find a checkbox using: {checkbox_name}.")
            else:
                return items[0]
        else:
            if func is not None:
                raise nte.NebulaTestException(f"Unable to find a checkbox using: {checkbox_name}.")

    def find_textbox(self, text_box_name):
        ''' Finds a text box.
        PUBLIC METHOD
        @param text_box_name - The name of the text box to find
        USES:
          - When you need to find a textbox or text input
          - Can be used in a recursive search but does not search IFrames
          - DO NOT call publicly. Only call from within this class
        '''
        items = self.find_textboxes(text_box_name)
        if items is not None and len(items) > 0:
            return items[0]
        raise nte.NebulaTestException(f"Unable to find a textbox using: {text_box_name}.")

    def find_elements_with_attribute(self, attribute_name):
        # Search by selector first
        selector = self._handle_selectors(attribute_name)
        if selector is not None:
            items = self._handle_search(selector)
            if len(items) > 0:
                return items

        items = self.find_textboxes(attribute_name)

        if len(items) > 0:
            return items

        items = self.find_checkboxes(attribute_name)

        if len(items) > 0:
            return items

        xpath = xpath_helper.build_xpath_to_find_elements_with_value(attribute_name)
        items = self._execute_search_xpath(self.root_element, xpath)

        if items is not None and len(items) > 0:
            return items
        return []

    def find_textboxes(self, text_box_attribute):
        # Search by selector first
        selector = self._handle_selectors(text_box_attribute)
        self._switch_to_default_content()
        if selector is not None:
            items = self._handle_search(selector)
            if len(items) is 0 and self._has_iframes():
                items = self._handle_frames(lambda: self._handle_search(selector))
        else:
            text_box_name = self.get_translation(text_box_attribute)
            xpath = xpath_helper.build_xpath_to_find_textbox(text_box_name)
            items = self._execute_search_xpath(self.root_element, xpath)
            if len(items) is 0 and self._has_iframes():
                items = self._handle_frames(lambda: self._execute_search_xpath(self.root_element, xpath))

        return items

    def find_checkboxes(self, checkbox_attribute):
        # Search selectors first
        selector = self._handle_selectors(checkbox_attribute)
        if selector is not None:
            try:
                items = self._handle_search(selector)
                # lets not force people to add attribute_selector as no one is really using it
            except KeyError:
                items = self._handle_search(selector)
        else:
            checkbox_attribute = self.get_translation(checkbox_attribute)
            xpath = xpath_helper.build_xpath_for_checkbox(checkbox_attribute)
            items = self._execute_search_xpath(self.root_element, xpath)

        return items

    def find_item_in_list(self, identifier, list_name, entry):
        '''Find list and item and get it's value.
        PUBLIC METHOD
        @parm identifier - The text or name of the element you are trying to find
        @param list_name - The name of the list to find
        @param entry - which row in the list to select
        USES:
          - Use this first for a general search. You then may need to do a more specific search if this does not return anything
          - Can be used in a recursive search but does not search IFrames
          - DO NOT call publicly. Only call from within this class
          - Translation needs to have already happened
        '''
        try:
            item = self.find_list_exact_entry(list_name, entry, identifier)
            if item is None:
                raise nte.NebulaTestException(f"Unable to find item associated with {identifier}. Searching iframes...")
            return item
        except Exception as e:
            if isinstance(e, nte.NebulaTestException):
                logging.verbose(e)
            else:
                logging.exception(f"Error searching for {identifier}: {str(e)}")
#TODO:  This conditional only performed in the case of an exception?
            if self._has_iframes():
                return self._handle_frames(
                    lambda: self.find_element(identifier) if (list_name is None or list_name.strip() is '') else self.find_list_exact_entry(list_name, entry, identifier)
                )

    def find_element_in_table_to_click(self, parent_name, text):
        '''Helps to find an element to click on based on a parent and the text/value that the element has.
        PUBLIC METHOD
        '''
        attrs = self.driver.execute_script(
            'var items = {}; for (index = 0; index < arguments[0].attributes.length;'
            ' ++index) { items[arguments[0].attributes[index].name] = '
            'arguments[0].attributes[index].value }; return items;',
            parent_name)
        logging.verbose(str(attrs))
        all_children_by_xpath = parent_name.find_elements_by_xpath(".//*")
        for child in all_children_by_xpath:
            if child.text.find(text) != -1:
                return child
            attrs = self.driver.execute_script(
                'var items = {}; for (index = 0; index < arguments[0].attributes.'
                'length; ++index) { items[arguments[0].attributes[index].name] = '
                'arguments[0].attributes[index].value }; return items;', child)
            if str(attrs).find(text) != -1:
                return child

    def find_dropdown(self, dropdown_name):
        '''Select a value in a dropdown.
        @param dropdown_name - The name of the dropdown to find
        @param select_menu_option - The option in the dropdown to select
        USES:
          - Best way for this to work is by using a selector file. See the README.md on how to use
          - This will work as is on HTML Select elements
          - Performs the translation of the text if available
          - Searches recursively in IFrames for HTML Selects
        '''
        selectors = self._handle_selectors(dropdown_name)
        dropdown_name = self.get_translation(dropdown_name)

        selection = None
        if selectors is not None:
            item = self._handle_search(selectors)
            if len(item) > 0:
                selection = item
            else:
                if self._has_iframes():
                    selection = self._handle_frames(lambda: self._handle_search(selectors))
        else:
            selection = Select(self.find_element(dropdown_name))
        if selection is None:
            raise nte.NebulaTestException(f"The dropdown {dropdown_name} was not found on the page. Selectors was {selectors}")
        return selection

    def find_dialogfile(self, dialog_button):
        '''Select a file in the open dialog.
        @param dialog_button - The name of the button to open the dialog
        @param dialog_file - The file in the open dialog to select
        USES:
          - Best way for this to work is by using a selector file. See the README.md on how to use
          - This will work as is on HTML Select elements
          - Performs the translation of the text if available
          - Searches recursively in IFrames for HTML Selects
        '''
        selectors = self._handle_selectors(dialog_button)
        dialog_button = self.get_translation(dialog_button)

        selection = None
        if selectors is not None:
            item = self._handle_search(selectors)
            if len(item) > 0:
                selection = item
            else:
                if self._has_iframes():
                    selection = self._handle_frames(lambda: self._handle_search(selectors))
        else:
            selection = self.find_element(dialog_button)
        if selection is None:
            raise nte.NebulaTestException(f"The dialog button {dialog_button} was not found on the page. Selectors was {selectors}")
        return selection

    def find_list_option(self, list_name, list_option):
        '''Returns list option element
        @param list_name - The name of the list to find
        @param list_option - The option in the list to select
        USES:
          - Best way for this to work is by using a selector file. See the README.md on how to use
          - Performs the translation of the text if available
          - Searches recursively in IFrames for HTML Selects
        '''
        selectors = self._handle_selectors(list_name)
        list_name = self.get_translation(list_name)
        list_option = self.get_translation(list_option)
        if selectors is not None:
            list_item_result = self._handle_search(selectors, sub_selector_text=list_option)
            if list_item_result is not None and len(list_item_result) > 0:
                try:
                    self.scroll_into_view(list_item_result[0])
                    # possible stale element here try once again
                except StaleElementReferenceException as stfe:
                    logging.debug(stfe)
                    list_item_result = self._handle_search(selectors, sub_selector_text=list_option)
                    self.scroll_into_view(list_item_result[0])
                return list_item_result[0]
            else:
                raise nte.NebulaTestException(f"Unable to find item associated with: {list_name} > {list_option}")
        else:
            item = self.find_items_by_tag(list_name)[int(list_option) - 1]
            try:
                self.scroll_into_view(item)
                # possible stale element here try once again
            except StaleElementReferenceException as stfe:
                logging.debug(stfe)
                item = self.find_items_by_tag(list_name)[int(list_option) - 1]
                self.scroll_into_view(item)

    def find_list_exact_entry(self, list_name, list_option, sub_element) :
        '''Find an entry or an entry's sub element. Does not look for inputs. Returns the web element
        @param list_name - The name of the list to find
        @param list_option - The option in the list to select
        @param sub_element - If supplied, goes down one level from the list
        USES:
          - Best way for this to work is by using a selector file. See the README.md on how to use
          - Performs the translation of the text if available
          - Searches recursively in IFrames for HTML Selects
        '''
        selectors = self._handle_selectors(list_name)
        list_name = self.get_translation(list_name)
        list_option = self.get_translation(list_option)
        if selectors is not None:
            list_item_result = self._handle_search(selectors, sub_selector_text=list_option)
            if list_item_result is not None and len(list_item_result) > 0:
                if sub_element is None or sub_element is '':
                    return list_item_result[0]
                else:
                    return list_item_result[0].find_element_by_tag_name(sub_element)
        else:
            if " " not in list_name:
                items = self.find_items_by_tag(list_name)

                if (len(items) > 0):
                    item = items[int(list_option) - 1]
                else:
                    item = items
                if sub_element is None or sub_element is '':
                    return item
                else:
                    if " " not in sub_element:
                        xpath = xpath_helper.build_xpath_for_item_by_tag(sub_element)
                        xpath += " | "
                    xpath += xpath_helper.build_xpath_to_find_elements_with_value(sub_element)
                    return item.find_element_by_xpath(xpath)

    def find_items_by_tag(self, tag_name):
        '''Find elements that have certain tag
        @param tag_name - The name of the tag
        @returns Web Element
        USES:
          - This is a comprehensive method.
          - This returns a Web Element which you can use for other functions
          - Can be used in a recursive search but does not search IFrames
          - DO NOT call publicly. Only call from within this class
          - Translation needs to have already happened
        '''
        self._switch_to_default_content()
        xpath = xpath_helper.build_xpath_for_item_by_tag(tag_name)
        return self._execute_search_xpath(self.root_element, xpath)

    def find_elements(self, element):
        '''Find elements using all the find methods available
        @param element - context of the element
        @returns Web Elements
        USES:
          - This is a comprehensive method.
          - This returns a Web Elements which you can use for other functions
          - Can be used in a recursive search
          - Translation needs to have already happened
          - DO Not make this the default find method
        '''
        wait_time = float(self.variable_store.get_value('${WAIT_TIME}'))
        timestamp = time.time() + wait_time
        while time.time() < timestamp:
            try:
                logging.debug(f"Trying to find {element} using all available methods")
                item_list = self.find_textboxes(element)
                if item_list and len(item_list) > 0:
                    return item_list
                self._switch_to_default_content()
                logging.debug(f"No textboxes found with identifier {element}")
                item_list = self.find_checkboxes(element)
                if item_list and len(item_list) > 0:
                    return item_list
                self._switch_to_default_content()
                logging.debug(f"No checkboxes found with identifier {element}")
                item = self.find_exact_item_by_text(element)
                if item:
                    return [item]
                logging.debug(f"No items found with exact text {element}")
                if " " not in element:
                    item_list = self.find_items_by_tag(element)
                if item_list and len(item_list) > 0:
                    return item_list
                item_list = self._execute_search_id(self.root_element, element)
                if item_list and len(item_list) > 0:
                    return item_list
                logging.debug(f"No items found with id {element}")
            except:
                pass
        raise nte.NebulaTestException(f"Unable to find element associated with: {element}.")

    def find_element(self, element):
        '''Find element using all the find methods available
        @param element - context of the element
        @returns Web Element
        USES:
          - This is a comprehensive method.
          - This returns a Web Element which you can use for other functions
          - Can be used in a recursive search
          - Translation needs to have already happened
        '''
        item = self.find_elements(element)
        if len(item) > 0:
            return item[0]
        raise nte.NebulaTestException(f"Unable to find element associated with: {element}.")

    def get_attribute_value(self, element, attribute_name):
        '''Returns the value of an elements attribute
        @param element A WebElement to be searched
        @param attribute_name What is being searched for
        @return A string for most attributes, a boolean for truthy attributes, or None if it isn't found
        '''
        return element.get_attribute(attribute_name)

    def get_css_property(self, element, css_property):
        '''Returns the value of an elements css property
        @param element A WebElement to be searched
        @param css_property What is being searched for
        @return A string for most css properties, a boolean for truthy css properties, or None if it isn't found
        '''
        return element.value_of_css_property(css_property)

    def get_filepath_url(self, fileIndicator, query):
        filename = query[query.find(fileIndicator) + len(fileIndicator):]
        filename = filename.split(' ', maxsplit=2)[1]
        url = os.getcwd() + '/' + filename.strip()
        url = Path(url).as_uri()
        return url

    def css_color_conversion_check(self, color, color_type):
        '''Checks if a CSS property value is a color and if it is, then it converts it to specified rgb/rgba value.
        @param color the css value we want to convert if needed
        @param color_type the actual css value that selenium returns
        @return A string with either an rgb/rgba value for colors, or the original css property value if not a color
        '''

        # fix issue where double quotes were added because the variable value had a # so it needed quotes
        if color.startswith('"'):
            color = color.strip('\"')
        elif color.startswith("'"):
            color = color.strip("\'")

        exp_value = color
        original_exp_value = exp_value
        # we check the length of the string to not mistakenly try to convert colors for css properties that
        # return multiple values like ex: rgba(0, 0, 0, 0.15) 0px 2px 4px 0px
        # an actual color value shouldn't be larger than 30 characters
        str_length = len(color_type)

        if color_type.startswith("rgba(") and not original_exp_value.startswith("rgba(") and color_type.endswith(")") and str_length <= 30:
            # even though the css shows a rgb value , selenium can return an rgba value so we must convert it
            if original_exp_value.startswith("rgb("):
                hex = Color.from_string(original_exp_value).hex
                exp_value = Color.from_string(hex).rgba
            else:
                try:
                    # if we are checking a hex code we need to add the # symobl since nebula strips it out
                    exp_value = Color.from_string("#" + str(original_exp_value)).rgba
                except ValueError:
                    # if we are checking color word like white , red, blue then we don't need the # symbol
                    exp_value = Color.from_string(original_exp_value).rgba
        elif color_type.startswith("rgb(") and not original_exp_value.startswith("rgb(") and color_type.endswith(")") and str_length <= 30:
            # even though the css shows a rgba value , selenium can return an rgb value so we must convert it
            if original_exp_value.startswith("rgba("):
                hex = Color.from_string(original_exp_value).hex
                exp_value = Color.from_string(hex).rgb
            else:
                try:
                    # if we are checking a hex code we need to add the # symobl since nebula strips it out
                    exp_value = Color.from_string("#" + str(original_exp_value)).rgb
                except ValueError:
                    # if we are checking color word like white , red, blue then we don't need the # symbol
                    exp_value = Color.from_string(original_exp_value).rgb
        return exp_value

    def find_item_by_text(self, selector, func):
        item = self.find_element(selector)
        if item is None:
            raise nte.NebulaTestException(f"Unable to find element: {selector}.")
        try:
            func(item)
        except StaleElementReferenceException as stfe:
            logging.debug(stfe)
            item = self.find_element(selector)
            if item is None:
                raise nte.NebulaTestException(f"Unable to find element: {selector}.")
            func(item)
         # only scroll if necessary
        except WebDriverException as wde:
            if "is not clickable at point" in wde.msg:
                self.scroll_into_view(item)
                try:
                    func(item)
                except Exception as e:
                    # allow for click away without raising the exception
                    logging.verbose(e)
            else:
                # allow for click away without raising the exception
                logging.verbose(e)

########## Verification Methods  ##############################################
    def verify_list_entry_does_not_exist(self, list_name, list_option, sub_element):
        '''Looks to see if the entry is avaiable.
        @param list_name - The name of the list to find
        @param list_option - The option in the list
        @param sub_element - If supplied, look for sub element of the option
        USES:
          - Best way for this to work is by using a selector file. See the README.md on how to use
          - Performs the translation of the text if available
          - Searches recursively in IFrames for HTML Selects
        '''
        try:
            if self.find_list_exact_entry(list_name, list_option, sub_element) is not None:
                raise nte.NebulaTestException(f"Unable to miss_exact_entry {list_name}.")
        except Exception as e:
            logging.verbose(e.args[0])

    def verify_item_does_not_exists(self, text):
        '''Look for the absence of a single item by it's text. Does not look for inputs.
        @param text - The text of the item to find
        '''
        try:
            self.find_element(text)
        except nte.NebulaTestException:
            return
        raise nte.NebulaTestException(f"FAIL: Was able to find item {text}.")

    def is_enabled(self, item_name):
        ''' Checks if an element is enabled. Search for element by name.
        @param item_name - The name of the item to find
        @returns boolean
        '''
        item = self.find_element(item_name)
        try:
            return item.is_enabled()
            # possible stale element here try once again
        except StaleElementReferenceException as stfe:
            logging.debug(stfe)
            item = self.find_element(item_name)
            return item.is_enabled()

    def is_radio_selected(self, item_name):
        ''' Checks if an element is enabled. Search for element by name.
        @param item_name - The name of the item to find
        @returns boolean
        '''
        item = self.find_element(item_name)
        try:
            return item.is_selected()
            # possible stale element here try once again
        except StaleElementReferenceException as stfe:
            logging.debug(stfe)
            item = self.find_element(item_name)
            return item.is_selected()

    def verify_current_url(self, operator, url):
        ''' Verifies whether the current url matches the passed in url.
        @operator - the type of comparison being done
        @param url - The url to match
        @returns boolean
        '''
        tries = 3
        self.wait_for_page_to_load()
        self.wait_for_angular()
        # Firefox is slower than Chrome so lets give it a chance to catch up
        for i in range(tries):
            current_url = self.driver.current_url
            if current_url is not "about:blank":
                break
            if i < tries - 1:
                continue

        # go through the url to see if there are any env
        # using while is needed just in case there are more than one
        if url.startswith('\"') and url.endswith('\"'):
            url = url[1: (len(url) - 1)]

        cleartext = self.variable_store.var_replace(url)
        self.compare_factory.compare(current_url, cleartext, operator)

    def verify_checkbox_value(self, checkbox_name, is_checked):
        ''' Find a checkbox and verify it has the correct value
        @param checkbox_name - The name of the checkbox to find
        @param is_checked - The value specifying if the checkbox is selected
        '''
        item = self.find_checkbox(checkbox_name)
        if item is not None:
            try:
                value = item.is_selected()
                # possible stale element here try once again
            except StaleElementReferenceException as stfe:
                logging.debug(stfe)
                self.find_checkbox(checkbox_name).is_selected()
            if is_checked != value:
                raise nte.NebulaTestException(f"Checkbox {checkbox_name} is {value}")
        else:
            raise nte.NebulaTestException(f"Checkbox {checkbox_name} was not found")

    def validate_results_on_page(self, results_to_validate, count):
        # TODO: Not sure how to do this yet. Let us think about it
        # For now it's checking if at least a certain count of results are displayed

        search_results = self.driver.find_elements_by_xpath("//*[contains(text(), '{0}')]".format(results_to_validate))
        if len(search_results) < int(count):
            raise nte.NebulaTestException("Error validating results are on a page")

    def verify_enabled(self, text):
        if not self.is_enabled(text) == True:
            raise nte.NebulaTestException(f"The element {text} is not enabled")

    def verify_disabled(self, text):
        if not self.is_enabled(text) == False:
            raise nte.NebulaTestException(f"The element {text} is not disabled")

    def verify_radio_enabled(self, text):
        return self.is_radio_selected(text) == True

    def verify_radio_disabled(self, text):
        return self.is_radio_selected(text) == True

    def _check_for_webdriver(self):
        ''' This method checks for webdriver and if not present, downloads them
        '''

        # base path defined by nebula variable. Used in docker images and pypi
        # default path is os.getcwd() + '/drivers'
        base_path = self.variable_store.get_value('${WEBDRIVER_PATH}')

        # By default we don't add a suffix to the path
        path_suffix = ''
        operating_system = platform.system()
        if operating_system == "Windows":
            path_suffix = '.exe'

        # Make sure drivers are present
        if self.browser.lower() == 'firefox':
            driver_path = base_path + '/geckodriver' + path_suffix
            if not os.path.exists(driver_path):
                logging.info("Firefox webdriver was not found. Downloading it now")
                try:
                    WebActions.driver_install_sem.acquire()
                    #need to check again, as driver may have been installed by another thread while acquiring semaphore
                    if not os.path.exists(driver_path):
                        dw.get_geckodriver()
                except Exception:
                    raise ite.InvalidTestException("Failed to download and extract firefox webdriver correctly. This may be due to an internet issue, or a permission issue when extracting.")
                finally:
                    WebActions.driver_install_sem.release()

        elif self.browser.lower() == 'chrome':
            driver_path = base_path + '/chromedriver' + path_suffix
            if config_helper.get_chrome_version() is None:
                if not os.path.exists(driver_path):
                    raise ite.InvalidTestException("To download Chromedriver, you need to provide Chrome version. Please run nebula again and include the -cv argument with Chrome version as the parameter")
            else:
                # check to see if the chrome driver version is different than -cv argument
                if os.path.exists(driver_path):
                    try:
                        WebActions.driver_install_sem.acquire()
                        #need to check again, as driver may have been removed by another thread while acquiring semaphore
                        if os.path.exists(driver_path):
                            options = webdriver.ChromeOptions()
                            options.add_argument('--headless')
                            cvVersion = config_helper.get_chrome_version()
                            chromeDriver = webdriver.Chrome(executable_path=driver_path, chrome_options=options)
                            chromedriverVersion = chromeDriver.capabilities['chrome']['chromedriverVersion'].split(' ')[0]
                            chromeDriver.quit()
                            if cvVersion.split('.')[0] != chromedriverVersion.split('.')[0]:
                                logging.info(f"Chrome driver version: {chromedriverVersion.split('.')[0]} is different than -cv chrome driver: {cvVersion.split('.')[0]}")
                                logging.info(f"Removing previously installed chrome driver located at : {driver_path}")
                                os.remove(driver_path)
                    except Exception as e:
                        # catching message where chrome driver is not compatible with browser
                        # and grabbing the version of chrome driver from the error message
                        chromedriverVersion = str(e).split()[-1]
                        if cvVersion.split('.')[0] != chromedriverVersion.split('.')[0]:
                            logging.info(f"Chrome driver version: {chromedriverVersion.split('.')[0]} is different than -cv chrome driver: {cvVersion.split('.')[0]}")
                            logging.info(f"Removing previously installed chrome driver located at : {driver_path}")
                            os.remove(driver_path)
                    finally:
                        WebActions.driver_install_sem.release()

                if not os.path.exists(driver_path):
                    logging.info("Chrome webdriver was not found. Downloading it now")
                    try:
                        WebActions.driver_install_sem.acquire()
                        #need to check again, as driver may have been installed by another thread while acquiring semaphore
                        if not os.path.exists(driver_path):
                            dw.get_chromedriver(config_helper.get_chrome_version())
                    except Exception:
                        raise ite.InvalidTestException("Failed to download and extract chrome webdriver correctly. This may be due to an internet issue, or a permissions issue when extracting.")
                    finally:
                        WebActions.driver_install_sem.release()

        elif self.browser.lower() == 'ie':
            driver_path = base_path + '/IEDriverServer' + path_suffix
            if not os.path.exists(driver_path):
                logging.info("To download IE Driver, a compatible IE Driver version is needed. Please run nebula again and include the -cv argument with the IE Driver version")
            else:
                if not os.path.exists(driver_path):
                    logging.info("IE webdriver was not found. Downloading it now")
                    WebActions.driver_install_sem.acquire()
                    #need to check again, as driver may have been installed by another thread while acquiring semaphore
                    if not os.path.exists(driver_path):
                        try:
                            dw.get_iedriver(config_helper.get_chrome_version())
                        except Exception:
                            raise ite.InvalidTestException("Failed to download and extract IE webdriver correctly. This may be due to an internet issue, or a permissions issue when extracting")
                        finally:
                            WebActions.driver_install_sem.release()

        elif self.browser.lower() == 'opera':
            driver_path = base_path + '/operadriver' + path_suffix
            if not os.path.exists(driver_path):
                raise ite.InvalidTestException("To download opera webdriver, you need to provide the operadriver version. Please run nebula again and include the -cv argument with opera driver version as the parameter")
            else:
                if not os.path.exists(driver_path):
                    logging.info("Opera webdriver was not found. Downloading it now")
                    WebActions.driver_install_sem.acquire()
                    #need to check again, as driver may have been installed by another thread while acquiring semaphore
                    if not os.path.exists(driver_path):
                        try:
                            dw.get_operadriver(config_helper.get_chrome_version())
                        except Exception:
                            raise ite.InvalidTestException("Failed to download and extract webdriver correctly. This may be due to an internet issue, or a permissions issue when extracting.")
                        finally:
                            WebActions.driver_install_sem.release()

        elif self.browser.lower() == 'safari':
            driver_path = '/usr/bin/safaridriver'
            if not os.path.exists(driver_path):
                raise ite.InvalidTestException("Safari Driver was not found")
            else:
                logging.info("Safari webdriver was not found. Downloading it now")
                WebActions.driver_install_sem.acquire()
                #need to check again, as driver may have been installed by another thread while acquiring semaphore
                if not os.path.exists(driver_path):
                    try:
                        dw.get_operadriver(config_helper.get_chrome_version())
                    except Exception:
                        raise ite.InvalidTestException("Failed to download and extract webdriver correctly. This may be due to an internet issue, or a permissions issue when extracting.")
                    finally:
                        WebActions.driver_install_sem.release()

        elif "remote" in self.browser.lower():
            logging.info(f"Using remote browser: {self.browser}")

        else:
            raise ite.InvalidTestException(f"Browser definition was not understood: {self.browser}")

        return

    def _get_driver(self, browser, executable=None):
        '''Get the driver instance which will be used'''

        resize_window = True

        # base path defined by nebula variable. Used in docker images and pypi
        # default path is os.getcwd() + '/drivers'
        base_path = self.variable_store.get_value('${WEBDRIVER_PATH}')

        # By default we don't add a suffix to the path
        path_suffix = ''
        operating_system = platform.system()
        if operating_system == "Windows":
            path_suffix = '.exe'

        headless = config_helper.get_headless()

        # setup the chrome driver
        if browser.lower() == 'chrome':
            driver_path = base_path + '/chromedriver' + path_suffix
            options = webdriver.ChromeOptions()
            options.add_argument('--disable-infobars')
            options.add_argument('--ignore-certificate-errors')
            options.add_argument('--disable-gpu')
            options.add_argument('--no-sandbox')
            logging.verbose(f"Looking for chromedriver located at {driver_path}")

            if headless:
                options.add_argument('--headless')
                options.add_argument('--incognito')
                options.add_argument('--web-security=false')
                options.add_argument('--ignore-ssl-errors')
                options.add_argument('--ignore-certificate-errors-spki-list')
                options.add_argument('--disable-dev-shm-usage')

            driver = webdriver.Chrome(executable_path=driver_path, chrome_options=options)
            logging.verbose(f"Browser capabilities: \n{pprint.pformat(driver.capabilities)}")
            driver.implicitly_wait(0.6)

        # setup an electron test driver
        elif browser.lower() == 'electron':
            # electron base path defined by nebula variable. Used for electron apps
            driver_path = self.variable_store.get_value('${EX_NEB_ELECTRON_WEBDRIVER_PATH}') + '/chromedriver' + path_suffix
            resize_window = False
            options = webdriver.ChromeOptions()
            options.binary_location = executable
            logging.verbose(f"Looking for chromedriver located at {driver_path} to drive the {executable} Electron app.")
            driver = webdriver.Chrome(executable_path=driver_path,chrome_options=options)
            driver.implicitly_wait(0.6)

        elif browser.lower() == 'firefox':
            driver_path = base_path + '/geckodriver' + path_suffix
            # setup firefox driver
            profile = webdriver.FirefoxProfile()
            profile.accept_untrusted_certs = True

            if headless:
                os.environ['MOZ_HEADLESS'] = '1'

            logging.verbose(f"Looking for geckodriver located at {driver_path}")

            driver = webdriver.Firefox(executable_path=driver_path, firefox_profile=profile)
            logging.verbose(f"Browser capabilities: \n{pprint.pformat(driver.capabilities)}")
            driver.implicitly_wait(1)

        elif browser.lower() == 'ie':
            driver_path = base_path + '/IEDriverServer' + path_suffix
            # Setup Internet Explorer Driver
            if headless:
                logging.info('Headless mode is not supported by Internet Explorer. Running in normal mode.')

            logging.verbose(f"Looking for IEDriver located at {driver_path}")
            driver = webdriver.Ie(executable_path=driver_path)
            logging.verbose(f"Browser capabilities: \n{pprint.pformat(driver.capabilities)}")

        elif browser.lower() == 'edge':
            driver_path = base_path + '/MicrosoftWebDriver' + path_suffix
            logging.verbose(f"Looking for edgedriver located at {driver_path}")
            driver = webdriver.Edge(executable_path=driver_path)
            logging.verbose(f"Browser capabilities: \n{pprint.pformat(driver.capabilities)}")

        elif browser.lower() == 'opera':
            driver_path = base_path + '/operadriver' + path_suffix
            options = webdriver.ChromeOptions()
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-infobars')
            options.add_argument('--homepage=about:blank')
            options.add_argument('--ignore-certificate-errors')
            options.add_argument('--ignore-gpu-blacklist')
            opera_capabilities = desired_capabilities.DesiredCapabilities.OPERA.copy()
            logging.verbose(f"Looking for operadriver located at {driver_path}")

            if headless:
                options.add_argument('--headless')
                options.add_argument('--incognito')
                options.add_argument('--web-security=false')
                options.add_argument('--ignore-ssl-errors')
                options.add_argument('--ignore-certificate-errors-spki-list')
                options.add_argument('--disable-dev-shm-usage')

            driver = webdriver.Opera(executable_path=driver_path, options=options, desired_capabilities=opera_capabilities)
            logging.verbose(f"Browser capabilities: \n{pprint.pformat(driver.capabilities)}")
            driver.implicitly_wait(1)

        elif browser.lower() == 'safari':
            driver_path = '/usr/bin/safaridriver'
            safari_capabilities = desired_capabilities.DesiredCapabilities.SAFARI.copy()
            driver = webdriver.Safari(executable_path=driver_path, desired_capabilities=safari_capabilities)

        else:
            desired_cap = self.variable_store.get_value('${EX_NEB_REMOTE_DESIRED_CAP}')
            if desired_cap == None:
                raise ite.InvalidTestException(f"Browser not supported: {browser}.  Please define json named {browser} or define EX_NEB_REMOTE_DESIRED_CAP to use remote browsers. Refer to https://extron.atlassian.net/wiki/spaces/SWEIET/pages/939819864/API+Testing or https://www.browserstack.com/automate/capabilities or https://wiki.saucelabs.com/display/DOCS/Platform+Configurator#/ for properly filling out the JSON")
            desired_cap['name'] = self.test_info.testfile
            # this is needed b/c opera webdriver doesn't support setting the window size that we do below
            # driver.set_window_size(1600, 900)
            resize_window = False
            provider = desired_cap.pop("provider",None).lower()

            if provider is "saucelabs":
                driver = saucelabs.get_driver(desired_cap=desired_cap)
                self.test_info.sessionId = driver.session_id
                if self.test_info.sessionId != "":
                        logging.info(f"{bcolors.HEADER}Saucelabs Session URL: https://app.saucelabs.com/tests/{self.test_info.sessionId}{bcolors.ENDC}")

            elif provider is "browserstack":
                driver = browserstack.get_driver(desired_cap=desired_cap)
                self.test_info.sessionId = driver.session_id
                if self.test_info.sessionId != "":
                    try:
                        requestget = requests.get(
                            f"https://api-cloud.browserstack.com/automate/sessions/{self.test_info.sessionId}.json",
                            auth=(os.environ.get('BROWSERSTACK_USER'), os.environ.get('BROWSERSTACK_KEY')))
                        session_information = requestget.json()
                        logging.info(f"{bcolors.HEADER}Browserstack Session URL: {session_information['automation_session']['browser_url']}{bcolors.ENDC}")
                    except:
                        pass
            else:  #the only thing left is everything else
                options = None
                if "options" in desired_cap:
                    if desired_cap['browserName'].lower() == 'chrome':
                        options = webdriver.ChromeOptions()
                    elif  desired_cap["browserName"].lower() == 'firefox':
                        options = webdriver.FirefoxOptions()
                        log = webdriver.firefox.options.Log()
                        log.level = "TRACE"
                        options.add_argument(log.level)
                        #options.headless=True
                        options.set_capability('marionette', False)
                    elif  desired_cap["browserName"].lower() == 'microsoftedge':
                        options = EdgeOptions()
                        options.use_chromium = True
                        options.add_argument("headless")
                        options.add_argument("disable-gpu")
                    for option in desired_cap["options"]:
                        options.add_argument(option)
                    desired_cap.pop("options",None)
                driver = webdriver.Remote(command_executor = provider, desired_capabilities=desired_cap, options=options)

        if resize_window:
            browser_resolution = config_helper.get_browser_resolution()
            driver.set_window_size(browser_resolution[0], browser_resolution[1])

        return driver

    def _switch_to_default_content(self):
        self.driver.switch_to_default_content()
        return

    def _parse_url(self, url, query):
        url = self.variable_store.var_replace(url)
        if url == '' or url.startswith("file"):
            fileIndicator = 'file'
            query = self.variable_store.var_replace(query)
            url = self.get_filepath_url(fileIndicator, query)
            logging.verbose(f"Snips didn't recognize a url, so trying to open a file named {url}")
            logging.verbose(f"The file to be opened is indicated with the keyword '{fileIndicator}'")
        elif not url.startswith("http"):
            url = "http://" + url
        return url
