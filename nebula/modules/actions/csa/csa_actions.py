import base64
import datetime
import json
import logging
import os
import re
from time import gmtime, sleep, strftime, time

from nebula.modules.actions.base import base_actions
from nebula.modules.actions.csa.common.QATestEnv import QATestEnv
from nebula.modules.actions.csa.common.QAUtils import QAUtils
from nebula.modules.actions.csa.functional import JsonParser
from nebula.modules.common import config_helper, variable_enum, variable_store
from nebula.modules.report import invalid_test_exception as ite
from nebula.modules.report import nebula_test_exception as nte


class CsaActions(base_actions.BaseActions):
    session = None
    sessions = {}
    filename = None
    reporttype = 'json'
    logfilename = None 
    _HEXPRINT = False
    _VERBOSE = False
    _DEBUG = False
    logger = None
    testresults = []
    TestNote = ""
    login = None
    passwd =  None
    iphost = None
    port = None
    connecttype = None

    def __init__(self, vs):
        super().__init__(vs)

    def _extract_data(self,jsonfilename, json_string):
        if len(jsonfilename.strip()) == 0:
           return
   
        self.filename = jsonfilename.strip()
        QATestEnv.envworkingdir = os.path.expanduser("~") 
        QATestEnv.workingdir = os.path.expanduser("~") 
        if jsonfilename.startswith("~"):
           #get home directory
           jsonfilename = jsonfilename.replace("~",QATestEnv.envworkingdir) 
           self.filename = jsonfilename
        if jsonfilename.startswith("/"):
           pass  #as is
        else:
            QATestEnv.workingdir = os.getcwd()
            QATestEnv.envworkingdir = QATestEnv.workingdir
            self.filename = QATestEnv.workingdir + "/" + jsonfilename 

        if(json_string != None):
            try:
                js = json.loads(json_string)            
                self.login = js['usr']
                self.passwd = js['pwd']
                self.connecttype = js['conn']
                temp = js['iphost'].split(':')
                if len(temp) == 2:
                    self.port = temp[1]
                    self.iphost = temp[0]       
            except Exception as error:
                #bad json form
                logging.info("Fail to extract uut settings:" + json_string)
                logging.exception(error)
                raise ite.InvalidTestException(f"Nebula does not recognize the following directive.  Note that API directives are case sensitive.\n")

    def process_csanebula(self,testline):
        #execute test file ~/testing/sisdata.json
        searchObj = None
        param = ""
        testline = testline.lower().strip()
        if testline.startswith(('execute','run')):
           searchObj = re.search(r"test file (.*?) with uut settings:(.*)",testline,re.I)
           if(searchObj == None):
             searchObj = re.search(r"test file (.*)",testline,re.I)
             if (searchObj):
                 #data is initialize at __init__
                 self.filename = searchObj.group(1)
                 self._extract_data(self.filename,None)

                 if self.filename.startswith("~"):
                    #get home directory
                    QATestEnv.envworkingdir = os.path.expanduser("~") 
                    QATestEnv.workingdir = os.path.expanduser("~") 
                    self.filename = self.filename.replace("~",QATestEnv.envworkingdir) 
                 else:
                    QATestEnv.envworkingdir = os.path.dirname(self.filename)
                    QATestEnv.workingdir = os.path.dirname(self.filename)
             else:
                #raise an error                
                raise ite.InvalidTestException(f"Nebula does not recognize the following directive.\n'{testline}'")                
           else:
               self.filename = searchObj.group(1)               
               param = searchObj.group(2)
               self._extract_data(self.filename,param)

        if self.filename  == None or len(self.filename) == 0:
           logging.info("No Testing data file to be processed.")
           raise ite.InvalidTestException(f"Nebula does not recognize the following directive.\n'{testline}'")
        
        self._process_csa()

    def _process_csa(self):
        jsondata = QATestEnv.ReadJson(self.filename,QATestEnv.envworkingdir,QATestEnv.workingdir)
        if jsondata == None:
           print("No Testing data to be processed.") #need to raise exection
           raise ite.InvalidTestException(f"No Testing data to be processed for {self.filename}")            
        self.testresults = []
        try:            
            JsonParser.start(self.testresults,jsondata,self.connecttype,self.iphost,self.port,self.passwd,self.login,self._DEBUG,self._VERBOSE,self.logger,self._HEXPRINT)
        except nte.NebulaTestException as ne:
            raise type(ne)(repr(ne))
        except Exception as error:
            #it could be Connection
            if(str(error).find("Connection fails to uut") != -1):
                #replace testnote with connection error
                raise ite.InvalidTestException(f"Fail to connect to uut:"+str(error))
            else:
                raise ite.InvalidTestException(f"Unrecognize failure:"+str(error))
        return self.testresults
