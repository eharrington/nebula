'''
Creator: Bouda Meas
Date Create: 11/8/2016
Description:
'''
import sys
import copy
import json
class JsonReport:
    """description of class"""
    
    def __init__(self,filename='default.json'):
        self.__tcHeader = None  #TestHeader
        self.__tcList = []    #new List<TestAttributes>()
        self.__status = False   #default to a fail test case
        self.__filename = filename
        
    def __createHeaderReport(self,headerName):
        if self.__tcHeader != None:
            return "\"" + headerName + "\"" + ":" + self.__tcHeader.GetJsonString()

        return ""

    def __createResultReport(self):
        status,str_result = self.GetResult()
        if (str_result == None) or (len(str_result.strip()) == 0):
            #print("No test result found.")
            return status,None

        str_result = "\"" + "TestDetails" + "\"" + ":{" + str_result
        str_result = str_result + "}"

        return status,str_result
    def __count(self,subitemtest):
        passes = 0
        failures = 0
        skipped = 0

        if len(subitemtest) > 0:            
           for item in subitemtest:
               if str(item.Status).lower() == "pass":                
                  passes += 1
               elif str(item.Status).lower() == "fail":                
                    failures += 1
               else:                
                    skipped += 1
            
        return passes, failures, skipped
    def __buildHTMLTable(self,subitemtest):
        str_result = ""
        passes = 0
        failures = 0
        skipped = 0

        if len(subitemtest) == 0:
            #print("No sub-test found.")
            return passes, failures, skipped,None
        print('.', end='')
        str_result = str_result + ("<table width:100% border=1>")
        '''header'''
        str_result = str_result + ("<tr>")
        str_result = str_result + ("<th>Attribute Name</th>")
        str_result = str_result + ("<th>Description</th> ")
        str_result = str_result + ("<th>Pass Criteria</th> ")
        str_result = str_result + ("<th>Expect Value</th>")
        str_result = str_result + ("<th>Actual/UUT Value</th>")  # bm 06/02/2018:Added "/UUT"
        str_result = str_result + ("<th>Pass/Fail</th>")
        str_result = str_result + ("</tr>")


        for item in subitemtest:
            str_result = str_result + ("<tr>")
            #bm 07/10/19:REmove AttributeName
            str_result = str_result + ("<th>" + str(item.Description) + "</th>")
            str_result = str_result + ("<th>" + str(item.PassCriteria) + "</th>")
            str_result = str_result + ("<th>" + str(item.Expectvalue) + "</th> ")
            if (type(item.Result) == type(b'')):
                str_result = str_result + ("<th>" + str(item.Result.decode("utf-8")) + "</th> ")
            else:
                str_result = str_result + ("<th>" + str(item.Result) + "</th> ")
            if str(item.Status).lower() == "pass":
                str_result = str_result + ("<th>Pass</th> ")
                passes += 1
            elif str(item.Status).lower() == "fail":
                str_result = str_result + ("<th>Fail</th> ")
            else:
                str_result = str_result + ("<th>Skp</th> ")
                skipped += 1
            str_result = str_result + ("</tr>")

        str_result = str_result + ("</table>")
        return passes, failures, skipped,str_result

    def GetJsonString(self,testattrb):
        str_result = testattrb.GetJsonString()        
        passes, failures, skipped = self.__count(testattrb.GetSubTestItems())
        
        return passes, failures, skipped, str_result

    def GetResult(self): #StringBuilder
        index = 0
        str_result  = ""
        str1 = ""
        status = False  #use for indicting a failure in a TC.
        if len(self.__tcList) == 0:
            #print("No Test case found.")
            return status, str_result

        for tc in self.__tcList:
            index = index + 1            
            #json_str = self.GetJsonString(tc) # tc.GetJsonString()
            passes, failures, skipped, json_str = self.GetJsonString(tc)
            json_str = "\"" + "{0}".format(index) + "\"" + ":" + json_str #tc.GetJsonString()
            str_result = str_result + (","+json_str)
            if failures > 0:
                status = True
            print('*', end='')
        print("\n")
        if str_result[:1] == ",":         #Substring(0,1)
            str_result = str_result[1:]   #Remove(0, 1)
        
        return status, str_result

    def SetHeader(self, testheader):
        self.__tcHeader = testheader
        return True   #a hard True for now
    def AddTestAttribute(self, tcAttr):  #bm 03/21/2019: Rename from AddAttribute to AddTestAttribute
        errors = 0
        failures = 0
        passes = 0
        skipped = 0
    
        for ti in tcAttr.GetSubTestItems():
            if ti.Status.lower() == "pass":
                passes += 1
            elif ti.Status.lower() == "fail":
                failures += 1
            elif ti.Status.lower() == "error":
                errors += 1
            else:
                skipped += 1
        self.__totalcount['errors'] += errors
        self.__totalcount['passes'] += passes
        self.__totalcount['failures'] += failures
        self.__totalcount['skipped'] += skipped  
        self.__tcList.append(copy.deepcopy(tcAttr))  #bm 03/07/2019: Make a copy of it.
        return True   #a hard True for now
    def AddTestAttributeList(self,tcAttrs): #'''List<TestAttributes> '''
        if tcAttrs != None:
            for tcAttr in tcAttrs:
                self.__tcList.append(copy.deepcopy(tcAttr))
        return True   #a hard True for now

    def DeleteTestAttribute(self, tcAttr):
        errors = 0
        failures = 0
        passes = 0
        skipped = 0

        try:
            for ti in tcAttr.GetSubTestItems():
                if ti.Status.lower() == "pass":
                    passes += 1
                elif ti.Status.lower() == "fail":
                    failures += 1
                elif ti.Status.lower() == "error":
                    errors += 1
                else:
                    skipped += 1
            self.__totalcount['errors'] -= errors
            self.__totalcount['passes'] -= passes
            self.__totalcount['failures'] -= failures
            self.__totalcount['skipped'] -= skipped  
            self.__tcList.remove(tcAttr)
        except:
            e = sys.exc_info()
            print('Delete Attribute error:')
            print(e)
            return False        
        return True   #a hard True for now
    def DeleteTestAttributebyIndex(self,index):
        errors = 0
        failures = 0
        passes = 0
        skipped = 0

        try:
            for ti in self.__tcList[index].GetSubTestItems():
                if ti.Status.lower() == "pass":
                    passes += 1
                elif ti.Status.lower() == "fail":
                    failures += 1
                elif ti.Status.lower() == "error":
                    errors += 1
                else:
                    skipped += 1
            self.__totalcount['errors'] -= errors
            self.__totalcount['passes'] -= passes
            self.__totalcount['failures'] -= failures
            self.__totalcount['skipped'] -= skipped
            self.__tcList.pop(index)
        except:
            e = sys.exc_info()
            print('Delete Attribute error:')
            print(e)
            return False        
        return True   #a hard True for now

    def TestAttributesToJsonString(self, tcAttr):
        if(tcAttr != None):
            return tcAttr.GetJsonString()
        return None
    
    def HeaderToJsonString(self, hd):
        if (hd != None):
            return hd.GetJsonString()
        return None
    def GetHeader(self, headerName=""):
        if( len(headerName) == 0 or headerName == None ):
            headerName = "AutomationTest"
        return self.__createHeaderReport(headerName)
   
    def GetReport(self):
        return self.GetJsonReport()
    def GetJsonReport(self):
        #Note a fail TC = tcstatus
        try:
            header = self.__createHeaderReport("AutomationTest")
            tcstatus,strReport = self.__createResultReport()

            if (strReport == None):
                #print("No Test case found.")
                return tcstatus,None

            if header is None:
                header = ""

            strReport = "{"+header + "," + strReport  #insert at position 0
            strReport = strReport[:len(strReport)] + "}" + strReport[len(strReport):] #insert at position len(strReport)
            
            return tcstatus, strReport
        except Exception as e:
            print("Exception occur generating report content:"+str(e))
            tcstatus = True
        
        return tcstatus,None

    def CreateReport(self,filename=None):
        try:
            if filename == None and (self.__filename != None or len(self.__filename.strip()) > 0):               
               filename = self.__filename
            if filename == None or len(filename.strip()) == 0:
                filename = 'default.json'

            status,body = self.GetReport()
            if body == None:
                body = ""
            with open(filename+".json" , 'w') as ff:               
                ff.write(body)
                ff.close()
            return status
        except Exception as e:
            raise type(e)("Creation JSON report error:" + str(e))
        
        return status