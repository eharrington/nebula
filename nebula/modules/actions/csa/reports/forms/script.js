
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
$(document).ready(function()
{
    window.myDataSetArray = [
        ${myDataSetArray}
		];
	
	pagination()
        
        document.getElementById("record").rows[0].click();
        
        var table = document.getElementById("use cases");
        for(var i = 0, row; row = table.rows[i]; ++i)
            row.click()
    });

var btnUsedToResetDisplayedRows = null
function resetDisplayedRows()
{
    if(btnUsedToResetDisplayedRows == null)
        $(".btn1").click()
    else
        btnUsedToResetDisplayedRows.click()
}
function compareVals(val1, val2)
{
    if(val1 < val2)
        return -1
    else if(val1 == val2)
        return 0
    else//if(val1 > val2)
        return 1
}
function compareJSONIndex(a, b)
{
    return compareVals(a["JSON Index"], b["JSON Index"])
}
function compareTestType(a, b)
{
    return compareVals(a["Test Type"], b["Test Type"])
}
function compareDescription(a, b)
{
    return compareVals(a["Description"], b["Description"])
}
function compareStatus(a, b)
{
    return compareVals(a["Status"], b["Status"])
}
function comparePassCriteria(a, b)
{
    return compareVals(a["Pass Criteria"], b["Pass Criteria"])
}
var sortedByDefaultOrdering = true
var sortedByTestTypeAscending = false, sortedByTestTypeDescending = false
var sortedByDescriptionAscending = false, sortedByDescriptionDescending = false
var sortedByStatusAscending = false, sortedByStatusDescending = false
var sortedByPassCriteriaAscending = false, sortedByPassCriteriaDescending = false
function setAllSortFlags(setValue)
{
    sortedByDefaultOrdering = setValue
    sortedByTestTypeAscending = sortedByTestTypeDescending = setValue
    sortedByDescriptionAscending = sortedByDescriptionDescending = setValue
    sortedByStatusAscending = sortedByStatusDescending = setValue
    sortedByPassCriteriaAscending = sortedByPassCriteriaDescending = setValue
}
function anyFlagIsTrue()
{
    return sortedByDefaultOrdering || sortedByTestTypeAscending || sortedByTestTypeDescending ||
        sortedByDescriptionAscending || sortedByDescriptionDescending ||
        sortedByStatusAscending || sortedByStatusDescending ||
        sortedByPassCriteriaAscending || sortedByPassCriteriaDescending
}
function restoreHeaderNames()
{
    jQuery("tr.headers").find(".header1").html("Test Type")
    jQuery("tr.headers").find(".header2").html("Description")
    jQuery("tr.headers").find(".header3").html("Status")
    jQuery("tr.headers").find(".header4").html("Pass Criteria")
}
function restoreToDefaultSorting()
{
    myDataSetArray.sort(compareJSONIndex)
    setAllSortFlags(false)
    sortedByDefaultOrdering = true
    
    restoreHeaderNames()
}
function orderByTestType()
{
    function sortTestTypeAscending()
    {
        myDataSetArray.sort(compareTestType)
        setAllSortFlags(false)
        sortedByTestTypeAscending = true
        restoreHeaderNames()
        jQuery("tr.headers").find(".header1").html("Test Type ^")
    }
    
    if(anyFlagIsTrue() && !sortedByTestTypeAscending && !sortedByTestTypeDescending)
        sortTestTypeAscending()
    else if(sortedByTestTypeAscending)
    {
        myDataSetArray.reverse()
        setAllSortFlags(false)
        sortedByTestTypeDescending = true
        jQuery("tr.headers").find(".header1").html("Test Type v")
    }
    else
        restoreToDefaultSorting()
    
    resetDisplayedRows()
}
function orderByDescription()
{
    function sortDescriptionAscending()
    {
        myDataSetArray.sort(compareDescription)
        setAllSortFlags(false)
        sortedByDescriptionAscending = true
        restoreHeaderNames()
        jQuery("tr.headers").find(".header2").html("Description ^")
    }
    
    if(anyFlagIsTrue() && !sortedByDescriptionAscending && !sortedByDescriptionDescending)
        sortDescriptionAscending()
    else if(sortedByDescriptionAscending)
    {
        myDataSetArray.reverse()
        setAllSortFlags(false)
        sortedByDescriptionDescending = true
        jQuery("tr.headers").find(".header2").html("Description v")
    }
    else
        restoreToDefaultSorting()
    
    resetDisplayedRows()
}
function orderByStatus()
{
    function sortStatusAscending()
    {
        myDataSetArray.sort(compareStatus)
        setAllSortFlags(false)
        sortedByStatusAscending = true
        restoreHeaderNames()
        jQuery("tr.headers").find(".header3").html("Status ^")
    }
    
    if(anyFlagIsTrue() && !sortedByStatusAscending && !sortedByStatusDescending)
        sortStatusAscending()
    else if(sortedByStatusAscending)
    {
        myDataSetArray.reverse()
        setAllSortFlags(false)
        sortedByStatusDescending = true
        jQuery("tr.headers").find(".header3").html("Status v")
    }
    else
        restoreToDefaultSorting()
    
    resetDisplayedRows()
}
function orderByPassCriteria()
{
    function sortPassCriteriaAscending()
    {
        myDataSetArray.sort(comparePassCriteria)
        setAllSortFlags(false)
        sortedByPassCriteriaAscending = true
        restoreHeaderNames()
        jQuery("tr.headers").find(".header4").html("PassCriteria ^")
    }
    
    if(anyFlagIsTrue() && !sortedByPassCriteriaAscending && !sortedByPassCriteriaDescending)
        sortPassCriteriaAscending()
    else if(sortedByPassCriteriaAscending)
    {
        myDataSetArray.reverse()
        setAllSortFlags(false)
        sortedByPassCriteriaDescending = true
        jQuery("tr.headers").find(".header4").html("PassCriteria v")
    }
    else
        restoreToDefaultSorting()
    
    resetDisplayedRows()
}

function pagination()
{
    var requestedNumOfRows = 7;//needs to match the number of rows you have in your HTML code (doesn't have to match the myDataSetArray's size)
    var pageSelectorsLimit = 50;//TO MANIPULATE PAGINATION, CHANGE THIS VARIABLE
    var totalNumOfRows = myDataSetArray.length;
    var numOfPages = 0;
    if(totalNumOfRows%requestedNumOfRows == 0)
        numOfPages = totalNumOfRows/requestedNumOfRows;
    else//if(totalNumOfRows%requestedNumOfRows > 0)
    {
        numOfPages = totalNumOfRows/requestedNumOfRows;
        numOfPages = Math.floor(++numOfPages);
    }
    
    numOfArrows = 0
    if(numOfPages > pageSelectorsLimit)
        if(numOfPages%pageSelectorsLimit != 0)
            numOfArrows = Math.floor(numOfPages/pageSelectorsLimit) + 1;
        else
            numOfArrows = numOfPages/pageSelectorsLimit;
    if(numOfArrows != 0)
    {
        for(var i = 1; i < numOfArrows + 1; ++i)
        {
            if(i > 1)
            {
                jQuery('#softwareReportInformation').append("<a href = '#' class = 'shiftLeft" + i + "' style = 'font-size: 10px; font-family = helvetica'><<</a> ")
                $(".shiftLeft" + i).hide()
            }
            
            for(var j = pageSelectorsLimit * (i - 1) + 1; j <= (pageSelectorsLimit * i > numOfPages ? numOfPages : pageSelectorsLimit * i); ++j)
                jQuery('#softwareReportInformation').append("<a href = '#' class = 'btn" + j + "' style = 'font-size: 10px; font-family = helvetica'>" + j + "</a> ");
            if(i < numOfArrows)
                jQuery('#softwareReportInformation').append("<a href = '#' class = 'shiftRight" + i + "' style = 'font-size: 10px; font-family = helvetica'>>></a>")
            if(i > 1)
                $(".shiftRight" + i).hide()
        }
        for(var i = pageSelectorsLimit + 1; i < numOfPages + 1; ++i)
            $(".btn" + i).hide()
    }
    else
        for(var j = 1; j < numOfPages + 1; ++j)
            jQuery('#softwareReportInformation').append("<a href = '#' class = 'btn" + j + "' style = 'font-size: 10px; font-family = helvetica'>" + j + "</a> ");
    
    jQuery('#softwareReportInformation a').click(function(e) { clickPaginationButton(e, requestedNumOfRows, this, numOfArrows, numOfPages, pageSelectorsLimit) } );
}

function clickPaginationButton(event, numOfRows, clicked, numOfArrows, numOfPages, pageSelectorsLimit)
{
    //processing the arrow click or number click. Returning on arrow click
    for(var i = 1; i < numOfArrows + 1; ++i)
        if($(clicked).hasClass("shiftRight" + i))
        {
            jQuery("#softwareReportInformation a").hide();
            var temp = $(clicked).next();
            temp.show();//to show <<
            for(var j = 1; j < (i + 1 < numOfArrows ? pageSelectorsLimit + 1 : ((numOfPages - 1)%pageSelectorsLimit + 1) + 1); ++j)
            {
                temp = temp.next();
                temp.show();
            }
            if(i + 1 < numOfArrows)
                temp.next().show();//to show >>
            return;
        }
        else if($(clicked).hasClass("shiftLeft" + i))
        {
            jQuery("#softwareReportInformation a").hide();
            var temp = $(clicked).prev();
            temp.show();//to show >>
            for(var j = 1; j < pageSelectorsLimit + 1; ++j)
            {
                temp = temp.prev();
                temp.show();
            }
            if(i - 1 > 1)
                temp.prev().show();//to show <<
            return;
        }
    //preliminary set up
    testRows = jQuery('tr.test');
    detailsRows = jQuery('tr.details');
    for(var i = 0; i < numOfRows; ++i)
    {
        testRows.eq(i).hide();
        detailsRows.eq(i).hide();
        if(!testRows.eq(i).hasClass("collapsed"))
            testRows.eq(i).click();
    }
    //core processing of new display
    btnUsedToResetDisplayedRows = clicked
    var btn = event["target"]["className"]
    btn = btn.substr(3, btn.length);
    var start = (btn - 1) * numOfRows;
    if(start + numOfRows > myDataSetArray.length)
        numOfRows = numOfRows - (start + numOfRows - myDataSetArray.length);//can be simplified
    for(var i = 0; i < numOfRows; ++i)
    {
        jQuery("tr.test." + (i + 1)).find(".testType").html(myDataSetArray[start + i]['Test Type']);
        jQuery("tr.test." + (i + 1)).find(".description").html(myDataSetArray[start + i]['Description']);
        jQuery("tr.test." + (i + 1)).find(".status").html(myDataSetArray[start + i]['Status']);
        jQuery("tr.test." + (i + 1)).find(".passCriteria").html(myDataSetArray[start + i]['Pass Criteria']);
        
        jQuery("tr.details." + (i + 1)).find(".testDetails").html(myDataSetArray[start + i]['Test Details']);
        
        testRows.eq(i).show();
        detailsRows.eq(i).show();
    }
}

$('.test').click(function()
    {
        if($(this).hasClass("collapsed"))
        {
            $(this).nextUntil('tr.test').find('td').parent().find('td > div').slideDown("fast", function()
                {
                    var $set = $(this);
                    $set.replaceWith($set.contents());
                }
            );
            $(this).removeClass("collapsed");
            $(this).find('.expandCollapse').html("<B>-</B>");
        }
        else
        {
            $(this).nextUntil('tr.test').find('td').wrapInner('<div style="display: block;" />').parent().find('td > div').slideUp("fast");
            $(this).addClass("collapsed");
            $(this).find('.expandCollapse').html("<B>+</B>");
        }
    }
);

$('.title').click(function()
{
    if($(this).hasClass("collapsed"))
    {
        $(this).nextUntil('tr.title').find('td').parent().find('td > div').slideDown("fast", function()
        {
            var $set = $(this);
            $set.replaceWith($set.contents());
        });
        
        $(this).removeClass("collapsed");
        $(this).find('.showHideNotes').html("<B>-</B>");
    }
    else
    {
        $(this).nextUntil('tr.title').find('td').wrapInner('<div style="display: block;" />').parent().find('td > div').slideUp("fast");
        $(this).addClass("collapsed");
        $(this).find('.showHideNotes').html("<B>+</B>");
    }
});
</script>
