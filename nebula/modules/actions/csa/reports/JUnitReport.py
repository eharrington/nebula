'''
Creator: Tao Qin
Date Create: 09/18/2018
Description:
'''

import sys
import copy
sys.path.append('..')
class JUnitReport():
    def __init__(self,filename='default.xlm'):
        self.__tcHeader = None
        self.__tcList = []    #new List<TestAttributes>()
        self.__status = False  # default to a fail test case
        self.__filename = filename
        self.__ff = None #file io
        self.__totalcount = {'passes':0,'errors':0,'failures':0,'skipped':0}
        return

    def __SkipReason_to_xml(self,skipreason):
        return "<skip message={0}></skip>\n".format(skipreason)
    def __xmlEscapedValues(self, v):
        v = v.replace("&", "&amp;")
        v = v.replace("<", "&lt;")
        v = v.replace(">", "&gt;")
        v = v.replace("\"", "&quot;")
        v = v.replace("'", "&apos;")
        return v

    """
    This function is rewriting TestCaseJunit as required combining in one file
    """
    def __TestItem_to_xml(self, ti,ta):
        tc_name = self.__xmlEscapedValues(ti.Description.replace("/r","<cr>").replace("/n","<lf>").replace("\r","<cr>").replace("\n","<lf>"))
        class_name = self.__xmlEscapedValues(ti.Description.replace("/r","<cr>").replace("/n","<lf>").replace("\r","<cr>").replace("\n","<lf>"))
        try:
            duration = ti.Duration.replace("sec","")
            duration = float(duration)
        except:
            duration = 0.0
            
        expected_val = self.__xmlEscapedValues(ti.Expectvalue.replace("value=","").replace("<br>HEX:"," [HEX]:"))  #remove <br> because it was manually inserted.
        actual_val   = self.__xmlEscapedValues(ti.Result.replace("value=","").replace("<br>HEX:"," [HEX]:"))
        get_last_sucess_ver = self.__xmlEscapedValues(ta.LastSuccessfulVersion)
        test_id = ta.TestID
        pass_criteria = self.__xmlEscapedValues(ti.PassCriteria)
        #bm 07/10/19:remove PassFail
        get_fail_or_skip_reason = self.__xmlEscapedValues(ta.FailureOrSkipReason)
        if(ti.Status.lower() == "pass"):
            internal_attr = ""
        elif(ti.Status.lower() == "fail"):
            internal_attr = "<failure>" + expected_val + "\n" + get_fail_or_skip_reason + "</failure>"
        elif(ti.Status.lower() == "error"):
            internal_attr =  "<error></error>"
        else:
            internal_attr = "<skipped></skipped>"

        param_list = [tc_name, class_name, duration, expected_val, actual_val, get_last_sucess_ver, test_id, pass_criteria, internal_attr]
        return '\t\t\t<testcase name=\"{0}\" classname=\"{1}\" time=\"{2}\" expectedvalue=\"{3}\" actualvalue=\"{4}\" lastSuccessfullversion=\"{5}\" testid=\"{6}\" passcriteria=\"{7}\">{8}</testcase>'.format(*param_list)

    """
    This function is rewriting TestSuiteJunit as required combining in one file
    """
    def __TestAttr_to_xml(self, ta):
        #GetDescription
        suite_name = ta.TestName
        errors = 0
        failures = 0
        passes = 0
        skipped = 0
        ti_info = ""
        for ti in ta.GetSubTestItems():
            #print('.', end='')
            if ti.Status.lower() == "pass":
                passes += 1
            elif ti.Status.lower() == "fail":
                failures += 1
            elif ti.Status.lower() == "error":
                errors += 1
            else:
                skipped += 1
            ti_info += self.__TestItem_to_xml(ti, ta) + "\n"            
        
        #tests = passes + failures + errors + skipped
        #param_list = [suite_name, tests, errors, failures, skipped,ti_info]
        #xml_attribute = "\t\t<testsuite name=\"{0}\" tests=\"{1}\" errors=\"{2}\" failures=\"{3}\" skipped=\"{4}\">\n{5}\t\t</testsuite>".format(*param_list)
        #print(xml_attribute)
        return passes, errors, failures, skipped, ti_info

    def AddTestAttribute(self, tcAttr):
        errors = 0
        failures = 0
        passes = 0
        skipped = 0
    
        for ti in tcAttr.GetSubTestItems():
            if ti.Status.lower() == "pass":
                passes += 1
            elif ti.Status.lower() == "fail":
                failures += 1
            elif ti.Status.lower() == "error":
                errors += 1
            else:
                skipped += 1
        self.__totalcount['errors'] += errors
        self.__totalcount['passes'] += passes
        self.__totalcount['failures'] += failures
        self.__totalcount['skipped'] += skipped  

        self.__tcList.append(copy.deepcopy(tcAttr))      
        return True   #a hard True for now
    def AddTestAttributeList(self,tcAttrs): #'''List<TestAttributes> '''
        if tcAttrs != None:
            for tcAttr in tcAttrs:
                self.AddTestAttribute(copy.deepcopy(tcAttr))
            return True   #a hard True for now
        return False
    def DeleteTestAttribute(self, tcAttr):
        errors = 0
        failures = 0
        passes = 0
        skipped = 0

        try:
            for ti in tcAttr.GetSubTestItems():
                if ti.Status.lower() == "pass":
                    passes += 1
                elif ti.Status.lower() == "fail":
                    failures += 1
                elif ti.Status.lower() == "error":
                    errors += 1
                else:
                    skipped += 1
            self.__totalcount['errors'] -= errors
            self.__totalcount['passes'] -= passes
            self.__totalcount['failures'] -= failures
            self.__totalcount['skipped'] -= skipped  
            self.__tcList.remove(tcAttr)           
        except:
            e = sys.exc_info()
            print('Delete Attribute error:')
            print(e)
            return False
        return True   #a hard True for now
    def DeleteTestAttributebyIndex(self, index):
        errors = 0
        failures = 0
        passes = 0
        skipped = 0

        try:
            for ti in self.__tcList[index].GetSubTestItems():
                if ti.Status.lower() == "pass":
                    passes += 1
                elif ti.Status.lower() == "fail":
                    failures += 1
                elif ti.Status.lower() == "error":
                    errors += 1
                else:
                    skipped += 1
            self.__totalcount['errors'] -= errors
            self.__totalcount['passes'] -= passes
            self.__totalcount['failures'] -= failures
            self.__totalcount['skipped'] -= skipped  
            self.__tcList.pop(index)
        except:
            e = sys.exc_info()
            print('Delete Attribute error:')
            print(e)
            return False
        return True   #a hard True for now
    def SetHeader(self, testheader):
        self.__tcHeader = testheader
        return True   #a hard True for now
    def GetJunitReport(self):
        return self.GetReport()

    def GetReport(self):
        testattr = ""
        total_passes=0; total_errors=0; total_failures=0; total_skipped=0
        
        header = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n'
        header +='\t<testsuites name=\"{0}\" swname=\"{1}\" swversion=\"{2}\" envr=\"{3}\" datetime=\"{4}\" duration=\"{5}\" '
        header +='totalpass=\"{6}\" totalfail=\"{7}\" totalskipped=\"{8}\">\n'
            
        header_param = [self.__filename, self.__tcHeader.SW_Name, self.__tcHeader.SW_Version,
                    self.__tcHeader.Environment, self.__tcHeader.DateTime,
                    str(self.__tcHeader.TestRunDuration),
                    self.__totalcount['passes'], #self.__tcHeader.TotalPassed(),
                    self.__totalcount['errors'] +self.__totalcount['failures'] , #self.__tcHeader.TotalFailed(),
                    self.__totalcount['skipped'] #self.__tcHeader.Skipped()
                    ]

        ret = ""
        try:
            self.__ff.write(header.format(*header_param))
        except Exception as e:
            raise type(e)("Create Junit header error:" + str(e))

        for ta in self.__tcList:
            print('*', end='')
            passes, errors, failures, skipped, subtc_xml = self.__TestAttr_to_xml(ta)            
            total_passes +=passes
            total_errors += errors
            total_failures += failures
            total_skipped += skipped

            if len(subtc_xml) == 0:
                subtc_info =self.__SkipReason_to_xml(ta.GetValue(ta.FieldName.FailureOrSkipReason))
            else:
                subtc_xml = subtc_xml[:len(subtc_xml)]
            ret += subtc_xml+"\n"
            
            try:
                duration = ta.Duration.replace("sec","")
                duration = float(duration)
            except:
                duration = 0.0
            param_list = [self.__xmlEscapedValues(ta.TestName), duration, (passes + failures + errors + skipped), errors, failures, skipped,subtc_xml]
            xml_attribute = "\t\t<testsuite name=\"{0}\" time=\"{1}\" tests=\"{2}\" errors=\"{3}\" failures=\"{4}\" skipped=\"{5}\">\n{6}\n\t\t</testsuite>\n".format(*param_list)
            try:
                self.__ff.write(xml_attribute)
            except Exception as e:
                raise type(e)("Create Junit body error:" + str(e))

        print("\n")

        if total_errors > 0 or total_failures > 0:
            status = True
        else:
            status = False
        
        header_param = [self.__filename, self.__tcHeader.SW_Name, self.__tcHeader.SW_Version,
                        self.__tcHeader.Environment, self.__tcHeader.DateTime,
                        str(self.__tcHeader.TestRunDuration),
                        total_passes, #self.__tcHeader.TotalPassed(),
                        total_errors+total_failures , #self.__tcHeader.TotalFailed(),
                        total_skipped #self.__tcHeader.Skipped()
                        ]

        #ret = header.format(*header_param)+ret+"  </testsuites>"
        try:
            self.__ff.write("\n</testsuites>")
        except Exception as e:
            raise type(e)("Create Junit header error:" + str(e))


        return status,ret

    def CreateReport(self,filename=None):
        try:
            if filename == None and (self.__filename != None or len(self.__filename.strip()) > 0):
               filename = self.__filename
            if filename == None or len(filename.strip()) == 0:
               filename = 'default.xml'
            self.__ff = open(filename+".xml" , 'w')

            status,body = self.GetReport()
            #with open(filename+".xml" , 'w') as ff:
            #self.__ff.write(body)
            self.__ff.close()
            return status
        except Exception as e:
            raise type(e)("Creation Junit report error:" + str(e))
        
        return status