'''
Creator: Bouda Meas
Date Create: 03/7/2019
Description:
'''
import sys
import copy
import os
import json
class HTMLReport:
    """description of class"""
    def __xmlEscapedValues(self, v):
        v = v.replace("&", "&amp;")
        v = v.replace("<", "&lt;")
        v = v.replace(">", "&gt;")
        v = v.replace("\"", "&quot;")
        v = v.replace("'", "&apos;")
        return v    
    def __buildHTMLTable(self,subitemtest):
        str_result = ""
        colors = {"green":"#33b55c","red":"#de003"}
        if len(subitemtest) == 0:
            return None

        str_result = str_result + ("<table width:100% border=1>")
        '''header'''
        str_result = str_result + ("<tr>")
        #bm 07/10/19: remove Attribute Name        
        str_result = str_result + ("<th>Description</th> ")
        str_result = str_result + ("<th>Pass Criteria</th> ")
        str_result = str_result + ("<th>Expect Value</th>")
        str_result = str_result + ("<th>Actual/UUT Value</th>") #bm 06/02/2018:Added "/UUT"
        str_result = str_result + ("<th>Pass/Fail</th>")
        str_result = str_result + ("</tr>")

        for item in subitemtest:            
            if str(item.Status).lower() == "fail":
               str_result = str_result + ("<tr bgcolor=\"yellow\">")
            else:
               str_result = str_result + ("<tr>")
               
            #str_result = str_result + ("<tr>")
            #bm 07/10/19: remove Attribute Name
            #bm 07/19/19: replace "<" with escape html character
            str_result = str_result + ("<th>" + self.__xmlEscapedValues(str(item.Description.replace("/r","<cr>").replace("/n","<lf>").replace("\r","<cr>").replace("\n","<lf>")))  + "</th>")            
            str_result = str_result + ("<th>" + str(item.PassCriteria) + "</th>")
            str_result = str_result + ("<th>" +  self.__xmlEscapedValues(str(item.Expectvalue)) +"</th> ")
            if (type(item.Result) == type(b'')):
                str_result = str_result + ("<th>" + (str(item.Result.decode("utf-8"))) + "</th> ")
            else:
                str_result = str_result + ("<th>" +  self.__xmlEscapedValues((str(item.Result))) + "</th> ")  #self.__xmlEscapedValues
            if str(item.Status).lower() == "pass":                
                str_result = str_result + ("<th>{}</th>".format("<font color = {}>Pass</font>".format(colors['green'])))
            else: #style="background-color:red" ${yellow background bold red text}
                str_result = str_result + ("<th bgcolor=\"yellow\">{}</th>".format("<font color = {}><b>Fail</b></font>".format(colors['red'])))
                #("<font \"color=\"{}\"><th>Fail</th></font>".format(colors['red']))
            str_result = str_result + ("</tr>")

        str_result = str_result + ("</table>")
        return str_result
        
    def __BuildHeader(self,TotalPassed,TotalFailed,TotalSkipped):
        keylist = ['${SW_Name}','${SW_Version}','${Environment}','${DateTime}','${TestRunDuration}',\
               '${TotalPassed}','${TotalFailed}','${Skipped}','${TAS}','${TMID}','${TestNotes}']  #,'${rowbodyhtml}'

        body = copy.deepcopy(self.__contentBody)
        for item in self.__tcHeader.FieldName:
            value = self.__tcHeader.GetValue(item)
            if ("${"+item.name+"}") in keylist:
               if "${"+item.name+"}" in ['${TotalPassed}','${TotalFailed}','${Skipped}']:
                  switch_name = {
                      '${TotalPassed}':body.replace("${"+item.name+"}",str(TotalPassed)),
                      '${TotalFailed}':body.replace("${"+item.name+"}",str(TotalFailed)),
                      '${Skipped}':body.replace("${"+item.name+"}",str(TotalSkipped))
                  }
                  body = switch_name.get("${"+item.name+"}",body)                  
               else:
                   body = body.replace("${"+item.name+"}",value)
            
        return body
    def __BuildRow(self):
        paginationData = []  #this get pass to the script.js -- myDataSetArray
        paginationDataFormat ="\"JSON Index\": {0}, \"Test Type\": \"{1}\", \"Description\": \"{2}\", \"Status\": \"<font color = {3}>{4}</font>\",\"Pass Criteria\": \"{5}\", \"Test Details\":'{6}'"
        colors = {"green":"#33b55c","red":"#de003"}
        rowsbody = ""
        keylist = ["${TestName}", "${TestType}","${TestSuite}","${TestID}","${TestLink}","${Status}",\
                   "${Duration}","${Description}","${LastSuccessfulVersion}","${PassCriteria}","${MemoryUsage}",\
                   "${FailureOrSkipReason}"]                   
        index = 1
        paginationstring = ""
        for testcase in self.__tcList:
            row = copy.deepcopy(self.__contentRow)  #<tr class = "test x"> <tr class = "details x"> rowbody.html
            testdetail = copy.deepcopy(self.__testDetail)  #detail.html
            pagination = paginationDataFormat.format(index,"${TestType}","${Description}","${color}","${Status}","${PassCriteria}","${testDetails}")
            value = ""
            print('*', end='')
            for item in testcase.FieldName:
                if item == testcase.FieldName.FailureOrSkipReason:
                    value = self.__buildHTMLTable(testcase.GetSubTestItems())
                else:
                    value = self.__xmlEscapedValues(testcase.GetValue(item))
                    
                #if item.name == "MemoryUsage":
                #    testdetail = testdetail.replace("${"+item.name+"}",value)
                if ("${"+item.name+"}") in keylist:
                    
                    if item.name.lower() == 'status':
                       if value.lower() == 'pass':
                            testdetail = testdetail.replace("${color}",colors['green'])
                            pagination = pagination.replace("${color}",colors['green'])
                            row = row.replace("${color}",colors['green']).replace("${bgcolor}","")
                            
                       else:
                            testdetail = testdetail.replace("${color}",colors['red'])
                            pagination = pagination.replace("${color}",colors['red'])
                            row = row.replace("${color}",colors['red']).replace("${bgcolor}","bgcolor=\"yellow\"")                            
                            value = "<b>" + value + "</b>"
                    #${testDetails} -- <U>Failure Or Skip Reason</U>
                    pagination = pagination.replace("${"+item.name+"}",value)
                    testdetail = testdetail.replace("${"+item.name+"}",value)
                    row = row.replace("${"+item.name+"}",value)

            numfound = row.count("${TestType}")
            row = row.replace("${index}",str(index))           
            rowsbody += row.replace("${testDetails}",testdetail)
                        
            #myDataSetArray -- ${testDetails}
            pagination = pagination.replace("${testDetails}",testdetail)
            if pagination.find("${testDetails}") > -1:  #"${testDetails}"
               print("[FAIL error]: {0}>>{1}".format(len(pagination),pagination))
               pass #fail to replace            

            pagination=pagination.replace("\n","\\n").replace("\r","\\r")
            #search internal {}
           #pos = pagination.find("{",2)
            #if pos > 0:
            #   pagination = pagination[:pos] + self.__xmlEscapedValues(pagination[pos:])
            paginationData.append("{"+pagination+"}")
            index += 1
        
        if len(paginationData) > 0:
           paginationstring = ",\r".join(paginationData) #.join(",")
        else:
            paginationstring = ""

        if paginationstring.find("\n") > -1:
           paginationstring=paginationstring.replace("\n","\\n")

        try:
            js = json.loads(paginationstring)
            pass
        except Exception as error:
            pass
        return rowsbody,paginationstring
    def __readfile(self,filename):
        content = None
        try:            
            fp = open(self.__defaultpath  + filename, 'r')
            content = fp.read()
            fp.close()
        except Exception as e:
            raise type(e)("Problem with template file:{0}\n{1}".format(self.__defaultpath+filename,str(e)))
            
        return content
        
    def __init__(self,tempplatepath=None,filename='default.html'):
        self.__tcHeader = None  #TestHeader
        self.__tcList = []    #new List<TestAttributes>()
        self.__status = False   #default to a fail test case
        self.__filename = filename
        self.__templatepath = tempplatepath
        frozen = 'not'        
        if getattr(sys, 'frozen', False):
            # we are running in a bundle
            frozen = 'ever so'
            #the path needs to be sync with "datas" in the pyinstaller spec file.
            self.__defaultpath = sys._MEIPASS + "/reports/forms/"
        else:
            # we are running in a normal Python environment
            #bundle_dir = os.path.dirname(os.path.abspath(__file__))
            if tempplatepath != None:
                self.__defaultpath = tempplatepath  
            else:           
                self.__defaultpath = './reports/forms/'
        
        self.__content = None
        self.__filelist = {"body":"body.html","script":"script.js","row":"rowbody.html","detail":"detail.html"}
        self.__contentBody = self.__readfile(self.__filelist['body'])
        self.__contentRow = self.__readfile(self.__filelist['row'])
        self.__testDetail = self.__readfile(self.__filelist['detail'])
        self.__contentScript = self.__readfile(self.__filelist['script'])
        self.__totalpass = 0
        self.__totalfail = 0
        self.__totalskip = 0
        return
    
    def SetHeader(self, testheader):
        self.__tcHeader = testheader
        return True   #a hard True for now
    def AddTestAttribute(self, tcAttr):
        errors = 0
        failures = 0
        passes = 0
        skipped = 0
    
        for ti in tcAttr.GetSubTestItems():
            if ti.Status.lower() == "pass":
                passes += 1
            elif ti.Status.lower() == "fail":
                failures += 1
            elif ti.Status.lower() == "error":
                errors += 1
            else:
                skipped += 1
        self.__totalcount['errors'] += errors
        self.__totalcount['passes'] += passes
        self.__totalcount['failures'] += failures
        self.__totalcount['skipped'] += skipped  
        self.__tcList.append(copy.deepcopy(tcAttr))  #bm 03/07/2019: Make a copy of it.
        return True   #a hard True for now
    def AddTestAttributeList(self,tcAttrs): #'''List<TestAttributes> '''
        if tcAttrs != None:
            for tcAttr in tcAttrs:
                self.__tcList.append(copy.deepcopy(tcAttr))
            return True   #a hard True for now
        return False
    def DeleteTestAttribute(self, tcAttr):        
        errors = 0
        failures = 0
        passes = 0
        skipped = 0

        try:
            for ti in tcAttr.GetSubTestItems():
                if ti.Status.lower() == "pass":
                    passes += 1
                elif ti.Status.lower() == "fail":
                    failures += 1
                elif ti.Status.lower() == "error":
                    errors += 1
                else:
                    skipped += 1
            self.__totalcount['errors'] -= errors
            self.__totalcount['passes'] -= passes
            self.__totalcount['failures'] -= failures
            self.__totalcount['skipped'] -= skipped  
            self.__tcList.remove(tcAttr)
            return True
        except:
            e = sys.exc_info()
            print('Delete Attribute error:')
            print(e)
            return False
        
        return True   #a hard True for now
    def DeleteTestAttributebyIndex(self,index):        
        errors = 0
        failures = 0
        passes = 0
        skipped = 0

        try:
            for ti in self.__tcList[index].GetSubTestItems():
                if ti.Status.lower() == "pass":
                    passes += 1
                elif ti.Status.lower() == "fail":
                    failures += 1
                elif ti.Status.lower() == "error":
                    errors += 1
                else:
                    skipped += 1
            self.__totalcount['errors'] -= errors
            self.__totalcount['passes'] -= passes
            self.__totalcount['failures'] -= failures
            self.__totalcount['skipped'] -= skipped
        except:
            e = sys.exc_info()
            print('Delete Attribute error:')
            print(e)
            return False
        return True   #a hard True for now
        
    def GetReport(self,testnote=""):
        '''Create Review Style HTML and indicate if there is a failure test case
          
           Return True = No Failure found.  Otherwise, Failure found
                  HTML Content
        '''
        self.__totalpass = 0
        self.__totalfail = 0
        self.__totalskip = 0
        
        for testcase in self.__tcList:
            subtotalpass = 0
            subtotalfail = 0
            subtotalskip = 0            
            for subtestcase in testcase.GetSubTestItems():
                if subtestcase.Status.lower() == 'pass':
                    subtotalpass +=1
                elif subtestcase.Status.lower() == 'fail':
                    subtotalfail +=1
                elif subtestcase.Status.lower() == 'skip':
                    subtotalskip +=1
            
            if subtotalpass == 0:
                if testcase.Status.lower() == 'pass':
                   self.__totalpass +=1
            else:                    
                   self.__totalpass += subtotalpass

            if subtotalfail == 0:
                if testcase.Status.lower() == 'fail':
                   self.__totalfail +=1
            else:                    
                   self.__totalfail +=subtotalfail

            if subtotalskip == 0:
                if testcase.Status.lower() == 'skip':
                   self.__totalskip +=1
            else:                    
                   self.__totalskip +=subtotalskip

        body = self.__BuildHeader(self.__totalpass,self.__totalfail,self.__totalskip)                
        rowhtml,paginationData = self.__BuildRow()
        body = body.replace("${theTestNotes}",testnote) + "\n\n"
        body = body.replace("${rowbodyhtml}",rowhtml) + "\n\n"

        body += self.__contentScript.replace("${myDataSetArray}",paginationData) + "\n"
        
        print("\n")
        return (self.__totalfail > 0), body

    def CreateReport(self,filename=None):
        try:
            if filename == None and (self.__filename != None or len(self.__filename.strip()) > 0):
               filename = self.__filename
            if filename == None or len(filename.strip()) == 0:
               filename = 'default.html'
            
            status,body = self.GetReport()
            with open(filename+".html" , 'w') as ff:               
               ff.write(body)
               ff.close()
            return status
        except Exception as e:
            raise type(e)("Creation HTML report error:" + str(e))
        
        return status