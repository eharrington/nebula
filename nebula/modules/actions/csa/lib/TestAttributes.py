"""Creator: Bouda Meas """
"""Date Create: 11/7/2016 """
"""Description: Bouda Meas """

import json
import copy
import dicttoxml
from enum import Enum

class TestAttributes(object):
    """
    Test case object
    Note: Require json, copy modules
    
    Parameters
    ----------
    None
    
    """
    class FieldName(Enum):
        """
        Enumerator

        Parameters
        ----------
        None
        
        """
        TestType = 1
        Description = 2
        Status = 3     #pass/fail
        Duration = 4
        TestSuite = 5
        TestName = 6
        PassCriteria = 7
        FailureOrSkipReason = 8
        LastSuccessfulVersion = 9
        TestID = 10
        TestLink = 11
        ExpectValue = 12
        MemoryUsage = 13  #bm 09/013/17:change from TestRunDuration to TestRunDuration

    class __TestAttributesStore(object):
        def __init__(self):  #bm 01/24/2019:Remove extra _
            self.TestType = ""
            self.Description = ""
            self.Status = ""
            self.Duration = ""
            self.TestSuite = ""
            self.TestName = ""
            self.PassCriteria = ""
            self.FailureOrSkipReason = ""
            self.LastSuccessfulVersion = ""
            self.TestID = ""
            self.TestLink = ""
            self.ExpectValue = ""
            self.MemoryUsage = ""

            return

        def toXML(self,includeHeader=True):  #bm 07/18/19:Added
            xml = dicttoxml.dicttoxml(self.__dict__, custom_root='TestAttributes', attr_type=False, root=includeHeader)
            return str(xml).replace("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>","")
        def toString(self):
            return json.dumps(self, default=lambda o: o.__dict__,
                              sort_keys=True, indent=4)

    def __init__(self):
        self.__subitemtest = []
        self.__testattrb = self.__TestAttributesStore()
        return   
    def __SkipReason_to_xml(self,skipreason):
        return "<skip message={0}></skip>\n".format(skipreason)
    def __xmlEscapedValues(self, v):
        v = v.replace("&", "&amp;")
        v = v.replace("<", "&lt;")
        v = v.replace(">", "&gt;")
        v = v.replace("\"", "&quot;")
        v = v.replace("'", "&apos;")
        return v       

    """
    This function is rewriting TestCaseJunit as required combining in one
    """
    def __TestItem_to_xml(self, ti):
        tc_name = ti.Description
        class_name = self.__xmlEscapedValues(ti.Description)
        try:
            time = float(ti.Duration)
        except:
            time = 0
        expected_val = self.__xmlEscapedValues(ti.Expectvalue)
        get_last_sucess_ver = self.__xmlEscapedValues(ti.LastSuccessfulVersion)
        test_id = self.TestID
        pass_criteria = self.__xmlEscapedValues(ti.PassCriteria)
        #bm 07/10/19:remove PassFail
        get_fail_or_skep_reason = self.__xmlEscapedValues(self.FailureOrSkipReason)
        if(ti.Status.lower() == "pass"):
            internal_attr = ""
        elif(ti.Status.lower() == "fail"):
            internal_attr = "<failure>" + expected_val + "\n" + get_fail_or_skep_reason + "</failure>"
        elif(ti.Status.lower() == "error"):
            internal_attr =  "<error></error>"
        else:
            internal_attr = "<skipped></skipped>"

        param_list = [tc_name, class_name, time, expected_val, get_last_sucess_ver, test_id, pass_criteria, internal_attr]
        return '\t\t\t<testcase name=\"{0}\" classname=\"{1}\" time=\"{2}\" ExpectedValue=\"{3}\" LastSuccessfulVersion=\"{4}\" TestID=\"{5}\" PassCriteria=\"{6}\">{7}</testcase>'.format(*param_list)
        
    @property
    def __buildXML(self):
        xml_result = ""
        for ti in ti.GetSubTestItems():
            xml_result +=ti.GetXMLString(includeHeader=True)+"\n"
        return xml_result
    @property
    def __buildHTMLTable(self):
        str_result = ""
        colors = {"green":"#33b55c","red":"#de003"}
        if len(self.__subitemtest) == 0:
            return None

        str_result = str_result + ("<table width:100% border=1>")
        '''header'''
        str_result = str_result + ("<tr>")
        #bm 07/10/19: remove Attribute Name        
        str_result = str_result + ("<th>Description</th> ")
        str_result = str_result + ("<th>Pass Criteria</th> ")
        str_result = str_result + ("<th>Expect Value</th>")
        str_result = str_result + ("<th>Actual/UUT Value</th>") #bm 06/02/2018:Added "/UUT"
        str_result = str_result + ("<th>Pass/Fail</th>")
        str_result = str_result + ("</tr>")

        for item in self.__subitemtest:            
            if str(item.Status).lower() == "fail":
               str_result = str_result + ("<tr bgcolor=\"yellow\">")
            else:
               str_result = str_result + ("<tr>")
               
            #str_result = str_result + ("<tr>")
            #bm 07/10/19: remove Attribute Name
            #bm 07/19/19: replace "<" with escape html character
            str_result = str_result + ("<th>" + self.__xmlEscapedValues(str(item.Description.replace("/r","<cr>").replace("/n","<lf>").replace("\r","<cr>").replace("\n","<lf>")))  + "</th>")            
            str_result = str_result + ("<th>" + str(item.PassCriteria) + "</th>")
            str_result = str_result + ("<th>" + str(item.Expectvalue) +"</th> ")
            if (type(item.Result) == type(b'')):
                str_result = str_result + ("<th>" + self.__xmlEscapedValues(str(item.Result.decode("utf-8"))) + "</th> ")
            else:
                str_result = str_result + ("<th>" + self.__xmlEscapedValues(str(item.Result)) + "</th> ")
            if str(item.Status).lower() == "pass":                
                str_result = str_result + ("<th>{}</th>".format("<font color = \"{}\">Pass</font>".format(colors['green'])))
            else: #style="background-color:red" ${yellow background bold red text}
                str_result = str_result + ("<th bgcolor=\"yellow\">{}</th>".format("<font color = \"{}\"><b>Fail</b></font>".format(colors['red'])))
                #("<font \"color=\"{}\"><th>Fail</th></font>".format(colors['red']))
            str_result = str_result + ("</tr>")

        str_result = str_result + ("</table>")
        return str_result
    @property
    def VERSION(self):
        return "1.0.0.19"
    @property
    def TestType(self):
        return self.GetValue(self.FieldName.TestType.value)    
    @property
    def Description(self):
        return self.GetValue(self.FieldName.Description.value)    
    @property    
    def Status(self):
        return self.GetValue(self.FieldName.Status.value)
    @property   
    def Duration(self):
        return self.GetValue(self.FieldName.Duration.value)
    @property
    def TestSuite(self):
        return self.GetValue(self.FieldName.TestSuite.value)
    @property
    def TestName(self):
        return self.GetValue(self.FieldName.TestName.value)
    @property  
    def PassCriteria(self):
        return self.GetValue(self.FieldName.PassCriteria.value)
    @property  
    def FailureOrSkipReason(self):
        #bm 07/19/19: exception to the rule because XML use actual data
        return self.__testattrb.FailureOrSkipReason
        #return self.GetValue(self.FieldName.FailureOrSkipReason.value)
    @property    
    def LastSuccessfulVersion(self):
        return self.GetValue(self.FieldName.LastSuccessfulVersion.value)    
    @property
    def TestID(self):
        return self.GetValue(self.FieldName.TestID.value)
    @property 
    def TestLink(self):
        """
        Return a html ancher with url and test id
        """
        return "<a href=\"{0}\">{1}</a>".format(self.GetValue(self.FieldName.TestLink.value), self.GetValue(self.FieldName.TestID.value))    
    @property  
    def ExpectValue(self):
        return self.GetValue(self.FieldName.ExpectValue.value)
        
    def AddSubItemTest(self,tcItem):
        """
        Parameters
        ----------
        itemtcItem: TestItem
        
        Return True if the item is successfully added.
        """
        #localcopy = copy.deepcopy(item)
        #bm 07/05/2018: Add feature.
        
        if str(type(tcItem)) == "<class 'list'>":
            self.__subitemtest.extend(copy.deepcopy(tcItem))  #bm 03/07/2019: Make a copy of it
        else:
            self.__subitemtest.append(copy.deepcopy(tcItem))  #bm 03/07/2019: Make a copy of it
        return True

    def RemoveSubItemTest(self,tcItem):
        """
        Parameters
        ----------
        tcItem : TestItem
        
        Return True if the item is successfully removed.
        """
        try:
            return self.__subitemtest.remove(tcItem)
        except Exception:
            pass
        return False
    def RemoveSubItemTestByIndex(self, index):
        """
        Delete a sub test item.

        Parameters
        ----------
        index : int
        
        Return True/False
            True  -- Test item removes successfully.
            False -- Remove test item fail.
        """
        try:
            self.__subitemtest.pop(index)
            return True
        except Exception:
            pass
        return False
    def GetSubTestItems(self):
        """
        Get a list of sub test items.

        Parameters
        ----------
        None
        
        Return array of TestItem -- sub test cases           
        """
        #bm 07/05/2018: Add feature
        return self.__subitemtest.copy()
    def GetSubItemTestByIndex(self, index):
        """
        Get a sub test item by index.

        Parameters
        ----------
        index : int
        
        Return TestItem or None
           
        """
        try:
            return copy.deepcopy(self.__subitemtest[index])
        except Exception:
            pass
        return None    

    def GetXMLString(self,includeHeader=True):
        """
         Parameters
        ----------
        includeHeader : True or False.        
        
        Return a pair value of the TestAttributes(test case) in xml format
        """
        savestr = self.__testattrb.FailureOrSkipReason  #need to reset later        
        
        if self.__testattrb.FailureOrSkipReason.lower().strip() == 'tbd':
           self.__testattrb.FailureOrSkipReason = str(self.__buildXML)
        else:
           self.__testattrb.FailureOrSkipReason = self.__testattrb.FailureOrSkipReason + "\n" + str(self.__buildXML)
        str_result = self.__testattrb.toXML(includeHeader=includeHeader)
        self.__testattrb.FailureOrSkipReason = savestr
        
        return  str_result    
        
    def GetJsonString(self):
        """
        Return a pair value of the test attribute in json format
        """ 
        savestr = self.__testattrb.FailureOrSkipReason  #need to reset later
        html =  self.__buildHTMLTable  #to make it compatible with Vasantha's report viewer
        if html is None:
            html = ""
            
        if self.__testattrb.FailureOrSkipReason.lower().strip() == 'tbd':
           self.__testattrb.FailureOrSkipReason = html
        else:
           self.__testattrb.FailureOrSkipReason = self.__testattrb.FailureOrSkipReason + html
           
        str_result = self.__testattrb.toString()
        self.__testattrb.FailureOrSkipReason = savestr
        return  str_result
        
    def GetValue(self,fieldname):
        """
        Parameters
        ----------
        filedname : enumerator/field name/enumerator value        
        
        
        Return a value of a given enumerator/field name/enumerator value.
        
        Example:
            GetValue(TestAttributes.FieldName.Status)
            GetValue("Status")
            GetValue(5)
        """
        strValue = ""
        if (self.FieldName.TestType == fieldname) or \
           (self.FieldName.TestType.name == fieldname) or \
           (self.FieldName.TestType.value == fieldname):
             strValue = self.__testattrb.TestType
        elif (self.FieldName.Description == fieldname) or \
             (self.FieldName.Description.name == fieldname) or \
             (self.FieldName.Description.value == fieldname):
               strValue = self.__testattrb.Description
        elif (self.FieldName.Status == fieldname) or \
             (self.FieldName.Status.name == fieldname) or \
             (self.FieldName.Status.value == fieldname):
               strValue = self.__testattrb.Status
        elif (self.FieldName.Duration == fieldname) or \
             (self.FieldName.Duration.name == fieldname) or \
             (self.FieldName.Duration.value == fieldname):
               strValue = self.__testattrb.Duration
        elif (self.FieldName.MemoryUsage == fieldname) or \
             (self.FieldName.MemoryUsage.name == fieldname) or \
             (self.FieldName.MemoryUsage.value == fieldname):  #bm 09/013/17:change from TestRunDuration to TestRunDuration
               strValue = self.__testattrb.MemoryUsage
        elif (self.FieldName.TestSuite == fieldname) or \
             (self.FieldName.TestSuite.name == fieldname) or \
             (self.FieldName.TestSuite.value == fieldname):
               strValue = self.__testattrb.TestSuite
        elif (self.FieldName.TestName == fieldname) or \
             (self.FieldName.TestName.name == fieldname) or \
             (self.FieldName.TestName.value == fieldname):
               strValue = self.__testattrb.TestName
        elif (self.FieldName.PassCriteria == fieldname) or \
             (self.FieldName.PassCriteria.name == fieldname) or \
             (self.FieldName.PassCriteria == fieldname):
               strValue = self.__testattrb.PassCriteria
        elif (self.FieldName.FailureOrSkipReason == fieldname) or \
             (self.FieldName.FailureOrSkipReason.name == fieldname) or \
             (self.FieldName.FailureOrSkipReason.value == fieldname):
               strValue = self.__testattrb.FailureOrSkipReason
        elif (self.FieldName.LastSuccessfulVersion == fieldname) or \
             (self.FieldName.LastSuccessfulVersion.name == fieldname) or \
             (self.FieldName.LastSuccessfulVersion.value == fieldname):
               strValue = self.__testattrb.LastSuccessfulVersion
        elif (self.FieldName.TestID == fieldname) or \
             (self.FieldName.TestID.name == fieldname) or \
             (self.FieldName.TestID.value == fieldname):
               strValue = self.__testattrb.TestID
        elif (self.FieldName.TestLink == fieldname) or \
             (self.FieldName.TestLink.name == fieldname) or \
             (self.FieldName.TestLink.value == fieldname):
               strValue = self.__testattrb.TestLink
        elif (self.FieldName.ExpectValue == fieldname) or \
             (self.FieldName.ExpectValue.name == fieldname) or \
             (self.FieldName.ExpectValue.value == fieldname):
               strValue = self.__testattrb.ExpectValue

        """ bm 07/19/2019: remove this bock of code because other type of report format needs as-is
        #build sub-testcase
        if (fieldname == self.FieldName.FailureOrSkipReason) or \
           (fieldname == self.FieldName.FailureOrSkipReason) or \
           (fieldname == self.FieldName.FailureOrSkipReason.value):
             str_result = self.__buildHTMLTable
             if str_result != None:
                 if str(strValue).lower().strip() == 'tbd':
                    strValue = "\r\n</br>" + str_result
                 else:
                    strValue = str(strValue) + "\r\n</br>" + str_result
        """
        return str(strValue)

    def SetValue(self,fieldname,strValue):
        """        
        Parameters
        ----------
        filedname : enumerator/field name/enumerator value        
        strValue  : string
        
        Example:
            SetValue(TestAttributes.FieldName.Status,"pass")
            SetValue("Status","pass")
            SetValue(5,"pass")
        """
        if (self.FieldName.TestType == fieldname) or \
           (self.FieldName.TestType.name == fieldname) or \
           (self.FieldName.TestType.value == fieldname):
             self.__testattrb.TestType = strValue
        elif (self.FieldName.Description == fieldname) or \
             (self.FieldName.Description.name == fieldname) or \
             (self.FieldName.Description.value == fieldname):
               self.__testattrb.Description = strValue
        elif (self.FieldName.Status == fieldname) or \
             (self.FieldName.Status.name == fieldname) or \
             (self.FieldName.Status.value == fieldname):
               self.__testattrb.Status = strValue
             #bm 05/7/2019:Added self.FieldName.Duration
        elif (self.FieldName.Duration == fieldname) or \
             (self.FieldName.Duration.name == fieldname) or \
             (self.FieldName.Duration.value == fieldname):
               self.__testattrb.Duration = strValue
        elif (self.FieldName.TestSuite == fieldname) or \
             (self.FieldName.TestSuite.name == fieldname) or \
             (self.FieldName.TestSuite.value == fieldname): #bm 07/13:added
               self.__testattrb.TestSuite = strValue
        elif (self.FieldName.TestName == fieldname) or \
             (self.FieldName.TestName.name == fieldname) or \
             (self.FieldName.TestName.value == fieldname): #bm 07/13:added
               self.__testattrb.TestName = strValue
        elif (self.FieldName.PassCriteria == fieldname) or \
             (self.FieldName.PassCriteria.name == fieldname) or \
             (self.FieldName.PassCriteria.value == fieldname):
               self.__testattrb.PassCriteria = strValue
        elif (self.FieldName.FailureOrSkipReason == fieldname) or \
             (self.FieldName.FailureOrSkipReason.name == fieldname) or \
             (self.FieldName.FailureOrSkipReason.value == fieldname):
               self.__testattrb.FailureOrSkipReason = strValue
        elif (self.FieldName.LastSuccessfulVersion == fieldname) or \
             (self.FieldName.LastSuccessfulVersion.name == fieldname) or \
             (self.FieldName.LastSuccessfulVersion.value == fieldname):
               self.__testattrb.LastSuccessfulVersion = strValue       
        #bm 05/07/19: bug replace LastSuccessfulVersion with TestID
        elif (self.FieldName.TestID == fieldname) or \
             (self.FieldName.TestID.name == fieldname) or \
             (self.FieldName.TestID.value == fieldname):
               self.__testattrb.TestID = strValue
        elif (self.FieldName.TestLink == fieldname) or \
             (self.FieldName.TestLink.name == fieldname) or \
             (self.FieldName.TestLink.value == fieldname):
               self.__testattrb.TestLink = strValue
        elif (self.FieldName.ExpectValue == fieldname) or \
             (self.FieldName.ExpectValue.name == fieldname) or \
             (self.FieldName.ExpectValue.value == fieldname):
               self.__testattrb.ExpectValue = strValue
        #bm 05/07/19: Add missing MemoryUsage
        elif (self.FieldName.MemoryUsage == fieldname) or \
             (self.FieldName.MemoryUsage.name == fieldname) or \
             (self.FieldName.MemoryUsage.value == fieldname):
               self.__testattrb.MemoryUsage = strValue
        else:
            return False

        return True