###############################################################################
# The original implementation is from JITC Core.
# This library expands to include non SSH connection.
# Author: Bouda Meas
# Create: 9/26/2017
# Last Update: 04/04/2018
# Requirement:
#   Paramiko v2.2.1
###############################################################################
import datetime
import io
import paramiko
import time
import os
import os.path
import telnetlib
import traceback
import socket
import array
import urllib.request
import base64
import ssl
import json
import logging
import re


class ExtronQAConnection(object):
    """
    TCP/IP connection to Extron non ssh and ssh support.
    Note: Require paramiko,telnetlib, urllib.request, ssl,json,logging,base64 modules
        
    Parameters
    ----------
    conntype : str
        Connection Type: ssh or telnet(tel).  Default tel
    iphost : str
        IP Address/Hostname of the device
    port : int 
        Port number of the device
    login : str
        Login ID or name.  Default 'admin'
    password : str
        Password to the device.  Default 'extron'
    phrase : None
        The prhase to read the ppk_file content.  Default None
    ppk_file : None
        Filename or RSA content.  Default None
    --------------------------------------------------------------------------------------------------------------
    Example:
        Simple Telnet:<br/>
            (conntype="tel",iphost="192.168.1.10",login="admin",password="mypassword",port=23)
        
        Simple SSH without private key:<br/>
            (conntype="ssh_key",iphost="192.168.1.10",login="admin",password="extron",port=22023)

        Simple SSH with default private key:<br/>
            (conntype="ssh_key",iphost="192.168.1.10",login="admin",password="B@Z!ng@+@wQ249v",port=22)
            
        Simple SSH with a private key:<br/>
            (conntype="ssh_key",iphost="192.168.1.10",login="adminz",password="A@Y!ng$+@wQ2490v",
               port=22,phrase="myspecialkey",ppk_file="my_rsa_key_location_file")
            <br/>
            (conntype="ssh_key",iphost="192.168.1.10",login="adminz",password="A@Y!ng$+@wQ2490v",
               port=22,phrase="myspecialkey",ppk_file="-----BEGIN RSA PRIVATE KEY----- <rest of the content> ......")
    """
    width = 80
    height = 25
    term = 'vt100'
    buffersize = 2048
    uriResources = "{0}://{1}/api/swis/resources?uri={2}"
    uriResource = "{0}://{1}/api/swis/resource{2}"
    uri = "{0}://{1}/api{2}"
    __hostip = ""
    DEPTH = 0
    
    @property    
    def __ppk(self):
        """Default JITC PPK content"""
        if (self.__logger != None):
            self.__logger.logDebug("".join(traceback.format_stack()))
        return """PuTTY-User-Key-File-2: ssh-rsa
Encryption: aes256-cbc
Comment: imported-openssh-key
Public-Lines: 6
AAAAB3NzaC1yc2EAAAADAQABAAABAQDo/zZmbgta7tZWa1Pzyki/htFjCH3f5rhW
gZ6FsrvjjrlPJ6yGC3ezGz4JOMGvhCLLj7CW0g5jmCPzXFlbFihdhPUnVnoto6/S
f7lVjDrgTOIhdfDWQIxZWMOtee5MUQt2G1JYGajGR+k5lRkg9aIc6ocaR2hQK8O4
8Ddr+lvIspIDzyUMcdmfFVaOEUggnltqaP0QUCoZoj3Xqj5JiK+y2m+hPdNhIOqL
CqMeVNaHMw/lsDRaV5UAS8mlUtepFEXe9usD1ntKCAU3MEUZQJ3RnQdsgkQ80I91
xzNHczfHHmyYsBq3sp6Xd+OFSsqz4ypHX0Erf84opAp1c1SHFokP
Private-Lines: 14
iS9XUvmGCgudquxjP3g5rj0NwsL4rnBCf0rUt4BxBXJed4BIXYyaWcB5C2ZrKCrp
jVNHSYibpffxnv0O/U+Kj8bOe7tTz7XCJtCkX3DA+SUmOLEUhYJP4jIV5U2/tmuO
Pvz4apBJRXWJxxb8VwevMJxhk+Xbb5jeUs98LIZjBgTBQsHBsYpsaDBf7rec+l4d
XhaR4xtXrnYNcHh0MPlgSxbsuQOR+pGXNlpmkXZpNTRkuytRaqhMaDqmgKFo5JhH
ksqVABSCEPhLCPUVmoUI3x1UTCpw5gzcnMC/3zv8wtcOm2u0qaY1JlTGNlwBBa/E
YWpNKBYyLTA5pmuUaQ+VoZgfXNMDsnkoRiqd06zNd7VvOYgb7+cN+g0v19/szTiV
oh1z0ON/mk7k17eTKvL6taF6yLkAs6RGUKRi4FFqPoDIiuQEZYSLQUFW2uW9LXfH
nEJvqFdUI4+Sy0J9gwAwy1KmVlWfy6s/gF4CuO7klB6Eqo0KQfxlHNWNWG8+8wYO
DiWmxgnL6xmnVi0F9o6P4WWvi+miFeBBMoMdAw/aHeJG0V8i+3T48PikapGyLqGR
pY+Y8/2tchSh41ixgt8efO3JOQaN8OpUVR6wujLTG1dBFaMgdOSYho5qsnYJFXCV
71DVZoCyPvFwfwZWgymLqjt4YzOku2evXIfzSH/uMYvYe9G5X5rlDrLaQnyyp6OS
Aa6Qw2c/OREempTMZOBEIII4x9IPymq02AFhmEKSeqWUWNJ+j0Qqfo9A6djWfO/p
1vA4ZhOhdadG7N0u/8u4jh6cPVcY+jH6txD0CNNRoL2o+CzMKigmOfs6YG0/kZ7b
3kgR9NYIK71DQsImO1bz82CWEBlLAAQNJpLiYtsF3QRu3RZy3ISf4Pp+Ve2vT8t5
Private-MAC: 2728835dfeda4a7fb64bd289c198a83bb42e38b2
"""
    
    @property
    def __defaultrsakey(self):
        """Default JITC RSA content"""
        if (self.__logger != None):
            self.__logger.logInfo("".join(traceback.format_stack()))
       
        return"""-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: DES-EDE3-CBC,46C78224BFF19A78

OkMizhRhXh4LSu3/oievFgvo2XcM+rn/lIoDEetJSmy2wIMpXx7jRW/B/ztWeUgi
0dBjXbfCzh4eeFyYIi5kjLqlbf0xldDwRCMeKUAmXW91Z8Zz4kHqpfO2w/RxGxpq
YMx+adSzBaSWkirJMgSx/erZbfEF7XS/QbhQmiIV7c9UZUz71kWjFoujOYd7rIZl
vsZIAqv0Y1hgbDEJbzIirlY3oRrFmPLX2+pHlRmKugfQXykNk6dX+mRi1AZZ/USL
XI1CTVG2lQX7gaYGvi1oMlFH2SG2lTAPeV3hXOpp8N3Xs++l6fgSJPSNs8qp77FF
aHLNquoKV69mhzHYGP3fJspvq9zmo7bNgOKYEn5EyFuXSG2iBjIKakSxR4XcJ9XL
sI84VxiGpunqDBnstZYeQckQEpF5RIASWY8Dx00BHtkZ1uV3HKrGzOaUE+IvvDIM
cMepwJmQcFp69FSgeRBqZzBRGHOYfKRtM1gxtEz63jmPoFTpC4rYPcKOQhdAaBTL
wkTdwAIgJfgV4wD5Qy8Jb0/hzwfc4YygJvW2+DaG6u7OZdwPqzwgslLb9bKrGG7t
scMR4U05SSgiIyhtLcQb0hZIOCVcC5i9gkRPs5tgvJGPmlFK1/NFRfWjgGkQQ5/I
7pLv1A/bSpaVZvvYwNkX54tTkPXt4/rFrHZXRknzu8FOg9Gx97E/Sb/0VqISHbrn
KbWE6I2BFq3MC+qnSlzXVd4TkOrZ+CN2gUmnfG+JT5Gz0oeH/I6sJNAtPrKUUudu
SE5XHsDbaWd3HKwBeQIzWVIzoE7+R1qw8vtHSrJ+D1GNIi9UxhLLbEhIjqrF91Kt
+JgeXhY9fymyaX8l+c8mwQWNW0MzSukWvnNLJOzR1CqCG1IZ1cBii8eml9/PZop1
1gxuSevEK9sW7rVnotw43QU7EW4uUksWOFDsKLn1TfPDoMmqnTJpg6NXBKmByBy6
1vus+OtrQ0j/WhYJISf/e2xTsTvJ7QxK17Mo5fM8aEMbpmX9/EHmaDlRt/T7sVrn
kv7Ngp+GH2b7MBiBRW68RWFsrYgP1txikUe8/qqDuHVJo0kivcMjh7rmXK9/Ekva
TClaGgH2Q8XeVgjTQaPd1PJ1gm+AE+EvUpuSGvebaifyhrvpEnhS/J+yAzQ6ZWXE
MUjLupH/MQWzv6Lq3AqNUmkDgEnYsKcF7bf3cCjkiV0Re3Xfwm9dZeNzxQ5Pqdj2
XWDh85YBVefHFGy31Yye5w5QFBfCefNPz7BdMjkannB+3lNPY/n9/IJV0PDTNJjf
tNqP1Vtd8T5/+v96yZGSUsYSeFC9gjG7kfLsCFvjd1tQ6Z03/waglVawjjZIGoQ1
bRtaj0eE31GUJLGXFzxdWiVe5snQz3yuAuXO+jSy8odCUQpdTbrqEYbfbYpZRn9j
LB+8tlQ87LjXuWC7LsYCJpryADBZsBxZIrNmm9+gQezkXzKOIONsBMxmXp/tUyX/
fhy2gwHQj7P/Ke8Sg0tlCYlcG4939DCf/4gQV1wye3LBFWKffYx/La7l7b9T0b9G
BlqqaGUM+pvGkfCWy7+y7sGjfs9hBFak4s/xT98NoMW7itloi72Fsw==
-----END RSA PRIVATE KEY-----"""

    @property
    def __MKEY(self):
        keyfile = None
        mykey = None
        if (self.__logger != None):
            self.__logger.logInfo("".join(traceback.format_stack()))
        try:

            keyfile = io.StringIO(self.__defaultrsakey)
            if keyfile != None:
               mykey = paramiko.RSAKey.from_private_key(keyfile,"12nortxe-Admin") #self.__pkey) ## password="12nortxe-Admin") #'Adminnortxe')

        except Exception:
            if(self.__logger == None):
               self.__logger.logErr("Key file password doesn't match. Parse correct password_Auth and re-run the script" + '\n' + "Program terminated")
            raise ValueError("Key file password doesn't match. Parse correct password_Auth and re-run the script" + '\n' + "Program terminated")

        return mykey

    def __init__(self,conntype="tel", iphost=None, port=23, login='admin', password='extron', phrase=None,ppk_file=None):
        self.__hostip = iphost
        self.__port = port
        self.__login = login
        self.__password = password
        self.__conntype = conntype
        if ppk_file != None and len(ppk_file) > 0:
           self.PublicKey(pub_file=ppk_file,phrase=phrase)
        else:
           self.__pkey = phrase    #will use as keeping prhase or acutal key    
        self.__ppkfile = ppk_file
        self.__mkey = None
        self.__terminal = None
        self.__channel = None
        self.__loginBuffer = None
        self.__response = None
        self.__senddata = None
        self.__logger = None
        self.__debugOn = False
        self.__ssh_stdin = None
        self.__ssh_stdout = None
        self.__ssh_stderr = None
        self.__session = None
        self.__previousbuffer = None
        return
    def __trace(self,msg):
        if (self.__logger != None):  #bm 07/17/2018:Bug fix should check for not None
            self.__logger.logInfo(msg)
        if (self.__debugOn):
            print("[DEBUG] "+msg)
        return
    def __readdataTelnet(self):
        data = None
        buffer = None
        buff = None
        eout = False
        start = time.time()
        try:
            if(self.__terminal!=None):
                buff = self.__terminal.read_eager()
                while(buff!=b''):
                    if(buffer==None):
                        buffer=b''
                    buffer+=buff
                    buff = self.__terminal.read_eager()
                #if(buffer==None):
                 #   buffer=self.__terminal.read_all()
                #else:
                 #   buffer+=self.__terminal.read_all()
        except Exception as e:
            print(str(e))
            if(str(e)=='timed out'):
                eout = True
            pass
        if(buff==None and eout):
            return b''
        end = time.time()
        readtime = end - start        
        self.__trace("Read time:{0}".format(str(time.strftime("%H:%M:%S", time.gmtime(readtime)))))
        if str(type(buffer)) == "<class 'str'>":
           data = bytearray(buffer,'utf-8')
        elif str(type(buffer))== "<class 'bytes'>":
            data = bytearray(buffer)
        elif str(type(buff))=="<class 'bytes'>":
            data = bytearray(buff)
        else:
            print('typeof buffer '+str(type(buffer))+"\n")
            print('typeof buff '+str(type(buff))+"\n")
        if(data==None):
            return None
        if(data==b''):
            self.DEPTH+=1
            if(self.DEPTH>50):
                return None
            time.sleep(0.5)
            return self.__readdataTelnet()

        return bytes(data)

    def __readdata(self):
        data = None
        buffer = None
        start = time.time()
        if str(type(self.__terminal)) == "<class 'telnetlib.Telnet'>":
          data = self.__readdataTelnet()         
        elif self.__terminal != None:
           while (self.__channel.recv_ready() == True):  # need to check for non-blocking read
                try:
                    buffer = self.__channel.recv(self.buffersize)
                    if (buffer != None):
                        if data == None or data == "":
                            data = buffer
                        else:
                            data += buffer
                    
                except Exception:
                    buffer = "" #dummy
                    break
        end = time.time()
        readtime = end - start        
        self.__trace("Read time:{0}".format(str(time.strftime("%H:%M:%S", time.gmtime(readtime)))))
        if str(type(data)) == "<class 'str'>":
           data = bytearray(data,'utf-8')
        #print(str(type(data)))
        return data
    def x__readdata(self):
        data = None
        buffer = None
        start = time.time()
        if str(type(self.__terminal)) == "<class 'telnetlib.Telnet'>":
           try:
              buffer = self.__terminal.read_eager() #bm 07/17/19: 
              while (1):
                try:
                    if (data == None) or data == "":
                        data = buffer
                    else:
                        data += buffer
                    buffer = self.__terminal.read_some() #this determines if more data in the buffer.  TIme-out trigger if no more data
                    if not buffer:  #blank line.  possible no more data
                      break
                except Exception:
                    break

              try:
                buffer = self.__terminal.read_all() #this determines if more data in the buffer.  TIme-out trigger if no more data
                data += buffer
              except Exception:
                pass

           except Exception:
              pass  #no data
        elif self.__terminal != None:
           while (self.__channel.recv_ready() == True):  # need to check for non-blocking read
                try:
                    buffer = self.__channel.recv(self.buffersize)
                    if (buffer != None):
                        if data == None or data == "":
                            data = buffer
                        else:
                            data += buffer
                    
                except Exception:
                    buffer = "" #dummy
                    break
        end = time.time()
        readtime = end - start        
        self.__trace("Read time:{0}".format(str(time.strftime("%H:%M:%S", time.gmtime(readtime)))))
        if str(type(data)) == "<class 'str'>":
           data = bytearray(data,'utf-8')
        
        return data
    def __httpsend(self,hostname,uri,data,method="GET",cookie=""):
        response = None
        if uri[:1] != "/":
            uri = "/"+uri
        url = self.uriResource.format(self.__conntype.lower().strip(),hostname,uri)
        
        try:
            req = urllib.request.Request(url)
            if(data !=None) and len(data) > 0:
                jsondata = json.dumps(data)
                jsondataasbytes = jsondata.encode('utf-8')                  

                req.add_header('Content-Type', 'application/json; charset=utf-8')
                req.add_header('Content-Length', len(jsondataasbytes))
                req.add_header('cookie', cookie)
                req.get_method = lambda: method.upper()
                response = urllib.request.urlopen(req, jsondataasbytes)            
            else:            
                req.add_header('cookie', cookie)
                req.get_method = lambda: method.upper()
                response = urllib.request.urlopen(req)
        except Exception:
            response = None
            #print(e)

        return response

    
    def __LoginBanner(self,bannerlines = 6):
        """ Read in the login banner"""
        buffer = ""
        retry  = 5  #about 5ms
        self.__loginBuffer = None

        #bm 07/17/19: Some devices require longer time for the first buffer to come in
        self.__loginBuffer = self.__readdata()
        while self.__loginBuffer == None and retry > 0:
            time.sleep(0.016)
            self.__loginBuffer = self.__readdata()
            retry -= 1

        lines = self.__loginBuffer.decode("utf-8").split("\r\n")

        bannerlines -= len(lines)
        retry = 2
        while bannerlines > 0 and retry > 0:
            buffer = self.__readdata()
            if(buffer != None):
                self.__loginBuffer += buffer
                lines = buffer.decode("utf-8").split("\r\n")
                bannerlines -= len(lines)
            else:
                retry -= 1
        self.__trace("SSH Login banner:"+str(self.__loginBuffer))
        return

    def __searchLogin(self,terminal):
        # FIRST TIME
        data = None
        buffer = None
        try:  # no easy way to detect if it is EOF.  Need to use timeout.
            # Extron may not have [login] prompt, but others may.  Need to check both a the same time.
            # Note:Extron needs at least 1 second before password or login shows up.
            data = self.__terminal.expect(
                [b"login:", b"login", b"Login", b"Login:", b"xPassword:", b"password:", b"password"], 1)
        except Exception:
            #print("Read Err {0}".format(datetime.datetime.now().strftime('%H:%M:%S.%f')))
            data = self.__terminal.read_some()

        keyword = None
        if (str(type(data)) == "<class 'tuple'>"):
            if (data[1] != None):
                buffer = data[1]
                keyword = data[1].group(0)
            else:  # it did not match any login or password pattern.
                keyword = data[2].lower().strip()
                i = keyword.decode("utf-8").find("login")
                if (i == -1):
                    i = keyword.decode("utf-8").find("password")
                    if (i != -1):
                        buffer = keyword[:i]  # banner
                        keyword = keyword[i:]
                    else:
                        buffer = keyword
                        keyword = b""  # something gone wrong.
                else:
                    buffer = keyword[:i]  # banner
                    keyword = keyword[i:]

        if (keyword.decode("utf-8") == 'login:') or (keyword.decode("utf-8")== 'login'):
            login = self.__login + "\r\n"
            self.__terminal.write(login.encode('utf-8'))

        return keyword, buffer
    def __IsAlive(self):
        msg = "".join(traceback.format_stack())
        self.__trace(msg)
        status = True
        if str(type(self.__terminal)) == "<class 'telnetlib.Telnet'>":
           try:
             sock = self.__terminal.get_socket()
             sock.sendall(b"")
             #self.__terminal.write("")  #may not the write way             
           except Exception as e:
             print("Alive error %s" % str(e))
             status = False           
        else:
            try:
              transport = self.__terminal.get_transport()
              transport.send_ignore()
            except EOFError as e:              
              status = False
            except Exception as e:
              status = False
              
        return status
    
    def __TelentLogin(self,hostname,port=23,username='admin',password='extron',timeout=2):
        msg = "".join(traceback.format_stack())
        self.__trace(msg)

        msg = "Non SSH:hostname={0}, port={1}, username={2},".format(hostname, port, username)
        self.__trace(msg)

        data = None
        try:
            self.__terminal = telnetlib.Telnet(hostname, port,timeout)
            #self.__terminal.set_debuglevel(1000)
            time.sleep(0.05)  # allow remote device to response
        except Exception as e:
            msg = "Connection:" + str(e)
            self.__trace(msg)
            return None

        #print("Read {0}".format(datetime.datetime.now().strftime('%H:%M:%S.%f')))
        keyword, self.__loginBuffer = self.__searchLogin(self.__terminal)

        #check the last keyword
        if (keyword.decode("utf-8") == 'login:') or (keyword.decode("utf-8")== 'login'):
            try:
                data = self.__terminal.expect([b"Password:", b"password:"], 1)
                if (str(type(data)) == "<class 'tuple'>"):
                    if (data[1] != None):
                        keyword = data[1].group(0)
                    else:  # it did not match any password pattern.
                        keyword = b""
            except Exception as e:
                data = self.__terminal.read_some()

        if (keyword.decode("utf-8") == 'password:') or (keyword.decode("utf-8") == 'password'):
            password = self.__password + "\r\n"
            self.__terminal.write(password.encode('utf-8'))
            time.sleep(0.06)  # allow remote device to response
            try:
                data = self.__terminal.expect([b"Password:", b"password:"], 1)  #extron only device
                # print("Read {0}".format(datetime.datetime.now().strftime('%H:%M:%S.%f')))
                if (str(type(data)) == "<class 'tuple'>"):
                    if (data[1] != None):
                        i = data[1].group(0).decode("utf-8").lower().strip().find("password")
                        #extron device only
                        if(i > -1):
                          data = self.__terminal.read_some() # __terminal.read_some()
            except Exception as e:
                data = None

        return self.__terminal

    def __SSHLogin(self,hostname="",port=22023,username='admin',password='extron',pkey=None,bannerlines=6):
        msg = "".join(traceback.format_stack())
        self.__trace(msg)
        try:        
            self.__terminal = paramiko.SSHClient()
            if (self.__pkey == None) or (type(self.__pkey) == type("") and (len(self.__pkey) == 0)):
                self.__terminal.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                self.__terminal.connect(hostname=hostname, port=port, username=username,
                                                    password=password)
                if (self.__logger != None):
                    self.__logger.logInfo(
                        "SSH with no key:hostname={0}, port={1}, username={2},".format(hostname,port,username))
            else:
                self.__terminal.set_missing_host_key_policy(paramiko.AutoAddPolicy())                
                self.__terminal.connect(hostname=hostname, port=port, username=username,
                                                    password=password, pkey=pkey)
                if (self.__logger != None):
                    self.__logger.logInfo(
                        "SSH with key:hostname={0}, port={1}, username={2},".format(hostname,port,username))
            self.__channel = self.__terminal.get_transport().open_session()
            self.__channel.get_pty(self.term, self.width, self.height)
            self.__channel.invoke_shell()
            time.sleep(0.016)  # allow remote device to response

            #self.__loginBuffer = self.__terminal.get_transport().get_banner()
            if (self.__loginBuffer == None):
                self.__LoginBanner(bannerlines)  # remove login data before sending the command                
                if(self.__channel.recv_ready()): #bm 07/17/19: add force a flush
                  self.__readdata()
                
        except Exception as e:
            msg = "Connection:" + str(e)
            self.__trace(msg)
            self.__terminal = None

        return self.__channel 
    
    def __HTTPSLogin(self,conntype="http",hostname="",username="admin",password = "extron"):
        cookie = ""
        if(conntype == "https"):
            try:
                if(getattr(ssl, '_create_unverified_context', None)):
                    ssl._create_default_https_context = ssl._create_unverified_context
                #print(ssl.get_default_verify_paths())
            except AttributeError as e:
                print(e)
        
        credentials = ('%s:%s' % (username, password))
        encoded_credentials = base64.b64encode(credentials.encode('ascii'))
        URLOGIN = self.uri.format(conntype,hostname,"/login")
        
        try:

            req = urllib.request.Request(URLOGIN,method="GET")
            req.add_header('Authorization', 'Basic %s' % encoded_credentials.decode("ascii"))
            res = urllib.request.urlopen(req)
            cookie = res.headers.get('Set-Cookie')
            #print(cookie)
        except Exception as e:
            msg = "HTTP Connection:" + str(e)
            self.__trace(msg)
            cookie = None  #dummy
        #ret = res.read()

        return cookie
    
    def __makeConnection(self,bannerlines=6):
        status = False
        msg = "".join(traceback.format_stack())
        self.__trace(msg)
        try:  #catch all exception
            if self.__conntype.lower().strip() == "ssh" or self.__conntype.lower().strip() == "ssh_key":
               if self.__conntype.lower().strip() == "ssh_key":
                 #self.PublicKey(pub_file=self.__defaultrsakey,phrase=self.__pkey)
                 self.__pkey = self.__MKEY
               self.__terminal = self.__SSHLogin(hostname=self.__hostip,port=self.__port,username=self.__login,password=self.__password,pkey=self.__pkey)
               
               if self.__terminal != None:
                  #self.RawSend(rawdata="", readafter=True)  #flush shell   #bm 07/16/2019: cannot flush shell -- remove code
                  status = True
            elif self.__conntype.lower().strip() == "http" or self.__conntype.lower().strip() == "https":                
                self.__terminal = self.__HTTPSLogin(self.__conntype.lower().strip(), self.__hostip,self.__login,self.__password)
                if self.__terminal != None:
                   status = True
            else:
               if self.__conntype.lower().strip() == "tel" or self.__conntype.lower().strip() == "telnet":
                  msg = "" #dummy
               else:
                  self.__trace("Default to Telnet")

               self.__terminal = self.__TelentLogin(self.__hostip, self.__port, self.__login, self.__password)
               if self.__terminal != None:                   
                  #self.RawSend(rawdata="", readafter=True)  # bm 07/17/19:Need a better to perform the flush
                  status = True

        except Exception as e:
            msg = "Connection:" + str(e)
            self.__trace(msg)
            status = False

        return status
   
    @property
    def VERSION(self):
        """Module version"""
        return "1.0.5.89"
    def SetLogger(self,Filename):
        """Capture the interal logging"""        
        if Filename != None and str(type(Filename)) != "<class 'lib.ExtronQALogger.ExtronQALogger'>":           
           self.__logger = logging.getLogger(Filename)
           self.__logger.setLevel(logging.DEBUG)
           return True
        else:
           self.__logger = Filename
           return True
        return False
        
    def VerboseOn(self,ON=False):
        """Turn on Debug ON/OFF"""
        self.__debugOn = ON
        return
    
    def PublicKey(self,pub_file=None,phrase=None):
        """
        Set a new key file.
        Note: This will close the current connection.
        
        Parameters
        ----------
        pub_file : str
            Filename.   Default None
        phrase : str
            Key phrase. Default None
        
        Returns
        -------
        True/False
            Return True if the private key can be read.        
        """
        
        file = None
        buffer = None
        status = False
        msg = "".join(traceback.format_stack())
        self.__trace(msg)

        if pub_file.startswith("-----BEGIN RSA PRIVATE KEY-----"):
            buffer = pub_file
        elif os.path.isfile(pub_file) == True:
            try:
                file = open(pub_file, 'r')
                buffer = file.read()
            except Exception:
               return False
        else:
            return status

        
        if len(buffer) > 0:
            try:
                keyfile = io.StringIO(buffer)
                self.__pkey = paramiko.RSAKey.from_private_key(keyfile, password=phrase)
                self.Close()
                status = True
            except Exception:
                status = False

        return status

    def Config(self,conntype="tel", iphost=None, port=23, login='admin', password='extron', phrase=None, ppk_file=None):
        """ 
        ReConfigure the connection settings.
        
        Parameters
        ----------
        conntype : str
            Connection Type: ssh, ssh_key or telnet(tel).  Default tel
            Note: ssh_key uses default private key.
        iphost : str
            IP Address/Hostname of the device
        port : int 
            Port number of the device
        login : str
            Login ID or name.  Default 'admin'
        password : str
            Password to the device.  Default 'extron'
        phrase : None
            The pass prhase to read the ppk_file content.  Default None
        ppk_file : None
            Filename or RSA content.  Default None

        Example:

        """
        msg = "".join(traceback.format_stack())
        self.__trace(msg)

        if self.__terminal != None:
           if str(type(self.__terminal)) != "<class 'str'>":
              self.__terminal.close()
        self.__conntype = conntype
        self.__hostip = iphost
        self.__port = port
        self.__login = login
        self.__password = password        
        self.__pkey = phrase
        self.__ppkfile = ppk_file
        self.__mkey = None
        self.__response = None
        self.__terminal = None
        
        if ppk_file != None and len(ppk_file) > 0:
           return self.PublicKey(pub_file=ppk_file,phrase=phrase)
        
        return True  #for now

    @property
    def IsConnected(self):
        """ 
        Determine if the connection is still active.
        
        Parameters
        ----------
        None
        
        Returns
        -------
        Active returns True.  Otherwise, False
        
        """
        return self.__IsAlive()
    def Connect(self,bannerlines = 6):
        """ 
        Attempt to connect to a device
        
        Parameters
        ----------
        bannerlines : int
        
        Returns
        -------
        Connect success returns True.  Otherwise, False        
        """
    
        msg = "".join(traceback.format_stack())
        self.__trace(msg)
        return self.__makeConnection(bannerlines)

    def Close(self):
        """ 
        Close connection
        """
        msg = "".join(traceback.format_stack())
        self.__trace(msg)
        if str(type(self.__terminal)) != "<class 'str'>":
           try:
              self.__terminal.close()
           except Exception as e:
              if str(e) != "'NoneType' object has no attribute 'close'":                 
                 msg = "Close Connection:" + str(e)
                 self.__trace(msg)
                 
        self.__terminal = None
        return

    def RawSend(self,wait=0.05,rawdata=None,flush=True,readafter=True):
        """ 
        Send data as is to the device.
        
        Parameters
        ----------
        wait : float
            Connection Type: ssh or telnet(tel).  Default tel        
        rawdata : str 
            Data to send       
        flush : True or False
            Flush the input buffer before sending(telnet or ssh).
        readafter  : True or False
            Read after sending.
        Returns
        -------
        Success return True.  Otherwise, False
        """
        status = False
        msg = "".join(traceback.format_stack())
        self.__trace(msg)
        self.DEPTH = 0#reset the depth after each send
        start = time.time()
        if rawdata == None:
            return status
        elif str(type(self.__terminal)) == "<class 'str'>":
            uri = rawdata["uri"]
            data = rawdata["data"]
            method = rawdata["method"]
            if method == None or len(method.strip()) == 0:
               method = "GET"
            response = self.__httpsend(self.__hostip,uri,data,method=method,cookie=self.__terminal)
            self.__response = response.read() #http is a connectionless
            self.__senddata = rawdata
            status = True
        elif str(type(self.__terminal)) == "<class 'telnetlib.Telnet'>":            
            try:
               #if(flush):  #bm 07/17/19: added
               #  self.__readdata() 

               if str(type(rawdata)) == "<class 'bytes'>":
                    self.__terminal.write(rawdata)
               else:
                   self.__terminal.write(rawdata.encode('utf-8'))
               self.__senddata = rawdata 
               time.sleep(wait)  # allow remote device to response
               status = True
            except Exception:
                status = False
                self.__senddata = ""            
        else:  #ssh
            try:                
                if(flush):  #bm 07/17/19: added
                  self.__readdata()

                if (self.__channel.send_ready() == True):   #need to check for non-blocking write
                    if str(type(rawdata)) == "<class 'bytes'>":
                        self.__channel.sendall(rawdata)
                    else:
                        self.__channel.sendall(rawdata.encode('utf-8'))
                    time.sleep(wait)  # allow remote device to response
                    status = True
                    self.__senddata = rawdata
            except Exception:
                status = False
                self.__senddata = ""
        end = time.time()
        sendtime = end - start        
        self.__trace("Send time:{0}".format(str(time.strftime("%H:%M:%S", time.gmtime(sendtime)))))
        
        if(readafter):
          self.__response = self.RawResponse()
        return status
    def SendCommand(self,wait=0.05,command=None,flush=True,readafter=False):
        """ 
        Send data to the device.
        
        Parameters
        ----------
        wait : float
            Connection Type: ssh or telnet(tel).  Default tel       
        command : str 
            Data to send
            Format - {"uri":<command>,"method":<method>,"data":<data>}
               Example: {"uri":"ci\r","method":"esc","data":""}  SIS command ^ci\r
                        {"uri":"Q","method":"","data":""}  SIS command Q
        flush : True or False
            Flush the input buffer before sending.
        readafter : True or False
            Read the response after sending the data.
        
        Returns
        -------
        Success return True.  Otherwise, False
        """
        status = False
        self.__response = None
        msg = "".join(traceback.format_stack())
        self.__trace(msg)
        if type(wait) != type(0.01) and type(wait) != type(0) :
           self.__trace("[wait] bad data type")
           return False  #bad data
        elif type(readafter) != type(True):
           self.__trace("[readafter] bad data type")
           return False
        elif str(type(self.__terminal)) == "<class 'str'>":            
           #command needs to be in a json form: {uri:,method:,data:}
           status = self.RawSend(wait=wait,rawdata=command,readafter=readafter)           
        else:
           command = command["method"] + command["uri"]
           status = self.RawSend(wait=wait,rawdata=command,readafter=readafter)
       
        return status
    
    @property    
    def LastSend(self):
        """ 
        Last success command sent
        
        Returns
        -------
        str or bytearray or json(uri:,method:,data:)        
        """
        return self.__senddata
        
    def Response(self):
        """ 
        Read the buffer 
        
        Returns
        -------
        str        
        
        """
        receive_buffer_temp = self.RawResponse()
        if str(type(receive_buffer_temp)) == "<class 'bytes'>":
            return str(receive_buffer_temp.decode()) #
        return str(receive_buffer_temp) #
    def RawResponse(self):
        """ 
        Read the buffer 
        
        Returns
        -------
        str or bytearray
        """
        #receive_buffer_temp = b''
        #msg = "".join(traceback.format_stack())
        #self.__trace(msg)
        #data = None
        if str(type(self.__terminal)) == "<class 'str'>":
            #since http is a connectionless, there is no data to read
            #receive_buffer_temp = self.__response  #itself  
            pass 
        else:
            self.__response = self.__readdata()
        return self.__response
       
    @property
    def InternalSendRepsone(self):
        """ 
        The last received data
        Note: This normally uses with [readafter=True] parameter.
        Returns
        -------
        str or bytearray
        """
        msg = "".join(traceback.format_stack())
        self.__trace(msg)
        try:
            return self.__response
        except Exception:
            return ""
        return ""

    @property
    def LoginBanner(self):
        """ 
        Login banner
        
        Returns
        -------
        string
        """
        msg = "".join(traceback.format_stack())
        self.__trace(msg)
        try:
            return str(self.__loginBuffer)
        except Exception:
            return ""
        return ""

        