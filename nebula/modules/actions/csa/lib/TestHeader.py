"""Creator: Bouda Meas """
"""Date Create: 11/7/2016 """
"""Description: Bouda Meas """

import copy
import datetime
import json
import dicttoxml
from enum import Enum


class TestHeader(object):
    """
    Test Header
    """

    class FieldName(Enum):
        """
        Enumerator        
        """
        SW_Name = 0
        SW_Version = 1
        Environment = 2
        DateTime = 3
        TestRunDuration = 4
        TotalPassed = 5
        TotalFailed = 6
        Skipped = 7
        TAS = 8
        TMID = 9
        TestNotes = 10

    class __TestHeaderStore:
        def __init__(self):
            self.SW_Name = ""
            self.SW_Version = ""
            self.Environment = ""
            self.DateTime = datetime.date.today().strftime("%B %d, %Y")
            self.TestRunDuration = "0"
            self.TotalPassed = ""
            self.TotalFailed = ""
            self.Skipped = ""
            self.TAS = ""
            self.TMID = ""
            self.TestNotes = ""
            return

        def toXML(self,includeHeader=True):  #bm 07/18/19:Added
            xml = dicttoxml.dicttoxml(self.__dict__, custom_root='TestItem', attr_type=False, root=includeHeader)
            return str(xml).replace("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>","")
        def toString(self):
            return json.dumps(self, default=lambda o: o.__dict__,
                              sort_keys=True, indent=4)

    def __init__(self):
        self.__headerdata = self.__TestHeaderStore()
        return
    @property
    def VERSION(self):
        return "1.0.0.11"
    @property
    def SW_Name(self):
        return self.GetValue(self.FieldName.SW_Name)
    @property
    def SW_Version(self):
        return self.GetValue(self.FieldName.SW_Version)
    @property
    def Environment(self):
        return self.GetValue(self.FieldName.Environment)
    @property
    def DateTime(self):
        return self.GetValue(self.FieldName.DateTime)
    @property
    def TestRunDuration(self):
        return self.GetValue(self.FieldName.TestRunDuration)
    @property
    def TotalPassed(self):
        return self.GetValue(self.FieldName.TotalPassed)
    @property
    def TotalFailed(self):
        return self.GetValue(self.FieldName.TotalFailed)
    @property
    def Skipped(self):
        return self.GetValue(self.FieldName.Skipped)
    @property
    def TAS(self):
        return self.GetValue(self.FieldName.TAS)
    @property
    def TMID(self):
        return self.GetValue(self.FieldName.TMID)
    @property
    def TestNotes(self):
        return self.GetValue(self.FieldName.TestNotes)
    
    def GetXMLString(self,includeHeader=True): #bm 07/18/19:Added
        """
         Parameters
        ----------
        includeHeader : True or False.        
        
        Return a pair value of the header in xml format
        """
        self.__headerdata.toXML(includeHeader=includeHeader)
        return
    def GetJsonString(self):
        """
        Return a pair value of the test item attribute in json format
        """    
        return self.__headerdata.toString()
  
    def GetValue(self,fieldname):
        """
        Get a value.
        
        Parameters
        ----------
        fieldname : enumerator/field name/enumerator value.
        
        Return a value.
        ------------------------------------------------------
        Example:
            GetValue(TestHeader.FieldName.SW_Name)
            GetValue("SW_Name")
            GetValue(0)
        """
        strValue = None

        if (self.FieldName.SW_Name == fieldname) or \
           (self.FieldName.SW_Name.name == fieldname) or \
           (self.FieldName.SW_Name.value == fieldname):
            strValue = self.__headerdata.SW_Name
        elif (self.FieldName.SW_Version == fieldname) or \
             (self.FieldName.SW_Version.name == fieldname) or \
             (self.FieldName.SW_Version.value == fieldname):
               strValue = self.__headerdata.SW_Version
        elif (self.FieldName.Environment == fieldname) or \
             (self.FieldName.Environment.name == fieldname) or \
             (self.FieldName.Environment.value == fieldname):
               strValue = self.__headerdata.Environment
        elif (self.FieldName.DateTime == fieldname) or \
             (self.FieldName.DateTime.name == fieldname) or \
             (self.FieldName.DateTime.value == fieldname):
               strValue = self.__headerdata.DateTime
        elif (self.FieldName.TestRunDuration == fieldname) or \
             (self.FieldName.TestRunDuration.name == fieldname) or \
             (self.FieldName.TestRunDuration.value == fieldname):
               strValue = self.__headerdata.TestRunDuration
        elif (self.FieldName.TotalPassed == fieldname) or \
             (self.FieldName.TotalPassed.name == fieldname) or \
             (self.FieldName.TotalPassed.value == fieldname):
               strValue = self.__headerdata.TotalPassed
        elif (self.FieldName.TotalFailed == fieldname) or \
             (self.FieldName.TotalFailed.name == fieldname) or \
             (self.FieldName.TotalFailed.value == fieldname):
               strValue = self.__headerdata.TotalFailed
        elif (self.FieldName.Skipped == fieldname) or \
             (self.FieldName.Skipped.name == fieldname) or \
             (self.FieldName.Skipped.value == fieldname):
               strValue = self.__headerdata.Skipped
        elif (self.FieldName.TAS == fieldname) or \
             (self.FieldName.TAS.name == fieldname) or \
             (self.FieldName.TAS.value == fieldname):
               strValue = self.__headerdata.TAS
        elif (self.FieldName.TMID == fieldname) or \
             (self.FieldName.TMID.name == fieldname) or \
             (self.FieldName.TMID.value == fieldname):
               strValue = self.__headerdata.TMID
        elif (self.FieldName.TestNotes == fieldname) or \
             (self.FieldName.TestNotes.name == fieldname) or \
             (self.FieldName.TestNotes.value == fieldname):
               strValue = self.__headerdata.TestNotes

        return copy.deepcopy(strValue)
    
    def SetValue(self,fieldname,strValue):
        """
        Set a value.
        
        Parameters
        ----------
        fieldname : enumerator/field name/enumerator value.
        strValue : str
        
        Return True if the value is successfully save.
        ------------------------------------------------------
        Example:
            GetValue(TestItem.FieldName.Status)
            GetValue("Status")
            GetValue(5)
        """
        if (self.FieldName.SW_Name == fieldname):
            self.__headerdata.SW_Name = strValue
        elif (self.FieldName.SW_Version == fieldname):
            self.__headerdata.SW_Version = strValue
        elif (self.FieldName.Environment == fieldname):
            self.__headerdata.Environment = strValue
        elif (self.FieldName.DateTime == fieldname):
            self.__headerdata.DateTime = strValue
        elif (self.FieldName.TestRunDuration == fieldname):
            self.__headerdata.TestRunDuration = strValue
        elif (self.FieldName.TotalPassed == fieldname):
            self.__headerdata.TotalPassed = strValue
        elif (self.FieldName.TotalFailed == fieldname):
            self.__headerdata.TotalFailed = strValue
        elif (self.FieldName.Skipped == fieldname):
            self.__headerdata.Skipped = strValue
        elif (self.FieldName.TAS == fieldname):
            self.__headerdata.TAS = strValue
        elif (self.FieldName.TMID == fieldname):
            self.__headerdata.TMID = strValue
        elif (self.FieldName.TestNotes == fieldname):
            self.__headerdata.TestNotes = strValue
        else:
            return False

        return True
    
  
        #return self.GetValue(self.FieldName.TestNotes)
         