"""Creator: Bouda Meas """
"""Date Create: 11/7/2016 """
"""Description: Bouda Meas 
   Split from TestAttributes.py
"""
import copy
import json
import dicttoxml
from enum import Enum

class TestItem(object):
    """
    Sub test case

    Parameters
    ----------
    None
    
    """
    class FieldName(Enum):
        """
        Enumerator

        Parameters
        ----------
        None
        
        """        
        PassCriteria = 2
        Description = 3
        ExpectValue = 4        
        Status = 5
        Duration = 6
        MemUsage = 7
        Result = 8

    class __TestItemStore:

        def __init__(self):            
            self.PassCriteria = ""
            self.Description = ""
            self.ExpectValue = ""            
            self.Status = ""
            self.ResultValue = ""
            self.MemUsage = ""
            self.Duration = ""
            return
        
        def toXML(self,includeHeader=True):  #bm 07/18/19:Added
            xml = dicttoxml.dicttoxml(self.__dict__, custom_root='TestItem', attr_type=False, root=includeHeader)
            return str(xml).replace("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>","")
        def toString(self):
            return json.dumps(self, default=lambda o: o.__dict__,
                              sort_keys=True, indent=4)

    def __init__(self):
        self.__item = self.__TestItemStore()
        self.__jsonpayload__ = None       
        return
    @property
    def VERSION(self):
        return "1.0.0.15"

    #bm 07/10/19: remove AttributeName property    
    @property
    def Description(self):
        return self.GetValue(self.FieldName.Description.value)
    @property
    def PassCriteria(self):
        return self.GetValue(self.FieldName.PassCriteria.value)
    @property
    def Expectvalue(self):
        return self.GetValue(self.FieldName.ExpectValue.value)
    #bm 07/10/19:Remove PassFail property
    @property
    def Status(self):
        return self.GetValue(self.FieldName.Status.value)
    @property
    def Duration(self):
        return self.GetValue(self.FieldName.Duration.value)
    @property
    def MemUsage(self):
        return self.GetValue(self.FieldName.MemUsage.value)
    @property
    def Result(self):
        return self.GetValue(self.FieldName.Result.value)

    def GetXMLString(self,includeHeader=True): #bm 07/18/19:Added
        """
         Parameters
        ----------
        includeHeader : True or False.        
        
        Return a pair value of the test item(sub test case) in xml format
        """
        self.__item.toXML(includeHeader=includeHeader)
        return
    
    def GetJsonString(self):
        """
        Return a pair value of the test item(sub test case) in json format
        """
        return  self.__item.toString()
    
    def GetValue(self,fieldname):
        """
        Parameters
        ----------
        filedname : enumerator/field name/enumerator value
        
        Return a value.
        
        Example:
            GetValue(TestItem.FieldName.Status)
            GetValue("Status")
            GetValue(5)
        """
        value = None
        #bm 07/10/19:Remove AttributeName property check        
        if (self.FieldName.PassCriteria == fieldname) or \
             (self.FieldName.PassCriteria.name == fieldname) or \
             (self.FieldName.PassCriteria.value == fieldname):
               value = self.__item.PassCriteria
        elif (self.FieldName.Description == fieldname) or \
             (self.FieldName.Description.name == fieldname) or \
             (self.FieldName.Description.value == fieldname):
               value = self.__item.Description
        elif (self.FieldName.ExpectValue == fieldname) or \
             (self.FieldName.ExpectValue.name == fieldname) or \
             (self.FieldName.ExpectValue.value == fieldname):
               value = self.__item.ExpectValue        
        elif (self.FieldName.Status == fieldname) or \
             (self.FieldName.Status.name == fieldname) or \
             (self.FieldName.Status.value == fieldname):
               value = self.__item.Status
        elif (self.FieldName.Result == fieldname) or \
             (self.FieldName.Result.name == fieldname) or \
             (self.FieldName.Result.value == fieldname):
               value = self.__item.ResultValue
        elif (self.FieldName.MemUsage == fieldname) or \
             (self.FieldName.MemUsage.name == fieldname) or \
             (self.FieldName.MemUsage.value == fieldname):
               value = self.__item.MemUsage
        elif (self.FieldName.Duration == fieldname) or \
             (self.FieldName.Duration.name == fieldname) or \
             (self.FieldName.Duration.value == fieldname):
               value = self.__item.Duration

        return copy.deepcopy(value)
        
    def SetValue(self, fieldname, value):
        """        
        Set a value of a given enumerator/field name/enumerator value.
        
        Parameters
        ----------
        fieldname : enumerator/field name/enumerator value.
        value : str
        
        Return True if the value is successfully save.
        --------------------------------------------------------------------------------
        Example:
            SetValue(TestItem.FieldName.Status,"pass")
            SetValue("Status","pass")
            SetValue(5,"pass")
        """
        localvalue = copy.deepcopy(value)      
        #bm 07/10/19:Remove AttributeName property check          
        if (self.FieldName.PassCriteria == fieldname) or \
             (self.FieldName.PassCriteria.name == fieldname) or \
             (self.FieldName.PassCriteria.value == fieldname):
               self.__item.PassCriteria = localvalue
        elif (self.FieldName.Description == fieldname) or \
             (self.FieldName.Description.name == fieldname) or \
             (self.FieldName.Description.value == fieldname):
               self.__item.Description = localvalue
        elif (self.FieldName.ExpectValue == fieldname) or \
             (self.FieldName.ExpectValue.name == fieldname) or \
             (self.FieldName.ExpectValue.value == fieldname):
               self.__item.ExpectValue = localvalue        
        elif (self.FieldName.Status == fieldname) or \
             (self.FieldName.Status.name == fieldname) or \
             (self.FieldName.Status.value == fieldname):
               self.__item.Status = localvalue
        elif (self.FieldName.Result == fieldname) or \
             (self.FieldName.Result.name == fieldname) or \
             (self.FieldName.Result.value == fieldname):
               self.__item.ResultValue = localvalue
        elif (self.FieldName.MemUsage == fieldname) or \
             (self.FieldName.MemUsage.name == fieldname) or \
             (self.FieldName.MemUsage.value == fieldname):
               self.__item.MemUsage = localvalue
        elif (self.FieldName.Duration == fieldname) or \
             (self.FieldName.Duration.name == fieldname) or \
             (self.FieldName.Duration.value == fieldname):
               self.__item.Duration = localvalue
        else:
            return False

        return True