import datetime
import time
import logging
import json
import re
import traceback
import os
from time import gmtime,time, strftime, sleep
from collections import OrderedDict

#bm 12/4/2019: To handle different development modes.
_NEBULA_MODE = False
try:
    from nebula.modules.report import invalid_test_exception as ite
    from nebula.modules.report import nebula_test_exception as nte
    _NEBULA_MODE = True
except ImportError as error: 
    pass
except Exception as exception:
    pass


if(_NEBULA_MODE):
    from nebula.modules.actions.csa.common.QAUtils import QAUtils
    from nebula.modules.actions.csa.common.ExtronQALogger import ExtronQALogger
    from nebula.modules.actions.csa.common.QATestEnv import QATestEnv
    from nebula.modules.actions.csa.common.QAHelpers import QAHelpers
    from nebula.modules.actions.csa.common.LoginInfo import LoginInfo
    from nebula.modules.actions.csa.lib.TestHeader import TestHeader
    from nebula.modules.actions.csa.lib.TestAttributes import TestAttributes
    from nebula.modules.actions.csa.lib.TestItem import TestItem
# else:
#     import common.QAUtils
#     from common.ExtronQALogger import ExtronQALogger
#     from common.QATestEnv import QATestEnv
#     from common.QAHelpers import QAHelpers
#     from common.LoginInfo import LoginInfo
#     from lib.TestHeader import TestHeader
#     from lib.TestAttributes import TestAttributes
#     from lib.TestItem import TestItem


def JITCLINEFEED():
    return b"\r\r\n"
def SISLINEFEED():
    return b"\r\n"

_PERFORMANCE = False
_LOGGER = None
_VERBOSE = True
_DEBUG = False
_HEXPRINT = False

lastCommandSent = ""
tcattr = None
connect = None
logininfo = None
pcs4 = None
responseAction = ""
connectPCS4 = None
responseActionPCS4 = ""
workingDir = ""
envworkingdir = ""
linefeed = JITCLINEFEED()
banner = {"linefeed":linefeed,"numlines":4}


def LINEFEED():
    global linefeed
    if len(linefeed) == 0:
        linefeed = JITCLINEFEED()

    return  linefeed



def PCS4Unit(powercommand,portnumber,timeout=0.05, waittime=0):
    global connectPCS4; global pcs4; global _DEBUG;global _VERBOSE; global _LOGGER
    global responseActionPCS4
    status = False
    result = None
    #return FunctionJITC.utils.PCS4Unit(powercommand, portnumber,connectPCS4,pcs4,timeout=timeout,waittime=waittime)

    if (pcs4 == None):
        print("PCS4 unit is not connected.")
        if (_LOGGER != None):
            _LOGGER.logErr("PCS4 unit is not connected.")
        return status
    elif (connectPCS4 == None):
        print("PCS4 unit is not connected.")
        print("Attempt to re-establish connection.")
        if (_LOGGER != None):
            _LOGGER.logErr("PCS4 unit is not connected.")
            _LOGGER.logErr("Attempt to re-establish connection.")

        connectPCS4 = QAUtils.Connect(pcs4,_LOGGER)
        if (connectPCS4 == None):
            print("Re-establish connection failed.")
            if (_LOGGER != None):
                _LOGGER.logInfo("Re-establish connection failed.")
            return status

    try:
        sis = "w{0}*{1}PC\r"
        if(powercommand.lower() == "on"):
            sis = sis.format(portnumber,"1")
        else:
            sis = sis.format(portnumber, "0")
        status = connectPCS4.SendCommand(wait=timeout, command=sis,readafter = False)
        if status:
            responseActionPCS4 = connectPCS4.Response() #InternalResponse()
            result = "Cpn0{0} Ppc{1}\r\n"
            if (powercommand.lower() == "on"):
                result = result.format(portnumber,"1")
            else:
                result = result.format(portnumber, "0")

            status = False
            if(responseActionPCS4 == result):
              #reconfirm
              sis = "w{0}PC\r".format(portnumber)
              status = connectPCS4.SendCommand(wait=timeout, command=sis)
              if status:
                  responseActionPCS4 = connectPCS4.Response()
                  status = False
                  if (powercommand.lower() == "on"):
                      if responseActionPCS4 == "1\r\n":
                         status = True
                  else:
                      if responseActionPCS4 == "0\r\n":
                          status = True

    except Exception as e:
        if(_VERBOSE):
            print("Something gone wrong with PCS4:"+ str(e))
        if (_LOGGER != None):
            _LOGGER.logInfo("Something gone wrong with PCS4:"+ str(e))
        status = False

    return status

def Reconnect():
    global connect; global logininfo; global banner; global _DEBUG;global _VERBOSE; global _LOGGER

    if(connect != None):
       try:
         connect.Close()
       except Exception as e:
           if(_VERBOSE):
              print(str(e))
           if (_LOGGER != None):
              _LOGGER.logErr(str(e))

    connect = None    
    try:
       connect = QAUtils.Connect(logininfo,_LOGGER)
    except Exception as e:
       if (_VERBOSE):
           print(str(e))
       if(_LOGGER != None):
          _LOGGER.logErr(str(e))

    if(connect != None):
      QAHelpers.ProcessConfigurationBanner(banner)
    
    return (connect != None)

def ProcessConfigurationLogin(loginToken,logininfo):
    #global logininfo
    if (loginToken == None):
        return False
        
    if loginToken != None:
        for jprop in loginToken:
            if jprop.lower() == "connect":
               if (logininfo.ConnectType == None): 
                  logininfo.ConnectType = loginToken.get(jprop)    
            elif jprop.lower() == "privatekey":
                logininfo.PrivateKey = loginToken.get(jprop)  #file name
            elif jprop.lower() == "privatekeytype":
                logininfo.PrivateKeyType = loginToken.get(jprop)
            elif jprop.lower() == "privatekeyphrase" or jprop.lower() == "passphrase":
                 logininfo.PrivateKeyPhrase = loginToken.get(jprop)
            elif jprop.lower() == "port":
                if (logininfo.Port == None): 
                   port = loginToken.get(jprop)
                   if type(port) == type("") and len(port) > 0:
                        logininfo.Port = int(port)
                   elif type(port) == type(1) and  port > 0:
                          logininfo.Port = int(port)
            elif jprop.lower() == "host":
                if (logininfo.HostName == None): #bm 01/4/2019:Override config file
                   logininfo.HostName = loginToken.get(jprop)           
            elif jprop.lower() == "login":
                if (logininfo.Username == None): #bm 01/4/2019:Override config file
                   logininfo.Username = loginToken.get(jprop)               
            elif jprop.lower() == "password":
                if (logininfo.Password == None): #bm 01/4/2019:Override config file
                   logininfo.Password = loginToken.get(jprop)
               
        # done for-loop
    if logininfo.ConnectType == None:
       logininfo.ConnectType = "tel"  #default
    if logininfo.ConnectType.lower().strip() in ("https","http"):
       if (logininfo.Port == None):
          logininfo.Port = "80"    

    return True
def ProcessConfiguration(configToken):
    global envworkingdir; global workingDir; global connect; global logininfo
    global banner;global pcs4; global connectPCS4; global _DEBUG;global _VERBOSE; global _LOGGER

    try:
        for jprop in configToken:
            name = jprop

            if name.lower() == "envworkingdir":
                QATestEnv.envworkingdir = QAUtils.GetOS_EnvironmentVar(configToken.get(name)) #get env from OS
              # VerifyDirectoryExist(envworkingdir)
            elif name.lower() == "setworkingdir":
                QATestEnv.workingdir = configToken.get(name)
              # VerifyDirectoryExist(envworkingdir)
            elif name.lower() == "logininfo":
              ProcessConfigurationLogin(configToken.get(name),logininfo)
              try:
                #connect = FunctionJITC.utils.Connect(logininfo,_LOGGER)
                connect = QAUtils.Connect(logininfo,_LOGGER,banner["numlines"])
              except Exception as e:
                  if (_VERBOSE):
                      print(str(e))
                  if (_LOGGER != None):
                      _LOGGER.logErr(str(e))
            elif name.lower() == "banner":
              banner = QAHelpers.ProcessConfigurationBanner(configToken.get(name))
            elif name.lower() == "pcs4":
              pcs4 = LoginInfo()
              ProcessConfigurationLogin(configToken.get(name),pcs4)
              try:
                 connectPCS4 = QAUtils.Connect(pcs4,_LOGGER)
              except Exception as e:
                  if (_VERBOSE):
                      print(str(e))
                  if (_LOGGER != None):
                      _LOGGER.logErr(str(e))
    except Exception as e:
        if (_VERBOSE):
            print(str(e))
        if (_LOGGER != None):
            _LOGGER.logErr(str(e))
    return
###############################################################################
#
###############################################################################

def _comparelist(srcList, tarList):
    notFoundList = []
    
    for src_item in srcList:
      found = None
      for tar_item in tarList:
          if src_item == tar_item:
             found = src_item
             tarList.remove(tar_item)  #left over is extra
             break
      
      if(found == None):
         notFoundList.append(src_item)

    return notFoundList, tarList
def _compareJsonObject(echoOFF,node,src_jobject,target_jobject):
    results = []
    #need to handle the echo data
    if(echoOFF):  #bm 10/9: Change business rules.  Ignore echo response
      #assume it is SIS       
      if (type(connect.LastSend) == type("")):
          command = connect.LastSend #QAUtils.StringToByte(connect.LastSend+"\n")
      else:
          command = QAUtils.ByteToString(connect.LastSend)
      
      if type(target_jobject) == type("") and target_jobject.startswith(command):
         target_jobject = target_jobject[len(command):]
      elif type(target_jobject) == type(b''):
           tempstr = QAUtils.ByteToString(target_jobject)
           if tempstr.startswith(target_jobject):
             target_jobject = QAUtils.StringToByte(tempstr[len(command):])
    
    echoOFF = False   #Turn on because of the recurse call.  It should be one time only

    try: #convert to python form
        if type(target_jobject) == type(b''):
            newdata = QAUtils.ByteToString(target_jobject)
            target_jobject = json.loads(newdata)
        elif type(target_jobject) != type({}):
            msg = "Target:{0} is not a json object".format(str(target_jobject))
            if (_VERBOSE):
               print("[INFO] " + str(msg))
            if (_LOGGER != None):
               _LOGGER.logInfo(str(msg))
            target_jobject = None
            
        if type(src_jobject) == type(b''):
            newdata = QAUtils.ByteToString(src_jobject)
            src_jobject = json.loads(newdata)
        elif type(src_jobject) != type({}):
            msg = "Source:{0} not a json object".format(str(src_jobject))
            if (_VERBOSE):
               print("[INFO] " + str(msg))
            if (_LOGGER != None):
               _LOGGER.logInfo(str(msg))
            src_jobject = None
            
    except Exception as e:
        #something when wrong with the conversion
        if (_VERBOSE):
            print("[ERR] " + str(e))
        if (_LOGGER != None):
            _LOGGER.logErr(str(e))
        return None

    if src_jobject == None:
       results.append({"key":"NA","value":str(src_jobject),"pass":"Fail","reason":"Source is not a Json Object"})
    if target_jobject == None:
       results.append({"key":"NA","value":str(target_jobject),"pass":"Fail","reason":"Target is not a Json Object"})
    
    if src_jobject == None or target_jobject == None:
       return results
    
    srcKeyList = QAUtils.GetKeys(src_jobject.keys())
    targetKeyList = QAUtils.GetKeys(target_jobject.keys())
    while len(srcKeyList) > 0:
        key = srcKeyList.pop()
        value = src_jobject[key]
        keynode =  node + "."+key
        try:
            target_value = target_jobject[key]
            targetKeyList.remove(key)
            if type(value) == type(target_value):
               if (value != target_value): #if one is a miss match goes through them all
                  if type(value) == "<class 'dict'>":
                      pass
#                     results.extend(_compareJsonObject(echoON, keynode, value, target_value))
                  elif str(type(value)) == "<class 'list'>":
                       size = len(value)
                       if (len(target_value) > 0):
                          for i in range(size):
                              newkeynode = keynode+"[{0}]".format(i)
#                              results.extend(_compareJsonObject(echoON, newkeynode, value[i], target_value[i]))
                       else:
                           #missing all items
                           results.append({"key":keynode, "value": str(value), "pass": "Fail",
                                           "reason": "Target has empty list."})
                  else:
                       msg = "node:{0}  value not match:{1} == {2}".format(keynode[1:],value,target_value)
                       if (_VERBOSE):
                           print("[INFO] " + str(msg))
                       if (_LOGGER != None):
                           _LOGGER.logInfo(str(msg))

                       source = ":".join("{:02x}".format(c) for c in bytes(value, 'utf-8'))
                       target = ":".join("{:02x}".format(c) for c in bytes(target_value, 'utf-8'))
                       value = value + "\r" + "HEX:" + source
                       target_value = target_value + "\r" + "HEX:" + target
                       results.append( {"key":keynode[1:],"value":value+"\r"+target_value,"index":"NA","pass":"Fail","reason":"Value not Match"})
               else:
                    results.append({"key":keynode[1:],"value":value,"pass":"Pass","reason":"All Match"})
            else:
                #same key, but miss match type
                msg = "node:{0}  miss match type:{1} == {2}".format(keynode[1:], str(type(value)), str(type(target_value)))
                if (_VERBOSE):
                   print("[INFO] " + str(msg))
                if (_LOGGER != None):
                   _LOGGER.logInfo(str(msg))
                
                value = "source:{0} <==> target:{1}".format(str(type(value)),str(type(target_value)))
                results.append({"key":key,"value":value,"pass":"Fail","reason":"Type not Match"})
                   
        except Exception as e: #target missing key
            results.append({"key": keynode[1:], "value": value, "pass": "Fail", "reason": "Missing a key."})
            msg = "missing node:{0}".format(keynode[1:])#,json.dumps(value))
            if (_VERBOSE):
                print("[INFO] " + str(msg))
            if (_LOGGER != None):
                _LOGGER.logInfo(str(msg))

    if (len(srcKeyList) > 0):  # target has some left over
        while len(srcKeyList) > 0:
            key = targetKeyList.pop()
            value = src_jobject[key]
            #keynode = "." + key
            results.append({"key":key,"value":value,"index":"NA","pass":"Fail","reason":"Missing Key(s)"})
            msg = f"Missing Key:{key}"
            if (_VERBOSE):
                print("[INFO]: " + str(msg))
            if (_LOGGER != None):
                _LOGGER.logInfo(str(msg))

    if(len(targetKeyList) > 0): #target has some left over
       while len(targetKeyList) > 0:
            key = targetKeyList.pop()
            value = target_jobject[key]
            #keynode = "." + key
            results.append({"key":key,"value":value,"index":"NA","pass":"Fail","reason":"Extra Key(s)"})
            msg = f"Extra Key:{key}"
            if (_VERBOSE):
                print("[INFO]: " + str(msg))
            if (_LOGGER != None):
                _LOGGER.logInfo(str(msg))
    #"".join(chr(x) for x in bytearray(data))
    return results

###############################################################################
#
###############################################################################
def iterationvalue(indexdata,tagname,looptype):
    global _DEBUG;global _VERBOSE;global _LOGGER
    try:
        data = indexdata[str(looptype+tagname)]
        if data != None:
            return data['index']
    except Exception:
        data = None
    return ""

#bm 03/28/2019: Add table lookup support
def InsertIndexData(looptype,indexdata,command):
    global _DEBUG;global _VERBOSE; global _LOGGER
    #determine which one goes first, left to right
    # range or loop
    #regex = r"_#((r|R)(a|A)(n|N)(g|G)(e|e)|(l|L)(o|O)(o|O)(p|P))\w+_"  #no talbe
    start = time()
    regex = r"_\$\w+_"  #envinorment variable
    matches = re.match(regex, command)
    if matches == None:
      try:        
        regex = r"_#((r|R)(a|A)(n|N)(g|G)(e|e)|(l|L)(o|O)(o|O)(p|P))(\w+.table\[\S+\]|\w+)_"  #include table
        matches = re.finditer(regex, command)        
        keylist = []
        for match in matches:
            tagname = match.group()   
            prefix = ""     
            if tagname.lower().startswith("_#range"):
                #range
                prefix = "r"+tagname[len("_#range"):][:-1]           
            elif tagname.lower().startswith("_#loop"):
                #loop
                prefix = "l"+tagname[len("_#loop"):][:-1]           
            # elif tagname.lower().startswith("_$"):
            #     prefix = getEnv()

            if not(tagname in keylist):           
                matchtable = re.finditer(r".(t|T)(a|A)(b|B)(l|L)(e|E)\[\S+\]",prefix)  #does it contain ".table[xxxx]"
                match = next(matchtable,None)
            if match == None:              
                value = iterationvalue(indexdata,prefix[1:],prefix[:1])      #rxxxx or lxxxx
                command = re.sub(tagname, str(value), command) #search and replace
            else:  #use table --> .table[column name]     
                keyname = prefix[:match.start()]   #rxxxx or lxxxx                    
                matchtable = re.finditer(r"( +|\[)\S+( +|\])",prefix)   #get column name or index
                match = next(matchtable,None)  #should only be one.  If not, the logic above fail.
                colname = ""
                cell_value = ""
                if match != None: #get column name or index and cell value
                    colname = match.group()[1:][:-1]                    
                    value = iterationvalue(indexdata,keyname[1:],keyname[:1])    #rxxxx or lxxxx
                    cell_value = QAUtils.GetDict_Data(value,colname)                     
                    
                command = re.sub(tagname.replace("[","\\[").replace("]","\\]"),str(cell_value),command ) #search and replace
            pass
            keylist.append(tagname)
      except Exception as e:
        if(_VERBOSE):
          print(traceback.print_exc())
          print(str(e)) 
        if(_LOGGER != None):
           _LOGGER.logErr(traceback.print_exc())
           _LOGGER.logErr(str(e))
    else:
        tagname = matches.group()[2:]
        command = os.getenv(tagname[:-1])
    if(_VERBOSE):
      logging.info("InExt Data elaspe time:{0}".format(str(strftime("%H:%M:%S", gmtime(time()- start)))))
    return command

def LookUpValue(lookup, value):
    global _DEBUG;global _VERBOSE; global _LOGGER
    keytag = "_%lookupvalues["
    l = value.lower().find(keytag)

    if(lookup == None):
        return value
        
    if(_VERBOSE):
       print("[INFO]: Lookup]{0}".format(lookup))
    if (_LOGGER != None):
       _LOGGER.logInfo("Lookup:{0}".format(lookup))

    if (l == -1):
       keytag = "_%rangevalues["
       l = value.lower().find(keytag)

    if(l > -1):
        partionalcommand = ""
        r = value.lower().find("_%rangevalues")
        l = value.lower().find("_%lookupvalues")

        if(r > -1) and (l > -1): #should only be one
            r = r ##dummy
        elif(l > -1):
            tag = value[l:len(keytag) + l]
            partionalcommand = value[len(tag):]
            i = partionalcommand.lower().find("]")  # replace all _%loopZZ_ with loop value
            if(i > -1):
                try:
                    index = int(partionalcommand[:i])
                except Exception as e:  # bm 05/14/2018:use the last one
                    if (_VERBOSE):
                        print("[ERR]: {0}".format(str(e)))
                    if (_LOGGER != None):
                        _LOGGER.logErr("{0}".format(lookup))
                    index = -1

                if(index > 0) and (index <= len(lookup)): #bm 05/14/2018:need to check within the index range
                    #if str(type(lookup)) == "<class 'telnetlib.Telnet'>":
                    #    value = lookup[index - 1]
                    #else:
                    try:  #bm 05/14/2018:need to check within the index range
                        value = lookup[index - 1]
                    except Exception as e: #bm 05/14/2018:use the last one
                        value = lookup[len(lookup)-1]
                else:  #bm 05/14/2018:use the last one
                    if(index < 0):
                        value = None
                    else:
                        value = lookup[len(lookup)-1] #bm 05/23: may not need it
        elif (r > -1) and \
            (str(type(lookup)) == "<class 'dict'>" or str(type(lookup)) =="<class 'collections.OrderedDict'>"):
            tag = value[r:len(keytag) + r]
            partionalcommand = value[len(tag):]
            i = partionalcommand.lower().find("]")  # replace all _%loopZZ_ with loop value
            if (i > -1):
                try:
                    key = str(partionalcommand[:i])
                    #bm 05/23/2018: use the key as is ???
                    #key = key.replace("\"", "")
                    #key = key.replace("'", "")
                    key = key.strip()
                    value = lookup.get(key)
                except Exception as e: #bm 05/14/2018:use the last one
                    value = lookup[len(lookup)-1]
        elif value.lower().startswith("_$"):
             #OS Environment variable
             key = value[2:]
             if (key[len(key)-1] == '_'):
                key = key[:len(key)-1]
                value = os.getenv(key)

    return value

###############################################################################
#
###############################################################################
def CompareRawResult(echoOFF,rvalue,lookup,iterindex, actualvalue):
    global _DEBUG;global _VERBOSE; global _LOGGER;global connect
    status = False
    testitem = None
    subtest_info = None
    target = None
    source = None
    newvalue = None

    if (rvalue == None):
        return status

    for jprop in rvalue:
        if jprop.lower() == "value":
            value = rvalue.get(jprop)  #possible json object
            jsondata = json.dumps(value)
            value = jsondata.encode('utf-8')
            newvalue = value
        elif jprop.lower() == "subtest_info":
            subtest_info = rvalue.get(jprop)
        elif jprop.lower().startswith("_comment"):
            continue
        else:
            if (_VERBOSE):
                print("[ERR]: Unknown tag name< " + jprop + ">")
            if (_LOGGER != None):
                _LOGGER.logErr("Unknown tag name<" + jprop + ">")
    # done for-loop

    if (_VERBOSE):
        print("[INFO]: " + str(value))
    if (_LOGGER != None):
        _LOGGER.logInfo(str(value))

    if type(value) == type("") or type(value) == type(b''):
        if (actualvalue != None): #bm 05/03/2018: update the () of and logic
            if (type(actualvalue)== type(b'') and (actualvalue.decode("utf-8") == newvalue)) or \
               (actualvalue == newvalue):
                status = True

        if (actualvalue != None) and len(actualvalue) > 0:
            if (type(actualvalue) == type(b'')):
                source = ":".join("{:02x}".format(c) for c in actualvalue)
            else:
                source = ":".join("{:02x}".format(c) for c in bytes(actualvalue, 'utf-8'))
        else:
            source = ""

        if (newvalue != None) and len(newvalue) > 0:
            if (type(newvalue) == type(b'')):
                target = ":".join("{:02x}".format(c) for c in newvalue)
            else:
                target = ":".join("{:02x}".format(c) for c in bytes(newvalue, 'utf-8'))
        else:
            target = ""

    # bm 05/03/2018: wrong position of the 'else:'
    if (subtest_info != None):
        try:
            testitem = QATestEnv.CreateTestItem(subtest_info)
        except Exception:
            testitem = TestItem()
            testitem.SetValue(TestItem.FieldName.Description.value,"Verify a command or value")
            testitem.SetValue(TestItem.FieldName.ExpectValue.value, "UKNOWN")
            testitem.SetValue(TestItem.FieldName.PassCriteria.value, "UKNOWN")
    else:
        testitem = TestItem()
        testitem.SetValue(TestItem.FieldName.Description.value, "Verify a command or value.")
        testitem.SetValue(TestItem.FieldName.ExpectValue.value, "UKNOWN")
        testitem.SetValue(TestItem.FieldName.PassCriteria.value, "UKNOWN")

    if (type(actualvalue) == type(b'')):
        testitem.SetValue(TestItem.FieldName.Result.value, actualvalue.decode("utf-8"))
    else:
        testitem.SetValue(TestItem.FieldName.Result.value, actualvalue)
    testitem.SetValue(TestItem.FieldName.ExpectValue.value, newvalue)

    if (actualvalue == value):
        status = True

    if (status == True):
        testitem.SetValue(TestItem.FieldName.Status.value, "Pass")
        #testitem.SetValue(TestItem.FieldName.PassFail.value, "Pass")  #bm 07/10/19:No longer use
    else:
        testitem.SetValue(TestItem.FieldName.Status.value, "Fail")
        #testitem.SetValue(TestItem.FieldName.PassFail.value, "Fail")  #bm 07/10/19:No longer use

    if(actualvalue == None):
        actualvalue = ""

    if (type(actualvalue) == type(b'')):
        if _HEXPRINT:
           actualvalue = "value=" + actualvalue.decode("utf-8") + "<br>" + "HEX:"+source
        else:
           actualvalue = "value=" + actualvalue.decode("utf-8")
    else:        
        if _HEXPRINT:
           actualvalue = "value=" + actualvalue + "<br>" + "HEX:" + source
        else:
           actualvalue = "value=" + actualvalue

    testitem.SetValue(TestItem.FieldName.Result.value, actualvalue)

    if (newvalue == None):
        newvalue = ""
    if _HEXPRINT:
       newvalue = "value=" + str(newvalue) + "<br>" + "HEX:" + target
    else:
       newvalue = "value=" + str(newvalue) 

    testitem.SetValue(TestItem.FieldName.ExpectValue.value, newvalue)

    # bm 12/26/2018: add support for tag in test description
    newvalue = testitem.GetValue(TestItem.FieldName.Description)
    if (iterindex != None):
        newvalue = InsertIndexData("", iterindex, newvalue)

    testitem.SetValue(TestItem.FieldName.Description.value, newvalue)

    if(_VERBOSE or (_LOGGER != None)):
        newvalue = newvalue.replace("<br>","  ")
        actualvalue = actualvalue.replace("<br>","  ")
        msg = "Validate UUT {0} -- Compare {1}".format(actualvalue, newvalue)
        if (_VERBOSE):
            print("[INFO]: "+msg)
        if (_LOGGER != None):
            _LOGGER.logInfo(msg)

    return testitem

def CompareJValue(echoOFF,jvalue,lookup,iterindex, actualvalue):
    global _DEBUG; global _VERBOSE; global _LOGGER;global connect
    overallstatus = True
    results = None
    testitem = None
    target = None
    uut = None
    newvalue = None

    if (jvalue == None):
        return False, None

    for jprop in jvalue:
        if jprop.lower() == "value":
            value = jvalue.get(jprop)  # possible json object
            jsondata = json.dumps(value)
            value = jsondata.encode('utf-8')
        elif jprop.lower() == "subtest_info":
            subtest_info = jvalue.get(jprop)
        elif jprop.lower().startswith("_comment"):
            continue
        else:
            if (_VERBOSE):
                print("[ERR]: Unknown tag name< " + jprop + ">")
            if (_LOGGER != None):
                _LOGGER.logErr("Unknown tag name<" + jprop + ">")
    # done for-loop

    if (_VERBOSE):
        print("[INFO] " + str(value))
    if (_LOGGER != None):
        _LOGGER.logInfo(str(value))

    results = _compareJsonObject(echoOFF,"", value, actualvalue)
    

    for result in results:
        if (result["pass"].lower() == "fail"):
            overallstatus = False

    # bm 05/03/2018: wrong position of the 'else:'
    if (subtest_info != None):
        try:
            testitem = QATestEnv.CreateTestItem(subtest_info)
        except Exception:
            testitem = TestItem()
            testitem.SetValue(TestItem.FieldName.Description.value, "Verify a command or value")
            testitem.SetValue(TestItem.FieldName.ExpectValue.value, "UKNOWN")
            testitem.SetValue(TestItem.FieldName.PassCriteria.value, "UKNOWN")
    else:
        testitem = TestItem()
        testitem.SetValue(TestItem.FieldName.Description.value, "Verify a command or value.")
        testitem.SetValue(TestItem.FieldName.ExpectValue.value, "UKNOWN")
        testitem.SetValue(TestItem.FieldName.PassCriteria.value, "UKNOWN")

    if _HEXPRINT:
       uut = "value=" + actualvalue.decode("utf-8") + "<br>" + "HEX:" + uut
       target = "value=" + newvalue.decode("utf-8") + "<br>" + "HEX:" + target
    else:
       uut = "value=" + actualvalue.decode("utf-8")
       target = "value=" + newvalue.decode("utf-8")
    
    # bm 12/26/2018: add support for tag in test description
    newvalue = testitem.GetValue(TestItem.FieldName.Description)
    if (iterindex != None):
        newvalue = InsertIndexData("", iterindex, newvalue)

    testitem.SetValue(TestItem.FieldName.Description.value, newvalue)
    testitem.SetValue(TestItem.FieldName.Result.value, uut)
    testitem.SetValue(TestItem.FieldName.ExpectValue.value, target)

    if (overallstatus == True):
        testitem.SetValue(TestItem.FieldName.Status.value, "Pass")
        #testitem.SetValue(TestItem.FieldName.PassFail.value, "Pass") #bm 07/10/19:No longer use
    else:
        testitem.SetValue(TestItem.FieldName.Status.value, "Fail")
        #testitem.SetValue(TestItem.FieldName.PassFail.value, "Fail") #bm 07/10/19:No longer use

    if (_VERBOSE or (_LOGGER != None)):
        msg = "uut:{0}   expect:{1}".format(uut, target)
        if (_VERBOSE):
            print("[INFO]: " + msg)
        if (_LOGGER != None):
            _LOGGER.logInfo(msg)

    return overallstatus, results

def CompareSValue(echoOFF,svalue,lookup,iterindex, actualvalue):
    global _DEBUG; global _VERBOSE; global _LOGGER;global connect
    status = False
    testitem = None
    subtest_info = None
    target = None
    uut = None
    newvalue = None
    matchvalue = None
    matchtype = "match"
    value = None
    if (svalue == None):
        testitem = TestItem()
        testitem.SetValue(TestItem.FieldName.Description.value, "NO DATA TO VERIFY")
        testitem.SetValue(TestItem.FieldName.ExpectValue.value, "UKNOWN")
        testitem.SetValue(TestItem.FieldName.PassCriteria.value, "UKNOWN")
        return status,testitem  #should not happen

    for jprop in svalue:
        if jprop.lower() == "value":
           if matchvalue == None:
              value = svalue.get(jprop)
        elif jprop.lower() in["match","search"]:
            if value == None:
               matchvalue = svalue.get(jprop)
               matchtype = jprop.lower()
        elif jprop.lower() == "subtest_info":
            subtest_info = svalue.get(jprop)
        elif jprop.lower().startswith("_comment"):
            continue
        else:
            if (_VERBOSE):
                print("[ERR]: Unknown tag name< " + jprop + ">")
            if (_LOGGER != None):
                _LOGGER.logErr("Unknown tag name<" + jprop + ">")
                # done for-loop
    
    #convert to bytearray to have a common comparsion
    try:        
        if (actualvalue != None):
            if type(actualvalue) != type(b''):
               actualvalue = QAUtils.StringToByte(actualvalue)
        else:
            actualvalue = b""
    except Exception as e:
         if (_VERBOSE):
             print("[ERR]:" + str(e))
         if (_LOGGER != None):
            _LOGGER.logErr("[ERR]:" + str(e))
    try:
        value =  matchvalue if value == None else value #default    
        if (value != None):
            if type(value) == type(b''):
                newvalue = QAUtils.ByteToString(value)
            else:
                newvalue = value

            if (iterindex != None):
                newvalue = InsertIndexData("", iterindex, newvalue)
            newvalue = LookUpValue(lookup, newvalue)
            newvalue = QAUtils.StringToByte(newvalue)
        else:
            newvalue = b""
    except Exception as e:
         if (_VERBOSE):
             print("[ERR]:" + str(e))
         if (_LOGGER != None):
            _LOGGER.logErr("[ERR]:" + str(e))

    if (_DEBUG or _VERBOSE):
        try:
            if newvalue == None or len(newvalue) == 0:
                msg = "Lookup orginal:{0}   new value:{1}".format(value, '<no value>')
            else:
                msg = "Lookup orginal:{0}   new value:{1}".format(value, newvalue.decode("utf-8"))
            if (_VERBOSE):
                print("[INFO]: " + msg)
            if (_LOGGER != None):
                _LOGGER.logInfo(msg)
        except Exception as e:
            if (_VERBOSE):
                print("[ERR]:" + str(e))
            if (_LOGGER != None):
                _LOGGER.logErr("[ERR]:" + str(e))
    
    command = ""
    if(echoOFF):  #bm 10/9: Change business rules.  Ignore echo response
      #assume it is SIS       
      if (type(connect.LastSend) == type("")):
          command = connect.LastSend #QAUtils.StringToByte(connect.LastSend+"\n")
      else:
          command = QAUtils.ByteToString(connect.LastSend)
      
      tempstr = QAUtils.ByteToString(actualvalue)
      if tempstr.startswith(command):
         actualvalue = QAUtils.StringToByte(tempstr[len(command):])

    if matchvalue != None:
        if type(actualvalue) == type(b''):
           tempstr =  QAUtils.ByteToString(actualvalue)  #pattern
        if type(newvalue) == type(b''):
           pattern =  QAUtils.ByteToString(newvalue)
        try:           
           #eval("re.{0}({1},{2})".format(matchtype,pattern,tempstr))
           regexpress = "(re.{0}(r\"{1}\",\"{2}\") != None)".format(matchtype,pattern,tempstr)
           newvalue = QAUtils.StringToByte("{0}(\"{1}\")".format(matchtype,pattern))
           status = eval(regexpress)        
        except Exception as err:
            status = False
            if (_VERBOSE):
                print("[ERR]:{0}\n{1}".format(str(err),"{0}({1},{2})".format(matchtype,pattern,tempstr)))
            if (_LOGGER != None):
                _LOGGER.logErr("[ERR]:{0}\n{1}".format(str(err),"{0}({1},{2})".format(matchtype,pattern,tempstr)))
    elif (newvalue == actualvalue):
       status = True

    if (actualvalue != None) and len(actualvalue) > 0:
        uut = ":".join("{:02x}".format(c) for c in actualvalue)
    else:
        uut = ""

    if (newvalue != None) and len(newvalue) > 0:
       target = ":".join("{:02x}".format(c) for c in newvalue)
    else:
        target = ""

    # bm 05/03/2018: wrong position of the 'else:'
    if (subtest_info != None):
        try:
            testitem = QATestEnv.CreateTestItem(subtest_info)
        except Exception as e:
            testitem = TestItem()
            testitem.SetValue(TestItem.FieldName.Description.value, "Verify a command or value")
            testitem.SetValue(TestItem.FieldName.ExpectValue.value, "UKNOWN")
            testitem.SetValue(TestItem.FieldName.PassCriteria.value, "UKNOWN")
    else:
        testitem = TestItem()
        testitem.SetValue(TestItem.FieldName.Description.value, "Verify a command or value.")
        testitem.SetValue(TestItem.FieldName.ExpectValue.value, "UKNOWN")
        testitem.SetValue(TestItem.FieldName.PassCriteria.value, "UKNOWN")

    if _HEXPRINT:
       uut = "value=" + actualvalue.decode("utf-8") + "<br>" + "HEX:" + uut
       target = "value=" + newvalue.decode("utf-8") + "<br>" + "HEX:" + target
    else:
       uut = "value=" + actualvalue.decode("utf-8")
       target = "value=" + newvalue.decode("utf-8")
       
    try:
        # bm 12/26/2018: add support for tag in test description
        newvalue = testitem.GetValue(TestItem.FieldName.Description)
        if (iterindex != None):
            newvalue = InsertIndexData("", iterindex, newvalue)

        testitem.SetValue(TestItem.FieldName.Description.value, newvalue)
        testitem.SetValue(TestItem.FieldName.Result.value, uut)
        testitem.SetValue(TestItem.FieldName.ExpectValue.value, target)

        if (status == True):
            testitem.SetValue(TestItem.FieldName.Status.value, "Pass")
            #testitem.SetValue(TestItem.FieldName.PassFail.value, "Pass")   #bm 07/11:No longer use
        else:
            testitem.SetValue(TestItem.FieldName.Status.value, "Fail")
            #testitem.SetValue(TestItem.FieldName.PassFail.value, "Fail")   #bm 07/11:No longer use

        if (_VERBOSE or (_LOGGER != None)):
            msg = "uut:{0}   expect:{1}".format(uut, target)
            if (_VERBOSE):
                print("[INFO] " + msg)
            if (_LOGGER != None):
                _LOGGER.logInfo(msg)
    except Exception as e:
        if (_VERBOSE):
           print(str(e)) 
    return status, testitem

###############################################################################
#
###############################################################################
def GetSISComand(mainToken):
    global _DEBUG;global _VERBOSE; global _LOGGER
    waitms = None
    status = False
    hexcommand = None
    name = ""
    command = None
    wait = None
    waittime = 0
    reponsewait = None
    timeout = 3

    if (mainToken == None):
        return False

    for jprop in mainToken:
        if name.lower() == "sis":
            if (hexcommand == None):
                command = mainToken.get(name)
                hexcommand = False
        elif name.lower() == "sishex":
            if (hexcommand == None):
                command = mainToken.get(name)
                hexcommand = True
        elif name.lower() == "wait":
            if (waitms == None):
                wait = mainToken.get(name)
                waitms = False
        elif name.lower() == "waitms":
            if (waitms == None):
                wait = mainToken.get(name)
                waitms = True
        elif name.lower() == "timeout":
            reponsewait = mainToken.get(name)
        else:
            if (_VERBOSE):
                print("[ERR]: Unknown tag name< " + jprop + ">")
            if (_LOGGER != None):
                _LOGGER.logErr("Unknown tag name<" + jprop + ">")

    if (command == None):
        if (_VERBOSE):
            print("[INFO]: Command is missing.")
        if (_LOGGER != None):
            _LOGGER.logInfo("Command is missing.")
        return status

    if (reponsewait == None):  # timeout
        timeout = 3  # no wait
    else:
        timeout = int(reponsewait)

    if (wait == None):
        waittime = 0
    else:
        waittime = int(wait)
        if (waitms):
            waittime = -waittime

    return command, timeout, waittime

def ActionSendCommand(tcattr,sis, readafter=True,timeout=3, waittime=0):
    global connect;global responseAction;global _DEBUG;global _VERBOSE; global _LOGGER
    status = False
    start = None
    source = ""
    starttime = ""
    source = ":".join("{:02x}".format(c) for c in bytes(sis["uri"], 'utf-8'))
    sendmsg = "Send:{0}     Hex:{1}".format(repr(sis["uri"]),source)
    sendmsg = sendmsg.replace("\r","<cr>")

    if(_VERBOSE or (_LOGGER != None)):
        try:            
            if(_VERBOSE):
               print("[INFO]: "+sendmsg)
            if (_LOGGER != None):
                _LOGGER.logInfo(sendmsg)
        except Exception as e:
            if (_VERBOSE):
                print("[ERR]: "+str(e))
            if (_LOGGER != None):
                _LOGGER.logErr(str(e))
    if(_DEBUG):
       return True

    start = datetime.datetime.now()    
    starttime = datetime.datetime.now().strftime('%H:%M:%S')

    try:
        status = connect.SendCommand(wait=waittime,command=sis)

        if(readafter):
          responseAction = connect.RawResponse() #InternalSendRepsone
          if (_VERBOSE):
             print("[INFO]: reponse - {0}".format(str(repr(responseAction.decode("utf-8")))))
          if (_LOGGER != None):
            _LOGGER.logInfo("reponse - {0}".format(str(repr(responseAction.decode("utf-8")))))
    except Exception as e:
        status = False
        if(_VERBOSE):
            print("[ERR]: Send fail:" + str(e))
        if (_LOGGER != None):
            _LOGGER.logInfo("Send fail:" + str(e))
    
    if (_VERBOSE):#(_PERFORMANCE):
       print("[INFO]:{0} Start:{1} \tDurration:{2}".format(sis,starttime,datetime.datetime.now() - start))

    if status == False:
        testitem = TestItem()
        testitem.SetValue(TestItem.FieldName.Description.value, sendmsg)
        testitem.SetValue(TestItem.FieldName.Result.value, "Fail")
        testitem.SetValue(TestItem.FieldName.ExpectValue.value, "Sucess")
        testitem.SetValue(TestItem.FieldName.Status.value, "Fail")
        #testitem.SetValue(TestItem.FieldName.PassFail.value, "Fail") #bm 07/10/19:No longer use
        if(tcattr != None):
           tcattr.AddSubItemTest(testitem)

    return status

###############################################################################
#
###############################################################################
def ProcessValidate(iterindex=None, tcattr=None, validateToken=None):
    global connect;global _DEBUG;global _VERBOSE; global _LOGGER;global _NEBULA_MODE

    status = False
    stripresponse = True
    echoOFF = True
    svalue = None
    jvalue = None
    reponse = None
    lookup = None
    testitem = None

    if (validateToken == None):
        return status

    for jprop in validateToken:

        i = jprop.find("_")
        if (i > 0):
            name = jprop[:i]
        else:
            name = jprop

        if name.lower() == "stripcr":
            stripresponse = validateToken.get(name)
            if type(stripresponse) == type(""):
               stripresponse = stripresponse.lower().strip()
               stripresponse =  True if stripresponse in ["true","yes","y",True] else False if stripresponse in ["false","no","n",False] else True #default    
            elif type(stripresponse) == type(True):
               continue
            else:
               stripresponse = True
        elif  name.lower() == "lookupvalues" or  name.lower() == "rangevalues":
            lookup = validateToken.get(name)
        elif name.lower() == "svalue":
            svalue = validateToken.get(name)
        elif name.lower() == "jvalue":
            jvalue = validateToken.get(name)
        elif name.lower() == "noecho":
            echoOFF = validateToken.get(name)
        elif name.lower().startswith("_comment"):
            continue
        else:
            if (_VERBOSE):
                print("[ERR]: Unknown tag name< " + jprop + ">")
            if (_LOGGER != None):
                _LOGGER.logErr("Unknown tag name<" + jprop + ">")
    # done for-loop

    if(svalue == None) and (jvalue == None):
        return status  #nothing to do

    response=None
    try:
        if (connect.InternalSendRepsone != None) and (type(connect.InternalSendRepsone) != type(b'')):
            response = connect.InternalSendRepsone.encode()
        else:
            response = connect.InternalSendRepsone

        if type(response) == "<class 'collections.OrderedDict'>":
           response = bytearray(str(reponse), 'utf-8')
    except Exception:
        response = None

    try:
        #bm 07/22/19: Add this section to handle global echo on or off
        if type(echoOFF) == type(0):
           if echoOFF == 1:
              echoOFF = True
        elif type(echoOFF) == type(False):
             pass
        elif type(echoOFF) == type(""):
             if echoOFF.lower().strip() == "false" or echoOFF.lower().strip() == "0":
                echoOFF = False
             else:
                 echoOFF = True
        
        if (stripresponse):  # compare striplinefeed
           if (response != None) and response[-len(LINEFEED()):] == LINEFEED():  # JITC
              response = response[:-(len(LINEFEED()))]
           elif (response != None) and response[-len(SISLINEFEED()):] == SISLINEFEED():
              response = response[:-len(SISLINEFEED())]
           else:
                response = ""  #double check
        if(svalue != None):           
           status, testitem = CompareSValue(echoOFF,svalue, lookup, iterindex, response)
        
        if (jvalue != None):
            status, _, _ = CompareJValue(echoOFF,jvalue,lookup,iterindex, response)

    except Exception:
        status = False
        #tcattr.AddSubItemTest(testitem)

    if testitem.Status.lower() == "pass":    #bm 07/10/19: change PassFail to Status
        status = True        
    if(_NEBULA_MODE and not status):
        #raise nebula exception for fail tc  
        raise nte.NebulaTestException(testitem.Description + "-->Expect:"+testitem.Expectvalue+"\tReceive:"+testitem.Result)
    return status

###############################################################################
#
###############################################################################

def ProcessComand(iterindex=None,tcattr=None, mainToken=None):
    global _DEBUG;global _VERBOSE;global _LOGGER
    status = False
    command = None
    method = ""
    autocr = False     #default
    data = None
    readafter = True  #default
    waittime = 0.05

    if (mainToken == None):
        return False

    for jprop in mainToken:

        i = jprop.find("_")
        if (i > 0):
            name = jprop[:i]
        else:
            name = jprop

        if name.lower() == "method":   #http-- PUT/GET/PUB/....  #bm will deprecate
            method = mainToken.get(name)
        elif name.lower() == "autocr":
            autocr = mainToken.get(name)
            if type(autocr) == type(""):
               autocr = autocr.lower().strip()
            elif type(autocr) == type(True):
               continue
            else:
               autocr = True    
        elif name.lower() in ['put','get','pub']:
            command = mainToken.get(name)
            method = name.lower()
            continue
        elif name.lower() == "data":
            data = mainToken.get(name)
        elif name.lower() == "exec":   #bm will deprecate
            command = mainToken.get(name)
        elif name.lower() in["escsis","wsis","sis"]:   #bm replace exec and combine method
            command = mainToken.get(name)
            method = name.lower()[:-3]
        elif name.lower() == "reponsewait":
            waitresp = mainToken.get(name)
        elif name.lower() == "readafter":
            readafter = mainToken.get(name)
            if type(readafter) == type(""):
               readafter = readafter.lower().strip()
            elif type(readafter) == type(True):
               continue
            else:
               readafter = True
        elif name.lower().startswith("_comment"):
            continue
        else:
            if (_VERBOSE):
                print("[ERR]: Unknown tag name< " + jprop + ">")
            if (_LOGGER != None):
                _LOGGER.logErr("Unknown tag name<" + jprop + ">")

    method = method.lower().strip()
    if (method in ("put","get","pub")):
        method = method.upper().strip()
    elif (method in ("esc")):
        method = "\u001B"
    elif (method != "w"):
        method = ""

    autocr =  "\r" if autocr in ["true",True] else "" if autocr in ["false",False] else "\r" #default    
    readafter =  True if readafter in ["true",True] else False if readafter in ["false",False] else False

    try:
        command = InsertIndexData("", iterindex, command) + autocr
        waittime = float(waitresp)
    except Exception:
        waittime = 0.05

    command = {"uri":command,"method":method, "data":data}
    status = ActionSendCommand(tcattr,sis=command,readafter=readafter,waittime=waittime)

    return status

def ProcessPCS4Comand(iterindex=None, tcattr=None, mainToken=None):
    global _DEBUG;global _VERBOSE;global _LOGGER
    status = False
    name = ""
    i = 0
    command = None
    port = None

    if (mainToken == None):
        return False

    for jprop in mainToken:

        i = jprop.find("_")
        if (i > 0):
            name = jprop[:i]
        else:
            name = jprop

        if name.lower() == "state":
            command = mainToken.get(name)
        elif name.lower() == "port":
            port = int(mainToken.get(name))
        elif name.lower().startswith("_comment"):
            continue
        else:
            if (_VERBOSE):
                print("[ERR]: Unknown tag name< " + jprop + ">")
            if (_LOGGER != None):
                _LOGGER.logErr("Unknown tag name<" + jprop + ">")

    if (command == None):
        print("Command is missing.")
        return status

    if (port != None):
        if (int(port) < 1 or int(port) > 4 ):
           print("Invalid Port Number:"+port)
           return status
    else:
        print("Port Number is missing.")
        return status
    status = PCS4Unit(command, int(port), timeout=1, waittime=0)

    if status == False:
        testitem = TestItem()
        testitem.SetValue(TestItem.FieldName.Description.value, "Send Power {0} PCS4 port#{1} fails.".format(command,port))
        testitem.SetValue(TestItem.FieldName.Result.value, "Fail")
        testitem.SetValue(TestItem.FieldName.ExpectValue.value, "Sucess")
        testitem.SetValue(TestItem.FieldName.Status.value, "Fail")
        #testitem.SetValue(TestItem.FieldName.PassFail.value, "Fail") #bm 07/10/19:No longer use
        if(tcattr != None):
           tcattr.AddSubItemTest(testitem)

        msg = "Send Power {0} PCS4 port#{1} fails.".format(command,port)
        if (_VERBOSE):
            print("[INFO] " + msg)
        if (_LOGGER != None):
           _LOGGER.logInfo(msg)

    return status

def ProcessReconnect(iterindex=None, tcattr=None, loginToken=None):
    global connect; global logininfo; global banner;global _DEBUG;global _VERBOSE; global _LOGGER
    newlogin = None
    newconnect = None

    status = False

    if (loginToken != None) and len(loginToken) > 0:
        newlogin = ProcessConfigurationLogin(loginToken, logininfo)
    elif(logininfo != None):
        newlogin = logininfo
    else:
        return False

    try:
        if(not _DEBUG):
           if(connect != None):
              connect.Close()  #force to close
           newconnect = QAUtils.Connect(logininfo,_LOGGER)
           connect = newconnect
        logininfo = newlogin
        status = True
    except Exception as e:
        if (_VERBOSE):
            print("INFO]:" + str(e))
        if (_LOGGER != None):
           _LOGGER.logErr(str(e))

    return status

def ProcessDisconnect(iterindex=None, tcattr=None, loginToken=None):
    global connect; global logininfo; global banner
    status = False

    try:
        if(not _DEBUG):
           if(connect != None):
              connect.Close()  #force to close
              connect = None
        status = True
    except Exception as e:
        if (_VERBOSE):
            print("INFO]:" + str(e))
        if (_LOGGER != None):
           _LOGGER.logErr(str(e))


    return status

# =====================================================

def ProcessLoopAction(looptype, prefixstack, index, tcattr, mainToken):
    global _DEBUG;global _VERBOSE;global _LOGGER
    status = False
    overallstatus = True
    includeJson = None
    name = ""
    i = 0
    prefix = ""
    
    if (mainToken == None):
        return False

    for jprop in mainToken:
        name = jprop
        i = jprop.find("_")
        if (i > 0):
            name = jprop[:i]
            prefix = jprop[i + 1:]
        else:
            name = jprop
            prefix = ""
        status = False

        msg = "Element<{0}>".format(jprop)
        if (_VERBOSE):
            print("[INFO]: " + msg)
        if (_LOGGER != None):
           _LOGGER.logInfo(msg)
        
        if name.lower() == "wait":
            try:
                sleep(float(mainToken.get(jprop)))  # allow remote device to response
                status = True
            except Exception as e:
                if( _VERBOSE):
                    print("[ERR]: {0}".format(str(e)))
                    print("[ERR]:  bad wait value:{format(mainToken.get(jprop))}")
                if (_LOGGER != None):
                   _LOGGER.logErr(str(e))
                   _LOGGER.logErr(f"bad wait value: {format(mainToken.get(jprop))}")

        elif name.lower() == "execommand":
            status = ProcessComand(prefixstack,tcattr, mainToken.get(jprop))
        elif name.lower() == "pcs4power":
            status = ProcessPCS4Comand(prefixstack, tcattr, mainToken.get(jprop))
        elif name.lower() == "reconnect":
            status = ProcessReconnect(prefixstack, tcattr, mainToken.get(jprop))
        elif name.lower() == "disconnect":
            status = ProcessDisconnect(prefixstack, tcattr, mainToken.get(jprop))
        elif name.lower() == "validate":
            status = ProcessValidate(prefixstack, tcattr, mainToken.get(jprop))
        elif name.lower() == "#loop":           
            status = ProcessLoop(prefixstack, prefix, tcattr, mainToken.get(jprop))
        elif name.lower() == "#range":            
            status = ProcessRange(prefixstack, prefix, tcattr, mainToken.get(jprop))
        elif name.lower() == "#include":
            includeJson = QATestEnv.ReadJson(mainToken.get(jprop), QATestEnv.envworkingdir,QATestEnv.workingdir)
            #includeJson = FunctionJITC.ReadJson(mainToken.get(jprop))
            if(includeJson != None):
               if(_VERBOSE):
                  tempfilename = mainToken.get(jprop) 
                  print("[INFO]:#include {0} --> {1}".format(tempfilename,QATestEnv.GetEnvFileName(tempfilename,QATestEnv.envworkingdir,QATestEnv.workingdir))) 
               status =  ProcessLoopAction(looptype, prefixstack, index, tcattr, includeJson)
            else:
                status = False
            includeJson = None
        elif jprop.lower().startswith("_comment"):
            status = True
        else:
            if (_VERBOSE):
                print("[ERR]: Unknown tag name< " + jprop + ">")
            if (_LOGGER != None):
                _LOGGER.logErr("Unknown tag name<" + jprop + ">")

            status = True
            
        if (status == False):
            overallstatus = False

    return overallstatus

#bm 03/04/2019: Need update to handle external data
#bm 03/28/2019: Add Excel and CVS support as a dataset
def ProcessRange(prefixstack, prefix, tcattr, mainToken):
    global _DEBUG;global _VERBOSE;global _LOGGER
    jpropiteration = None
    jpropaction = None

    status = False
    overallstatus = True
    name = ""
    if (mainToken == None):
        return False

    #if (_VERBOSE):
    #    print("Process Range Loop {0}".format(datetime.datetime.now().strftime('%H:%M:%S')))
    
    for jprop in mainToken:
        name = jprop

        if name.lower() == "iteration":
            jpropiteration = mainToken.get(name)
        elif name.lower() == "action":
            jpropaction = mainToken.get(name)
        elif name.lower().startswith("_comment"):
            continue
        else:
            if (_VERBOSE):
                print("[ERR]: Unknown tag name< " + jprop + ">")
            if (_LOGGER != None):
                _LOGGER.logErr("Unknown tag name<" + jprop + ">")

    #if (_VERBOSE):
    #    print("End Process Range Loop {0}".format(datetime.datetime.now().strftime('%H:%M:%S')))

    if (jpropiteration == None):
        return False  # bad data

    if type(jpropiteration) == type(""):  #possible external data
       # Dummy_{0} [{1},{2},{3}] -->  Dummy_<name> [@C:\Testing\Book1.csv,True] or [@C:\Testing\Book1.xlm,Sheet2,True]
       jpropiteration = jpropiteration.strip().split(",")
       jpropiteration[0] = QATestEnv.GetEnvFileName(jpropiteration[0], QATestEnv.envworkingdir, QATestEnv.workingdir)
       prefix,jpropiteration = QAHelpers.GetLoopParam("Dummy_{0} [@{1}]".format(prefix,",".join(jpropiteration)))
      
    iteration = jpropiteration

    if type(iteration) == type([]):
       stack = {"r" + prefix: {"iteration": iteration, "index": iteration[0]}}
    else:
        stack = {"r" + prefix: {"iteration": iteration, "index": iteration.get()}}
    newprefixstack = QAUtils.MergeDicts(prefixstack, stack)

    for index in iteration:       
        newprefixstack["r" + prefix]["index"] = index
        if (_VERBOSE):
            print("[INFO]: Range index:{0}".format(index))  #datetime.datetime.now().strftime('%H:%M:%S'),
        if (_LOGGER != None):
           _LOGGER.logInfo("Range: index:{0}".format(index))

        status = ProcessLoopAction('r', newprefixstack, index, tcattr, jpropaction)

        if (_DEBUG):
            print("\r")

        if(status == False):
            overallstatus = False
    #if (validate != None):
    #    status = ProcessValidate(tcattr, validate)

    return overallstatus
	
#bm 03/04/2019: Need update to handle external data
def ProcessLoop(prefixstack, prefix, tcattr, mainToken):
    global _DEBUG;global _VERBOSE;global _LOGGER
    jpropstart = None
    jpropincby = None
    jpropiteration = None
    jpropaction = None
    status = False
    overallstatus = True
    name = ""
    i = 0

    if (mainToken == None):
        return False

    for jprop in mainToken:
        name = jprop

        if name.lower() == "start":
            jpropstart = mainToken.get(name)
        elif name.lower() == "incby":
            jpropincby = mainToken.get(name)
        elif name.lower() == "iteration":
            jpropiteration = mainToken.get(name)
        elif name.lower() == "action":
            jpropaction = mainToken.get(name)
        elif name.lower().startswith("_comment"):
            continue
        else:
            if (_VERBOSE):
                print("[ERR]: Unknown tag name< " + jprop + ">")
            if (_LOGGER != None):
                _LOGGER.logErr("Unknown tag name<" + jprop + ">")

    #bm 12/26/2018:conversion.  Accept string or number
    if type(jpropstart) == type(0):
        jpropstart = str(jpropstart)
    if type(jpropincby) == type(0):
        jpropincby = str(jpropincby)
    if type(jpropiteration) == type(0):
        jpropiteration = str(jpropiteration)

    if (jpropstart == None) or len(jpropstart.strip()) == 0:
        jpropstart = "0"
    if (jpropincby == None) or len(jpropincby.strip()) == 0:
        jpropincby = "1"
    if (jpropiteration == None) or len(jpropiteration.strip()) == 0:
        if (_VERBOSE):
            print("[ERR]: Bad data:"+mainToken)
        if (_LOGGER != None):
            _LOGGER.logErr("Bad data:"+mainToken)
        return False  # bad data

    startindex = int(jpropstart)
    incby = int(jpropincby)
    iteration = int(jpropiteration)
    index = startindex

    stack = {"l" + prefix: {"startindex": startindex, "incy": incby, "iteration": iteration, "index": startindex}}
    newprefixstack = QAUtils.MergeDicts(prefixstack, stack)
    for i in range(iteration):
        newprefixstack["l" + prefix]["index"] = index
        if(_VERBOSE):
            print("[INFO]: Loop index:{0}".format(i))
        if (_LOGGER != None):
           _LOGGER.logInfo("Loop: index:{0}".format(i))

        #if (_PERFORMANCE):
        #    print("Process Loop Action{0}".format(datetime.datetime.now().strftime('%H:%M:%S')))
        status = ProcessLoopAction('l', newprefixstack, index, tcattr, jpropaction)
        #if (_PERFORMANCE):
        #    print("End Process Loop Action{0}".format(datetime.datetime.now().strftime('%H:%M:%S')))

        if (_DEBUG or _VERBOSE):
            print("\r")
        if (status == False):
            overallstatus = False
        index = index + incby
    
    return overallstatus

def Process(prefixstack, tcattr, mainToken):
    return ProcessLoopAction("", prefixstack, "", tcattr, mainToken)
    
def ProcessTC(testresults, testcasenum, mainToken):
    global _DEBUG;global _VERBOSE; global _LOGGER
    status = False
    test_info = None
    precondition = None
        
    if(_VERBOSE):
        print("[INFO]: Test name - {0}".format(testcasenum))
    if (_LOGGER != None):
       _LOGGER.logInfo("Test name - {0}".format(testcasenum))

    if (mainToken == None):
        return status

    for jprop in mainToken:

        if jprop.lower() == "precondition":
            precondition = mainToken.get(jprop)
        elif jprop.lower() == "steps":
            steps = mainToken.get(jprop)
        elif jprop.lower() == "action":
            steps = mainToken.get(jprop)
        elif jprop.lower() == "test_info":
            test_info = mainToken.get(jprop)
        elif jprop.lower().startswith("_comment") or jprop.lower().startswith("comment"):
            continue
        else:
            if (_VERBOSE):
                print("[ERR]: Unknown tag name< " + jprop + ">")
            if (_LOGGER != None):
                _LOGGER.logErr("Unknown tag name<" + jprop + ">")

    
    # done for loop
    precondition_tcattr = None
    if (test_info != None):
        tcattr = QATestEnv.CreateTestAttributes(test_info)
    else:
        tcattr = TestAttributes()
        
    start = time()  #start timer
    #if (_VERBOSE):
    print("[INFO]: {0}".format(tcattr.TestName))
    
    if (precondition != None) and len(precondition) > 0:
        precondition_tcattr = TestAttributes()    #bm 07/03/2017: to handle the skip
        precondition_tcattr.SetValue(TestAttributes.FieldName.TestName.value, tcattr.TestName)
        precondition_tcattr.SetValue(TestAttributes.FieldName.Description.value, "Precondition")
        status = Process({}, precondition_tcattr, precondition)  # start with an empty prefix stack
    else:
        status = True
    
    if(status == True):
        try:
            if (steps == None):  #indicate in the report it is a skip.
                print("No action given")
                if (_LOGGER != None):
                    _LOGGER.logInfo("No action given")
            else:
                status = Process({}, tcattr, steps)  # start with an empty prefix stack
        except nte.NebulaTestException as ne:
            raise type(ne)(repr(ne))
        except Exception:
            status = False
        
        if (status == True):
            tcattr.SetValue(TestAttributes.FieldName.Status.value, "Pass")
        else:
            tcattr.SetValue(TestAttributes.FieldName.Status.value, "Fail")

    else: #bm 07/05/2018:  Precondition failed.
        msg = "Precondition setup failed."
        if (_VERBOSE):
            print("[INFO]: {0}".format(msg))
        if (_LOGGER != None):
            _LOGGER.logInfo(msg)
        tcattr.SetValue(TestAttributes.FieldName.Description.value, "Precondition setup failed.")
        tcattr.SetValue(TestAttributes.FieldName.Status.value, "Skip")
        if precondition_tcattr != None:
           tcattr.AddSubItemTest(precondition_tcattr.GetSubTestItems())
    
    end = time() - start  #end timer    
    print("\telapse time {0}".format(str("{:5.4f}".format(end))+"sec" ))
    #jsheader.SetValue(TestHeader.FieldName.TestRunDuration, str("{:10.4f}".format(end)))
    tcattr.SetValue(TestAttributes.FieldName.Duration.value, str("{:5.4f}".format(end))+"sec" )
    #tcattr.SetValue(TestAttributes.FieldName.Duration.value, str(strftime("%H:%M:%S", gmtime(end))))
    
    testresults.append(tcattr)

    return status

def ProcessTCScheme(testresults, mainToken):
    global _DEBUG; global _VERBOSE; global _LOGGER
    status = False
    includeJson = None
    name = ""
    i = 0

    for jprop in mainToken:

        i = jprop.find("_")
        if (i > 0):
            name = jprop[:i]
        else:
            name = jprop

        # name = name.lower()

        if name.lower() == "testcase":
            testcasenum = jprop[i + 1:]
            status = ProcessTC(testresults, testcasenum, mainToken.get(jprop))        
        elif name.lower() == "#include":            
            includeJson = QATestEnv.ReadJson(mainToken.get(jprop), QATestEnv.envworkingdir, QATestEnv.workingdir)            
            if (includeJson != None):
                if(_VERBOSE):
                  print("[INFO]:#include {0}".format(mainToken.get(jprop)))
                status = ProcessTCScheme(testresults, includeJson)
            else:
                print("Cannot load file:{0}".format(mainToken.get(jprop)))
                status = False
            includeJson = None
        elif jprop.lower().startswith("_comment"):
            continue
        else:
            if (_VERBOSE):
                print("[ERR]: Unknown tag name< " + jprop + ">")
            if (_LOGGER != None):
                _LOGGER.logErr("Unknown tag name<" + jprop + ">")

    return status

def ProcessScheme(testresults, mainToken,debug=False,verbose=False,log = None):
    global connect; global envworkingdir; global _DEBUG; global _VERBOSE; global _LOGGER;global logininfo
    jpropconfig = None
    jproptestexecute = None
    name = ""
    jprop = ""
    status = False

    if (log != None):
        _LOGGER = log

    if(debug == None):
        _DEBUG = False
    else:
        _DEBUG = debug

    if (verbose == None):
        _VERBOSE = False
    else:
        _VERBOSE = verbose

    if (mainToken == None):
        return
    try:
        for jprop in mainToken:
            name = jprop

            if name.lower() == "configuration":
                jpropconfig = mainToken.get(name)
            elif name.lower() == "testcaseexecution":
                jproptestexecute = mainToken.get(name)
            elif jprop.lower().startswith("_comment"):
                continue
            else:
                if (_VERBOSE):
                    print("[ERR]: Unknown tag name< " + jprop + ">")
                if (_LOGGER != None):
                    _LOGGER.logErr("Unknown tag name<" + jprop + ">")

        if (jpropconfig != None):
            ProcessConfiguration(jpropconfig)

        if (_DEBUG) and connect != None:
           connect.VerboseOn(ON=True)
        elif connect != None:
            connect.VerboseOn(ON=False)

        if(_DEBUG):
            if jproptestexecute == None:
                if (_VERBOSE):
                    print("No test steps define.")
                if (_LOGGER != None):
                    _LOGGER.logInfo("No test steps define.")
            else:
                if (connect != None):
                    status = ProcessTCScheme(testresults, jproptestexecute)
                else:
                    print("Cannot connect to uut:IP/Host:{0}\tUserName:{1}\tPassword:{2}\n".format(logininfo.HostName,logininfo.Username,logininfo.Password))
                    
        else:
            if (connect != None) and (jproptestexecute != None):
                status = ProcessTCScheme(testresults, jproptestexecute)
                connect.Close()
            elif jproptestexecute == None:
                if (_VERBOSE):
                    print("No test steps define.")
                if (_LOGGER != None):
                    _LOGGER.logInfo("No test steps define.")
            elif connect == None:
                #if (_VERBOSE or _DEBUG):
                print("Connection fails to uut[IP/Host:{0}\tUserName:{1}\tPassword:{2}]\n".format(logininfo.HostName,logininfo.Username,logininfo.Password))
                if (_LOGGER != None):
                    _LOGGER.logErr("Connection fails to uut[IP/Host:{0}\tUserName:{1}\tPassword:{2}]\n".format(logininfo.HostName,logininfo.Username,logininfo.Password))
                raise ValueError("Connection fails to uut[IP/Host:{0}\tUserName:{1}\tPassword:{2}]\n".format(logininfo.HostName,logininfo.Username,logininfo.Password))
    except nte.NebulaTestException as ne:
        raise type(ne)(repr(ne)) 
    finally:
        connect.Close()
    return status

def start(testresults,mainToken,conntype,iphost=None,port=None,password="",username="", debug=False,verbose=False,log = None, hexprint=True):
    global logininfo; global _HEXPRINT
    _HEXPRINT = hexprint
    logininfo = LoginInfo()

    logininfo.ConnectType = conntype
    logininfo.HostName = iphost
    logininfo.Password = password
    logininfo.Username = username
    logininfo.Port = port         #bm 07/15/2019:Add port
    return ProcessScheme(testresults, mainToken,debug,verbose,log)
