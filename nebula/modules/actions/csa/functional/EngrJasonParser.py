#import copy
import time
import re
from nebula.modules.actions.csa.lib.ExtronQAConnection import ExtronQAConnection
from nebula.modules.actions.csa.common.QAUtils import QAUtils
_DEBUG = False
_VERBOSE = False
_file = None
_params = ("",0,"")
connect =  None
linecount = 0

#scan from right to left; ignore \"
def helperrscanquote(data):
    tup = tuple(data)
    for i in range(len(tup)-1,0,-1):
        if tup[i] == "\"":
           if tup[i-1] != "\\":
              return i
    return -1
def helperlscanquote(data):
    tup = tuple(data)
    for i in range(0,len(tup)):
        if tup[i] == "\"":
           if tup[i-1] != "\\":
              return i
    return -1

def helperreplaceescape(data):    
    listtuple = list(tuple(data))
    newstring = ""
    i = 0
    while i < len(listtuple)-1:
        if listtuple[i] == "\\" and listtuple[i+1] != ' ':   #escape char
           try:
               char = eval("'\\{0}'".format(listtuple[i+1])) 
           except Exception:
               return None   #bad escape character
           newstring += char
           i += 1 #skip the next char
        else:
           newstring += listtuple[i]
        i += 1
    return newstring

def helperUpdateTuple(tup,index,newvalue):    
    try:
        listtuple = list(tup)
        listtuple[index] = newvalue
    except Exception:
        return None
    
    return tuple(listtuple)

def helperProcessRequest(line):
    line = line.strip()
    params = ""
    match = next(re.finditer(r"( +|\()\S+( +|\))",line),None)
    if (match == None):
       return (line,None,None,None)  #non param command

    command = match.string[:match.start()].strip()
    params = match.string[match.start():].strip()  #should (.....)
    line = params[1:len(params)-1]  #remove ( & )
    if (line[:1] == "\"" or line[:1] == "'") and (line[-1:] == "\"" or line[-1:] == "'"):
      line = line[1:-1]         
      line = helperreplaceescape(line)
      return (command,) + (line,None,None)         
   
    return(command,line,None,None)

###############################################################################
#       
###############################################################################
def ProcessCommpare(data):
    global connect
    
    if data == None:
       return None
    command = data[0]
    pattern = data[1]   #could be just plain value    
    actualvalue = None
    newvalue = pattern
    status = False

    #force to be common type
    if (connect.InternalSendRepsone != None):
       if (type(connect.InternalSendRepsone) != type(b'')):
          actualvalue = actualvalue = bytes(connect.InternalSendRepsone, 'utf-8')
       else:
          actualvalue = connect.InternalSendRepsone
    
    #to have the same base comparsion
    if (newvalue != None):
       if type(newvalue) != type(b''):               
         newvalue = bytes(newvalue, 'utf-8')    
    try:
        if command.lower().strip() == 'match':           
           status = re.match(pattern,actualvalue.decode())
           #if(_VERBOSE):
           print("[INFO]: MATCH[{0}]".format(status))           
        elif command.lower().strip() == 'search':           
           status = re.match(pattern,actualvalue.decode())
           #if (_VERBOSE):
           print("[INFO]: SEARCH[{0}]".format(status))        
        elif command.lower().strip() == 'findall':           
           status = re.match(pattern,actualvalue.decode())
           #if (_VERBOSE):
           print("[INFO]: FINDALL[{0}]".format(status))           
        elif command.lower().strip() =='eq':
           status = (pattern == actualvalue.decode())
           if not status:
              actual = ""  
              if type(actualvalue.decode()) != type(b''):
                 actual = ":".join("{:02x}".format(c) for c in bytes(actualvalue.decode(), 'utf-8'))
              else:
                 actual = ":".join("{:02x}".format(c) for c in QAUtils.StringToByte(actualvalue))

              if type(pattern) != type(b''):
                 source = ":".join("{:02x}".format(c) for c in bytes(pattern, 'utf-8'))
              else:
                 source = ":".join("{:02x}".format(c) for c in QAUtils.StringToByte(pattern))

              print("[INFO]:EQ Miss matches")
              print("\t{0}:{1}     Hex:{2}".format("Orig",pattern,source))
              print("\t{0}:{1}     Hex:{2}".format("target",actualvalue.decode().replace("\r","<cr>").replace("\n","<enter>"),actual))
           else:
              #if (_VERBOSE):
              print("[INFO]:EQ matches\t{0}     Hex:{1}".format(pattern, source))
    except Exception as e:  #catch all error
        if _DEBUG:
          print("[DEBUG]:{0}".format(str(e)))
        
    return status

def ProcessSend(data):
    if data == None:
       return None

    command = data[1]
    status = False
    senddata = ""

    if (data[2] == None and data[3]==None):
       status = connect.RawSend(wait=0.05,rawdata=command,flush=False,readafter=True)
       senddata = ":".join("{:02x}".format(c) for c in QAUtils.StringToByte(command))       
       #print("[INFO]:Send Success\t{0}  Hex:{1}".format(command,senddata))
    elif (data[2] != None) and (type(data[2]) == type(0) or type(data[2]) == type(0.1)):         
         status = connect.RawSend(wait=data[2],rawdata=command+"\r",flush=False,readafter=True)         
         senddata = ":".join("{:02x}".format(c) for c in QAUtils.StringToByte(command+"\r"))
         command += "<cr>"
    command = command.replace("\r","<cr>").replace("\n","<enter>")
    if status:       
       print("[INFO]:Send success\t{0}  Hex:{1}".format(command,senddata))
    else:
       print("[INFO]:Send fail\t{0}  Hex:{1}".format(command,senddata))
         

    return status
    
def ProcessRequest():
    global _DEBUG; global _VERBOSE; global connect
    setdata = None
    line = GetNextLine()
    while(line != None):
        setdata = helperProcessRequest(line)
        if setdata != None and setdata[0] != None:
            if (_VERBOSE):  #send(command,wait)
               print("[INFO]: Command[{0}]".format(setdata[0]))
               print("[INFO]: Data[{0}]".format(setdata[1]))               
            if setdata[0].lower() in ['send','sendesc','sendw']:
                ProcessSend(setdata)
            #elif setdata[0].lower() in ['send','sendesc','sendw']:
            #    ProcessSend(setdata)
            elif setdata[0] == 'match':
                ProcessCommpare(setdata)
            elif setdata[0] == 'search':
                ProcessCommpare(setdata)
            elif setdata[0] == 'findall':
                ProcessCommpare(setdata)
            elif setdata[0] == 'eq':
                ProcessCommpare(setdata)
            elif setdata[0] == 'wait':
                if setdata[1] != None:
                  if (type(setdata[1]) == type(0) or type(setdata[1]) == type(0.1)):
                     time.sleep(setdata[1])
                  else:
                      try:
                          time.sleep(float(setdata[1]))
                      except Exception:
                          print("Value Error --> {0}".format(line))
                          pass
            elif setdata[0] == 'showresp':
                if(_VERBOSE):
                   print("[INFO]: Reponse[{0}]".format(connect.InternalSendRepsone))
            elif setdata[0] == 'verbose':
                if setdata[1].lower().replace("\"","").strip() == "on":
                  _VERBOSE = True
                  if (_VERBOSE):
                   print("[INFO]: Verbose[{0}]".format("ON"))
                elif setdata[1].lower().replace("\"","").strip() == "off":
                  _VERBOSE = False  
                  if (_VERBOSE):
                   print("[INFO]: Verbose[{0}]".format("OFF"))
        else:
            print("Syntax Error --> {0}".format(line))
    
        line = GetNextLine()

    return True
    
###############################################################################
#       
###############################################################################
def ReadFile(filename=""): #temporary
    global _DEBUG; global _VERBOSE; global _file
    status = False
    try:
        if len(filename) > 0:
           CloseFile()

        if(_file == None):
           _file = open(filename, "rU")
        status = True
    except Exception as e:
        if _DEBUG:
          print("[DEBUG]:{0}".format(str(e)))
        _file = None


    return status

def GetNextLine():
    global _DEBUG; global _VERBOSE; global _file; global linecount
    line = ""
    if (_file != None):
        try:
            line = _file.readline()
            while line == '\n':
                line = _file.readline()
                if line == '':
                   break

            if not line:
                line = None
            else:
                linecount += 1
            if (line == "\n") or (line == "\r"): #skip blank line
                line = _file.readline() 
        except Exception:
            line = None  # read fail -- need to indicate another way
    if (_DEBUG):
       print("[DEBUG]: Read[{0}]".format(line))

    return line

def CloseFile():  # temporary
    global _file
    try:
        if (_file != None):
            _file = _file.close()
            _file = None
    except Exception as e:
        if _DEBUG:
          print("[DEBUG]:{0}".format(str(e)))
        _file = None
    return

def start(filename=None,conntype="tel",iphost=None,port=None,password="",username="", debug=False,verbose=False,log = None):
    global _DEBUG; global _VERBOSE; global connect
    _DEBUG = debug
    _VERBOSE = verbose

    if filename == None:
       return
    if conntype == None or conntype.strip() == "":
       conntype = "tel"

    connect = ExtronQAConnection(conntype=conntype,iphost=iphost,login=username,password=password,port=port)
    if connect != None:
       connect.VerboseOn(ON=False)
       if connect.Connect(4):    
          ReadFile(filename)        
          if ProcessRequest() == False:
                pass            
          CloseFile()
    else:        
       print("[INFO]:Connection fails to uut[IP/Host:{0}\tUserName:{1}\tPassword:{2}]\n".format(iphost,username,password))

    return