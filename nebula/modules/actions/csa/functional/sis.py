#
#
#

import base64
import optparse
import re
import socket
import struct
import sys
import time


def deltatime_ms(t):
	return int((time.time()-t)*1000)


class SisDevice:

	def __init__(self, addr, **kwargs):
		addr, port, _ = (addr + ':23:').split(":",2)

		self.addr = addr
		self.port = int(port)

		self.verbose = kwargs.get('verbose', True)
		self.send_prefix = kwargs.get('send_prefix', "\n> ")
		self.recv_prefix = kwargs.get('recv_prefix', "< ")
		self.err_prefix = kwargs.get('err_prefix', "! ")
		self.color = kwargs.get('color', True)
		self.send_time = 0


	def connect(self, ssh=None):
		try:
			sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
			sock.settimeout(1.5)

			sock.connect((self.addr, self.port))
		except:
			raise

		if ssh:
			try:
				import paramiko
			except:
				print('paramiko is required for SSH support')
				sys.exit(1)

			try:
				t = paramiko.Transport(sock)
				t.start_client()
				t.auth_password(ssh[0], ssh[1])

				chan  = t.open_session()
				chan.get_pty()
				chan.invoke_shell()

				self.transport = t

				self.sock = chan
				self.fp = chan.makefile()

			except paramiko.SSHException:
				print('*** SSH negotiation failed.')
				sys.exit(1)
		else:
			self.sock = sock
			self.fp = self.sock.makefile('r')

			self.sock.settimeout(60)


	def disconnect(self):
		if isinstance(self.sock, socket.socket):
			try:
				self.sock.close()
			except socket.error:
				pass

		self.sock = None


	def _write(self, s):
		try:
			self.sock.sendall(s)
			if hasattr(self.sock, 'setsockopt'):
				self.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_QUICKACK, 1)
		except socket.error:
			self.disconnect()
			raise


	def _readline(self):
		try:
			return self.fp.readline()
		except:
			self.disconnect()
			raise


	def Send(self, s, **kwargs):
		lverbose = kwargs.get('verbose', self.verbose)
		if lverbose:
			print(f"{self.send_prefix}{s}")

		self.send_time = time.time()
		self._write(s)


	def SendImd(self, cmd, *args, **kwargs):
		cmd = str(cmd)
		for arg in args:
			cmd += "" + str(arg)

		lverbose = kwargs.get('verbose', self.verbose)
		if lverbose:
			print(f"{self.send_prefix}{cmd}")

		self.send_time = time.time()
		self._write(cmd)


	def SendEsc(self, cmd, *args, **kwargs):
		cmd = str(cmd)
		for arg in args:
			cmd += "" + str(arg)

		prefix_char = kwargs.get('prefix', '\x1b')
		suffix_char = kwargs.get('suffix', '\n')

		lverbose = kwargs.get('verbose', self.verbose)
		if lverbose:
			c = prefix_char
			if c == '\x1b':
				c = '[Esc]'

			print(f"{self.send_prefix}{c}")

		self.send_time = time.time()
		self._write(prefix_char + cmd + suffix_char)

		if 'buffer' in kwargs:
			self._write(kwargs.get('buffer'))


	def Read(self, timeout=999.0):
		buf = self._readline()
		if (buf == None) or (len(buf) == 0):
			return None
		return buf.strip('\r\n')


	def ReadAll(self, timeout=5.0):
		all = []
		while 1:
			buf = self.Read(timeout)
			if (buf == None) or (len(buf) == 0): break
			all.append(buf)
			timeout = 0.01

		return all


	def ReadBuffer(self, timeout=5.0):
		s = self.fp.read(4)
		(l,) = struct.unpack('!L', s)
		return self.fp.read(l)


	def ReadBase64Buffer(self, timeout=5.0):
		s = self.fp.read(11)
		l = int(s[0:10], 10)
		b = self.fp.read(l)
		return base64.b64decode(b)


	def ReadAvailable(self, timeout=1.0):
		''' Reads characters until a timeout occurs '''
		self.default_timeout = self.sock.gettimeout()
		self.sock.settimeout(timeout)

		buf = ""
		while 1:
			try:
				buf += self.fp.read(1)
			except socket.timeout:
				break

		self.sock.settimeout(self.default_timeout)

		return buf


	def Expect(self, expect='', timeout=999.0, is_re=False, **kwargs):
		t0 = time.time()
		if type(expect) == list:
			rval = True
			rbuf = []
			for e in expect:
				r, buf = self.Expect(e,timeout)
				rval &= r
				rbuf.append(buf)
			return rval, rbuf

		lverbose = kwargs.get('verbose', self.verbose)

		while 1:
			buf = self.Read(timeout)
			dt = time.time() - t0

			if (buf == None):
				dt = deltatime_ms(self.send_time)
				pre = self.err_prefix % locals()
				self.PrintError( "%s%s" % (pre, "timeout" ) )
				return False, buf

			if buf.startswith('-->>'):
				if lverbose:
					pre = self.recv_prefix % locals()
					print(f"{pre}{buf}")
				continue

			break

		dt = deltatime_ms(self.send_time)
		if is_re:
			if re.compile(expect).search(buf) == None:
				pre = self.err_prefix % locals()
				self.PrintError("%s'%s' != '%s' re" % (pre, buf, expect))
				return False, buf
		elif expect and (expect != buf):
			pre = self.err_prefix % locals()
			self.PrintError("%s'%s' != '%s'" % (pre, buf, expect))
			return False, buf

		if lverbose:
			pre = self.recv_prefix % locals()
			print(f"{pre}{buf}")

		return True, buf


	def ExpectMany(self, expect='OK', timeout=999.0, is_re=False):
		rbuf = ''
		while 1:
			e, buf = self.Expect( expect, timeout, is_re )
			rbuf += buf
			if e == False:
				return e, rbuf
			timeout = 0.1

		return True, rbuf


	def PrintError(self, *args):
		if self.color:
			sys.stdout.write('\033[91m')

		for arg in args:
			sys.stdout.write( str(arg) )
			sys.stdout.write( ' ' )

		if self.color:
			sys.stdout.write('\r\n\033[0m')
		else:
			sys.stdout.write('\r\n')


def Print(*args, **kwargs):
	for arg in args:
		print(f"{arg},")
	print



