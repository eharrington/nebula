###############################################################################
# ******  JITC Automation Log File Script  ******
# Default hardware: Any JITC controller
#
# Author:Jacob Parsons
# Date Create: 5/30/2018
###############################################################################

class ExtronQALogger:
    
    #Constructor for the class
    #Returns without Creating Logger if error Occurred
    def __init__(self, fileName = ""):
        #Imports used in Function
        import logging
        import os
        from datetime import datetime
        
        ###############################################################################
        # Default Vaiables
        ###############################################################################
        #Location Log Files will be stored
        self.filePath = "./LogFiles/"
        #Default Log File Name
        self.logFileName = "Log" + datetime.now().strftime('(%m-%d-%Y)(%H;%M;%S.%f)')+ ".txt"
        #Log Entry Format (Time followed by message)
        self.logFormat = '%(asctime)s.%(msecs)03d %(message)s'
        #Date and time format shows in the log
        self.dateFormat = '%m/%d/%y %I:%M:%S'
        #Format for CSV Files (adds comma delimiter)
        self.csvLogFormat = '%(asctime)s,%(message)s'
        #Logger object used to store the list of existing log handlers
        self.logger = None

        
        self.fileName = fileName
        if self.fileName == "":
            self.fileName = self.logFileName
        self.fileName = os.path.basename(self.fileName)
        
        #Creates local ./LogFiles Directory if one does not already Exist
        if not os.path.exists(self.filePath):
            try:
                os.makedirs(self.filePath)
            except:
                return
        
        #Creates a Logger that will write to the log file
        self.logger = logging.getLogger(self.fileName)
        self.logger.setLevel(logging.DEBUG)
        if ".csv" in self.fileName:
            self.formatter = logging.Formatter(fmt=self.csvLogFormat, datefmt =self.dateFormat)
        else:
            self.formatter = logging.Formatter(fmt=self.logFormat, datefmt =self.dateFormat)
                
        try:
            self.fileHandler = logging.FileHandler(self.filePath + self.fileName)
        except:
            return
        self.fileHandler.setFormatter(self.formatter)
        self.logger.addHandler(self.fileHandler)

    #Returns the name of current log file being used
    def getFileName(self):
        return self.fileName

    #Prints an information message to the log
    #Returns False if error Occurred 
    def logInfo(self, message = ""):
        
        if ".csv" in self.fileName:
            try:
                self.logger.info('INFO,' + message)
            except:
                return False
        else:
            try:
                self.logger.info('\tINFO\t' + message)
            except:
                return False
            
    #Prints an error message to the log
    #Returns False if error Occurred 
    def logErr(self, message = ""):
        if ".csv" in self.fileName:
            try:
                self.logger.info('ERROR,' + message)
            except:
                return False
        else:
            try:
                self.logger.info('\tERROR\t' + message)
            except:
                return False

    #Prints an Ending line for the log file
    #Returns False if error Occurred 
    def logEnd(self):
        if ".csv" in self.fileName:
            try:
                self.logger.info('END,' + '---------------------')
            except:
                return False
        else:
            try:
                self.logger.info('\tEND \t' + '---------------------')
            except:
                return False
    
    #Destructor for the class
    def __del__(self):
        for handler in self.logger.handlers[:]:
            self.logger.removeHandler(handler)