# ******    ******
#
# Minimum suported Python version: 3.5
# Author:Bouda Meas
# Date Create: 03/04/2019
# Description: move all relate Test Case or Test Environment methods
#              from QAUtils to here.
###############################################################################
import string
import json
import collections
import os
import encodings


_NEBULA_MODE = False
try:
    from nebula.modules.report import invalid_test_exception as ite
    from nebula.modules.report import nebula_test_exception as nte
    _NEBULA_MODE = True
except ImportError as error: 
    pass
except Exception as exception:
    pass


if(_NEBULA_MODE):
    from nebula.modules.actions.csa.common.QAUtils import QAUtils
    from nebula.modules.actions.csa.lib.TestAttributes import TestAttributes
    from nebula.modules.actions.csa.lib.TestItem import TestItem
    from nebula.modules.actions.csa.lib.ExtronQAConnection import ExtronQAConnection
else:
    from QAUtils import QAUtils
#    from lib.TestAttributes import TestAttributes
#    from lib.TestItem import TestItem
#    from lib.ExtronQAConnection import ExtronQAConnection


#from common.ExtronQALogger import ExtronQALogger

class QATestEnv:
    envworkingdir = ""
    workingdir =  ""    
    @staticmethod
    def ValidateSubTestInfoField(subtest_info):
        dictkeywords = ["_member_names_","__module__","_member_map_","_member_type_","_value2member_map_","__new__","__doc__"]
        dictlist = {}
        dictname = None

        for dictname in TestItem.FieldName.__dict__:
            if (dictname not in dictkeywords):
                dictlist[dictname.lower()] = dictname

        for jprop in subtest_info:
            try:
                if jprop.lower() == "TestName".lower():
                    dictname = "AttributeName"
                elif jprop.lower() == "MemoryUsage".lower():
                    dictname = "MemUsage"
                else:
                    dictname = dictlist[jprop.lower()]
            except Exception:
                return f"Bad field name:[{jprop}]."

        return None
    @staticmethod
    def CreateTestItem(subtest_info):
        testitem = None; dictlist = {};   dictname = None
        index_element = -1

        if QATestEnv.ValidateSubTestInfoField(subtest_info) != None:
            return None

        testitem = TestItem()
        dictkeywords =  QAUtils.DictionaryKeyWords()

        for dictname in TestItem.FieldName.__dict__:
            if dictname not in dictkeywords:
                dictlist[dictname.lower()] = dictname

        for jprop in subtest_info:
            if jprop.lower() == "TestName".lower():  #non field name
                dictname = "AttributeName"
            elif jprop.lower() == "MemoryUsage".lower():
                dictname = "MemUsage"
            else:
                dictname = dictlist[jprop.lower()]

            if dictname != None:
                try:
                    index_element = TestItem.FieldName.__dict__[dictname].value
                except Exception:
                    index_element = -1
                    raise ValueError(f"Internal field name:[{dictname}] not found.")

                if (index_element > 0):
                    if(testitem.SetValue(index_element, subtest_info.get(jprop)) == False):
                        raise ValueError("Internal field name:[" + dictname + "] error.")
                elif jprop.lower() == "Value".lower():
                    testitem.SetValue(TestItem.FieldName.AttributeName.ExpectValue, subtest_info.get(jprop))
                else:
                    raise ValueError("Internal field name:[" + dictname + "] not found.")

        return testitem
    @staticmethod
    def CreateTestAttributes(test_info):
        try:
            testitem = TestAttributes()
        except Exception as ex:
            pass
        dictkeywords = ["_member_names_","__module__","_member_map_","_member_type_","_value2member_map_","__new__","__doc__"]
        #DictionaryKeyWords()
        dictlist = {}
        indexlist = []

        #for dictname in TestAttributes.FieldName.__dict__:
        for dictname in list(TestAttributes.FieldName):
            if dictname not in dictkeywords:
                try:
                    dictlist[dictname.name.lower()] = dictname.name
                    index_element = TestAttributes.FieldName.__dict__[dictname.name].value
                    testitem.SetValue(index_element, "")
                except Exception as ex:
                    print(str(ex))

        for jprop in test_info:
            try:
                dictname = dictlist[jprop.lower()]
                index_element = TestAttributes.FieldName.__dict__[dictname].value
            except ValueError:
                index_element = -1
            except Exception:  # bm 05/03/2018: Handle other exceptions
                index_element = -1

            if (index_element > 0):
                testitem.SetValue(index_element, test_info.get(jprop))
                indexlist.append(index_element)
            elif jprop.lower() == "Value".lower():
                testitem.SetValue(TestItem.FieldName.AttributeName.ExpectValue, test_info.get(jprop))
            #else:  # bm 05/03/2018: will add internal log here
            #    raise ValueError("Internal field name:[" + dictname + "] not found.")

        return testitem
    @staticmethod
    def GetEnvFileName(envfile, envworkingdir, workingdir):
        if (envfile == None) or type(envfile) != type(""):
           return envfile
        elif (envfile.lower().startswith("$workingdir//")) or (envfile.lower().startswith("$workingdir/")):  # replace $workingdir with the actual path
            if (len(workingdir) == 0):
                envfile = envworkingdir + envfile[len("$workingdir"):]  # Substring("$workingdir".Length);
            else:
                envfile = workingdir + envfile[len("$workingdir"):]  # .Substring("$workingdir".Length);

        return envfile
    @staticmethod
    def ReadJson(Filename,envworkingdir,workingdir):
        global _NEBULA_MODE
        if (Filename == None) or type(Filename) != type(""):
            if(_NEBULA_MODE):
               raise ite.InvalidTestException(f"No filename given.") 
            else:
               return None

        envfile = Filename
        if (envfile.lower().startswith("$workingdir/")):  # replace $workingdir with the actual path
            if (len(workingdir) == 0):
                envfile = envworkingdir + envfile[len("$workingdir"):]  # Substring("$workingdir".Length);
            else:
                envfile = workingdir + envfile[len("$workingdir"):]  # .Substring("$workingdir".Length);

        if (os.path.isfile(envfile) == False):
            if(os.path.isfile(envworkingdir+"/"+Filename) == True):  #try the offset directory
               envfile = envworkingdir+"/"+Filename
            elif(os.path.isfile(workingdir+"/"+Filename) == True):  #try the offset directory
               envfile = workingdir+"/"+Filename
            else:
                if(_NEBULA_MODE):
                    raise ite.InvalidTestException(f"Unable to find tests file(s) in the provided location:{envfile}") 
                else:
                    print(envfile + " file not found.")
                    return None
        
        try:
            return json.load(open(envfile), object_pairs_hook=collections.OrderedDict)
        except Exception as e:
            if(_NEBULA_MODE):
               raise ite.InvalidTestException(f"Unable to read josn file in the provided location:{envfile}\r\n"+envfile+str(e)) 
            else:
               print(str(e))
        
        return None
    