class LoginInfo:
    """description of class
       8/3:Remove ssh & privatekeylogin & privatekeylogin @property,
           rename privatekeypassword to privatekeyphrase
    """

    class __LoginIfoStore:
        def __init__(self):
            self.conntype = None
            self.privatekey = ""
            self.privatekeytype = ""
            self.privatekeyphrase = ""
            self.hostName = None
            self.username = None
            self.password = None
            self.port = 23
            return

    def __init__(self, connecttype="tel",privatekey=None,privatekeytype=None,privatekeyphrase=None,\
                        hostName=None,port=23,username="admin",password="extron"):

        self.__item = self.__LoginIfoStore()
        self.__item.conntype = connecttype
        self.__item.privatekey = privatekey
        self.__item.privatekeytype = privatekeytype
        self.__item.privatekeyphrase = privatekeyphrase
        self.__item.hostName = hostName
        self.__item.username = username
        self.__item.password = password
        self.__item.port = port

        return

    @property
    def ConnectType(self):
        return self.__item.conntype
    @ConnectType.setter
    def ConnectType(self,value):
        self.__item.conntype = value
    
    @property
    def HostName(self):
        return self.__item.hostName
    @HostName.setter
    def HostName(self,value):
        self.__item.hostName = value
    
    @property
    def PrivateKey(self):
        return self.__item.privatekey
    @PrivateKey.setter
    def PrivateKey(self,value):
        self.__item.privatekey = value
    
    @property
    def PrivateKeyType(self):
        return self.__item.privatekeytype
    @PrivateKeyType.setter
    def PrivateKeyType(self,value):
        self.__item.privatekeytype = value

    @property
    def PrivateKeyPhrase(self):
        return self.__item.privatekeyphrase
    @PrivateKeyPhrase.setter
    def PrivateKeyPhrase(self,value):
        self.__item.privatekeyphrase = value
    
    @property
    def Username(self):
        return self.__item.username
    @Username.setter
    def Username(self,value):
        self.__item.username = value

    @property
    def Password(self):
        return self.__item.password
    @Password.setter
    def Password(self,value):
        self.__item.password = value
    
    @property
    def Port(self):
        return self.__item.port
    @Port.setter
    def Port(self,value):
        self.__item.port = value