# ******    ******
#
# Minimum suported Python version: 3.5
# Author:Bouda Meas
# Date Create: 02/27/2019
# Description: Utilities -- Code taken from Functional_JITC
###############################################################################

import string
import json
import os
import re
import copy
import socket
import sys
import xlrd
import csv
from collections import OrderedDict
import collections

#from common.QATestEnv import QATestEnv
from nebula.modules.actions.csa.lib.ExtronQAConnection import ExtronQAConnection

class IterExcel(object):
    def _readFile_(self,path,sheetname,firstrowheader):
        try:
            fullpath = os.path.expanduser(path)
            if os.path.isfile(fullpath):
                st_size = os.path.getsize(fullpath)
                if(st_size > (1024 * 4000)):  #max 4meg size
                    raise Exception("File exceed 4Meg size.")
                self.book = xlrd.open_workbook(fullpath)
            else:
                 raise Exception('File <'+path+'>not found.')
        except Exception as e:
            raise type(e)('Cannot read file.' + str(e))
               
        try:
            if sheetname == None:
               self.sheet = self.book.sheet_by_index(0)
            elif ("{}".format(sheetname) in self.book.sheet_names()):
                self.sheet =  self.book.sheet_by_name(sheetname)
            else:
               self.sheet = self.book.sheet_by_index(0)   #should raise an error??

            #self.values = self.sheet.get_rows()
            if(firstrowheader):
                self.header = self.sheet.row_values(0)
                self.row = 1  #row 0 is the header
        except Exception as e:
            raise type(e)('Invalid Excel -' + str(e))
        
        pass
    def _get_row_(self,byindex):
        if byindex > self.sheet.nrows:
           return {}

        row = self.sheet.row_values(byindex)
        value = {}
        if self.header != None:           
           for i in range(len(self.header)):
               value[self.header[i]] = row[i]
        else:
            for i in range(len(row)):
               value[i] = row[i]

        return value
    def __init__(self,path,sheetname='Sheet1',firstrowheader=False):
        self.book = None
        self.sheet = None 
        self.header = None       
        self.row = 0
        self._readFile_(path,sheetname,firstrowheader)
    
    def __iter__(self):
        return self

    def __next__(self):
        if self.row == self.sheet.nrows:
            raise StopIteration
        self.row += 1
        return self._get_row_(self.row-1)
        
    def next(self):        
        return self.__next__()
    def reset_iterate(self):
        if (self.header != None):
            self.row = 1
        else:
            self.row = 0
        pass
    def get_index_byName(self,byame):
        if type(byame) != type(""):
           return -1
        elif self.header != None and byame in self.header:
           for i in range(len(self.header)):
               if self.header[i] == byame:
                   return i

        return -1  #not found
    def get_name_byIndex(self,byindex):
        if type(byindex) == type(1):
            return None   #raise an error??
        elif self.header != None and byindex > 0 and byindex <= len(self.header):           
           return self.header[byindex]
                   
        return None  #not found
    def get_row(self,byindex):
        if type(byindex) == type(1):
           return self._get_row_(byindex)
        return {}
    def get(self):        
        return self._get_row_(self.row)  #current row        

class IterCSV(object):
    class __CSVStore:
        def __init__(self):
            self.cvs_data = []
        @property
        def count(self):
            return len(self.cvs_data)
        def add(self,data):
            self.cvs_data.append(copy.deepcopy(data))
            return True  #a hard true for now
        def get(self,byindex):
            if type(byindex) == type(1) and byindex > -1 and byindex < len(self.cvs_data):
               return copy.deepcopy(self.cvs_data[byindex])
            return None
        def delete(self,byindex):
            if type(byindex) == type(1) and byindex > -1 and byindex < len(self.cvs_data):
               del self.cvs_data[byindex]
               return True
            return False
    def _readFile_(self,path,firstrowheader=False):
        fp = None
        try:
            fullpath = os.path.expanduser(path)
            if os.path.isfile(fullpath):
                #statinfo = os.stat(fullpath)
                st_size = os.path.getsize(fullpath)
                if(st_size > (1024 * 4000)):  #max 4meg size
                    raise Exception("File exceed 4Meg size.")
                fp = open(fullpath, 'rU')
            else:
                raise Exception('File <'+path+'>not found.')
        except Exception as e:
            raise type(e)('Cannot read file.' + str(e))
        
        try:
            self.csv_reader = csv.reader(fp,dialect = csv.excel)
            if (firstrowheader):
                #self.csv_reader = csv.DictReader(fp,fieldnames=header)
                self.header=self.csv_reader.__next__()
            for row in self.csv_reader:
                    self.csv_data.add(row)
            fp.close()         
        except Exception as e:
            raise type(e)('Invalid CSV -' + str(e))
        pass
    def _get_row_(self,byindex):
        if byindex > self.csv_data.count:
           return {}

        row = self.csv_data.get(byindex)
        value = {}
        if type(row) ==  type(collections.OrderedDict()):
           value = dict(value)
        else:
            #some how, the row returns a list form
            if self.header != None:
               for i in range(len(self.header)):
                   value[self.header[i]] = row[i]               
            else:
                for i in range(len(row)):
                   value[i] = row[i]

        return value
    def __init__(self,path,header=None):
        self.header = None
        self.csv_reader = None  
        self.csv_data = self.__CSVStore()  #use internal memory
        self.row = 0
        self.count = 0
        self._readFile_(path,header)

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            #self.fp.close()
            pass
        except Exception:
            pass   #ignore
        pass

    def __iter__(self):
        return self

    def __next__(self):
        if self.row == self.csv_data.count:
            raise StopIteration            
        self.row += 1
        return self._get_row_(self.row-1)        
        
    def __setattr__(self, name, value):
        if not (name in ['header','csv_reader','row','count','csv_data']):
            raise Exception("Not Allow")
        return super().__setattr__(name, value)
            
    def get_header(self):        
        return copy.deepcopy(self.header)
    def next(self):        
        return self.__next__()
    def reset_iterate(self):
        if (self.header != None):
            self.row = 1
        else:
            self.row = 0    
    def get_index_byName(self,byame):
        if self.header != None and byame in self.header:
           for i in range(len(self.header)):
               if self.header[i] == byame:
                   return i

        return -1  #not found
    def get_name_byIndex(self,byindex):
        if type(byindex) == type(1):
            return None   #raise an error??
        elif self.header != None and byindex > 0 and byindex <= len(self.header):           
           return self.header[byindex]                   
        return None  #not found

    def get_row(self,byindex):
        if type(byindex) == type(1) or byindex > self.csv_data.count:
           return {}        
        return self.csv_data.get(byindex)  #current row
        
    def get(self):
        if self.row < self.csv_data.count:
           return self._get_row_(self.row)  #current row
        return self._get_row_(self.count-1)   #last row

class QAUtils(object):
    def X__getattr__(self, name):
        pass
    def X__setattr__(self, name, value):
        pass
    @staticmethod
    def MergeDicts(*dict_args):
        '''
        Given any number of dicts, shallow copy and merge into a new dict,
        precedence goes to key value pairs in latter dicts.
        https://stackoverflow.com/questions/38987/how-to-merge-two-dictionaries-in-a-single-expression
        '''
        result = {}
        for dictionary in dict_args:
            result.update(dictionary)
        return result

    @staticmethod
    def GetKeys(_dict):
        keys = None
        if str(type(_dict)) == "<class 'collections.OrderedDict'>":
           keys = list(_dict.keys())
        elif str(type(_dict)) == "<class 'dict_keys'>":
            keys = list(_dict)
        return keys

    @staticmethod
    def DictionaryKeyWords():
        return ["_member_names_","__module__","_member_map_","_member_type_","_value2member_map_","__new__","__doc__"]

    @staticmethod
    def RemoveDictKeyWords(dictlist,keywords=None):
        newlist = dictlist[:]
        dictnames = ["_member_names_","__module__","_member_map_","_member_type_","_value2member_map_","__new__","__doc__"]

        if(keywords != None):
           dictnames = keywords

        for dictname in newlist:
            if dictname in dictnames:
                newlist.remove(dictname)
            elif (dictname[:2] == "__" and dictname[-2:]== "__") or \
               (dictname[:1] == "_" and dictname[-1:] == "_"):
                newlist.remove(dictname)

        return newlist

    @staticmethod
    def ByteToString(data):
        if type(data) == type(b''):
            return "".join(chr(x) for x in bytearray(data))
        return None

    @staticmethod
    def StringToByte(data):
        if type(data) == type(""):
            return bytes(data, 'utf-8')
        return None
    
    @staticmethod
    def ReadJson(Filename):
        envfile = Filename
        if (os.path.isfile(envfile) == True):
            try:
                return json.load(open(envfile), object_pairs_hook=OrderedDict)
            except Exception as e:
                print(f"Filename:{Filename} --> {str(e)}")
        else:
            print(envfile + " file not found.")

        return None

    @staticmethod
    def GetDict_byIndex(data,byindex):
        if type(byindex) == type(1):
           try:
              return data[byindex]
           except Exception:
              #need a logg???
              pass
        return None
    
    @staticmethod
    def GetDict_Data(data,keyname,casesentitive=False):
        if type(keyname) == type("") or type(keyname) == type(1):
           try:
               matchtable = re.finditer(r"'\S+'",keyname)  #check for single quote                    
               match = next(matchtable,None)
               if(match == None):
                 matchtable = re.finditer(r'\"\S+\"',keyname)  #check for double quotes
                 match = next(matchtable,None)
               
               if match != None:  #otherwise, take keyname as is
                  keyname = match.group()[1:][:-1] #should only be one
               else:  #possible a number
                   try:
                       keyname = int(keyname)
                   except Exception:
                       pass  #ignore

               #keyname.replace("'","")
               if type(keyname) == type("") and not (keyname in data.keys()) and not casesentitive:
                  for item in data.keys():
                      if type(item) == type("") and item.lower() == keyname.lower():
                         return data[item]
                      elif type(item) == type(0):
                          return data[item]
               else: 
                  if type(keyname) == type(1) and len(data.keys()) >= keyname:
                     keyname = list(data)[keyname]
                  return data[keyname]
           except Exception:
              #need a logg???
              pass
        return None
    
    @staticmethod
    def GetOS_EnvironmentVar(env_variable):
        env = ""
        try:
            env =os.environ(env_variable)
            if (env == None) or len(env) == 0:
                env = ""
        except Exception:
            pass

        return env
    
    @staticmethod
    def IsHostReachable(hostip):
        return True
    
    @staticmethod
    def Connect(logininfo,logger,numlines="3"):
        connection = None
        #print(cryptography.__version__)
        if ((logininfo.ConnectType.lower() != "ssh") and (logininfo.ConnectType.lower() != "ssh_key")) \
               and (not QAUtils.IsHostReachable(logininfo.HostName)):
            return connection

        if (logininfo.ConnectType.lower() == "ssh_key"):
            connection = ExtronQAConnection(iphost=str(logininfo.HostName), port=int(logininfo.Port), \
                                        login=str(logininfo.Username), password=str(logininfo.Password), \
                                        conntype=logininfo.ConnectType)
        elif(logininfo.ConnectType.lower() == "ssh"):
            if (logininfo.PrivateKey == None) or len(logininfo.PrivateKey) == 0:
                connection = ExtronQAConnection(iphost=str(logininfo.HostName), port=int(logininfo.Port), \
                                                login=str(logininfo.Username), password=str(logininfo.Password), \
                                                conntype=logininfo.ConnectType)
            else:
                connection = ExtronQAConnection(iphost=str(logininfo.HostName), port=int(logininfo.Port), \
                                                login=str(logininfo.Username), password=str(logininfo.Password), \
                                                conntype=logininfo.ConnectType, \
                                                phrase=logininfo.PrivateKeyPhrase, ppk_file=logininfo.PrivateKey)
        elif (logininfo.ConnectType.lower() == "tel" or logininfo.ConnectType.lower() == "http" \
             or logininfo.ConnectType.lower() == "https"):
            connection = ExtronQAConnection(iphost=str(logininfo.HostName), port=int(logininfo.Port), \
                                             login=str(logininfo.Username), password=str(logininfo.Password), \
                                             conntype=logininfo.ConnectType)

        if(connection != None):
            connection.SetLogger(logger)
            if (connection.Connect(int(numlines))):
                return connection

        return None

