
import copy
import json
import re
from nebula.modules.actions.csa.common.LoginInfo import LoginInfo
from nebula.modules.actions.csa.common.QAUtils import IterCSV, IterExcel, QAUtils

class QAHelpers:
    @staticmethod
    def ProcessConfigurationBanner(banner):
        if (banner == None):
            return None

        if banner != None:
            for jprop in banner:
                if jprop.lower() == "linefeed":
                    linefeed = banner.get(jprop)
                elif jprop.lower() == "numberlines":
                    numlines = banner.get(jprop)
                    numlines = int(numlines)
            # done for-loop

        return {"linefeed":linefeed,"numlines":numlines}
    
    @staticmethod
    def GetLoopParam(loopdata):
        #format -- <LoopRange>_name [@C:\Testing\Book1.csv,True]
        loopinfo = loopdata.split(" ")
        loopname = loopinfo[0].split("_")[1]
        if len(loopinfo) > 1 and loopinfo[1].strip().startswith("["):
            #check if it contains a file name
            if loopinfo[1].strip().startswith("[@"):
               loopinfo = loopinfo[1].strip().replace("[","").replace("]","")
               loopinfo = loopinfo.strip().split(",")
               pos = loopinfo[0].find(".")
               header = False
               if (pos > 0) and loopinfo[0][pos+1:] in ['csv','xlsx','xls']:                   
                   if loopinfo[0][pos+1:] == 'csv':
                      if loopinfo[1].lower().strip() == 'true':
                         header = True
                      loopinfo = IterCSV(loopinfo[0][1:],header)
                   elif loopinfo[0][pos+1:] in ['xlsx','xls']:
                       sheetname = "Sheet1"
                       if len(loopinfo) == 3:
                          #filename, sheet, header
                          sheetname = loopinfo[1]
                          if loopinfo[2].lower().strip() == 'true':
                             header = True                          
                          #loopinfo = IterExcel(loopinfo[0][1:],sheet=loopinfo[1],firstrowheader=header)
                       elif len(loopinfo) == 2:
                          #filename, sheet or header                          
                          if loopinfo[1].lower().strip() == 'true':
                             header = True
                          else:
                              header = False
                              sheetname = loopinfo[1]            
                       #else:
                          #filename, no sheet and header                          
                       #  loopinfo = IterExcel(loopinfo[0][1:],firstrowheader=header)
                       loopinfo = IterExcel(loopinfo[0][1:],sheetname=sheetname,firstrowheader=header)
                   #loopinfo = {'filename':loppinfo[0].strip(),'type':loopinfo[0][pos+1:]}
            else:
                try:
                    loopinfo = json.loads(loopinfo[1])            
                except Exception as e:
                    #bad data
                    raise type(e)("Error evulating: " + loopinfo[1] + ". Due to " + str(e))                        
                
        elif len(loopinfo) > 1:
            try:
                loopinfo = json.loads("["+loopinfo[1]+"]")
            except Exception as e:
                #bad data
                raise type(e)("Error evulating: " + "["+loopinfo[1]+"]" + ". Due to " + str(e))
        else:
            loopinfo = None
            
        return loopname,loopinfo
    @staticmethod
    def GetIterateValue(interateData,varName):
        try:
            data = interateData[str(varName)]   #r|l + prefixName
            if data == None:
               data = data['index']
        except Exception as e:
            data = None
            raise type(e)("Error lookup name not found: <" + varName +"> ")
        
        return data
    @staticmethod
    #if there is no match, return the orignial data
    def ReplaceTableData(table_data,term,line_data=None):
        if type(line_data) != type(''):
           return line_data   #return as is.   Or, raise exception
           #raise Exception("string_data is not a string object.")
        elif len(line_data) == 0:
            return line_data

        where = 0
        string_data = copy.deepcopy(line_data)
        keyname = copy.deepcopy(term)
        keyname = keyname.replace("${","").replace("}","")
        
        keyprefix = "%{" + keyname + ".table"
        occurs = len(re.findall(r'(?=keyprefix+\[)', string_data))
        if occurs == 0: #no match found
           return line_data  #return as is
        
        keyprefix += "["        
        for _ in range(occurs):
            abs_where = string_data.find(keyprefix,where)  #find xxx.table[
            if (abs_where ==  -1):
                break   #done
                
            #get column name or index
            abs_pos = string_data[abs_where:].find("]")
            if abs_pos == -1:  #something bad happen
               return line_data  #return as is.   Or, raise exception
            
            abs_pos += abs_where               
            column_name = string_data[abs_where+len(keyprefix):abs_pos]  #starting at xxx.table[
            try:
                if(column_name.isnumeric()):
                   column_name = int(column_name)
            except Exception:
                pass 
            value = QAUtils.GetDict_Data(table_data,column_name.replace("'",""))  #column data
            
            relt_pos = string_data[abs_pos:].find("}")   #starting at ]
            if relt_pos == -1:  #something bad happen
               return line_data  #return as is.   Or, raise exception
            
            abs_pos += relt_pos+1
            keyname = string_data[abs_where:abs_pos]
            string_data = string_data.replace(keyname,"{}".format(value),occurs)
            
        return string_data
    @staticmethod
    def ProcessConfigurationLogin(loginToken):
        port = ""
        if (loginToken == None):
            return False
        logininfo = LoginInfo()

        if loginToken != None:
            for jprop in loginToken:
                if jprop.lower() == "connect":
                    logininfo.ConnectType = loginToken.get(jprop)        
                elif jprop.lower() == "privatekey":
                    logininfo.PrivateKey = loginToken.get(jprop)  #file name
                elif jprop.lower() == "privatekeytype":
                    logininfo.PrivateKeyType = loginToken.get(jprop)
                elif jprop.lower() == "privatekeyphrase" or jprop.lower() == "passphrase":
                    logininfo.PrivateKeyPhrase = loginToken.get(jprop)
                elif jprop.lower() == "port":                   
                    port = loginToken.get(jprop)
                    if type(port) == type("") and len(port) > 0:
                            logininfo.Port = int(port)
                    elif type(port) == type(1) and  port > 0:
                            logininfo.Port = int(port)
                elif jprop.lower() == "host":
                    if (logininfo.HostName == None): #bm 01/4/2019:Override config file
                        logininfo.HostName = loginToken.get(jprop)           
                elif jprop.lower() == "login":
                    if (logininfo.UseUsername == None): #bm 01/4/2019:Override config file
                        logininfo.UseUsername = loginToken.get(jprop)               
                elif jprop.lower() == "password":
                    if (logininfo.Password == None): #bm 01/4/2019:Override config file
                       logininfo.Password = loginToken.get(jprop)
                
            # done for-loop

        if logininfo.ConnectType.lower().strip() in ("https","http") and (logininfo.Port == None):          
            logininfo.Port = "80"    

        return logininfo