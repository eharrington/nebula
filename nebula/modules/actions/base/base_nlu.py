import json
import logging
import os
import re
import socket
import sys
from ast import literal_eval
from datetime import datetime
from pathlib import Path

from snips_nlu import SnipsNLUEngine, exceptions

from nebula.modules.actions.base import base_actions
from nebula.modules.actions.nlp import nlp_exception
from nebula.modules.common import config_helper, variable_store
from nebula.modules.common.base_colors import bcolors
from nebula.modules.common.variable_enum import Variable
from nebula.modules.report import invalid_test_exception as ite
from nebula.modules.report import nebula_test_exception as nte


class BaseNlu():
    def __init__(self, vs):
        self.base_actions = None
        self.variable_store = vs

    def close_all(self):
        if self.base_actions is not None:
            self.base_actions.close_all()

    def nlu_hack(self, test_line):
        ''' Read in the test line and add some special characters as hacks to prevent
        removal of some special characters
        @param: test_line - The string to look at
        @returns test_line after hacking the chars
        '''
        logging.debug("Hacking some special characters to prevent their removal")
        test_line = test_line.replace("}", "Sn1pS_H8Ck")
        test_line = test_line.replace("/", "Sn1ps_s1ash")
        test_line = test_line.replace("[", "Sn1ps_Lbr8")
        test_line = test_line.replace("]", "Sn1ps_Rbr8")
        test_line = test_line.replace(")", "Sn1ps_br8")
        test_line = test_line.replace("#", "Sn1ps_p0und")
        test_line = test_line.replace("@", "Sn1ps_a8t")
        test_line = test_line.replace('."', "Sn1ps_prI0d")
        test_line = test_line.replace("~", "Sn1ps_T1ld3")
        test_line = test_line.replace(";", "Sn1ps_S3m1c0l0n")
        test_line = test_line.replace(":", "Sn1ps_C0l0n")
        test_line = test_line.replace("^", "Sn1ps_Acc3nt")
        test_line = test_line.replace("=", "Sn1ps_EquA1")
        test_line = test_line.replace("-", "Sn1ps_Da5h")
        return test_line

    def nlu_unhack(self, test_line):
        '''Read in the test line and removes the hacks used to prevent the removal of the special characters'
        @param: test_line - string to look at
        @returns test_line after removing the hacks
        '''
        logging.debug("Unhacking special characters to find web elements")
        test_line = test_line.replace("Sn1pS_H8Ck", "}")
        test_line = test_line.replace("Sn1ps_s1ash", "/")
        test_line = test_line.replace("Sn1ps_Lbr8", "[")
        test_line = test_line.replace("Sn1ps_Rbr8", "]")
        test_line = test_line.replace("Sn1ps_br8", ")")
        test_line = test_line.replace("Sn1ps_p0und", "#")
        test_line = test_line.replace("Sn1ps_a8t", "@")
        test_line = test_line.replace("Sn1ps_prI0d", ".")
        test_line = test_line.replace("Sn1ps_T1ld3","~")
        test_line = test_line.replace("Sn1ps_S3m1c0l0n",";")
        test_line = test_line.replace("Sn1ps_C0l0n",":")
        test_line = test_line.replace("Sn1ps_Acc3nt","^")
        test_line = test_line.replace("Sn1ps_EquA1", "=")
        test_line = test_line.replace("Sn1ps_Da5h", "-")
        return test_line
