import boto3
import fastjsonschema
import json
import logging
import random
import re
import string
import subprocess
import time
import urllib.parse
import uuid
import yaml

from nebula.modules.common import config_helper, variable_store
from nebula.modules.common.base_colors import bcolors
from nebula.modules.common.compare.compare_factory import CompareFactory
from nebula.modules.common.variable_enum import Variable
from nebula.modules.report import invalid_test_exception as ite
from nebula.modules.report import nebula_test_exception as nte

from timeit import default_timer as timer


class BaseActions:
    boto_session = None
    def __init__(self, vs):
        self.compare_factory = CompareFactory()
        self.variable_store = vs
        aws_access_key_id = self.variable_store.get_value('${AWS_ACCESS_KEY_ID}')
        aws_secret_access_key = self.variable_store.get_value('${AWS_SECRET_ACCESS_KEY}')
        aws_session_token = self.variable_store.get_value('${AWS_SESSION_TOKEN}')
        if aws_access_key_id is not None and aws_secret_access_key is not None and aws_session_token is not None:
            boto_session = boto3.session.Session(
                aws_access_key_id=aws_access_key_id,
                aws_secret_access_key=aws_secret_access_key,
                aws_session_token=aws_session_token)
            logging.info(f"AWS is using role {boto_session.profile_name}.")


    def close_all(self):
        pass

############################### Validation Methods ################################

    def filter_using_regex(self, src_variable, regex, dest_variable):
        '''
        This method will apply a regex filter to a source variable
        and store the filtered result in a destination variable
        @param src_variable - The source variable, which can be a string or a list
        @param regex - The regex to be applied on the source variable
        @param dest_variable - The destination variable in which the filtered result is stored
        '''
        source = self.variable_store.var_replace(src_variable)
        regex = self.variable_store.var_replace(regex)
        destination = self.compare_factory.compare(source, regex, 'filter')

        if isinstance(destination, list):
            self.variable_store.set_value('${'+dest_variable+'}', str(destination), Variable.COLLECTION)

        elif destination is not None:
            self.variable_store.set_value('${'+dest_variable+'}', str(destination), Variable.PRIMITIVE)

    def print_variable_value(self, var_name):
        '''
        This method assumes that var_name will be enclosed in the ${} notation, and will not
        append it when getting the value.
        Accepts the name of a variable and prints it if it is not encrypted
        @param var_name - the name of the test derived object in the variable_store
        '''
        # indices will remain empty if target is not a complex object
        try:
            target, indices = self.variable_store._parse_var_name(str(var_name))

            # ${} notation not added when getting the value. This should be the way forward.
            if self.variable_store.get_type(target) == Variable.ENCRYPTED:
                logging.warning(f"${bcolors.WARNING}Cannot print value of encrypted variable {target}${bcolors.ENDC}")
                return

            target_val = self.variable_store.get_value(target.strip())
            if target_val is not None:
                target = self.variable_store._resolve_collection_target_val(target_val, indices)
        except:
            # The target value was not a stored variable, so we take it at face value
            pass
        logging.info(f"ECHO: The value is {target}")
        return

    def variable_compare(self, actual, operator, expected, print_object=None):
        '''
        Compares an actual, test derived value against a stated expected value
        @operator - the type of comparison being done
        @expected - what the value of actual should equal for the test to pass
        @returns A NebulaTestException is thrown if the comparison fails
        Variables and the expected values are assumed to be of the proper types for comparison
        '''
        # Will this ever be true? Looks like actual is the actual value, and not the variable reference
        if self.variable_store.get_type("${"+str(actual)+"}") is Variable.ENCRYPTED:
            print_object = "${"+actual+"}"
        else:
            print_object = actual

        self.compare_factory.compare(actual, expected, operator)

    def store_variable_length(self, variable, length_variable):
        # indices will remain empty if target is not a complex object
        try:
            target, indices = self.variable_store._parse_var_name(str(variable))

            target_val = self.variable_store.get_value(target.strip())
            if target_val is not None:
                target = self.variable_store._resolve_collection_target_val(target_val, indices)
        except:
            # The target value was not a stored variable, so we take it at face value
            pass
        length = len(target)
        self.variable_store.set_value("${"+length_variable+"}", str(length))
        return





#################################### Selector File Methods ###################################
    def _handle_selectors(self, text):
        '''Search for selector in selectors file.
        PRIVATE METHOD
        @param text - The key to search for
        @returns value
        '''
        selector_dictionary = self.variable_store.get_value('${SELECTOR_OBJECT}')
        if len(selector_dictionary) == 0:
            return None
        try:
            if isinstance(text, str) and text in selector_dictionary:
                logging.verbose(f"Found selector {text}: {selector_dictionary[text]}")
                return selector_dictionary[text]
            else:
                return None
        except NameError:
            return None

#################################### Translation Methods ###################################
    def get_translation(self, text):
        '''Gets the text translation from the specified lang_trans file if specified.
        Needs to be implemented in each public method
        @param text - The text to retrieve the translation
        '''
        lang_trans = config_helper.get_translation(text)
        if lang_trans is not None:
            logging.debug(f"Found translation: {lang_trans}")
            return lang_trans
        else:
            return text

    def wait_for(self, time_to_sleep):
        time_float = float(time_to_sleep)
        self.variable_store.set_value("${WAIT_TIME}", str(time_float))
        # Removing the hacky workaround in favor of waiting while finding the elements
        #actually, no.  Leave the hacky workaround so that we can have actual wait times.
        loop_amount = int(time_float / 0.01)
        # we are doing a loop here as a hacky workaround to an issue where 1 second sleep took 5-10 seconds see IE-2380
        for _ in range(loop_amount):
            time.sleep(0.01)


    def get_UUID(self, version=4, namespace=None, name=None, node=None, clock_seq=None):
        '''Returns a random UUID.  Use str() on the returned value to get a standard form
        hex string.  Calling with no parameters results in a version 4 (random) UUID.
        '''
        if version is 1:
            return self.get_UUID1(node, clock_seq)
        elif version is 3:
            if namespace is None or name is None:
                logging.error("creating a version 3 UUID requires a namespace and name.")

            return self.get_UUID3(namespace, name)
        elif version is 4:
            return self.get_UUID4()
        elif version is 5:
            if namespace is None or name is None:
                logging.error("creating a version 5 UUID requires a namespace and name.")
            return self.get_UUID5(namespace, name)
        return self.get_UUID4()


    def get_UUID1(self, node=None, clock_seq=None):
        return uuid.uuid1(node, clock_seq)

    def get_UUID3(self, namespace, name):
        return uuid.uuid3(namespace, name)

    def get_UUID4(self):
        return uuid.uuid4()

    def get_UUID5(self, namespace, name):
        return uuid.uuid5(namespace, name)

    def get_unique_string(self):
        guid = str(self.get_UUID4())
        guid = guid.replace("-", random.choice(string.ascii_letters))
        return guid


    def urlencode(self, var_name):
        '''Urlencodes a value in the variable store.  An undeclared variable is an error
        @param text - The text to retrieve the translation
        '''
        value = self.variable_store.get_value(var_name)
        if value is None:
            raise ite.InvalidTestException(f"Unable to urlencode {var_name}, because it has not been given a value. URLEncode only accepts defined variables")
        if isinstance(value, list) or isinstance(value, dict):
            raise ite.InvalidTestException(f"Unable to urlencode {var_name}. Cannot urlencode complex COLLECTION/JSON variables")
        value = urllib.parse.quote_plus(value)
        self.variable_store.set_value(var_name, value, self.variable_store.get_type(var_name))

    def execute(self, command, var_name="shell_response"):
        '''Execute the shell command
        @param command - the shell command to execute.
        @param var_name -  optional name of variable to store the subporcess.CompletedProcess object in
        '''
        try:
            value = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True)
            self.variable_store.set_value("${"+var_name+"}", value, Variable.SHELLRESPONSE)
            logging.verbose(f"Command '{command}' successful.\nSTDOUT:\n{value.stdout}\nSTDERR:\n{value.stderr}")
        except subprocess.CalledProcessError as e:
            raise nte.NebulaTestException(f"Command '{command}' not successful.\nSTDOUT:\n{e.stdout}\nSTDERR:\n{e.stderr}\n"+str(e))

    def start_timer(self):
        '''start the test timer.  If one has already started, it will be restarted.'''
        self.variable_store.set_value("${stopwatch}", timer())

    def stop_timer(self):
        if self.variable_store.get_value("${stopwatch}") is None:
            raise ite.InvalidTestException(f"You must start a timer before you can stop one.")
        elapsed_time = timer() - self.variable_store.get_value("${stopwatch}")
        self.variable_store.set_value("${EX_NEB_ELAPSED_TIME}", elapsed_time)

    def get_AWS_tokens(self, username, password, user_pool, client_id):
        if BaseActions.boto_session is None:
                raise ite.InvalidTestException("Must assume a role before attempting to retrieve security tokens.")

        #use boto to get access and id token from Cognito
        client = BaseActions.boto_session.client('cognito-idp')
        response = client.admin_initiate_auth(
            UserPoolId= user_pool,
            ClientId = client_id,
            AuthFlow = 'ADMIN_NO_SRP_AUTH',
            AuthParameters = {
                'USERNAME' : username,
                'PASSWORD' : password
            }
        )
        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            raise Exception("Unable to log into Greenville.  Response recieved:\n" + response)
        return response

