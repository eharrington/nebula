import base64
import json
import logging
import os
import re
import urllib

import boto3
import requests
from requests.auth import HTTPBasicAuth

from nebula.modules.actions.api import awsmfa_utils
from nebula.modules.actions.api.requests_aws_sign import AWSV4Sign
from nebula.modules.actions.base import base_actions
from nebula.modules.common import (config_helper, variable_store)
from nebula.modules.common.base_colors import bcolors
from nebula.modules.common.variable_enum import Variable
from nebula.modules.report import invalid_test_exception as ite
from nebula.modules.report import nebula_test_exception as nte


class ApiActions(base_actions.BaseActions):


    def __init__(self, vs):
        super().__init__(vs)
        self.session = None
        self.boto_session = None
        self.sessions = {}
        self.variable_store = vs
        self.credentials = {'AccessKeyId':None, 'SecretAccessKey':None, 'SessionToken':None, 'Expiration':None}
        self.region = self.variable_store.get_value('${EX_NEB_AWS_REGION}') or "us-east-2"
        self.sessions["default"] = self.session = requests.Session()

    def delete_request(self, delete_url, response_var, v4sig=False, service="execute-api"):
        endpoint, params = self._build_uri(delete_url)
        resp =self._execute_delete(endpoint, v4sig, service, parameters=params)
        self.variable_store.set_value("${"+response_var+"}", resp, Variable.RESTRESPONSE)

    def get_request(self, get_url, response_var, v4sig=False, service="execute-api"):
        endpoint, params = self._build_uri(get_url.strip())
        resp = self._execute_get(endpoint, v4sig, service, parameters=params)
        self.variable_store.set_value("${"+response_var+"}", resp, Variable.RESTRESPONSE)

    def json_schema_compare(self, schema_source, schema_target):
        rest_resp = self.variable_store.var_replace(schema_source.strip())
        expected = self.variable_store.var_replace(schema_target.strip())
        self.variable_compare(rest_resp.json(), "json schema compare", expected)

    def login_to_api(self, input, session_name, username, password):
        if 'client' in input and 'flow' in input:
            self._execute_client_flow_login(session_name)

        elif 'aws' in input.lower() or 'greenville' in input.lower():
            username = self.variable_store.get_value("${EX_NEB_COGNITO_USERNAME}")
            if username is None:
                ite.InvalidTestException(f"You must define EX_NEB_COGNITO_USERNAME before attempting to retrieve Greenville credentials")

            password = self.variable_store.get_value("${EX_NEB_COGNITO_PASSWORD}")
            if password is None:
                ite.InvalidTestException(f"You must define EX_NEB_COGNITO_PASSWORD before attempting to retrieve Greenville credentials")

            client_id = self.variable_store.get_value("${EX_NEB_CLIENT_ID}")
            if client_id is None:
                ite.InvalidTestException(f"You must define EX_NEB_CLIENT_ID before attempting to retrieve Greenville credentials")

            user_pool = self.variable_store.get_value("${EX_NEB_USER_POOL_ID}")
            if user_pool is None:
                ite.InvalidTestException(f"You must define EX_NEB_USER_POOL_ID before attempting to retrieve Greenville credentials")

            self._execute_Greenville_login(session_name)

        elif 'user' in input.lower() or 'username' in input.lower():
            self._execute_basic_login(username, password, session_name)

    def patch_request(self, patch_url, patch_data, patch_response, v4sig=False, service="execute-api"):
        body_data = self.variable_store.var_replace(patch_data)
        endpoint, params = self._build_uri(patch_url)
        resp = self._execute_patch(endpoint, v4sig, service, parameters=params, patch_data=body_data)
        self.variable_store.set_value("${"+patch_response+"}", resp, Variable.RESTRESPONSE)

    def post_request(self, post_url, post_data, post_response, v4sig=False, service="execute-api"):
        body_data = self.variable_store.var_replace(post_data)
        endpoint, params = self._build_uri(post_url)
        resp = self._execute_post(endpoint, v4sig, service, parameters=params, post_data=body_data)
        self.variable_store.set_value("${"+post_response+"}", resp, Variable.RESTRESPONSE)

    def pull_value(self, source, target):
        source, indices = self.variable_store._parse_var_name(source)

        if self.variable_store.get_type(source) == Variable.RESTRESPONSE:
            source = self.variable_store.var_replace(source).json()

        else:
            source = self.variable_store.var_replace(source)

        actual = self.variable_store._resolve_collection_target_val(source, indices)
        if isinstance(actual, dict):
            self.variable_store.set_value("${"+target+"}", json.dumps(actual), Variable.JSON)

        elif isinstance(actual, list):
            self.variable_store.set_value("${"+target+"}", str(actual), Variable.COLLECTION)

        else:
            self.variable_store.set_value("${"+target+"}", str(actual))

    def put_request(self, put_url, put_data, put_response, v4sig=False, service="execute-api"):
        body_data = self.variable_store.var_replace(put_data)
        endpoint, params = self._build_uri(put_url)
        resp = self._execute_put(endpoint, v4sig, service, parameters=params, put_data=body_data)
        self.variable_store.set_value("${"+put_response+"}", resp, Variable.RESTRESPONSE)

    def remove_header(self, header):
        self._remove_header(header)

    def switch_session(self, session_name):
        self._switch_session(session_name)

    def verify_response(self, api_response, target_var, operator):
        if operator == 'status is':
            rest_resp = self.variable_store.var_replace(api_response)
            actual = rest_resp.status_code
            operator = 'is'
            expected = self.variable_store.var_replace(target_var)

        elif operator == 'header is':
            actual, header = api_response.split()
            rest_resp = self.variable_store.var_replace(actual.strip())
            actual = rest_resp.headers[header]
            operator = 'is'
            expected = self.variable_store.var_replace(target_var)

        else:
            actual, indices = self.variable_store._parse_var_name(api_response)
            if self.variable_store.get_type(actual) == Variable.RESTRESPONSE:
                actual = self.variable_store.get_value(actual.strip()).json()

            else:
                actual = self.variable_store.var_replace(actual)

            actual = self.variable_store._resolve_collection_target_val(actual, indices)
            expected, indices = self.variable_store._parse_var_name(target_var)

            if self.variable_store.get_type(expected) == Variable.RESTRESPONSE:
                expected = self.variable_store.get_value(expected.strip()).json()

            else:
                expected =self.variable_store.var_replace(expected)

            expected = self.variable_store._resolve_collection_target_val(expected, indices)

        self.variable_compare(actual, operator, expected)

#################################### API Handler Methods ######################
    '''Allow an AWS account to assume a different role
    @param role - the name of a profile section that will be assumed

    A pre-requisite for assuming a role is a properly configured config file in the ~/.aws
    directory, and it must have a profile for the role being assumed. If MFA for the account
    is enabled, the user will be prompted for an MFA code at the console. Side effects of
    successful completion of this method include:
        -the AWS specific environment variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY,
        and AWS_SESSION_TOKEN will be set to the role being assumed.
        -the config file will be updated with a profile named "<name>-Nebula". This profile
        will have an MFA expiration set to the maximum value allowed by Extron policy, and an
        MFA code will not need to be entered until the current one expires.
    '''
    def _assume_role(self, role):
        self.credentials = awsmfa_utils.AwsMfaUtils(self.variable_store).assume_role(role)
        base_actions.BaseActions.boto_session = boto3.session.Session(
                aws_access_key_id=self.credentials['AccessKeyId'],
                aws_secret_access_key=self.credentials['SecretAccessKey'],
                aws_session_token=self.credentials['SessionToken'])
        logging.info(f"Assuming AWS role {role} has been successfully completed. Your credentials will expire at: {self.credentials['Expiration']}.")

    def _invoke_lambda(self, lambda_func_name, lambda_payload):
        try:
            client = boto3.session.Session().client('lambda')
            response = client.invoke(
                FunctionName=lambda_func_name,
                InvocationType="Event",
                LogType='Tail',
                Payload=lambda_payload
            )
            res = response.json()
            logging.verbose(res)
        except Exception as e:
            raise Exception(f"lambda_func_name has an exception: \n{e}")

    def aws_start_workflow(self, arn, name, params):
        uneeckifier = str(self.get_unique_string())
        uneeckifier = uneeckifier[:5]
        unique_name = name + uneeckifier

        if base_actions.BaseActions.boto_session is None:
            raise ite.InvalidTestException("Starting workflows requires that you first assume a role.")

        sfn = base_actions.BaseActions.boto_session.client('stepfunctions')
        try:
            response = sfn.start_execution(stateMachineArn = arn, name=unique_name, input=params)
            self.variable_store.set_value('${'+name+'}', response, Variable.JSON)
            arn = response['executionArn']
            start = response['startDate']
            logging.info(f"Started {arn} on {start}")
        except Exception as e:
            raise nte.NebulaTestException(f"Failed to start the workflow: \n{e}")

    def _execute_client_flow_login(self, session_name):
        self.session = requests.Session()
        self.session.headers.update({'Content-Type' : 'application/x-www-form-urlencoded'})
        body = urllib.parse.urlencode({'grant_type':'client_credentials',
                                        'client_id':self.variable_store.get_value('${EX_NEB_CLIENT_ID}'),
                                        'client_secret':self.variable_store.get_value('${EX_NEB_CLIENT_SECRET}')})
        response = self._execute_post(self.variable_store.get_value('${EX_NEB_CLIENT_HOST}'), False, "None", post_data=body)
        if response.status_code != 200:
            raise Exception("Unable to log into AWS using client-flow.  Response recieved:\n" + response.reason)
        res = response.json()
        logging.verbose(res)

        #put the newly aquired token in the session header in the proper format
        self.session.headers.update({'authorization' : f"Bearer {res['access_token']}"})
        self.session.headers.update({'content-type' : 'application/x-www-form-urlencoded'})
        self.session.verify = False #this will block certificate verification.
        self.sessions[session_name] = self.session

    def _execute_basic_login(self, username, password, session_name):
        self.session = requests.Session()
        self.session.auth = (username, password)
        self.session.verify = False
        self.sessions[session_name] = self.session


    def _execute_Greenville_login(self, session_name):
        #use boto to get access and id token from Cognito
        username =  self.variable_store.get_value('${EX_NEB_COGNITO_USERNAME}')
        password =  self.variable_store.get_value('${EX_NEB_COGNITO_PASSWORD}')
        client = boto3.session.Session().client(
            'cognito-idp',
            region_name=self.region,
            aws_access_key_id=self.credentials['AccessKeyId'],
            aws_secret_access_key=self.credentials['SecretAccessKey'],
            aws_session_token=self.credentials['SessionToken'])
        response = client.admin_initiate_auth(
            UserPoolId= self.variable_store.get_value('${EX_NEB_USER_POOL_ID}'),
            ClientId = self.variable_store.get_value('${EX_NEB_CLIENT_ID}'),
            AuthFlow = 'ADMIN_NO_SRP_AUTH',
            AuthParameters = {
            'USERNAME' : username,
            'PASSWORD' : password
            }
        )
        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            raise Exception("Unable to log into AWS.  Response recieved:\n" + response)
        access_token = response['AuthenticationResult']['AccessToken']
        id_token = response['AuthenticationResult']['IdToken']
        logging.verbose("Access token for AWS has been successfully retrieved.")

        #put the newly aquired tokens in the session header in the proper format
        self.session = requests.Session()
        self.session.headers.update({'authorization' : f'Bearer id_token={id_token}&access_token={access_token}'})
        self.session.headers.update({'content-type' : 'application/x-www-form-urlencoded'})
        self.session.verify = False #this will block certificate verification.
        self.sessions[session_name] = self.session

    def _switch_session(self, session_name):
        try:
            self.session = self.sessions[session_name]
        except Exception as e:
            raise Exception(f"Session {session_name} does not appear to exist.\n{e}")

    def setup_sqs_queue(self, queue_name):
        if base_actions.BaseActions.boto_session is None:
            raise ite.InvalidTestException("Manipulating a queue requires that you first assume a role.")

        sqs = base_actions.BaseActions.boto_session.client('sqs')
        queue = sqs.create_queue(QueueName=queue_name, Attributes={'DelaySeconds':'5'})
        logging.info(f"Queue Created: {queue['QueueUrl']}")
        #logging.info(f"Queue Attributes: {queue.attributes.get('DelaySeconds')}")

    def send_sqs_queue_message(self, queue_name, message_body):
        if base_actions.BaseActions.boto_session is None:
            raise ite.InvalidTestException("Manipulating a queue requires that you first assume a role.")

        sqs = base_actions.BaseActions.boto_session.client('sqs')
        queue = sqs.get_queue_url(QueueName=queue_name)
        response = sqs.send_message(QueueUrl= queue['QueueUrl'], MessageBody=message_body)
        logging.info(f"Sent message ID: {response.get('MessageId')}")

    def process_received_messages(self, queue_name):
        if base_actions.BaseActions.boto_session is None:
            raise ite.InvalidTestException("Manipulating a queue requires that you first assume a role.")

        sqs = base_actions.BaseActions.boto_session.client('sqs')
        queue = sqs.get_queue_url(QueueName=queue_name)
        messages = sqs.receive_message(QueueUrl= queue['QueueUrl'])
        if 'Messages' not in messages:
            logging.info(f"No messages in {queue_name}")
            return
        for message in messages['Messages']:
            sqs.delete_message(QueueUrl= queue['QueueUrl'], ReceiptHandle=message['ReceiptHandle'])
            logging.info(f"Message: {message['Body']}")

    def delete_sqs_queue(self, queue_name):
        if base_actions.BaseActions.boto_session is None:
            raise ite.InvalidTestException("Manipulating a queue requires that you first assume a role.")

        sqs = base_actions.BaseActions.boto_session.client('sqs')
        queue = sqs.get_queue_url(QueueName=queue_name)
        sqs.delete_queue(QueueUrl=queue['QueueUrl'])
        logging.info(f"Queue at {queue_name} deleted")

    def setup_sns_topic(self, topic_name):
        '''Creates an SNS topic and return an arn to access it by.
        '''
        if base_actions.BaseActions.boto_session is None:
            raise ite.InvalidTestException("Manipulating a SNS topic requires that you first assume a role.")

        sns = base_actions.BaseActions.boto_session.client('sns')
        response = sns.create_topic(Name=topic_name)
        self.variable_store.set_value("${"+topic_name+"}", response['TopicArn'])
        logging.info(f"Created SNS topic with arn {response['TopicArn']}")
        return response["TopicArn"]

    def subscribe_to_sns_topic(self, topic_name, queue_name):
        #get the arn for the queue from its name
        if base_actions.BaseActions.boto_session is None:
            raise ite.InvalidTestException("Manipulating messaging requires that you first assume a role.")

        sqs = base_actions.BaseActions.boto_session.client('sqs')
        queue_url = sqs.get_queue_url(QueueName=queue_name)['QueueUrl']
        queue_arn = sqs.get_queue_attributes(QueueUrl=queue_url, AttributeNames=['QueueArn'])
        queue_arn = queue_arn['Attributes']['QueueArn']

        #get the ARN for the topic.  It is a test error if the topic has not been created
        topic_arn = self.variable_store.get_value("${"+topic_name+"}")
        if topic_arn is None:
            raise ite.InvalidTestException(f"You have to create the topic {topic_name} before you can subscribe to it.")

        #allow the topic to write to the queue
        sqs_policy = self.allow_sns_to_write_to_sqs(topic_arn, queue_arn)
        response = sqs.set_queue_attributes(QueueUrl=queue_url,
                                            Attributes={'Policy' : sqs_policy})
        #subscribe the queue to the topic
        sns = base_actions.BaseActions.boto_session.client('sns')
        response = sns.subscribe(TopicArn=topic_arn,
                                Protocol='sqs',
                                Endpoint=queue_arn,
                                ReturnSubscriptionArn=True)
        subscription_arn = response['SubscriptionArn']
        logging.info(f"{queue_name} has been subscribed to {topic_name} with arn {subscription_arn}")

    def allow_sns_to_write_to_sqs(self, topic_arn, queue_arn):
        policy_document = """{{
            "Version":"2012-10-17",
            "Statement":[
                {{
                    "Sid":"MyPolicy",
                    "Effect":"Allow",
                    "Principal" : {{"AWS" : "*"}},
                    "Action":"SQS:SendMessage",
                    "Resource": "{}",
                    "Condition":{{
                        "ArnEquals":{{
                            "aws:SourceArn": "{}"
                        }}
                    }}
                }}
            ]
        }}""".format(queue_arn, topic_arn)
        return policy_document

    def publish_to_sns_topic(self, topic_name, message):
        topic_arn = self.variable_store.get_value("${"+topic_name+"}")
        if topic_arn is None:
            raise ite.InvalidTestException(f"You have to create the topic {topic_name} before you can publish to it.")
        if base_actions.BaseActions.boto_session is None:
            raise ite.InvalidTestException("Publishing an SNS topic requires that you first assume a role.")

        sns = base_actions.BaseActions.boto_session.client('sns')
        response = sns.publish(TopicArn=topic_arn,
                                Message=message)
        logging.info(f"Message published to {topic_name}.  Id is {response['MessageId']}")

    def delete_sns_topic(self, topic_name):
        topic_arn = self.variable_store.get_value("${"+topic_name+"}")
        if topic_arn is None:
            raise ite.InvalidTestException(f"You have to create the topic {topic_name} before you can delete to it.")
        if base_actions.BaseActions.boto_session is None:
            raise ite.InvalidTestException("Manipulating a SNS topic requires that you first assume a role.")

        sns = base_actions.BaseActions.boto_session.client('sns')
        sns.delete_topic(TopicArn=topic_arn)
        logging.info(f"Deleted SNS topic {topic_name}")

    def _execute_get(self, uri, v4sig, service, parameters=None):
        '''Gets data from a service using the uri and stores it
        @param uri
        @param parameters
        @param dest_var - Name of a global variable to store the returned data in
        '''
        if uri is None:
            logging.error("URI must be specified when performing a REST GET")

        auth=self.session.auth
        if v4sig:
            if base_actions.BaseActions.boto_session is None:
                raise ite.InvalidTestException("Must assume a role before signing with v4sig.")
            auth = AWSV4Sign(
                base_actions.BaseActions.boto_session.get_credentials(), 
                base_actions.BaseActions.boto_session.region_name, 
                service)

        try:
            return self.session.get(uri, params=parameters, auth=auth)
        except Exception  as ce:
            logging.warning(bcolors.WARNING + "Possible problem with target:" + bcolors.ENDC)
            logging.warning(bcolors.WARNING + str(ce) + bcolors.ENDC)

    def _execute_post(self, uri, v4sig, service, parameters=None, post_data=None):
        '''Posts data to a service using the uri and stores the response
        @param uri
        @parameters
        @param post_data - data to send
        @param dest_var - Name of a global variable to store the returned data in
        '''
        if uri is None:
            logging.error("URI must be specified when performing a REST POST")

        auth=self.session.auth
        if v4sig:
            if base_actions.BaseActions.boto_session is None:
                raise ite.InvalidTestException("Must assume a role before signing with v4sig.")
            auth = AWSV4Sign(
                base_actions.BaseActions.boto_session.get_credentials(), 
                base_actions.BaseActions.boto_session.region_name, 
                service)

        try:
            logging.debug(f"post_data is of type {type(post_data)}")
            if isinstance(post_data, str):
                return self.session.post(uri, params = parameters, data=post_data, auth=auth)
            else:
                return self.session.post(uri, params = parameters, json=post_data, auth=auth)
        except Exception  as ce:
            logging.warning(bcolors.WARNING + "Possible problem with target:" + bcolors.ENDC)
            logging.warning(bcolors.WARNING + str(ce) + bcolors.ENDC)

    def _execute_put(self, uri, v4sig, service, parameters=None, put_data=None):
        '''Puts data to a service
        @param uri
        @param parameters
        @param put_data - data to send
        '''
        if uri is None:
            logging.error("URI must be specified when performing a REST PUT")

        auth=self.session.auth
        if v4sig:
            if base_actions.BaseActions.boto_session is None:
                raise ite.InvalidTestException("Must assume a role before signing with v4sig.")
            auth = AWSV4Sign(
                base_actions.BaseActions.boto_session.get_credentials(), 
                base_actions.BaseActions.boto_session.region_name, 
                service)

        try:
            if isinstance(put_data, str):
                return self.session.put(uri, params=parameters, data=put_data, auth=auth)
            else:
                return self.session.put(uri, params=parameters, json=put_data, auth=auth)
        except Exception  as ce:
            logging.warning(bcolors.WARNING + "Possible problem with target:" + bcolors.ENDC)
            logging.warning(bcolors.WARNING + str(ce) + bcolors.ENDC)

    def _execute_patch(self, uri, v4sig, service, parameters=None, patch_data=None):
        '''Updates partial data to a service
        @param uri
        @param parameters
        @param patch_data - data to send
        '''
        if uri is None:
            logging.error("URI must be specified when performing a REST PUT")

        auth=self.session.auth
        if v4sig:
            if base_actions.BaseActions.boto_session is None:
                raise ite.InvalidTestException("Must assume a role before signing with v4sig.")
            auth = AWSV4Sign(
                base_actions.BaseActions.boto_session.get_credentials(), 
                base_actions.BaseActions.boto_session.region_name, 
                service)

        try:
            if isinstance(patch_data, str):
                return self.session.patch(uri, params=parameters, data=patch_data, auth=auth)
            else:
                return self.session.patch(uri, params=parameters, json=patch_data, auth=auth)
        except Exception  as ce:
            logging.warning(bcolors.WARNING + "Possible problem with target:" + bcolors.ENDC)
            logging.warning(bcolors.WARNING + str(ce) + bcolors.ENDC)

    def _execute_delete(self, uri, v4sig, service, parameters=None):
        '''Puts data to a service
        @param uri
        @param data - data to send
        '''
        if uri is None:
            logging.error("URI must be specified when performing a REST DELETE")

        auth=self.session.auth
        if v4sig:
            if base_actions.BaseActions.boto_session is None:
                raise ite.InvalidTestException("Must assume a role before signing with v4sig.")
            auth = AWSV4Sign(
                base_actions.BaseActions.boto_session.get_credentials(), 
                base_actions.BaseActions.boto_session.region_name, 
                service)

        try:
            return self.session.delete(uri, params=parameters, auth=auth)
        except Exception  as ce:
            logging.warning(bcolors.WARNING + "Possible problem with target:" + bcolors.ENDC)
            logging.warning(bcolors.WARNING + str(ce) + bcolors.ENDC)

    def _execute_head(self, uri, v4sig, service):
        '''Puts data to a service
        @param uri
        @param data - data to send
        '''
        if uri is None:
            logging.error("URI must be specified when performing a REST HEAD")

        auth=self.session.auth
        if v4sig:
            if base_actions.BaseActions.boto_session is None:
                raise ite.InvalidTestException("Must assume a role before signing with v4sig.")
            auth = AWSV4Sign(
                base_actions.BaseActions.boto_session.get_credentials(), 
                base_actions.BaseActions.boto_session.region_name, 
                service)

        try:
            return self.session.head(uri, auth=auth)
        except Exception  as ce:
            logging.warning(bcolors.WARNING + "Possible problem with target:" + bcolors.ENDC)
            logging.warning(bcolors.WARNING + str(ce) + bcolors.ENDC)

    def _execute_options(self, uri, v4sig, service):
        '''Puts data to a service
        @param uri
        @param data - data to send
        '''
        if uri is None:
            logging.error("URI must be specified when performing a REST OPTIONS")

        auth=self.session.auth
        if v4sig:
            if base_actions.BaseActions.boto_session is None:
                raise ite.InvalidTestException("Must assume a role before signing with v4sig.")
            auth = AWSV4Sign(
                base_actions.BaseActions.boto_session.get_credentials(), 
                base_actions.BaseActions.boto_session.region_name, 
                service)

        try:
            return self.session.options(uri, auth=auth)
        except Exception  as ce:
            logging.warning(bcolors.WARNING + "Possible problem with target:" + bcolors.ENDC)
            logging.warning(bcolors.WARNING + str(ce) + bcolors.ENDC)


#################################### Helper Methods ######################
    def _add_header(self, header, value):
        if self.session:
            self.session.headers.update({header: value})
        else:
            logging.error("A session must be created by logging in before a header can be added")


    def _remove_header(self, header):
        if self.session:
            self.session.headers.pop(header)
        else:
            logging.error("A session must be created by logging in before a header can be removed")



    def _build_uri(self, ep):
        '''Takes a formatted string and returns a properly formatted URI for an endpoint.
        Parameters are parsed into a dictionary
        @param ep - The text to be built into an endpoint
        @returns endpoint - The correctly formatted endpoint for the API
        @returns params - A dictionary of parameters for the API call
        '''
        # ed -> endpoint data
        params = {}
        if '{' in ep:
            #pull string from between brackets, then split on comma
            values = ep[ep.find('{')+1:ep.find('}')]
            ep = ep.split('{')[0]
            #then collect parameters, and replace the placeholders in the endpoint
            if values.startswith('(') and values.endswith(')'):
                values = values.strip('(').strip(')')
                value_list = []
                if '&' not in values:
                    value_list.append(values)
                else:
                    value_list = values.split('&')
                for value in value_list:
                    key, val = value.split('=', 2)
                    key_obj = re.search(r"{(.*?)}", key)
                    if key_obj:
                        key = self.variable_store.var_replace(key_obj.group(1))
                    val_obj = re.search(r"{(.*?)}", val)
                    if val_obj:
                        val = self.variable_store.var_replace(val_obj.group(1))
                    params[key] = val
            else:
                raise ite.InvalidTestException(f"Parameters for the endpoint '{ep}' were not specified correctly. The correct format is {{(key1=val1&key2=val2&key3=val3)}}")
        return ep, params
