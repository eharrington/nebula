import json
import logging
import os
import re
import sys
from datetime import datetime

from snips_nlu import SnipsNLUEngine, exceptions

from nebula import run
from nebula.modules.actions.api import api_actions
from nebula.modules.actions.base import base_nlu
from nebula.modules.actions.nlp import nlp_exception
from nebula.modules.common import config_helper, variable_store
from nebula.modules.common.base_colors import bcolors
from nebula.modules.common.variable_enum import Variable
from nebula.modules.report import invalid_test_exception as ite
from nebula.modules.report import nebula_test_exception as nte


class ApiNlu(base_nlu.BaseNlu):
    def __init__(self, vs):
        super().__init__(vs)
        self.api_actions = None
    
    def close_all(self):
        if self.api_actions is not None:
            self.api_actions.close_all()

    def do_action(self, test_line, nlu_engine, test_info):
        test_line = self.nlu_hack(test_line)
        response = nlu_engine.parse(test_line)
        if response['intent']['probability'] < 0.25:
            logging.warning(f"{bcolors.WARNING}Intent confidences of {response['intent']['probability']} is very low{bcolors.ENDC}")
        cutoff = config_helper.get_confidence_cutoff()
        if response['intent']['probability'] < cutoff:
            raise ite.InvalidTestException(f"Intent confidence of {response['intent']['probability']} falls below the {cutoff} cutoff you specified.")

        if self.api_actions is None:
            self.api_actions = api_actions.ApiActions(self.variable_store)

        try:
            intent_name = response['intent']['intentName']
        except KeyError:
            raise nlp_exception.NlpException(f"Response was not received from Snips")

        #undo the hack to stop snips from stripping trailing curly brace
        # TODO find a better solution for this
        for slot in response['slots']:
            if slot['value']['kind'] == 'Custom':
                slot['value']['value'] = self.nlu_unhack(slot['value']['value'])
            slot['rawValue'] = self.nlu_unhack(slot['rawValue'])

        response['input'] = self.nlu_unhack(response['input'])
        logging.verbose(f"{bcolors.CYAN}With confidence of {response['intent']['probability']} Intent Returned: {intent_name} with parameters {response['slots']}{bcolors.ENDC}")

        test_info.intent_tracker['datetime'] = str(datetime.now())
        test_info.intent_tracker['intentName'] = intent_name
        test_info.intent_tracker['confidence'] = response['intent']['probability']
        test_info.intent_tracker['response'] = json.dumps(response['slots'])

        if (intent_name is None):
            parameters = ""
            if len(response['slots']) > 0:
                parameters = ". Parameters: " + str(response['slots'])

            logging.error("Unable to map this text to an API intent: " + response['input'] +  parameters + "\n")

        elif (intent_name == 'addheader'):
            header_value = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=header_value', 'value', 'value']))
            header_type = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=header_type', 'value', 'value']))
            self.api_actions._add_header(header_type, header_value)

        elif (intent_name == 'assumerole'):
            role = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=role_name', 'value', 'value']))
            self.api_actions._assume_role(role)

        elif (intent_name == 'aws_workflow'):
            workflow_arn = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=workflow_arn', 'value', 'value'])
            name = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=name', 'value', 'value'])
            params = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=params', 'value', 'value'])
            self.api_actions.aws_start_workflow(workflow_arn, name, params)

        elif (intent_name == 'deleterequest'):
            delete_request = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=apiurl', 'value', 'value']))
            delete_response = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable', 'value', 'value'])
            sign = self.variable_store.check_exist_collection_target_val(response, ['slots', 'slotName=sig', 'value', 'value'])
            self.api_actions.delete_request(delete_request, delete_response, v4sig=sign)
            return

        elif (intent_name == 'get_aws_creds'):
            username = self.variable_store.get_value("${EX_NEB_COGNITO_USERNAME}")
            if username is None:
                ite.InvalidTestException(f"You must define EX_NEB_COGNITO_USERNAME before attempting to retrieve Greenville credentials")

            password = self.variable_store.get_value("${EX_NEB_COGNITO_PASSWORD}")
            if password is None:
                ite.InvalidTestException(f"You must define EX_NEB_COGNITO_PASSWORD before attempting to retrieve Greenville credentials")

            client_id = self.variable_store.get_value("${EX_NEB_CLIENT_ID}")
            if client_id is None:
                ite.InvalidTestException(f"You must define EX_NEB_CLIENT_ID before attempting to retrieve Greenville credentials")

            user_pool = self.variable_store.get_value("${EX_NEB_USER_POOL_ID}")
            if user_pool is None:
                ite.InvalidTestException(f"You must define EX_NEB_USER_POOL_ID before attempting to retrieve Greenville credentials")

            cred_store = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable', 'value', 'value'])
            resp = self.api_actions.get_AWS_tokens(username, password, user_pool, client_id)
            self.variable_store.set_value("${"+cred_store+"}", resp, Variable.JSON)
            return


        elif (intent_name == 'getresponse'):
            get_url = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=apiurl', 'value', 'value']))
            get_response = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=apiresponse', 'value', 'value'])
            sign = self.variable_store.check_exist_collection_target_val(response, ['slots', 'slotName=sig', 'value', 'value'])
            self.api_actions.get_request(get_url, get_response, v4sig=sign)

        elif (intent_name == 'invokelambda'):
            lambda_function = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=lambda', 'value', 'value'])
            lambda_payload = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=payload', 'value', 'value'])
            self.api_actions._invoke_lambda(lambda_function, lambda_payload)

        elif (intent_name == 'jsonschemacompare'):
            schema_source = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=first_schema', 'value', 'value'])
            schema_target = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=second_schema', 'value', 'value'])
            self.api_actions.json_schema_compare(schema_source, schema_target)

        elif (intent_name == 'logintoapi'):
            session = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=session_name', 'value', 'value']))
            username = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=username', 'value', 'value']))
            password = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=passwd', 'rawValue']))
            if session == '':
                session = 'default'
            self.api_actions.login_to_api(response['input'], session, username, password)

        elif (intent_name == 'patchrequest'):
            patch_url = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=apiurl', 'value', 'value']))
            patch_data = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=request_data', 'value', 'value'])
            patch_response = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable', 'value', 'value'])
            sign = self.variable_store.check_exist_collection_target_val(response, ['slots', 'slotName=sig', 'value', 'value'])
            self.api_actions.patch_request(patch_url, patch_data, patch_response, v4sig=sign)

        elif (intent_name == 'postrequest'):
            post_url = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=apiurl', 'value', 'value']))
            post_data = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=request_data', 'value', 'value']))
            post_response = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable', 'value', 'value'])
            sign = self.variable_store.check_exist_collection_target_val(response, ['slots', 'slotName=sig', 'value', 'value'])
            self.api_actions.post_request(post_url, post_data, post_response, v4sig=sign)

        elif (intent_name == 'pullvalue'):
            source = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=source', 'value', 'value'])
            target = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=target', 'value', 'value'])
            self.api_actions.pull_value(source, target)

        elif (intent_name == 'putrequest'):
            put_url = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=apiurl', 'value', 'value']))
            put_data = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=request_data', 'value', 'value'])
            put_response = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable', 'value', 'value'])
            sign = self.variable_store.check_exist_collection_target_val(response, ['slots', 'slotName=sig', 'value', 'value'])
            self.api_actions.put_request(put_url, put_data, put_response, v4sig=sign)

        elif (intent_name == 'removeheader'):
            header_type = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=header_type', 'value', 'value']))
            self.api_actions.remove_header(header_type)

        elif (intent_name == 'snsTopic'):
            if 'setup' in response['input']:
                topic_name = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=topic', 'value', 'value']))
                self.api_actions.setup_sns_topic(topic_name)
            elif 'subscribe' in response['input']:
                topic_name = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=topic', 'value', 'value']))
                queue_name = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=queue', 'value', 'value']))
                self.api_actions.subscribe_to_sns_topic(topic_name, queue_name)
            elif 'publish' in response['input']:
                topic_name = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=topic', 'value', 'value']))
                message_body = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=message', 'value', 'value']))
                self.api_actions.publish_to_sns_topic(topic_name, message_body)
            elif 'delete' in response['input']:
                topic_name = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=topic', 'value', 'value']))
                self.api_actions.delete_sns_topic(topic_name)
            else:
                raise ite.InvalidTestException('Could not understand the intended operation for SNS Topics')

        elif (intent_name == 'sqsQueue'):
            if 'setup' in response['input']:
                queue_name = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=queue', 'value', 'value']))
                self.api_actions.setup_sqs_queue(queue_name)
            elif 'send' in response['input']:
                queue_name = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=queue', 'value', 'value']))
                message_body = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=message', 'value', 'value']))
                self.api_actions.send_sqs_queue_message(queue_name, message_body)
            elif 'receive' in response['input']:
                queue_name = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=queue', 'value', 'value']))
                self.api_actions.process_received_messages(queue_name)
            elif 'delete' in response['input']:
                queue_url = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=queue', 'value', 'value']))
                self.api_actions.delete_sqs_queue(queue_url)
            else:
                raise ite.InvalidTestException('Could not understand the intended operation for SQS Queues')

        elif (intent_name == 'switchsession'):
            target_session = self.variable_store.var_replace(self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=sessionname', 'value', 'value']))
            self.api_actions.switch_session(target_session)

        elif (intent_name == 'verifyresponse'):
            api_response = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=apiresponse', 'value', 'value'])
            target = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=variable', 'value', 'value'])
            operator = self.variable_store._resolve_collection_target_val(response, ['slots', 'slotName=compare', 'value', 'value'])
            self.api_actions.verify_response(api_response, target, operator)

        else:
            logging.critical(f"Cannot find an associated method for intent: {intent_name}\n")
            raise nlp_exception.NlpException(f"Cannot find an associated method for intent: {intent_name}\n")
