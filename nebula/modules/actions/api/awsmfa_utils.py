try:
    import configparser
    from configparser import NoOptionError, NoSectionError
except ImportError:
    import ConfigParser as configparser
    from ConfigParser import NoOptionError, NoSectionError
import logging
import os
import sys
import nebula.modules.common.variable_store as variable_store
import boto3
from botocore.exceptions import ClientError, ParamValidationError
from nebula.modules.common.base_colors import bcolors
from nebula.modules.report import nebula_test_exception as nte
from nebula.modules.report import invalid_test_exception as ite


class AwsMfaUtils():
    AWS_CREDS_PATH = ''
    DURATION = 3600
    config = None

    def __init__(self, vs):
        self.variable_store = vs
        self.AWS_CREDS_PATH = '%s/.aws/config' % (os.path.expanduser('~'),)


    def assume_role(self, role_name):
        self.read_config(role_name)
        return self.get_credentials(role_name)

    def read_config(self, role_name):
        '''Read the AWS config file from the default location, then
        assume the specified role.  If the file is not present (like in
        a codebuild job), treat the role_name as an evironment variable
        that specifies an arn and assume that role'''
        self.config = configparser.RawConfigParser()
        if os.path.isfile(self.AWS_CREDS_PATH):
            try:
                self.config.read(self.AWS_CREDS_PATH)
            except configparser.ParsingError:
                e = sys.exc_info()[1]
                raise Exception(f"There was a problem reading or parsing your credentials file: {e.args[0]}")
        else:
            try:
                role_arn = self.variable_store.get_value("${"+role_name+"}")
                if role_arn is None:
                    raise ite.InvalidTestException(f"The AWS config was not found at {self.AWS_CREDS_PATH}, and there was no ARN defined for {role_name}")
                self.config.read_string(f"[{role_name}]\nrole_arn = {role_arn}")
                self.config.set(role_name, 'role_arn', role_arn)
            except configparser.ParsingError:
                e = sys.exc_info()[1]
                raise Exception(f"There was a problem reading or parsing the credentials string: {e.args[0]}")


    def get_credentials(self, profile_name):
        #verify the profile being assumed does exist
        config_profile_name = profile_name
        if not self.config.has_section(config_profile_name):
            config_profile_name = "profile " + profile_name
        if not self.config.has_section(config_profile_name):
            raise Exception(f"There is no configuration for {profile_name}.  Please choose an existing profile name")
        
        client = boto3.client('sts')
        console_input = self.prompter()
        boto_response = None
        try:
            boto_response = client.assume_role(
                RoleArn=self.config.get(config_profile_name, 'role_arn'),
                RoleSessionName=profile_name+'-session',
                DurationSeconds=self.DURATION)
        except (ClientError, ParamValidationError):
            try:
                mfa_token = console_input(f'{bcolors.OKGREEN}Enter AWS MFA code: {bcolors.ENDC}')
                boto_response = client.assume_role(
                    RoleArn=self.config.get(config_profile_name, 'role_arn'),
                    RoleSessionName=profile_name+'-session',
                    DurationSeconds=self.DURATION,
                    SerialNumber=self.config.get(config_profile_name, 'mfa_serial'),
                    TokenCode=mfa_token
                )
            except ClientError as ce:
                raise nte.NebulaTestException("An error occured while calling assume role: {}".format(ce))
            except ParamValidationError as pve:
                raise nte.NebulaTestException( "An error occured validating parameters while calling assume role: {}".format(pve))

        return boto_response['Credentials']

    def prompter(self):
        try:
            console_input = raw_input
        except NameError:
            console_input = input
        return console_input
