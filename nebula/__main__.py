import argparse
import concurrent.futures.thread
import logging
import ntpath
import pkg_resources
import os
import socket
import sys
import threading
import yaml
import json

from colorama import init, deinit
from concurrent.futures import ThreadPoolExecutor
from nebula import run
from nebula.modules.actions.nlp.nlp_exception import NlpException
from nebula.modules.common import config_helper
from nebula.modules.report.test_info import TestInfo
from nebula.modules.report import invalid_test_exception as ite
from nebula.modules.report import junit_xml_report_generator
from nebula.modules.report import nebula_test_exception as nte
from pathlib import Path
from snips_nlu import SnipsNLUEngine, exceptions
from time import localtime, strftime


init()


def main():
    # BEGIN SETUP
    parser = argparse.ArgumentParser(description='Start nebula')

    parser.add_argument('test_file_path', help='a file or directory containing the test file(s)')

    parser.add_argument('-b', '--browser', required=False, nargs='*',
                        help='browser to use (default: Chrome)', default=['Chrome'])

    parser.add_argument('-e', '--environment', required=False,
                        help='A file path leading to a yaml file of environment variables', default=None)

    parser.add_argument('-sof', '--failure', required=False, action='store_const', const=True,
                        help='Stops the execution of test suite on the first test case failure', default=False)
    parser.add_argument('-s', '--selectors_file', required=False,
                        help='A file path leading to the selectors file', default=None)

    parser.add_argument('-i', '--imageStore', required=False,
                        help='A url to the running nebula-image instance to use', default=None)

    parser.add_argument('-iu', '--imageUi', required=False,
                        help='A url to the running nebula-image UI instance to use', default=None)

    parser.add_argument('-it', '--intentTracker', required=False,
                        help='A url to the running intent tracker instance to use', default=None)

    parser.add_argument('-tr', '--test_report_location', required=False,
                        help='A directory path to store test reports that will be generated', default=None)

    parser.add_argument('--headless', required=False, action='store_const', dest="headless", const="headless",
                        help='Signifies if tests need to run in headless browser mode', default=False)

    parser.add_argument('-l', '--lang_file', required=False,
                        help='A file path leading to a language file', default=None)

    parser.add_argument('-id', '--image_identifier', required=False,
                        help='The identifier to use when storing images instead of the URL', default=None)

    parser.add_argument('-ic', '--intent_confidence_cutoff', required=False,
                        help='A confidence level between zero and 1 before failing a test', default=None)

    parser.add_argument('-cc', '--crawl_config', required=False,
                        help='A YAML file containing configurations to execute during the screenshot process', default=None)

    parser.add_argument('-cf', '--config_file', required=False,
                        help='A YAML file containing a configuration set', default=None)

    parser.add_argument('-ts', '--test_select', required=False,
                        help='A comma separated list of specific tests to run', default=None)

    parser.add_argument('-tt', '--test_tags', required=False,
                        help='A comma separated list of tags to select which tests to run', default=None)

    parser.add_argument('-p', '--processes', required=False,
                        help='Set the number of tests to run concurrently', default=None)

    parser.add_argument('-r', '--resolution', required=False,
                        help='Set the resolution of the browser window', default=None)

    parser.add_argument('-cv', '--chrome_version', required=False,
                        help='Specify the current version of Chrome', default=None)

    parser.add_argument('-log', '--log_level', required=False, help='Specify the log level for Nebula', default=None)

    parser.add_argument('--notracking', required=False, action='store_const', dest="notracking", const="notracking",
                        help='Signifies if tests will report to the nebula-intent-tracker', default=True)

    parser._get_option_tuples = lambda option: []
    args = parser.parse_args()

    try:
        package_path = Path(sys._MEIPASS)
    except AttributeError:
        package_path = os.path.dirname(os.path.abspath(__file__))
    try:
        with open(f'{package_path}/VERSION') as version_file:
            TestInfo.version = version_file.read().strip()
    except FileNotFoundError:
        try:
            package_path = os.path.dirname(package_path)
            with open(f'{package_path}/VERSION') as version_file:
                TestInfo.version = version_file.read().strip()
        except FileNotFoundError:
            TestInfo.version = pkg_resources.get_distribution('nebula').version

    TestInfo.hostname = socket.gethostname()

    # CMD Line Parameters take priority
    # configure based on command line parameter > environmental variable > config file

    if args.config_file is not None:
        config_helper.process_config_file(args.config_file)

    if args.log_level is not None:
        config_helper.set_log_level(int(args.log_level))
    else:
        if 'LOG_LEVEL' in os.environ:
            config_helper.set_log_level(os.environ['LOG_LEVEL'])
        else:
            config_helper.set_log_level(logging.INFO)

    if args.test_report_location is not None:
        logging.verbose(f"Test reports with be generated in the following location: {args.test_report_location}.")
        config_helper.set_report_location(args.test_report_location)
    else:
        config_helper.set_report_location('./logs')
    config_helper.set_logging_location(config_helper.get_report_location() + "/TEST-Nebula-{0}".format(strftime("%d_%b_%Y_%H%M%S", localtime())))
    if not os.path.exists(config_helper.get_report_location()):
        os.makedirs(config_helper.get_report_location())
    if not os.path.exists(config_helper.get_logging_location()):
        os.makedirs(config_helper.get_logging_location())

    logging.getLogger().setLevel(int(config_helper.get_log_level()))
    formatter = logging.Formatter("%(asctime)s [%(threadName)-20.20s] [%(levelname)-3.3s]  %(message)s")
    file_handler = logging.FileHandler(config_helper.get_logging_location()+"/main.log")
    file_handler.setFormatter(formatter)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logging.getLogger().addHandler(file_handler)
    logging.getLogger().addHandler(stream_handler)


    logging.info(f"Nebula Version: {TestInfo.version}")
    logging.verbose(f"Using logging level: {config_helper.get_log_level()}")
    logging.verbose("Priority of configurations is: Command line parameter > Environmental Variable > Config File option")
    logging.verbose("Searching for tests files in the following location: " + args.test_file_path)

    if args.config_file is not None:
        logging.verbose(f"Configuration pulled from {args.config_file}")

    logging.verbose(f"{args.browser} will be used for testing")
    config_helper.set_browser(args.browser)

    if args.failure is True:
        logging.verbose(f"Test suite will stop on first failure")
        config_helper.set_exit_on_failure(True)

    if args.intent_confidence_cutoff is not None:
        logging.verbose(f"Must be at least {args.intent_confidence_cutoff} confident to accept intent")
        config_helper.set_confidence_cutoff(float(args.intent_confidence_cutoff))

    if args.selectors_file is not None:
        logging.verbose(f"Selectors file used will be {args.selectors_file}")
        config_helper.set_selectors_file(args.selectors_file)

    if args.imageStore is not None:
        logging.verbose(f"Images with be captured in the following location: {args.imageStore}.")
        config_helper.set_image_store(args.imageStore)

    if args.imageUi is not None:
        logging.verbose(f"Images ui location: {args.imageUi}.")
        config_helper.set_image_ui(args.imageUi)

    if args.intentTracker is not None:
        logging.verbose(f"Intent tracker location: {args.intentTracker}.")
        config_helper.set_intent_tracker(args.intentTracker)

    if args.headless:
        logging.verbose("Running tests in headless mode")
        config_helper.set_headless(args.headless == 'headless')

    if args.lang_file is not None:
        logging.verbose(f"The following language file will be used while running tests: {args.lang_file}")
        config_helper.set_lang_file(args.lang_file)

    if args.chrome_version is not None:
        logging.verbose(f"The following Chrome version was specified: {args.chrome_version}")
        config_helper.set_chrome_version(args.chrome_version)

    if args.image_identifier is not None:
        logging.verbose(f"Images will be saved using the identifier {args.image_identifier}.")
        config_helper.set_image_identifier(args.image_identifier)

    else:
        logging.verbose(f"Images will be saved locally to the output_files directory.")
        config_helper.set_image_identifier(args.image_identifier)

    if args.test_file_path:
        logging.verbose(f"{args.test_file_path} will be searched for tests.")
        config_helper.set_test_file_path(args.test_file_path)

    if args.processes is not None:
        try:
            count = int(args.processes)
            config_helper.set_process_count(count)
        except ValueError as ve:
            logging.error(f"There was an error with the process count: {ve}")

    if args.resolution is not None:
        try:
            resolution_array = args.resolution.split("x")
            int_resolution = (int(resolution_array[0]), int(resolution_array[1]))
            config_helper.set_browser_resolution(int_resolution)
        except ValueError as ve:
            logging.error(f"There was an error with the input resolution: {ve}")


    if args.crawl_config is not None:
        config_helper.set_crawl_config_file(args.crawl_config)

    if args.test_select is not None:
        config_helper.set_specific_tests([x.strip() for x in args.test_select.split(',')])

    if args.test_tags is not None:
        config_helper.set_test_tags([x.strip() for x in args.test_tags.lower().split(',')])

    if args.notracking:
        logging.verbose("Running tests without tracking")
        config_helper.set_tracking(args.notracking != 'notracking') #invert this so the code is more readable

    try:
        PACKAGE_PATH = Path(sys._MEIPASS)
    except AttributeError:
        PACKAGE_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')

    if args.environment is not None:
        with open(args.environment, 'r') as env_file:
            try:
                env_variables = yaml.safe_load(env_file)
                for key in env_variables:
                    os.environ[key] = str(env_variables[key])
            except yaml.YAMLError as exc:
                logging.error(f"Failure loading the environment file: {exc}")
                exit(1)

    logging.verbose(f"Loading NLU Engines from: {PACKAGE_PATH}")
    try:
        snips_web_engine = SnipsNLUEngine.from_path(str(PACKAGE_PATH) + "/nebula/snips/web_engine/nebula_web_engine")
        snips_api_engine = SnipsNLUEngine.from_path(str(PACKAGE_PATH) + "/nebula/snips/api_engine/nebula_api_engine")
    except exceptions.LoadingError as le:
        raise NlpException(str(le))


    ''' The entry into Nebula. Test files are compiled into a list, a RunTest object created for each one,
    and then they are sent for processing and execution
    '''
    results = []
    test_file_location = config_helper.get_test_file_path()

    # start a new junit report
    test_run_report_generator = junit_xml_report_generator.JUnitXmlReportGenerator()
    test_run_report_generator.start_new_report(config_helper.get_report_location())

    fails = 0
    try:
        test_file_list = []
        if os.path.isfile(test_file_location):
            test_file_list.append(test_file_location)

        elif os.path.isdir(test_file_location):
            logging.info(f"+++++++++   Executing tests in the following directory and subdirectories: {test_file_location}")
            for root, _, files in os.walk(test_file_location, topdown=False):
                for filename in files:
                    if filename.endswith(('.txt', '.neb')):
                        test_file_list.append(os.path.join(root, filename))

        else:
            raise ite.InvalidTestException(f"Unable to find test file(s) in the provided location: {test_file_location}")

        run_list = []
        for test in test_file_list:
            for browser in config_helper.get_browser():
                run_list.append(run.RunTests(test, browser, snips_web_engine, snips_api_engine))

        with ThreadPoolExecutor(config_helper.get_process_count()) as executor:
            try:
                results = executor.map(thread_runner, run_list)
            except KeyboardInterrupt:
                import atexit
                atexit.unregister(concurrent.futures.thread._python_exit)
                executor.shutdown = lambda wait:None
                raise

    except Exception as e:
        logging.info(e)
        fails += 1

    finally:
        fails += test_run_report_generator.generate_report("TEST-Nebula-{0}.xml".format(strftime("%d_%b_%Y_%H%M%S", localtime())), list(results))
        deinit()
        sys.exit(fails)


def thread_runner(test):
    browser = ntpath.basename(test.browser)[0]
    testfile = ntpath.basename(test.file)
    threading.current_thread().name = f"{browser}.{testfile}"
    return test.run_test_suite()

if __name__ == '__main__':
    main()
