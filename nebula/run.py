import json
import logging
import ntpath
import os
import re
import sys
import threading
from datetime import datetime
from pathlib import Path
from sys import exit

import requests
import yaml

from nebula.modules.actions.api import api_nlu
from nebula.modules.actions.base import base_nlu
from nebula.modules.actions.csa import csa_actions
from nebula.modules.actions.image.image_actions import ImageActions
from nebula.modules.actions.nlp.nlp_exception import NlpException
from nebula.modules.actions.web import web_nlu
from nebula.modules.common import config_helper, variable_store
from nebula.modules.common.base_colors import bcolors
from nebula.modules.common import thread_log_filter
from nebula.modules.common.variable_enum import Variable
from nebula.modules.report import invalid_test_exception as ite
from nebula.modules.report import junit_xml_report_generator
from nebula.modules.report import nebula_test_exception as nte
from nebula.modules.report import test_file_report
from nebula.modules.report import test_info

meta_block_delimiters = ["@BeforeAll","@BeforeEach","@AfterAll","@AfterEach"]

class RunTests():

    test_has_failed=False  #a global class variable to that all tests know when one failed.

    def __init__(self, testfile, browser, web_engine, api_engine):
        self.file = testfile
        self.browser = browser
        self.snips_web_engine = web_engine
        self.snips_api_engine = api_engine
        self.var_store  = variable_store.VariableStore()
        self.var_store.set_value('${BROWSER}', browser, Variable.PRIMITIVE)
        self.test_info = test_info.TestInfo()
        self.directive = ""
        self.web_nlu_var = web_nlu.WebNlu(self.var_store, self.browser)
        self.api_nlu_var = api_nlu.ApiNlu(self.var_store)
        self.csa_actions_var = csa_actions.CsaActions(self.var_store)
        self.base_nlu = base_nlu.BaseNlu(self.var_store)
        # get the filename in the path so we can use it in the report
        self.test_info.testfile = ntpath.basename(self.file)
        self.reporter = test_file_report.TestFileReport(self.test_info.testfile, self.browser)


    def run_test_suite(self):
        ''' This method receives a file, parses it, separating out variables etc
        @param file - The test file to execute
        '''
        threadname = threading.current_thread().name
        # check if we are specifying a remote config file

        if os.path.isfile(self.browser):
            try:
                remote_config_json = json.load(open(self.browser, 'r'))
                if 'provider' not in remote_config_json:
                    raise ite.InvalidTestException("The configuration file for a remote browser test must have a provider specified.")
                self.var_store.set_value('${EX_NEB_REMOTE_DESIRED_CAP}', remote_config_json, Variable.JSON)
            except Exception as e:
                logging.error(f"Failure loading the remote config file: {e}")
                exit(1)

        fileHandler = logging.FileHandler(os.path.join(config_helper.get_logging_location(), threadname), mode='w')
        formatter = logging.Formatter('%(asctime)s [%(levelname)-3.3s]  %(message)s')
        fileHandler.setFormatter(formatter)
        f = thread_log_filter.ThreadLogFilter(threadname)
        fileHandler.addFilter(f)
        logging.getLogger().addHandler(fileHandler)

        if RunTests.test_has_failed and config_helper.get_exit_on_failure():
            return
        logging.info(f"{bcolors.HEADER}+++++++++   Executing tests in the following file: {self.test_info.testfile}{bcolors.ENDC}")

        try:
            with open(self.file, encoding="utf8") as f:
                contents = f.read()

            # Replace all includes with snippets from included file, concatenate lines with
            # line separation character '\'
            contents = self.replace_all_includes(contents, os.path.dirname(self.file))
            contents = self.concatenate_multiple_lines(contents)

            # split annotated blocks into an array
            test_content = re.split("^@", contents, flags=re.MULTILINE)

            # add '@' back, clean up and separate all blocks
            test_content = [("@" + x).strip() for x in test_content]

            # Separate out blocks to include external JSON files
            json_variable_blocks = [
                x for x in test_content if x.startswith('@IncludeJson')
            ]
            self.read_all_json_includes(json_variable_blocks, os.path.dirname(self.file))

            # Separate out the selector files specific to this test file
            selector_blocks = [
                x for x in test_content if x.startswith('@UseSelector')
            ]
            if len(selector_blocks) > 0:
                self.update_selector(selector_blocks=selector_blocks[-1], base_path=os.path.dirname(self.file))
            else:
                self.update_selector()

            #Pull out all of the testsuite level TEST_TAG blocks
            tag_blocks = [
                x for x in test_content if x.startswith('@TestTags')
            ]
            suite_tags = []
            for var_block in tag_blocks:
                try:
                    var_block = var_block.split("\n")[0]
                    var_block = var_block.split("#")[0]
                    varblock = var_block.replace('@TestTags ', "").strip()
                    value = varblock.split(',')
                    value = [x.strip(' ').lower() for x in value]
                    suite_tags += value
                except Exception as e:
                    logging.info(f"Definition for @TestTag was not understood\n{e}")
                    RunTests.test_has_failed = True
                    return

            # Pull out all the defined variables, and store in variable store
            variable_blocks = [
                x for x in test_content if x.startswith('@Define')
            ]

            for var_block in variable_blocks:
                var_type = Variable.PRIMITIVE
                try:
                    var_block = var_block.split("\n")[0]
                    var_block = var_block.split("#")[0]
                    term, value = var_block.split("=",1)
                    if 'Encrypted' in var_block:
                        term = term.replace('@DefineEncrypted ', "")
                        var_type = Variable.ENCRYPTED
                        value = value.strip()
                    elif 'Collection' in var_block:
                        term = term.replace('@DefineCollection ', "")
                        var_type = Variable.COLLECTION
                        value = value.strip()
                    elif 'Json' in var_block:
                        term = term.replace('@DefineJson ', "")
                        var_type = Variable.JSON
                        value = value.strip()
                    elif 'Regex' in var_block:
                        term = term.replace('@DefineRegex ', "")
                        var_type = Variable.REGEX
                        value = value[value.find('"')+1:value.rfind('"')]
                    elif 'Placeholder' in var_block:
                        term = term.replace('@DefinePlaceholder ', "")
                        var_type = Variable.PLACEHOLDER
                        value = value.strip()
                    else:
                        term = term.replace('@Define ', "")
                        value = value.strip()
                    term = "${" + term.strip() + "}"
                    self.var_store.set_value(term, value, var_type)
                except Exception as e:
                    logging.info(f"Variable definition for ${var_block} was not understood\n{e}")
                    RunTests.test_has_failed = True
                    return

            #split each block into individual directives, and remove comments
            test_content = [x.split('\n') for x in test_content]
            for i in range(len(test_content)):
                test = []
                for line in test_content[i]:
                    if line.startswith('#') or ('"' not in line and "'" not in line):
                        test.append(line.split('#')[0].strip())
                    else:
                        pattern = re.compile(r'''((?:[^#"']|"[^"]*"|'[^']*')+)''')
                        test.append(pattern.split(line)[1].strip())

                test_content[i] = [x for x in test if not x == '']

            # break out the meta blocks
            meta_blocks = [
                x for x in test_content
                if x[0].startswith(tuple(meta_block_delimiters))
            ]

            # break out the test blocks
            specific_tests = config_helper.get_specific_tests()
            test_tags = config_helper.get_test_tags()
            if len(specific_tests) > 0: #if a test is called out by name, run it regardless
                test_blocks = [
                    x for x in test_content
                        if x[0].startswith('@Test') and re.sub(r'@Test:|//.*|\(.*?\)','',x[0]).strip() in specific_tests
                ]
            elif len(test_tags) > 0:#next, we filter on tags, but just skip the filtering if no tags were provided
                test_blocks = [
                    x for x in test_content if x[0].startswith('@Test') and self.test_is_tagged(x[0], test_tags, suite_tags) and self.test_not_disabled(x[0])
                ]
            else:
                test_blocks = [
                    x for x in test_content if x[0].startswith('@Test') and self.test_not_disabled(x[0])
                ]

            if RunTests.test_has_failed and config_helper.get_exit_on_failure():
                return

            # Set the WAIT_FOR_ANGULAR variable to true by default
            self.var_store.set_value('${WAIT_FOR_ANGULAR}', True, Variable.PLACEHOLDER)

            # log an info message if no test cases are found.
            if len(test_blocks) == 0:
                logging.info("There are no test cases enabled/defined in file: "+ self.file)
            else:
                self.execute_block_steps('@BeforeAll', meta_blocks)

            # loop through each test case and execute
            try:
                for test_case in test_blocks:
                    if RunTests.test_has_failed and config_helper.get_exit_on_failure():
                        break
                    self.execute_block_steps('@BeforeEach', meta_blocks)
                    try:
                        self.execute_test(test_case)
                    finally:
                        self.execute_block_steps('@AfterEach', meta_blocks)
            finally:
                if len(test_blocks) > 0:
                    self.execute_block_steps('@AfterAll', meta_blocks)

        except IOError as i:
            logging.info("Failure in opening test file")
            logging.exception(i)
            RunTests.test_has_failed = True
            self.test_info.test_has_failed = True
            test_report = test_info.TestInfo()
            if str(i)[-5:] == ".yml'":
                test_report.name = '@UseSelector'
            test_report.intent_tracker = self.test_info.intent_tracker
            test_report.message = str(i)
            test_report.output = str(i)
            self.reporter.add_test_failure(test_report)
            logging.info(f"{bcolors.FAIL}TESTFAILURE: Error {test_report.message}{bcolors.ENDC}")
            return

        except Exception as e:
            logging.info(e)
            RunTests.test_has_failed = True
            if config_helper.get_exit_on_failure():
                raise NlpException("Exit on first failure activated")
            if e.args[0].find("BeforeAll") > 0:
                raise e

            if "Error in JSON file" in str(e):
                self.test_info.test_has_failed = True
                test_report = test_info.TestInfo()
                test_report.name = '@IncludeJson'
                test_report.intent_tracker = self.test_info.intent_tracker
                test_report.message = str(e)
                test_report.output = str(e)
                self.reporter.add_test_failure(test_report)
                logging.info(f"{bcolors.FAIL}TESTFAILURE: Error {test_report.message}{bcolors.ENDC}")

        finally:
            # Complete logging the test suite
            self.reporter.complete_testsuite(self.test_info)
            self.web_nlu_var.close_all()
            self.api_nlu_var.close_all()

            if self.test_info.sessionId != "":
                if RunTests.test_has_failed:
                    info_status = "failed"
                    info_reason = "test failed"

                else:
                    info_status = "passed"
                    info_reason = ""

                # If we want the test to tell browserstack/saucelabs whether we failed or not
                try:
                    desired_cap = self.var_store.get_value('${EX_NEB_REMOTE_DESIRED_CAP}')
                    # check if we are using saucelabs or browserstack
                    if desired_cap["provider"].lower() is 'saucelabs':
                        self.web_nlu_var.web_action.driver.execute_script(f'sauce:job-result={info_status}')
                    elif desired_cap["provider"].lower() is 'browserstack':
                        requests.put(
                            f"https://api.browserstack.com/automate/sessions/{self.test_info.sessionId}.json",
                            auth=(os.environ.get('BROWSERSTACK_USER'), os.environ.get('BROWSERSTACK_KEY')),
                            json={"status": info_status, "reason": info_reason})
                except:
                    pass
            return self.reporter

    def execute_block_steps(self, block_name, meta_blocks):
        ''' This method executes the meta blocks like beforeeach, aftereach etc
        @param block_name - The name of the block to execute
        @param meta_blocks - The list of standard meta blocks
        @param test_info - Test info for metrics
        '''
        try:
            for block in meta_blocks:
                if block[0].startswith(block_name):
                    self.test_info.name = block_name
                    logging.verbose(f"Executing {block_name} block.")
                    self.execute_directives(block)

        except Exception as e:
            RunTests.test_has_failed = True
            self.test_info.test_has_failed = True
            self.test_info.message = e.args[0]
            self.test_info.output = e.args[0]
            self.reporter.add_test_failure(self.test_info)
            logging.exception(e)
            raise NlpException(f"Exception in {block_name}:\n{e}")

    def execute_test(self, test_case):
        if RunTests.test_has_failed and config_helper.get_exit_on_failure():
            return

        self.var_store.set_value('${WAIT_TIME}', '1')
        test_definition = test_case[0]

        searchObj = re.search(r'data\s*=\s*\[(.*?)\]', test_definition)

        #if there was a searchObj, we are processing the testcase for each set of data
        if searchObj:
            params= self.var_store.var_replace(searchObj.group(1))
            param_list = re.findall(r'{(.*?)}', params)
            for param_set in param_list: #this for processes and supports data driven tests
                params = param_set.split(',')
                for param in params:
                    key, value = param.strip().split('=')
                    self.var_store.set_value('${'+key.strip()+'}', value.strip())
                test_case[0] = test_definition.replace(searchObj.group(0), str(param_set))
                self.execute_test_steps(test_case)
        else:
            self.execute_test_steps(test_case)

    def execute_directives(self, test_case):
            building_list = False
            nesting_depth = 0
            foreach_var = ""
            foreach_array = []
            valid_case = False
            case_processing = False
            directive_list = []

            for directive in test_case[1:]:  #the title, FOREACH:, or CASE: is skipped
                self.directive = directive
                if directive.startswith("END:"):
                    nesting_depth -= 1
                    if nesting_depth == 0:
                        if foreach_array:  # if FOREACH, we need to loop through each item in foreach_list and execute_test_steps
                            for iter_val in foreach_array:
                                logging.info(f"---------- Executing loop step")
                                self.var_store.set_value("${"+foreach_var+"}", iter_val)
                                self.execute_test_steps(directive_list, False)
                            foreach_array = []

                        elif case_processing:
                            if valid_case:
                                logging.info(f"---------- Conditional true. Executing block")
                                self.execute_test_steps(directive_list, False)
                            else:
                                logging.info(f"---------- Conditional negative. Skipping block")
                            case_processing = False
                        else:
                            raise ite.InvalidTestException("This END directive does not match a CASE or FOREACH directive.")
                        building_list = False
                        directive_list = []

                    else:
                        directive_list.append(directive)

                elif directive.startswith("CASE:"):
                    nesting_depth += 1
                    building_list = True
                    if nesting_depth == 1:
                        case_processing = True
                        searchObj = re.search(r"CASE:\s*(.*)", directive, re.I)
                        stringEval = self.var_store.var_replace(searchObj.group(1))
                        try:
                            valid_case = eval(stringEval)
                            if valid_case != True:
                                valid_case = False
                        except Exception as e:
                            raise ite.InvalidTestException(f"Invalid CASE statement:\n{e}")
                    directive_list.append(directive)

                elif directive.startswith("ON_PASS:"):
                    nesting_depth += 1
                    building_list = True
                    if nesting_depth == 1:
                        case_processing = True
                        searchObj = re.search(r"ON_PASS:\s*(.*)", directive, re.I)
                        directiveEval = searchObj.group(1)
                        try:
                            self.text_request(directiveEval)
                            valid_case = True  #if the Nebula directive passes, the code will fall through to here
                        except Exception:
                            valid_case = False          #if the Nebula directive fails, the code will fall through to here
                    directive_list.append(directive)

                elif directive.startswith("FOREACH:"):
                    nesting_depth += 1
                    building_list = True
                    if nesting_depth == 1:
                        #directive_list = [directive]  # this line is needed so that we can process the foreach line correctly
                        searchObj = re.search(r"FOREACH:\s+(\S*)\s+in\s+(\S*)", directive, re.I)
                        if searchObj:
                            foreach_var = searchObj.group(1)
                            foreach_list_var = searchObj.group(2)
                            foreach_array = self.var_store.get_value(foreach_list_var.strip())
                            # This is needed for Multi-layered variable references
                            while not isinstance(foreach_array, list) and foreach_array != None:
                                foreach_array = self.var_store.get_value(foreach_array.strip())

                        else:
                            raise ite.InvalidTestException(f"The following test directive is invalid: \n{directive}")
                    directive_list.append(directive)

                elif building_list:
                    directive_list.append(directive)

                else:
                    self.execute_test_line(directive)


    def execute_test_steps(self, test_case, isTest=True):
        if RunTests.test_has_failed and config_helper.get_exit_on_failure():
            return
        test_report = test_info.TestInfo()
        comment_line = test_case[0]

        try:
            if isTest:
                # save the comment
                test_report.name = comment_line.replace('@Test', '').strip(': -')
                self.var_store.set_value('${NEBULA_TEST_NAME}', test_report.name, Variable.PLACEHOLDER)
                # if there is not a comment line, make the name of the test the first line of the test
                if test_report.name == '':
                    test_report.name = test_case[0]

                logging.info(f"{bcolors.HEADER}**********   Executing test case: {test_report.name}{bcolors.ENDC}")

                #check to see if there is a devtime to track in the logs
                searchObj = re.search(r"devtime\s*=\s*([0-9]*)", test_report.name.lower())
                if searchObj:
                    try:
                        test_report.devtime = int(searchObj.group(1))
                    except Exception:
                        raise ite.InvalidTestException(f"Unable to convert the specified devtime to an integer. Record the development time in whole minutes.")

            self.execute_directives(test_case)

        except nte.NebulaTestException as ne:
            if isTest:
                RunTests.test_has_failed = True
                self.test_info.test_has_failed = True
                test_report.intent_tracker = self.test_info.intent_tracker
                test_report.message = ne.args[0]
                test_report.output = ne.args[0]
                self.reporter.add_test_failure(test_report)
            raise (ne)
        except NlpException as nlpe:
            if isTest:
                RunTests.test_has_failed = True
                self.test_info.test_has_failed = True
                test_report.intent_tracker = self.test_info.intent_tracker
                test_report.message = f"Unable to determine meaning of '{self.directive}'."
                test_report.output = nlpe.args[0]
                self.reporter.add_test_failure(test_report)
                logging.info(f"{bcolors.FAIL}TESTFAILURE: Error {test_report.message}{bcolors.ENDC}")
            raise (nlpe)
        except Exception as e:
            if isTest:
                RunTests.test_has_failed = True
                self.test_info.test_has_failed = True
                test_report.intent_tracker = self.test_info.intent_tracker
                test_report.message = e.args[0]
                test_report.output = e.args[0]
                self.reporter.add_test_failure(test_report)
                logging.exception(e)
            raise type(e)(repr(e))

        # Complete test case logging if successful. Failures handled in except clauses
        if isTest:
            self.reporter.add_test_success(test_report)

    def execute_test_line(self, test_line):
        if RunTests.test_has_failed and config_helper.get_exit_on_failure():
            return
        test_passed = "Passed"
        test_line_has_failed = False
        try:
            logging.info(bcolors.BOLD + "Execute Step: " + test_line + bcolors.ENDC)
            self.test_info.intent_tracker = {}
            self.test_info.intent_tracker['testLine'] = test_line
            self.test_info.intent_tracker['hostname'] = self.test_info.hostname
            self.test_info.intent_tracker['version'] = self.test_info.version
            self.test_info.intent_tracker['testfile'] = self.test_info.testfile

            self.text_request(test_line)

        except Exception as e:
            RunTests.test_has_failed = True
            self.test_info.test_has_failed = True
            test_line_has_failed = True
            test_passed = "Failed"
            errorText = f"'{e}' while executing: {test_line}"
            logging.info(f"{bcolors.FAIL}TESTFAILURE: Error {errorText}{bcolors.ENDC}")
            self.test_info.intent_tracker['error'] = errorText
            logging.debug(e)
            if self.web_nlu_var.web_action is not None:
                # take a screenshot of the failure and save it to output_files
                timestamp = datetime.now()
                image_name = f"./output_files/TestFailure_{threading.current_thread().name}_{timestamp}.png"
                image = ImageActions(self.web_nlu_var.web_action.driver)
                image.screenshot_current_page(image_name, True)

            if config_helper.get_exit_on_failure():
                logging.info("STOP ON FAIL flag detected. Stopping execution of tests")
            raise type(e)(repr(e))

        finally:
            test_passed = "Passed"

            if test_line_has_failed:
                test_passed = "Failed"

            self.test_info.intent_tracker['status'] = test_passed

            if config_helper.get_tracking():
                # we post to the server and if it fails we just ignore it
                try:
                    requests.post(f"{config_helper.get_intent_tracker()}/intents", json = self.test_info.intent_tracker)
                except:
                    pass

    def text_request(self, text):
        if text.startswith("POF:"):
            failed = False
            try:
                self.text_request(text[len("POF:"):].strip())
                failed = True
            except Exception:
                pass
            finally:
                if failed:
                    raise nte.NebulaTestException(f"Operation executed successfully when it should not have:\n{text}")

        elif text.startswith("API:"):
            # Check if api_actions object doesn't exist
            if self.api_nlu_var is None:
                self.api_nlu_var = api_nlu.ApiNlu(self.var_store)

            self.api_nlu_var.do_action(text[len("API: "):].strip(), self.snips_api_engine, self.test_info)

        elif text.startswith("CSA:"):
            csa_test_line = text[4:]
            if self.csa_actions_var is None:
                self.csa_actions_var = csa_actions.CsaActions(self.var_store)
            self.csa_actions_var.process_csanebula(csa_test_line)

        else:
            if self.web_nlu_var is None:
                # Initiate web_nlu class object
                self.web_nlu_var = web_nlu.WebNlu(self.var_store, self.browser)

            self.web_nlu_var.do_action(text, self.snips_web_engine, self.test_info)
            return

    def concatenate_multiple_lines(self, test_content):
        ''' Concatenate multiple lines when
        separated by a tab space. If a test line is too long,
        You can split it by entering the remainder of the text
        on a new line, starting with a tab space. This method
        concatenates them back to a single line
        @param - test_content
        '''
        return re.sub(r'\s*[\\\\]\s*\n|\s*[\\\\]\s*#.*?\n',' ',test_content)

    def update_selector(self, selector_blocks=None, base_path=None):

        selector_dictionary = {}

        if selector_blocks is None:
            selectors_file_location = config_helper.get_selectors_file()
        else:
            selector_file = selector_blocks[selector_blocks.find('=')+1:].strip()
            selectors_file_location = os.path.join(base_path, selector_file)

        if selectors_file_location is not None:
            logging.info(f'Reading in the following selector file: {selectors_file_location}')
            selector_dictionary = self.read_nested_selectors(selectors_file_location, selector_dictionary)
            self.var_store.set_value('${SELECTOR_OBJECT}', json.dumps(selector_dictionary), Variable.JSON)
        else:
            self.var_store.set_value('${SELECTOR_OBJECT}', json.dumps(selector_dictionary), Variable.JSON)

    def read_nested_selectors(self, include_file, selector_dictionary):
        '''This method allows for reading nested selectors in a DFS manner
        '''
        include_path = os.path.dirname(include_file)
        with open(include_file, 'r') as yamlfile:
            selector_dictionary.update(yaml.safe_load(yamlfile))
            while 'includes' in selector_dictionary:
                for inc in selector_dictionary.pop('includes', []):
                    include_path = os.path.join(include_path, inc)
                    logging.info(f'Reading in the following included selector file: {include_path}')
                    selector_dictionary = self.read_nested_selectors(include_path, selector_dictionary)
                    include_path = os.path.dirname(include_path)
        return selector_dictionary

    def replace_all_includes(self, contents, test_file_path):
        ''' Replace all include lines with snippets from the file mentioned in the include line.
        @param: contents - Contents of the test file
        @param: test_file_path - The path to the test file. This will help locate the snippetfile
        @returns: Modified file contents after replacing all include lines
        '''
        logging.debug("Replacing all Include snippets, if any")

        lines = contents.split("\n")
        include_lines = [x.strip() for x in lines if re.match(r"\s*@Include\s*\(", x)]

        for i, include_line in enumerate(include_lines):
            snippet_filename, _, snippetname = "".join(include_line[include_line.find("(")+1:include_line.find(")")].split()).partition(':')
            snippet_filename = os.path.join(test_file_path, snippet_filename)
            try:
                with open(snippet_filename, encoding="utf8") as f:
                    file_contents = f.read()
            except IOError as ioe:
                logging.exception(f"Error reading snippet file:\n{ioe}")
                exit(1)

            snippet_content = re.split("^@Snippet", file_contents, flags = re.MULTILINE)
            snippet_content = [("@Snippet" + x).strip() for x in snippet_content if x.startswith(':')]

            for block in snippet_content:
                if re.search(rf"\b{snippetname}\b", block):
                    _, _, replacement = block.partition('\n')
                    include_lines[i] = replacement #include_lines[i] now equals the contents of the snippet
                    snippet_path = os.path.dirname(snippet_filename) #we need snippets directory, to any included snippets can have relative addressing
                    include_lines[i] = self.replace_all_includes(include_lines[i], snippet_path) #include any snippets in the snippets
                    break

        for j, line in enumerate(lines):
            if re.match(r"\s*@Include\s*\(", line):
                lines.pop(j)
                lines[j:j] = include_lines.pop(0).split('\n')

        return '\n'.join(lines)

    def read_all_json_includes(self, json_includes, test_file_path):
        ''' Read in all json include lines and assign variables contents from the file
        mentioned in the include line.
        @param: json_includes - All @IncludeJson lines from the test file.
        @param: test_file_path - The path to the test file. This will help locate the json file
        @returns: Modified file contents after removing all json include lines, and creating test variables
        '''
        for include_line in json_includes:
            searchObj = re.search(r'@IncludeJson (.*?) as (\S+)', include_line, re.I)
            if searchObj:
                json_filename = searchObj.group(1)
                var_name = "${" + searchObj.group(2) + "}"
            json_filename = os.path.join(test_file_path, json_filename)
            try:
                with open (json_filename, encoding='utf-8') as data_file:
                    json_val = json.loads(data_file.read())
            except Exception as e:
                raise ite.InvalidTestException(f"Error in JSON file {json_filename}:\n{str(e)}")
            self.var_store.set_value(var_name, json.dumps(json_val), Variable.JSON)

    def test_not_disabled(self, testcase):
        '''Determine if the criteria for disabling a test have been met
        @param: Line of text from the @Test annotation
        @returns: True when the test has been disabled
        '''
        test = testcase.lower()
        if "(" in test and ")" in test and "disable" in test[test.find("(")+1:test.find(")")]:
            test_report = test_info.TestInfo()
            test_report.name = testcase.replace('@Test', '').strip(': -')
            test_report.message = "Skip disabled test case"
            self.reporter.add_test_skip(test_report)
            logging.info("**********   SKIP disabled test case: " + test)
            return False
        return True

    def test_is_tagged(self, testcase, tag_list, suite_tags):
        '''Determine if test is tagged with one of the provided tags
        returns true when the testcase is marked with one of the tag list, or with the keyword ALL
        '''
        specified_tags = ""
        test = testcase.lower()

        #in the regex, tags is followed by * (zero or more) so that the s doesn't have to be there and it can
        #match both tag and tags
        searchObj = re.search(r".*\(.*tags*\s*=\s*\[(.*?)]", test)
        if searchObj:
            specified_tags = searchObj.group(1).split(',') #split so that we can look for 'all' as a discrete entity

        if len(specified_tags) is 0:
            specified_tags = []
        specified_tags += suite_tags

        if 'all' in specified_tags:
            return True

        if any(tag in specified_tags for tag in tag_list): #foreach 'tag' in the tag_list, see if it is in tags that were specified
            return True

        return False
