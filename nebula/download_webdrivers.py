import json
import logging
import os
import platform
import shutil
import subprocess
import tarfile
import urllib.request
import zipfile
from pathlib import Path

driver_storage_location = "./drivers/"
operating_system = platform.system()

def get_chromedriver(chrome_version):
    '''
    Download the latest chromedriver and put it in the drivers folder
    '''
    chromedriver_url = "https://chromedriver.storage.googleapis.com"
    chromedriver_win = "chromedriver_win32.zip"
    chromedriver_linux = "chromedriver_linux64.zip"
    chromedriver_mac = "chromedriver_mac64.zip"

    logging.info(f"Entered Chrome version is {chrome_version}")
    if '.' in chrome_version:
        parsed_chrome_version = chrome_version[:chrome_version.rfind(".")]
    else:
        parsed_chrome_version = chrome_version
    logging.info(f"Parsed Chrome version to use for web cal is {parsed_chrome_version}")

    latest_release = f"{chromedriver_url}/LATEST_RELEASE_{parsed_chrome_version}"
    logging.info(f"Checking for associated chromedriver release version from {latest_release}")

    with urllib.request.urlopen(latest_release) as response:
        current_version = str(response.read()).replace("'", "").replace("b","")
        logging.info(f"Current chromedriver version for this Chrome version is {current_version}")

    download_url = f"{chromedriver_url}/{current_version}/"
    if operating_system == "Windows":
        zip_file = chromedriver_win
    elif operating_system == "Linux":
        zip_file = chromedriver_linux
    elif operating_system == "Darwin": #mac
        zip_file = chromedriver_mac
    
    zip_file_path = driver_storage_location + zip_file
    if not os.path.exists(driver_storage_location):
        os.mkdir(driver_storage_location)
    download_url = download_url + zip_file

    download_file(download_url, zip_file_path)

    extract_zip('chromedriver', zip_file_path)
    

def get_iedriver(iedriver_version):
    iedriver_url = "https://selenium-release.storage.googleapis.com"
    iedriver_64 = f"IEDriverServer_x64_{iedriver_version}.0.zip"
    logging.info(f"Entered IE Driver version is {iedriver_version}")

    download_url = f"{iedriver_url}/{iedriver_version}/{iedriver_64}"

    zip_file_path = driver_storage_location + iedriver_64
    if not os.path.exists(driver_storage_location):
        os.mkdir(driver_storage_location)
    
    download_file(download_url, zip_file_path)
    extract_zip('IEDriverServer', zip_file_path)

def get_edgedriver():
    # No set url to get the latest edge driver. Would appreciate input here
    edgedriver_url = ""

def get_safaridriver():
    # Safari Driver apparently comes with the installation of Safari
    # But Safari on Windows does not provide updates and the latest version is from 4 years ago
    # Not sure if this method is needed, but would appreciate someone checking
    safaridriver_url = ""

def get_operadriver(opera_version):
    operadriver_url = "https://github.com/operasoftware/operachromiumdriver/releases/download"
    operadriver_linux = "operadriver_linux64.zip"
    operadriver_mac = "operadriver_mac64.zip"
    operadriver_win = "operadriver_win64.zip"

    logging.info(f"Entered Opera driver version is {opera_version}")
    download_url = f"{operadriver_url}/v{opera_version}/"

    if operating_system == "Windows":
        zip_file = operadriver_win
    elif operating_system == "Linux":
        zip_file = operadriver_linux
    elif operating_system == "Darwin":
        zip_file = operadriver_mac
    
    zip_file_path = driver_storage_location + zip_file
    if not os.path.exists(driver_storage_location):
        os.mkdir(driver_storage_location)
    
    download_url = download_url + zip_file
    download_file(download_url, zip_file_path)
    extract_zip('operadriver', zip_file_path)

def get_geckodriver():
    '''
    Download the latest geckodriver and put it in the drivers folder
    '''
    geckodriver_url = "https://github.com/mozilla/geckodriver/releases"
    geckodriver_win = "geckodriver-version-win64.zip"
    geckodriver_linux = "geckodriver-version-linux64.tar.gz"
    geckodriver_mac = "geckodriver-version-macos.tar.gz"

    latest_release = "https://api.github.com/repos/mozilla/geckodriver/releases/latest"
    with urllib.request.urlopen(latest_release) as response:
        response_json = json.loads(response.read().decode('utf-8'))
        current_version = response_json["tag_name"]
        logging.info(f"Current geckodriver version is {current_version}")

    download_url = f"{geckodriver_url}/download/{current_version}/"
    if operating_system == "Windows":
        zip_file = geckodriver_win
    if operating_system == "Linux":
        zip_file = geckodriver_linux
    if operating_system == "Darwin": #mac
        zip_file = geckodriver_mac
    
    zip_file = zip_file.replace("version", current_version)

    zip_file_path = driver_storage_location + zip_file
    download_url = download_url + zip_file

    if not os.path.exists(driver_storage_location):
        os.mkdir(driver_storage_location)
    
    download_file(download_url, zip_file_path)

    extract_zip('geckodriver', zip_file_path)

def download_file(download_url, zip_file_path):
    if os.path.exists(zip_file_path):
        os.remove(zip_file_path)

    logging.info(f"Downloading from {download_url}")
    with urllib.request.urlopen(download_url) as response, open(zip_file_path, "wb") as out_file:
        shutil.copyfileobj(response, out_file)
        
    out_file.close()
    

def extract_zip(driver_name, zip_file_path):
    logging.info(f"Unzipping file {zip_file_path} to {driver_storage_location}")

    if(zip_file_path.endswith(".zip")):
        with zipfile.ZipFile(zip_file_path, "r") as driver_zipfile:
            driver_zipfile.extractall(driver_storage_location)
            driver_zipfile.close()
    elif(zip_file_path.endswith(".tar.gz")):
        tar = tarfile.open(zip_file_path)
        tar.extractall(driver_storage_location)
        tar.close()

    logging.info("Cleaning up.....")
    if os.path.exists(zip_file_path):
        os.remove(zip_file_path)

    if operating_system in ["Linux", "Darwin"] :
        subprocess.run(f"chmod +x {driver_storage_location + driver_name}", shell=True)
