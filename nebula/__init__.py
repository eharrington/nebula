import logging
logging.getLogger('root').addHandler(logging.NullHandler())

logging.VERBOSE = 15 
logging.addLevelName(logging.VERBOSE, "VERBOSE")

def verbose(message, *args, **kws):
    if logging.getLogger().isEnabledFor(logging.VERBOSE):
        # Yes, logger takes its '*args' as 'args'.
        logging.log(logging.VERBOSE, message) 

logging.verbose=verbose

